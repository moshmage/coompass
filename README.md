# Coompass App

## Tech stack
```json
{
  "@sequelize/core": "^7",
  "dappkit-react": "^0.1.5",
  "next": "14.0.4",
  "next-auth": "^5.0.0-beta.5",
  "react": "^18",
  "umzug": "^3.5.1"
}
```

## Web and API

### Development
- double check `.env.example` and create the `.env` file if you haven't yet
- Run the development server and follow on-terminal instructions with `npm run dev`

### Production
- double check `.env.example` and provide variables to the docker container environment
- run `docker-compose up app`

## Database

- check `POSTGRES_*` env variables on [`.env.example`](.env.example)
- issue `docker-compose up postgres`
- run `npm run build:db` and `npm run migrate` to run migrations

## Contracts
Contracts used are stored under `/src/contracts/[name]/(abi.ts|bytecode.ts|source.sol)`

### Building and Deploying
- create a script under `scripts/` folder using @taikai/dappkit for interactions

### Using
- use @taikai/dappkit-react and create custom hook for the contract

### Reacting to chain events
Interaction with contracts is made server-side, via the API, meaning that we can simply rely on the retrieved event log
to update the necessary values.

## Learn More

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Sequelize v7 Documentation](https://sequelize.org/docs/v7/) - learn about Sequelize features and API
- [Docker docs](https://docs.docker.com/)
