/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {

  },
  webpack: (config, { isServer }) => {
    // Ignore specific modules during build
    config.resolve.fallback = {
      ...config.resolve.fallback,
      ... !isServer ? {
        fs: false,
        crypto: require.resolve("crypto-browserify"),
        "node:crypto": require.resolve("crypto-browserify"),
      } : {},
      // Example: Ignore 'module-name' module
      'electron': false,
      'pino-pretty': false,
    };

    return config;
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'd3rhx2afwe3a94.cloudfront.net',
        pathname: '/**',
        port: '',
      }
    ]
  }
}

module.exports = nextConfig
