import {Erc721Standard} from "@taikai/dappkit";
import {configDotenv} from "dotenv";

(async () => {
  configDotenv()
  const nft = new Erc721Standard({web3Host: process.env.DEFAULT_CHAIN_RPC, privateKey: process.env.DEFAULT_PVT_KEY},);
  await nft.start();
  const tx = await nft.deployJsonAbi("CoompassPoP", "CPOP");
  console.log(`Deployed NFT`, tx.contractAddress);
})()