// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/**
 * @title MissionContract
 * @notice A contract for managing missions and participations using ERC-20 tokens.
 */
contract MissionContract {
    struct Mission {
        string mission_id;
        address mission_creator;
        uint256 token_amount;
        uint256 number_of_seats;
        bool closed;
    }

    uint256 public locked_tokens;

    /**
     * @notice List of missions containing details such as mission_id, mission_creator, token_amount, number_of_seats, and closed status.
     */
    Mission[] public missions;

    /**
     * @notice Mapping of mission indices to lists of addresses representing participants in each mission.
     */
    mapping(uint256 => address[]) public mission_participants;

    address public _governor;
    IERC20 public _token;

    event NewMission(uint256 indexed mission_index, string mission_id);
    event ClosedMission(uint256 indexed mission_index);
    event NewParticipation(uint256 indexed mission_index, address participant);

    modifier onlyGovernor() {
        require(msg.sender == _governor, "Not the governor");
        _;
    }

    /**
     * @notice Constructor to initialize the MissionContract with the ERC-20 token address.
     * @dev The sender of the deployment transaction becomes the initial governor.
     * @param tokenAddress The address of the ERC-20 token used for locking and distribution.
     */
    constructor(address tokenAddress) {
        _governor = msg.sender;
        _token = IERC20(tokenAddress);
    }

    /**
     * @notice Creates a new mission with specified details.
     * @dev The governor can create a mission, transferring the required tokens to the contract.
     * @param mission_id Identifier for the mission.
     * @param token_amount Amount of tokens to be locked for the mission.
     * @param number_of_seats Number of available seats in the mission.
     */
    function createMission(
        string memory mission_id,
        uint256 token_amount,
        uint256 number_of_seats
    ) external onlyGovernor {
        _token.transferFrom(msg.sender, address(this), token_amount);
        locked_tokens += token_amount;

        missions.push(Mission({
            mission_id: mission_id,
            mission_creator: msg.sender,
            token_amount: token_amount,
            number_of_seats: number_of_seats,
            closed: false
        }));

        emit NewMission(missions.length - 1, mission_id);
    }

    /**
     * @notice Registers participation in a mission.
     * @dev The governor can register participation for an address in a specified mission.
     * @param mission_index Index of the mission.
     */
    function registerParticipation(uint256 mission_index) external onlyGovernor {
        require(mission_index < missions.length, "Invalid mission index");
        require(!isParticipant(msg.sender, mission_index), "Participant already registered");

        mission_participants[mission_index].push(msg.sender);

        emit NewParticipation(mission_index, msg.sender);
    }

    /**
     * @notice Closes a mission and distributes tokens to participants.
     * @dev The governor can close a mission if conditions are met.
     * @param mission_index Index of the mission to be closed.
     */
    function closeMission(uint256 mission_index) external onlyGovernor {
        require(mission_index < missions.length, "Invalid mission index");
        require(!missions[mission_index].closed, "Mission is already closed");
        require(locked_tokens >= missions[mission_index].token_amount, "Not enough locked tokens");

        distributeTokens(mission_index);
        missions[mission_index].closed = true;

        emit ClosedMission(mission_index);
    }

    /**
     * @notice Checks if an address is a participant in a specific mission.
     * @param participant Address to check for participation.
     * @param mission_index Index of the mission to check.
     * @return true if the address is a participant, false otherwise.
     */
    function isParticipant(address participant, uint256 mission_index) public view returns (bool) {
        address[] memory participants = mission_participants[mission_index];
        for (uint256 i = 0; i < participants.length; i++) {
            if (participants[i] == participant) {
                return true;
            }
        }
        return false;
    }

    /**
     * @notice Distributes tokens to participants in a closed mission.
     * @param mission_index Index of the mission to distribute tokens for.
     */
    function distributeTokens(uint256 mission_index) internal {
        uint256 token_amount = missions[mission_index].token_amount;
        address[] memory participants = mission_participants[mission_index];

        for (uint256 i = 0; i < participants.length; i++) {
            _token.transfer(participants[i], token_amount);
        }

        locked_tokens -= token_amount;
    }
}