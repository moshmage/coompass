// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract DMO {
    /// @dev Struct to represent the chance of winning based on balance
    struct LotteryValueChance {
        uint256 balance;
        uint8 percentage;
    }

    /// @notice Array to store stakers' addresses
    address[] public stakers;

    /// @notice Array to store past block-numbers where a raffle took place
    uint256[] public past;

    /// @notice token that is used to check balance
    address public dmoToken;

    /// @notice contract that holds the hours spend
    /// @dev TODO needs to actually be implemented
    address public dmoMission;

    /// @notice Array to store chances of winning based on balance
    LotteryValueChance[5] private lotteryValueChances;

    event WINNERS(address[]);
    event RAFFLE_POOL(address[]);

    /// @dev Constructor to initialize chances of winning
    constructor(address dmoToken_/*, address dmoMission_*/) {
        lotteryValueChances[0] = LotteryValueChance(250, 5);
        lotteryValueChances[1] = LotteryValueChance(500, 14);
        lotteryValueChances[2] = LotteryValueChance(1000, 28);
        lotteryValueChances[3] = LotteryValueChance(3000, 49);
        lotteryValueChances[4] = LotteryValueChance(10000, 75);

        dmoToken = dmoToken_;
        // dmoMission = dmoMission_;
    }

    /// @notice Modifier to check if an address is not in the stakers array
    modifier addressNotInArray() {
        require(isAddressNotInArray(msg.sender), "Address is already in the array");
        _;
    }

    /// @notice Query ERC-20 balance using low-level call
    function getPassBalance(address address_) private returns (uint256) {
        (bool success, bytes memory data) = dmoToken.call(abi.encodeWithSelector(bytes4(keccak256("balanceOf(address)")), address_));

        if (success && data.length == 32) {
            return abi.decode(data, (uint256));
        }

        return 0;
    }

    /// @notice Function to get the total number of stakers
    /// @dev This function returns the length of the stakers array
    function stakersTotal() public view returns (uint) {
        return stakers.length;
    }

    /// @notice Function to check if an address is not in the stakers array
    /// @dev This function iterates over the stakers array to check for the existence of the given address
    function isAddressNotInArray(address _address) private view returns (bool) {
        for (uint256 i = 0; i < stakers.length; i++) {
            if (stakers[i] == _address) {
                return false; // Address is in the array
            }
        }
        return true; // Address is not in the array
    }

    /// @notice Function for an address to participate in the lottery's next round
    /// @dev This function adds the caller's address to the stakers array
    function participateInNextRound() public addressNotInArray {
        require(getPassBalance(msg.sender) > 0, "Not enough pass tokens");
        stakers.push(msg.sender);
    }

    /// @notice Function to get the percentage chance of winning based on balance
    /// @dev This function iterates over the lotteryValueChances array to find the appropriate percentage based on the given balance
    function getPercentageFromBalance(uint256 value) private view returns (uint8) {
        for (uint8 i = 0; i < lotteryValueChances.length; i++) {
            if (value >= lotteryValueChances[i].balance) {
                return lotteryValueChances[i].percentage;
            }
        }
        return 0;
    }



    /// @notice Function to create an array for the lottery pool based on stakers' balances
    /// @dev This function creates a pool array with addresses based on the percentage chance of winning
    function makeLotteryArray() private returns (address[] memory) {
        address[] memory pool = new address[](stakers.length * lotteryValueChances[4].percentage);
        uint256 index = 0;

        for (uint256 i = 0; i < stakers.length; i++) {
            uint256 balance = getPassBalance(stakers[i]);
            uint8 percentage = getPercentageFromBalance(balance);

            for (uint8 j = 0; j < percentage; j++) {
                pool[index] = stakers[i];
                index++;
            }

        }

        // Resize the array to the actual end index
        assembly {
            mstore(pool, index)
        }

        return pool;
    }

    /// @notice Function to shuffle an array using a random seed
    /// @dev This function shuffles the input array using a random seed generated from the current block's timestamp and basefee
    function shuffle(address[] memory array) private view returns (address[] memory) {
        for (uint256 i = array.length - 1; i > 0; i--) {
            uint256 j = uint256(keccak256(abi.encodePacked(block.timestamp, block.basefee))) % (i + 1);
            (array[i], array[j]) = (array[j], array[i]);
        }
        return array;
    }

    /// @notice Function to remove all occurrences of a staker's address from the pool
    /// @dev This function removes all occurrences of the given staker's address from the pool array
    function removeAddressFromPool(address[] memory pool, address stakerAddress)
    private
    pure
    returns (address[] memory)
    {
        uint256 occurrences = 0;

        // Count occurrences
        for (uint256 i = 0; i < pool.length; i++) {
            if (pool[i] == stakerAddress) {
                occurrences++;
            }
        }

        if (occurrences > 0) {
            // Create a new array with reduced size
            address[] memory newPool = new address[](pool.length - occurrences);
            uint256 newIndex = 0;

            // Copy elements to the new array, excluding occurrences of stakerAddress
            for (uint256 i = 0; i < pool.length; i++) {
                if (pool[i] != stakerAddress) {
                    newPool[newIndex] = pool[i];
                    newIndex++;
                }
            }

            return newPool;
        }

        return pool;
    }

    /// @notice Function to choose winners from the shuffled pool
    /// @dev This function selects winners from the shuffled pool and logs their addresses
    function chooseWinners(uint8 seats) public {
        address[] memory pool = makeLotteryArray();
        address[] memory shuffledPool = shuffle(pool);
        address[] memory winners = new address[](seats);

        emit RAFFLE_POOL(shuffledPool);

        if (seats > pool.length) {
            emit WINNERS(shuffledPool);
        } else {
            for (uint8 i = 0; i < seats; i++) {
                uint256 randomN = uint256(keccak256(abi.encodePacked(blockhash(block.number), block.basefee, i))) % shuffledPool.length;

                winners[i] = shuffledPool[randomN];
                shuffledPool = removeAddressFromPool(shuffledPool, shuffledPool[randomN]);
            }

            delete stakers;

            past.push(block.number);

            emit WINNERS(winners);


        }
    }

    /// @notice function that returns the stakers array
    function getStakers() public view returns (address[] memory) {
        return stakers;
    }
}