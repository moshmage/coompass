import "github-markdown-css/github-markdown-dark.css"
import './globals.css'
import {getLocale} from "@/i18n/server";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";

export default async function RootLayout({children,}: { children: React.ReactNode }) {
  const lang = await getLocale();

  return (
    <html lang={lang}>
    <head>
      <title>Coompass</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap"></link>
    </head>
    <body>
    <LocaleProvider value={lang}>
      {children}
    </LocaleProvider>
    </body>
    </html>
  )
}
