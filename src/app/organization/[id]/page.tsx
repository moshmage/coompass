import {LayoutApp} from "@/components/layouts/app/layout-app";
import {OrgPage} from "@/components/organization/page/page";

export default async function OrganizationPage({params}: any) {
  return <LayoutApp>
    <OrgPage companyId={params.id} />
  </LayoutApp>
}