import {LayoutApp} from "@/components/layouts/app/layout-app";
import {UsersTable} from "@/components/organization/users-table/users-table";

export default async function OrganizationUsersPage({params}: any) {

  return <LayoutApp>
    <UsersTable companyId={params.id} />

  </LayoutApp>
}