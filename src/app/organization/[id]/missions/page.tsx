"use server";

import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import {auth} from "@/handlers/server/credentials/auth";
import {User, UserTypes} from "@/types/user/user";
import {redirect} from "next/navigation";
import {SimpleTable} from "@/components/table/simple-table/simple-table";
import {MissionState} from "@/components/mission/mission-state/mission-state";
import {Id} from "@/components/id/id";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";
import {Mission, ParticipantsState} from "@/types/mission/mission";
import {OrgPagesHeader} from "@/components/organization/org-pages-header/org-pages-header";
import {XOfTotal} from "@/components/number/x-of-total/x-of-total";
import {format} from "date-fns";
import {MissionsTable} from "@/components/organization/missions-table/missions-table";

export default async function CompanyMissions({params}: any) {

  return <LayoutApp>
    <MissionsTable companyId={params.id} />

  </LayoutApp>
}