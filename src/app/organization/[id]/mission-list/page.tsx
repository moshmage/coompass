"use server";

import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {MissionCard} from "@/components/mission/mission-card/mission-card";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";
import {MissionsList} from "@/components/organization/missions-list/missions-list";

export default async function CompanyMissionList({params}: any) {
  return <LayoutApp>
    <MissionsList companyId={params.id} />
  </LayoutApp>
}