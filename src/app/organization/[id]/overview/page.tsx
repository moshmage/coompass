"use server";
import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {OrgOverview} from "@/components/organization/org-overview";

export default async function OrgOverviewPage({params}: any) {

  return <LayoutApp>
    <Grid container>
      <Grid item xs={0} sm={0} md={0} lg={1}/>
      <Grid item xs={12} sm={12} md={12} lg={10}>
        <OrgOverview companyId={params.id} />
      </Grid>
      <Grid item xs={0} sm={0} md={0} lg={1}/>
    </Grid>
  </LayoutApp>
}