import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {MissionPage} from "@/components/mission/mission-page/mission-page";
import {OrganizationHeader} from "@/components/mission/organization-header/organization-header";
import Link from "next/link";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";
import {Mission} from "@/types/mission/mission";
import {auth} from "@/handlers/server/credentials/auth";
import {User, UserTypes} from "@/types/user/user";
import {SimpleButton} from "@/components/buttons/simple/simple-button";

import styles from "@/app/missions/[id]/mission.module.css";
import {CaretLeftSvg} from "@/components/svg/caret-left-svg";
import {createTranslation} from "@/i18n/server";

export default async function MissionIdPage({params, searchParams}: any) {
  const {t} = await createTranslation()
  const [mission] = await fetchServerData<Mission[]>(`/api/missions/${params.id}`);

  const _auth = await auth();

  const type = (_auth?.user as User)?.type;
  const url =
    type === UserTypes.ngo
      ? `/organization/${mission.company.id}/missions`
      : !("fromOrg" in searchParams) ? `/missions` : `/organization/${mission.company.id}/mission-list`;

  const linkText =
    type === UserTypes.ngo
      ? t('missions.return.returnToMyMissions')
      : !("fromOrg" in searchParams) ? t('missions.return.returnToAllMissions') : t('missions.return.returnToOrgMissions')

  const filterState = (s: string) => ({state}: {state: string}) => state === s;
  const totalUsers = mission.participantsState?.filter(filterState("accepted")).length;
  const totalPendingUsers = mission.participantsState?.filter(filterState("pending")).length;

  return <LayoutApp>
    <Grid container spacing={1}>
      <Grid item xs={0} sm={1} md={1} lg={1} />
      <Grid item xs={12} sm={10} md={10} lg={10} className={styles.missionPage_back}>
        <Link href={url}><SimpleButton small style="black" label={<> <CaretLeftSvg width={20} height={20}/> {linkText}</>} /></Link>
      </Grid>
      <Grid item xs={0} sm={1} md={1} lg={1} />
    </Grid>
    <Grid container spacing={1}>
      <Grid item xs={0} sm={1} md={1} lg={1} />
      <Grid item xs={12} sm={10} md={10} lg={10}>
        <MissionPage header={mission.image}
                     companyId={mission.company.id}
                     id={mission.id}
                     description={mission.description}
                     orgName={mission.company.name}
                     totalUsers={totalUsers}
                     totalPendingUsers={totalPendingUsers}
                     causes={mission.causes??[]}
                     skills={mission.skills??[]}
                     title={mission.title}
                     state={mission.state}
                     datePosted={mission.createdAt??new Date()}
                     dateStarted={mission.startedAt}
                     dateFinished={mission.finishedAt}
                     participation={mission.participantsState?.find(({userId}) => userId === _auth?.user?.id) || null}
                     requirements={mission.requirements}
                     reward={mission.reward}
                     location={mission.location}
                     whenStart={mission.when_start}
                     whenFinish={mission.when_finish} />
        <Grid container>
          <Grid item xs={12}>
            <div className={styles.missionPage_aboutCompany}>{t('missions.aboutThisOrg')}</div>
            <OrganizationHeader orgName={mission.company.name}
                                id={mission.company.id} missionId={mission.id}
                                image={mission.company.image}
                                description={mission.company.description} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={0} sm={1} md={1} lg={1} />
    </Grid>

  </LayoutApp>
}