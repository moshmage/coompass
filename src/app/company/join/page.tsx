import {LayoutSimple} from "@/components/layouts/simple/layout-simple";
import {JoinCompanyBox} from "@/components/organization/join-company/join-company";
import Providers from "@/app/providers";

export default async function JoinCompanyPage() {
  return <LayoutSimple link="" linkText="">
    <Providers>
      <JoinCompanyBox />
    </Providers>
  </LayoutSimple>
}