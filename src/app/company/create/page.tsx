import {LayoutApp} from "@/components/layouts/app/layout-app";
import {CreateOrg} from "@/components/organization/create-org/create-org";
import {Grid} from "@mui/material";

export default async function CreateCompanyPage() {
  return <LayoutApp>
      <Grid container>
        <Grid item xs={0} sm={0} md={3} />
        <Grid item xs={12} sm={12} md={6}>
          <CreateOrg />
        </Grid>
        <Grid item xs={0} sm={0} md={3} />
      </Grid>
  </LayoutApp>
}