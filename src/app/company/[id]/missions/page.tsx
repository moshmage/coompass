import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {SwitcherButton} from "@/components/switcher/switcher-button";
import {CompanyMissionsTable} from "@/components/company/company-missions-table/company-missions-table";
import {createTranslation} from "@/i18n/server";

export default async function CompanyMissionsPage({params}: any) {
  const {t} = await createTranslation();
  const options = [
    {label: t('common.company.switcherLabels.Employees'), url: `/company/${params.id}/employees/`},
    {label: t('common.company.switcherLabels.Missions'), url: `/company/${params.id}/missions/`},
  ]

  return <LayoutApp>
    <Grid container>
      <Grid item xs={12} sm={5} md={4} lg={3}>
        <SwitcherButton options={options} activeLabel={t('common.company.switcherLabels.Missions')}/>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12}>
        <div className="hr"></div>
      </Grid>
    </Grid>
    <CompanyMissionsTable companyId={params.id} />

  </LayoutApp>
}