import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {SwitcherButton} from "@/components/switcher/switcher-button";
import {EmployeesList} from "@/components/company/employees-list";
import {RequestCertificate} from "@/components/certificate/request-certificate";
import {createTranslation} from "@/i18n/server";

export default async function CompanyEmployeesPage({params}: { params: any }) {
  const {t} = await createTranslation();
  const options = [
    {label: t('common.company.switcherLabels.Employees'), url: `/company/${params.id}/employees/`},
    {label: t('common.company.switcherLabels.Missions'), url: `/company/${params.id}/missions/`},
  ]

  return <LayoutApp>
    <Grid container sx={{display: {xs: "block", sm: "flex"}, justifyContent: {sm: "space-between"}}}>
      <Grid item xs={12} sm={5} md={4} lg={3}>
        <SwitcherButton options={options} activeLabel={t('common.company.switcherLabels.Employees')}/>
      </Grid>
      <Grid item xs={12} sm={5} md={4} lg={3} style={{justifyContent: "end", display: "flex"}}>
        <RequestCertificate companyId={params.id}/>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12}>
        <div className="hr"></div>
      </Grid>
    </Grid>

    <EmployeesList companyId={params.id}/>

  </LayoutApp>
}