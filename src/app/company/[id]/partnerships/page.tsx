import {LayoutApp} from "@/components/layouts/app/layout-app";
import {CompanyPartnerships} from "@/components/company/company-partnerships/company-partnerships";

export default async function PartnershipsPage({params}: any) {

  return <LayoutApp>
    <CompanyPartnerships companyId={params.id} />
  </LayoutApp>
}