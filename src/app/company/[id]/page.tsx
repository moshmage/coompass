"use server";

import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {UserLogs} from "@/components/window/user-log/user-logs";
import WindowCalendar from "@/components/window/calendar/window-calendar";
import {CompanyHeader} from "@/components/company/company-header/company-header";
import {CompanyMissions} from "@/components/company/company-missions/company-missions";

export default async function CompanyDashboardPage({params}: any) {

  return <LayoutApp>
    <Grid container style={{paddingBottom: "16px"}} spacing={3}>
      <Grid item xs={12}>
        <CompanyHeader companyId={params.id} />
      </Grid>
    </Grid>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={12} md={12} lg={8}>
        <Grid container>
          <Grid item xs={12}>
            <UserLogs companyId={params.id}/>
          </Grid>
        </Grid>
        <Grid container style={{marginTop: "16px"}}>
          <Grid item xs={12}>
            <CompanyMissions companyId={params.id}/>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={4}>
        <WindowCalendar />
      </Grid>
    </Grid>
  </LayoutApp>
}