import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {Organizations} from "@/components/organizations/organizations";
import {createTranslation} from "@/i18n/server";

export default async function MissionsPage() {
  const {t} = await createTranslation();
  return <LayoutApp>

    <Grid container>
      <Grid item xs={12} style={{borderBottom: "1px solid rgba(255, 255, 255, 0.12)", paddingBottom: "32px"}}>
        <h2>{t('organizations.title')}</h2>
      </Grid>
    </Grid>

    <Organizations />

  </LayoutApp>
}