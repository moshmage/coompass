import {LayoutApp} from "@/components/layouts/app/layout-app";
import {Grid} from "@mui/material";
import {UserSettings} from "@/components/user/user-settings";

export default function UserSettingsPage() {
  return <LayoutApp>
    <Grid item xs={3} />
    <Grid item xs={6}><UserSettings /></Grid>
    <Grid item xs={3} />
  </LayoutApp>
}