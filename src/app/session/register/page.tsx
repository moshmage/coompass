import {RegisterBox} from "@/components/session/register-box/register";
import {LayoutSimple} from "@/components/layouts/simple/layout-simple";

export default async function Register() {
  return <LayoutSimple>
    <RegisterBox />
  </LayoutSimple>
}