import {LayoutSimple} from "@/components/layouts/simple/layout-simple";
import {ForgotPasswordBox} from "@/components/session/forgot-password/forgot-password-box";

export default async function ForgotPasswordPage() {
  return <LayoutSimple>
    <ForgotPasswordBox />
  </LayoutSimple>
}