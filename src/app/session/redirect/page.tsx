"use client";
import {CircularProgress, Grid} from "@mui/material";
import Providers from "@/app/providers";
import {Redirector} from "@/components/session/redirector/redirector";

export default function RedirectPage() {

  return <Providers>
    <Redirector/>
    <Grid container>
      <Grid item xs={12} style={{height: "100vh", display: "flex", justifyContent: "center", alignItems: "center"}}>
        <CircularProgress color="info" title="Redirecting..."/>
      </Grid>
    </Grid>
  </Providers>
}