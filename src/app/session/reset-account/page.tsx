import {LayoutSimple} from "@/components/layouts/simple/layout-simple";
import {ResetAccount} from "@/components/session/reset-account/reset-account";

export default function ResetAccountPage({searchParams}: any) {
  return <LayoutSimple>
    <ResetAccount searchParams={searchParams}/>
  </LayoutSimple>
}