import {auth} from "@/handlers/server/credentials/auth";
import {User, UserTypes} from "@/types/user/user";
import {redirect} from "next/navigation";

export default async function AdminPage() {
  const session = await auth();
  if (!(session?.user as User)?.role?.includes(UserTypes.admin))
    redirect("/session");

  return <p>Hello {session?.user?.email}</p>
}