import {LayoutSimple} from "@/components/layouts/simple/layout-simple";
import {LoginBox} from "@/components/session/login-box/login-box";

export default async function Home({searchParams}: any) {

  return (
    <LayoutSimple>
      <LoginBox searchParams={searchParams} />
    </LayoutSimple>
  )
}
