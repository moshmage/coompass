import {auth} from "@/handlers/server/credentials/auth";
import {redirect} from "next/navigation";
import {UserTypes} from "@/types/user/user";
import {StepperLayout} from "@/components/layouts/stepper/stepper-layout";
import {CreateMissionSteps} from "@/constants/create-mission-steps";
import {Review} from "@/components/mission/create-mission/review/review";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";
import {Company} from "@/types/company/company";

export default async function CreateMissionReviewPage() {
  const _auth = await auth();
  if (!_auth)
    redirect("/");

  const user = await fetchServerData(`/api/users/${_auth.user?.id}`)

  if (![UserTypes.ngo].includes(user.type))
    redirect(`/session/redirect`);

  const companyId = user?.companies[0];

  if (!companyId)
    redirect(`/company/create`);

  const company = await fetchServerData<Company>(`/api/company/${companyId}`)

  return <StepperLayout active={2}
                        companyId={companyId}
                        steps={CreateMissionSteps}
                        backLink={`/create-mission/point-of-contact`}>
    <Review companyId={companyId} orgName={company?.name} />
  </StepperLayout>
}