import {auth} from "@/handlers/server/credentials/auth";
import {redirect} from "next/navigation";
import {UserTypes} from "@/types/user/user";
import {StepperLayout} from "@/components/layouts/stepper/stepper-layout";
import {CreateMissionSteps} from "@/constants/create-mission-steps";
import {CreateMissionDetails} from "@/components/mission/create-mission/mission-details/create-mission-details";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";

export default async function CreateMissionDetailsPage() {
  const _auth = await auth();
  if (!_auth)
    redirect("/");

  const user = await fetchServerData(`/api/users/${_auth.user?.id}`)

  if (![UserTypes.ngo].includes(user.type))
    redirect(`/session/redirect`);

  const companyId = user?.companies[0];

  if (!companyId)
    redirect(`/company/create`);

  return <StepperLayout active={0}
                        companyId={companyId}
                        steps={CreateMissionSteps}
                        backLink={`/organization/${companyId}`}>
    <CreateMissionDetails companyId={companyId} />
  </StepperLayout>
}