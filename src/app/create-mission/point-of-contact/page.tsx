import {auth} from "@/handlers/server/credentials/auth";
import {redirect} from "next/navigation";
import {UserTypes} from "@/types/user/user";
import {StepperLayout} from "@/components/layouts/stepper/stepper-layout";
import {CreateMissionSteps} from "@/constants/create-mission-steps";
import {PointOfContact} from "@/components/mission/create-mission/point-of-contact/point-of-contact";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";

export default async function CreateMissionPoCPage() {
  const _auth = await auth();
  if (!_auth)
    redirect("/");

  const user = await fetchServerData(`/api/users/${_auth.user?.id}`)

  if (![UserTypes.ngo].includes(user.type))
    redirect(`/session/redirect`);

  const companyId = user?.companies[0];

  if (!companyId)
    redirect(`/company/create`);

  return <StepperLayout active={1}
                        companyId={companyId}
                        steps={CreateMissionSteps}
                        backLink={`/create-mission/details`}>
    <PointOfContact />
  </StepperLayout>
}