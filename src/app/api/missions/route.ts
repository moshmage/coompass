import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getMissions} from "@/handlers/server/missions/get-missions";
import {createMission} from "@/handlers/server/missions/create-mission";
import {UserTypes} from "@/types/user/user";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {NextRequest} from "next/server";
import {parseQuery} from "@/handlers/server/fetch/parse-query";

const _GET = async (req: NextRequest) =>
  Response.json(await getMissions(parseQuery(req.url)), {status: 200})

const _POST = async (req: NextAuthRequest) =>
  Response.json(await createMission({...await req.json(), creator: req.auth.user.id}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));
export const POST = withErrorHandler(withRole(_POST, [UserTypes.company, UserTypes.ngo]));
