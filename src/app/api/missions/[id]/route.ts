// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {joinMission} from "@/handlers/server/missions/join-mission";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {changeMission} from "@/handlers/server/missions/change-mission";
import {UserTypes} from "@/types/user/user";
import {NextRequest} from "next/server";
import {getMissions} from "@/handlers/server/missions/get-missions";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {deleteMission} from "@/handlers/server/missions/delete-mission";

const _GET = async (request: NextRequest) =>
  Response.json(await getMissions({missionId: getId(request)}), {status: 200})

const _PUT = async (request: NextAuthRequest) =>
  Response.json(await joinMission({missionId: getId(request), userId: request.auth.user.id}), {status: 200})

const _PATCH = async (request: NextAuthRequest) =>
  Response.json(await changeMission({...await request.json(), id: getId(request)}, {requester: request.auth.user.id}), {status: 200})

const _DELETE = async (request: NextAuthRequest) =>
  Response.json(await deleteMission({id: getId(request)}, {requester: request.auth.user.id}), {status: 200})

export const GET = withErrorHandler(withRole(_GET));
export const PUT = withErrorHandler(withRole(_PUT, [UserTypes.user]));
export const PATCH = withErrorHandler(withRole(_PATCH, [UserTypes.ngo]));
export const DELETE = withErrorHandler(withRole(_DELETE, [UserTypes.ngo]));