import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {getParticipantsState} from "@/handlers/server/missions/get-participants-state";
import {addParticipantEntry} from "@/handlers/server/missions/add-participant-entry";
import {UserTypes} from "@/types/user/user";
import {patchParticipantEntry} from "@/handlers/server/missions/patch-participant-entry";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {parseQuery} from "@/handlers/server/fetch/parse-query";

const _GET = async (request: NextRequest) =>
  Response.json(await getParticipantsState({missionId: getId(request), ...parseQuery(request.url)}), {status: 200});

const _POST = async (request: NextAuthRequest) =>
  Response.json(await addParticipantEntry({missionId: getId(request), userId: request.auth.user.id}), {status: 200});

const _PATCH = async (request: NextAuthRequest) =>
  Response.json(await patchParticipantEntry({missionId: getId(request), ...await request.json(), requester: request.auth?.user?.id}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));
export const POST = withErrorHandler(withRole(_POST, [UserTypes.user]));
export const PATCH = withErrorHandler(withRole(_PATCH, [UserTypes.ngo]));