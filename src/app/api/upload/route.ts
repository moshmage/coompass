import {uploadRouteHandler} from "@/handlers/server/upload/upload-route-handler";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";

// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";

const _POST = async (request: NextAuthRequest) =>
  Response.json(await uploadRouteHandler(request, request.auth?.user?.id), {status: 200})

export const POST = withErrorHandler(withRole(_POST));