import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {createLog} from "@/handlers/server/user-log/create-log";
import {getUserLogs} from "@/handlers/server/user-log/get-user-logs";

const _POST = async (request: NextRequest) =>
  Response.json(await createLog(await request.json()), {status: 200})

const _GET = async (request: NextRequest) =>
  Response.json(await getUserLogs(await request.json()), {status: 200})

export const POST = withErrorHandler(withRole(_POST));
export const GET = withErrorHandler(withRole(_GET));


