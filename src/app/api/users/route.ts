// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getUser} from "@/handlers/server/users/get-user";
import {parseQuery} from "@/handlers/server/fetch/parse-query";

const _GET = async (req: NextAuthRequest) =>
  Response.json(await getUser({email: parseQuery(req.url)?.email}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));