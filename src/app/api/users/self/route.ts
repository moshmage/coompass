// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getUser} from "@/handlers/server/users/get-user";

const _GET = async (request: NextAuthRequest) =>
  Response.json(await getUser({userId: request.auth?.user?.id}), {status: 200});

export const GET = withErrorHandler(withRole(_GET))