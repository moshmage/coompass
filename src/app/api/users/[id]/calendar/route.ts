import {getCalendarForUser} from "@/handlers/server/calendar/get-calendar-for-user";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";


const _GET = async (request: NextAuthRequest) =>
  Response.json(await getCalendarForUser({userId: request.auth.user.id}), {status: 200})

export const GET = withErrorHandler(withRole(_GET));