import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getMissions} from "@/handlers/server/missions/get-missions";
import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";

const _GET = async (request: NextRequest) =>
  Response.json(await getMissions({ofUsers: [getId(request)]}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));