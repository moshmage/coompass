import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {getUserParticipations} from "@/handlers/server/missions/get-user-participations";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";

const _GET = async (request: NextRequest) =>
  Response.json(await getUserParticipations({userId: getId(request)}));

export const GET = withErrorHandler(withRole(_GET));