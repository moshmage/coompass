import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getUser} from "@/handlers/server/users/get-user";
import {updateUser} from "@/handlers/server/users/update-user";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {getId} from "@/handlers/server/fetch/get-request-id";


async function _GET(request: NextRequest) {
  return Response.json(await getUser({userId: getId(request)}), {status: 200});
}

const _PUT = async (request: NextAuthRequest) =>
  Response.json(await updateUser({...await request.json(), id: request.auth?.user?.id}), {status: 200});


export const GET = withErrorHandler(withRole(_GET));
export const PUT = withErrorHandler(withRole(_PUT));