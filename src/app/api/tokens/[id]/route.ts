import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";

const _GET = async (request: NextRequest) =>
  Response.json(({tokenId: getId(request)}));

export const GET = withErrorHandler(_GET);