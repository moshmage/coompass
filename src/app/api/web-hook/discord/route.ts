import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {parseVercelWebhookEvent} from "@/handlers/utils/web-hooks/vercel/parse-vercel-webhook-event";
import {createHmac} from "node:crypto";

const {WEBHOOK_DISCORD_LINK, WEBHOOK_DISCORD_VERCEL_SECRET} = process.env;

const _POST = async (request: NextRequest) => {

  const body = await request.text();

  const signature =
    createHmac("sha1", WEBHOOK_DISCORD_VERCEL_SECRET!)
      .update(body)
      .digest("hex");

  const vercelSignature = request.headers.get("x-vercel-signature");

  if (!vercelSignature || vercelSignature !== signature)
    throw new HttpBadRequestError(`Bad signature`);

  const event = JSON.parse(body);

  await fetch(new URL(WEBHOOK_DISCORD_LINK!),
    {
      method: "post",
      headers: {"Content-Type": "application/json",},
      body: JSON.stringify(parseVercelWebhookEvent(event)),
    })
    .catch(e => {
      console.error("Failed to ping discord", e);
    });

  return Response.json({message: "ok"}, {status: 200})
}


export const POST = withErrorHandler(_POST)