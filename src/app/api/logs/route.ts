import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getAllLogs} from "@/handlers/server/logs/get-all-logs";

const _GET = async (req: NextRequest) =>
  Response.json(await getAllLogs(), {status: 200});

export const GET = withErrorHandler(withRole(_GET));