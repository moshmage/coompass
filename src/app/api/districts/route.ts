import {getDistricts} from "@/handlers/server/districts/get-districts";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";

const _GET = async () =>
    Response.json(await getDistricts(), {status: 200});

export const GET = withErrorHandler(withRole(_GET));