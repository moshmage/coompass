import {NextRequest} from "next/server";
import {logSendgridActivity} from "@/handlers/server/email/log-sendgrid-activity";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";

const _POST = async (req: NextRequest) =>
    Response.json(await logSendgridActivity({headers: req.headers, body: await req.json()}), {status: 200});

export const POST = withErrorHandler(_POST);