import {NextRequest} from "next/server";
import {resetPassword} from "@/handlers/server/credentials/reset-password";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";

const _POST = async (request: NextRequest) =>
  Response.json(await resetPassword({...await request.json()}), {status: 200})

export const POST = withErrorHandler(_POST);