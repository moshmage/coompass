import {NextRequest} from "next/server";
import {forgotPassword} from "@/handlers/server/credentials/forgot-password";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";

const _POST = async (request: NextRequest) =>
  Response.json(await forgotPassword({...await request.json()}), {status: 200})

export const POST = withErrorHandler(_POST);