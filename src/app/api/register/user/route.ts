import {NextRequest} from "next/server";
import {userRegister} from "@/handlers/server/credentials/user-register";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";

const _POST = async (request: NextRequest) =>
  Response.json(await userRegister(await request.json()), {status: 200})

export const POST = withErrorHandler(_POST)