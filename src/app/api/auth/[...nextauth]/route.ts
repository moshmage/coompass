import {AuthHandlers} from "@/handlers/server/credentials/auth";

export const {GET,POST} = AuthHandlers;