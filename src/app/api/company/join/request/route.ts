import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {requestCompanyMembership} from "@/handlers/server/company/join/request";
import {UserTypes} from "@/types/user/user";

// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";

const _POST = async (request: NextAuthRequest) =>
  Response.json(await requestCompanyMembership(await request.json(), request.auth?.user?.id));

export const POST = withErrorHandler(withRole(_POST, UserTypes.user));