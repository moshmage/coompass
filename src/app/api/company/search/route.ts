import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getCompanies} from "@/handlers/server/company/get-companies";
import {UserTypes} from "@/types/user/user";

const _GET = async (req: NextRequest) =>
  Response.json(await getCompanies(await req.json()), {status: 200})

export const GET = withErrorHandler(withRole(_GET, UserTypes.user));