import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {UserTypes} from "@/types/user/user";
import {updateRequest} from "@/handlers/server/company/join/updated-request";
import {getId} from "@/handlers/server/fetch/get-request-id";

// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";

const _POST = async (request: NextAuthRequest) =>
  Response.json(await updateRequest({... await request.json(), companyId: getId(request)}, request.auth?.user?.id), {status: 200});

export const POST = withErrorHandler(withRole(_POST, UserTypes.company));