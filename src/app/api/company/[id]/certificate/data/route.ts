import {NextRequest} from "next/server";
import {getCertificateData} from "@/handlers/server/certificates/get-certificate-data";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {parseQuery} from "@/handlers/server/fetch/parse-query";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";

const _GET = async (request: NextRequest) =>
  Response.json(await getCertificateData({companyId: getId(request), ...parseQuery(request.url)}))

export const GET = withErrorHandler(withRole(_GET));