import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getCertificates} from "@/handlers/server/certificates/get-certificates";
import {createCertificate} from "@/handlers/server/certificates/create-certificate";
import {UserTypes} from "@/types/user/user";

// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";

const _GET = async (request: NextRequest) =>
  Response.json(await getCertificates({companyId: getId(request)}), {status: 200});

const _POST = async (request: NextAuthRequest) => {
  const encoder = new TextEncoder();
  const companyId = getId(request);
  const requestData = await request.json();

  const readable = new ReadableStream({
    async start(controller) {
      const certificate = await createCertificate({ companyId, ...requestData }, { id: request.auth.user.id });

      controller.enqueue(encoder.encode(JSON.stringify(certificate)));
      controller.close();
    }
  });

  return new Response(readable, { headers: { "Content-Type": "application/json" }, status: 200 });
};

export const GET = withErrorHandler(withRole(_GET));
export const POST = withErrorHandler(withRole(_POST, [UserTypes.company]));