import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getParticipantsState} from "@/handlers/server/missions/get-participants-state";
import {parseQuery} from "@/handlers/server/fetch/parse-query";

const _GET = async (request: NextRequest) =>
  Response.json(await getParticipantsState({companyId: getId(request), ...parseQuery(request.url)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));