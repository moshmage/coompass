import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {getCompanyEngagement} from "@/handlers/server/company/get-company-engagement";

const _GET = async (req: NextRequest) =>
  Response.json(await getCompanyEngagement({companyId: getId(req)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));