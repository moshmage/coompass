import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {addPartnership} from "@/handlers/server/company/add-partnership";
import {deletePartnership} from "@/handlers/server/company/delete-partnership";
import {UserTypes} from "@/types/user/user";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";

const _POST = async (req: NextAuthRequest) =>
  Response.json(await addPartnership({companyId: getId(req), partnerOrgId: getId(req, 5), requesterId: req.auth.user.id}), {status: 200});

const _DELETE = async (req: NextAuthRequest) =>
  Response.json(await deletePartnership({companyId: getId(req), partnerOrgId: getId(req, 5), requesterId: req.auth.user.id}), {status: 200});

export const POST = withErrorHandler(withRole(_POST, UserTypes.company));
export const DELETE = withErrorHandler(withRole(_DELETE, UserTypes.company));