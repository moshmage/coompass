import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getPartnerships} from "@/handlers/server/company/get-partnerships";

const _GET = async (req: NextRequest) =>
  Response.json(await getPartnerships({companyId: getId(req)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));
