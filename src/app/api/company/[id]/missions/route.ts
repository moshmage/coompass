import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getMissions} from "@/handlers/server/missions/get-missions";

const _GET = async (request: NextRequest) =>
  Response.json(await getMissions({participantsOf: request.nextUrl.pathname.split("/")[3]}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));