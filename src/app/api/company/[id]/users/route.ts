import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getCompanyUsers} from "@/handlers/server/company/get-company-users";
import {getId} from "@/handlers/server/fetch/get-request-id";

const _GET = async (request: NextRequest) =>
  Response.json(await getCompanyUsers({companyId: getId(request)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));