import {NextRequest} from "next/server";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {getCompanyGraph} from "@/handlers/server/company/get-company-graph";

const _GET = async (request: NextRequest) =>
  Response.json(await getCompanyGraph({companyId: getId(request)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));