import {getCompanies} from "@/handlers/server/company/get-companies";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";

const _GET = async (request: NextRequest) => {
  const [company] = await getCompanies({companyId: getId(request)});
  return Response.json(company, {status: 200});
}

export const GET = withErrorHandler(withRole(_GET));