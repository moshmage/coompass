import {NextRequest} from "next/server";
import {getId} from "@/handlers/server/fetch/get-request-id";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getParticipantsOfCompany} from "@/handlers/server/company/get-participants-of-company";

const _GET = async (request: NextRequest) =>
  Response.json(await getParticipantsOfCompany({companyId: getId(request)}), {status: 200});

export const GET = withErrorHandler(withRole(_GET));