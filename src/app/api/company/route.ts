import {registerCompany} from "@/handlers/server/company/register-company";
import {withErrorHandler} from "@/handlers/server/higher-order/with-error-handler";
import {withRole} from "@/handlers/server/higher-order/with-role";
import {getCompanies} from "@/handlers/server/company/get-companies";
import {joinCompany} from "@/handlers/server/company/join-company";
// @ts-ignore
import {NextAuthRequest} from "next-auth/lib";
import {UserTypes} from "@/types/user/user";
import {NextApiRequest} from "next";
import * as QueryString from "querystring";
import {parseQuery} from "@/handlers/server/fetch/parse-query";
import {NextRequest} from "next/server";

const _GET = async (req: NextRequest) =>
  Response.json(await getCompanies(parseQuery(req.url)), {status: 200});

const _POST = async (req: NextAuthRequest) =>
  Response.json(await registerCompany(await req.json(), req.auth?.user), {status: 200});

const _PUT = async (req: NextAuthRequest) =>
  Response.json(await joinCompany(await req.json(), req.auth?.user))

export const GET = withErrorHandler(withRole(_GET));
export const POST = withErrorHandler(withRole(_POST, [UserTypes.company, UserTypes.ngo]));
export const PUT = withErrorHandler(withRole(_PUT, UserTypes.user));