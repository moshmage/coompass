"use client";

import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {SnackbarProvider} from "notistack";
import {CustomSessionProvider} from "@/components/session/custom-session/custom-session-provider";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";
import {getLocaleClient} from "@/i18n/client";

const makeQueryClient = () =>
  new QueryClient({
    defaultOptions: {
      queries: {
        staleTime: 1000
      }
    }
  });

let browserQueryClient: QueryClient | undefined = undefined;

const getQueryClient = () => {
  if (typeof window === undefined)
    return makeQueryClient();
  else {
    if (!browserQueryClient)
      browserQueryClient = makeQueryClient();

    return browserQueryClient;
  }
}

export default function Providers({children}: any) {
  const queryClient = getQueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <CustomSessionProvider>
        <SnackbarProvider maxSnack={3}>
          <LocaleProvider value={getLocaleClient()}>
            {children}
          </LocaleProvider>
        </SnackbarProvider>
      </CustomSessionProvider>
    </QueryClientProvider>
  )
}