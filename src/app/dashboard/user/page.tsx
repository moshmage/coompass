import {LayoutApp} from "@/components/layouts/app/layout-app";
import WindowCalendar from "@/components/window/calendar/window-calendar";
import ScoreWindow from "@/components/window/score/score-window";
import BadePerk from "@/components/window/badge-perk/bade-perk";
import WorkGraph from "@/components/window/work-graph/work-graph";
import {Grid} from "@mui/material";
import {UserHeader} from "@/components/user/user-header/user-header";
import {CompanyHeader} from "@/components/user/company-header/company-header";
import {UserMissions} from "@/components/user/user-missions/user-missions";

export default async function DashboardPage() {

  return <LayoutApp>
    <Grid container spacing={1} style={{display: "flex", alignItems: "center", marginBottom: 8}}>
      <Grid item xs={12} sm={6} md={7} lg={6}>
        <UserHeader />
      </Grid>
      <Grid item xs={0} sm={0} md={0} lg={3} style={{paddingLeft: 0}}/>
      <Grid item xs={12} sm={6} md={5} lg={3}>
        <CompanyHeader />
      </Grid>
    </Grid>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={12} md={12} lg={8} xl={9}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={12} md={7}>
            <WorkGraph />
          </Grid>
          <Grid item xs={12} sm={12} md={5}>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <BadePerk/>
              </Grid>
              <Grid item xs={12}>
                <ScoreWindow earned={0} level={0} gaugeValue={0}/>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <UserMissions />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={4} xl={3}>
        <WindowCalendar/>
      </Grid>
    </Grid>
  </LayoutApp>
}