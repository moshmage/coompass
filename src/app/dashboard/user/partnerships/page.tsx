
import {LayoutApp} from "@/components/layouts/app/layout-app";
import {UserCompanyPartnerships} from "@/components/user/user-company-partnerships/user-company-partnerships";

export default function UserCompanyPartnershipsPage() {

  return <LayoutApp>
    <UserCompanyPartnerships/>
  </LayoutApp>
}