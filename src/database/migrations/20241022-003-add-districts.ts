import {QueryInterface} from "sequelize";
import {DistrictModelDefinition, DistrictsTableName} from "../models/definitions/districts";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(DistrictsTableName, DistrictModelDefinition);

    await queryInterface.bulkInsert(DistrictsTableName, [
      { pt: 'Aveiro', en: 'Aveiro' },
      { pt: 'Beja', en: 'Beja' },
      { pt: 'Braga', en: 'Braga' },
      { pt: 'Bragança', en: 'Bragança' },
      { pt: 'Castelo Branco', en: 'Castelo Branco' },
      { pt: 'Coimbra', en: 'Coimbra' },
      { pt: 'Évora', en: 'Évora' },
      { pt: 'Faro', en: 'Faro' },
      { pt: 'Guarda', en: 'Guarda' },
      { pt: 'Leiria', en: 'Leiria' },
      { pt: 'Lisboa', en: 'Lisbon' },
      { pt: 'Portalegre', en: 'Portalegre' },
      { pt: 'Porto', en: 'Porto' },
      { pt: 'Santarém', en: 'Santarém' },
      { pt: 'Setúbal', en: 'Setúbal' },
      { pt: 'Viana do Castelo', en: 'Viana do Castelo' },
      { pt: 'Vila Real', en: 'Vila Real' },
      { pt: 'Viseu', en: 'Viseu' }
    ]);

  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(DistrictsTableName);
  }
};