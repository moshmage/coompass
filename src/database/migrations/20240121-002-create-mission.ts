import {MissionModelDefinition, MissionTableName} from "../models/definitions/mission";
import {QueryInterface} from "sequelize";

module.exports = {
  async up(queryInterface: QueryInterface) {
    // Add your migration commands here
    await queryInterface.createTable(MissionTableName, MissionModelDefinition)
  },

  down: async (queryInterface: QueryInterface) => {
    // Add commands to revert the changes here
    await queryInterface.dropTable(MissionTableName);
  }
};