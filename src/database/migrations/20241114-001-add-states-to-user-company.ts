import {QueryInterface} from "sequelize";
import {UserCompanyModelDefinition, UserCompanyTableName} from "../models/definitions/user-company";

module.exports = {
  async up(queryInterface: QueryInterface) {
    const description = await queryInterface.describeTable(UserCompanyTableName);

    if (!("pending" in description))
      await queryInterface.addColumn(UserCompanyTableName, "pending", UserCompanyModelDefinition.pending);
    if (!("accepted" in description))
      await queryInterface.addColumn(UserCompanyTableName, "accepted", UserCompanyModelDefinition.accepted);


  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(UserCompanyTableName, "pending");
    await queryInterface.removeColumn(UserCompanyTableName, "accepted");
  }
};