import {DataTypes, QueryInterface} from "sequelize";
import {ResetAccountModelDefinition, ResetAccountTableName} from "../models/definitions/reset-account";
import {CertificatesTableName} from "../models/definitions/certificates";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.addColumn(CertificatesTableName, "sdgs", {type: DataTypes.ARRAY(DataTypes.STRING),})
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(CertificatesTableName, "sdgs")
  }
};