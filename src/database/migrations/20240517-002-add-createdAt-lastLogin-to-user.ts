import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionTableName} from "../models/definitions/mission";
import {MissionParticipantsTableName} from "../models/definitions/mission-participants";
import {UserModelTable} from "../models/definitions/user";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize){
    const attributes = {
      type: DataTypes.DATE,
    }

    await queryInterface.addColumn(UserModelTable, "createdAt", attributes);
    await queryInterface.addColumn(UserModelTable, "lastLogin", attributes);

  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(UserModelTable, "createdAt");
    await queryInterface.removeColumn(UserModelTable, "lastLogin");
  }
};