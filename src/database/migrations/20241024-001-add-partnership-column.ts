import {QueryInterface} from "sequelize";
import {PartnershipsModelDefinition, PartnershipsTableName} from "../models/definitions/partnerships";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(PartnershipsTableName, PartnershipsModelDefinition);
  },
  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(PartnershipsTableName);
  }
};