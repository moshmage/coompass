# Migrations
Migrations have to be written using umzug because, at the time of coding, sequelize-cli does not support sequelize-7.

## File naming convention

Full Year, Month, Day, Sequential Identifier, and Summary

In order to maintain a consistent and informative structure for our migration files, please adhere to the following 
extended naming convention:

- **Format:** `YYYYMMDD-SEQ-summary-of-what-does`
- **Example:** `20240120-001-create-users-table`

### Guidelines:

1. **Full Year (YYYY):**
    - Use a four-digit representation for the year.

2. **Month (MM):**
    - Use a two-digit representation for the month. For example, January would be '01', February would be '02', and so on.

3. **Day (DD):**
    - Use a two-digit representation for the day.

4. **Sequential Identifier:**
    - Add a sequential identifier starting with '001' after the date.

5. **Summary:**
    - Provide a concise and meaningful summary of the migration's purpose or changes. Use hyphens (-) to separate words.

### Handling Multiple Migrations on the Same Day:

If you have more than one migration to be executed on the same day, increment the sequential identifier.

- **Example:** If you have two migrations on January 20, 2024:
    - First migration: `20240120-001-create-users-table.mjs`
    - Second migration: `20240120-002-update-permissions.mjs`

### Important Note:

- **Maintain a clear and chronological order in filenames.**
- **Do not skip numbers when adding sequential identifiers.**

Adhering to this extended naming convention will provide a quick overview of the migration's purpose and help in 
tracking changes effectively.

## Base example

> Note: The migration files will be built using `npm run build-db` so we can use typescript, but it has to export through `module.exports`

```javascript
module.exports = {
  up: async ({context: queryInterface}) => {
    // Add your migration commands here
    await queryInterface.addColumn('table', 'key', {
      type: DataTypes.STRING,
      allowNull: false
    });
  },

  down: async ({context: queryInterface}) => {
    // Add commands to revert the changes here
    await queryInterface.removeColumn('table', 'column');
  }
}
```