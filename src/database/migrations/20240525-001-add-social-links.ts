import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {CompanyTableName} from "../models/definitions/company";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize) {
    const attributes = {
      type: DataTypes.STRING,
    }

    await queryInterface.addColumn(CompanyTableName, "website", attributes);
    await queryInterface.addColumn(CompanyTableName, "xcom", attributes);
    await queryInterface.addColumn(CompanyTableName, "linkedin", attributes);
    await queryInterface.addColumn(CompanyTableName, "facebook", attributes);

  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(CompanyTableName, "website");
    await queryInterface.removeColumn(CompanyTableName, "xcom");
    await queryInterface.removeColumn(CompanyTableName, "linkedin");
    await queryInterface.removeColumn(CompanyTableName, "facebook");
  }
};