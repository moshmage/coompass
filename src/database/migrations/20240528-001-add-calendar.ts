import {QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {CalendarModelDefinition, CalendarTableName} from "../models/definitions/calendar";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize) {
    await queryInterface.createTable(CalendarTableName, CalendarModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(CalendarTableName)
  }
};