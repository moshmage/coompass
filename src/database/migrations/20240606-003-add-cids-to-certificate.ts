import {QueryInterface} from "sequelize";
import {CertificatesModelDefinition, CertificatesTableName} from "../models/definitions/certificates";

module.exports = {
  async up(queryInterface: QueryInterface) {
    const description = await queryInterface.describeTable(CertificatesTableName);

    if (!("imageCid" in description))
      await queryInterface.addColumn(CertificatesTableName, "imageCid", CertificatesModelDefinition.imageCid);
    if (!("metadataCid" in description))
      await queryInterface.addColumn(CertificatesTableName, "metadataCid", CertificatesModelDefinition.metadataCid);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(CertificatesTableName, "metadataCid");
    await queryInterface.removeColumn(CertificatesTableName, "imageCid");
  }
};