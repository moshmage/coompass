import {QueryInterface} from "sequelize";
import {CompanyTableName} from "../models/definitions/company";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.removeColumn(CompanyTableName, 'location');
  },
};