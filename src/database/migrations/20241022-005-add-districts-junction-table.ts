import {QueryInterface} from "sequelize";
import {CompanyDistrictsModelDefinition, CompanyDistrictsTableName} from "../models/definitions/company-districts";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(CompanyDistrictsTableName, CompanyDistrictsModelDefinition);

  },

  async down(queryInterface: QueryInterface) {
    await queryInterface.dropTable(CompanyDistrictsTableName);
  }
};