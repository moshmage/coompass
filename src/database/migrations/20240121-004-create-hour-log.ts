import {HourLogModelDefinition, HourLogTableName} from "../models/definitions/hour-log";
import {QueryInterface} from "sequelize";

module.exports = {
  async up(queryInterface: QueryInterface){
    // Add your migration commands here
    await queryInterface.createTable(HourLogTableName, HourLogModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    // Add commands to revert the changes here
    await queryInterface.dropTable(HourLogTableName);
  }
};