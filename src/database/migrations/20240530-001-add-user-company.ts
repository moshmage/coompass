import {QueryInterface, QueryTypes} from "sequelize";
import {UserCompanyModelDefinition, UserCompanyTableName} from "../models/definitions/user-company";
import {UserModelTable} from "../models/definitions/user";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(UserCompanyTableName, UserCompanyModelDefinition);

    const users: any[] =
      await queryInterface.sequelize.query(`SELECT id, companies FROM "${UserModelTable}"`, {type: QueryTypes.SELECT,});

    const userCompanies: any[] = [];

    users.forEach(user => {
      if ((user).companies && user.companies.length > 0) {
        user.companies.forEach((companyId: string) => {
          userCompanies.push({
            userId: user.id,
            companyId: companyId,
          });
        });
      }
    });

    if (userCompanies.length)
      await queryInterface.bulkInsert(UserCompanyTableName, userCompanies, {});

  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(UserCompanyTableName)
  }
};