import {QueryInterface} from "sequelize";
import {UserLogDefinition, UserLogTableName} from "../models/definitions/user-log";
import {MissionParticipantsDefinition, MissionParticipantsTableName} from "../models/definitions/mission-participants";

module.exports = {
  async up(queryInterface: QueryInterface){
    await queryInterface.createTable(MissionParticipantsTableName, MissionParticipantsDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(MissionParticipantsTableName);
  }
};