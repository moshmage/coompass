import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionTableName} from "../models/definitions/mission";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize){
    const attributes = {
      when: {
        type: DataTypes.DATE,
      },
      where: {
        type: DataTypes.STRING,
        defaultValue: () => ""
      }
    }

    await queryInterface.addColumn(MissionTableName, "when_start", attributes.when);
    await queryInterface.addColumn(MissionTableName, "when_finish", attributes.when);
    await queryInterface.addColumn(MissionTableName, "location", attributes.where);

  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(MissionTableName, "when_start");
    await queryInterface.removeColumn(MissionTableName, "when_finish");
    await queryInterface.removeColumn(MissionTableName, "location");
  }
};