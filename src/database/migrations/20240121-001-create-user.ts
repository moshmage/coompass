import {UserModelDefinition, UserModelTable} from "../models/definitions/user";
import {QueryInterface} from "sequelize";

module.exports = {
  async up(queryInterface: QueryInterface){
    // Add your migration commands here
    await queryInterface.createTable(UserModelTable, UserModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    // Add commands to revert the changes here
    await queryInterface.dropTable(UserModelTable);
  }
};