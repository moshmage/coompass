import {QueryInterface} from "sequelize";
import {UserLogDefinition, UserLogTableName} from "../models/definitions/user-log";

module.exports = {
  async up(queryInterface: QueryInterface){
    await queryInterface.createTable(UserLogTableName, UserLogDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(UserLogTableName);
  }
};