import {QueryInterface} from "sequelize";
import {SendGridLogModelDefinition, SendGridLogTableName} from "../models/definitions/sendgrid-log";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(SendGridLogTableName, SendGridLogModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(SendGridLogTableName);
  }
};