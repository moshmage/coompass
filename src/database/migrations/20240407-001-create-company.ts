import {QueryInterface} from "sequelize";
import {CompanyModelDefinition, CompanyTableName} from "../models/definitions/company";

module.exports = {
  async up(queryInterface: QueryInterface){
    await queryInterface.createTable(CompanyTableName, CompanyModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(CompanyTableName);
  }
};