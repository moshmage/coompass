import {MissionLogModelDefinition, MissionLogTableName} from "../models/definitions/mission-log";
import {QueryInterface} from "sequelize";

module.exports = {
  async up(queryInterface: QueryInterface){
    // Add your migration commands here
    await queryInterface.createTable(MissionLogTableName, MissionLogModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    // Add commands to revert the changes here
    await queryInterface.dropTable(MissionLogTableName);
  }
};