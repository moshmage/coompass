import {DataTypes, QueryInterface} from "sequelize";
import {CompanyTableName} from "../models/definitions/company";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.addColumn(CompanyTableName, "location", {type: DataTypes.STRING, defaultValue: ""});
    await queryInterface.sequelize.query(`UPDATE ${CompanyTableName} SET location = '' WHERE location IS NULL OR location = '';`);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(CompanyTableName, "location")
  }
};