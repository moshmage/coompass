import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionTableName} from "../models/definitions/mission";
import {MissionParticipantsTableName} from "../models/definitions/mission-participants";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize){
    const attributes = {
      type: DataTypes.ARRAY(DataTypes.STRING),
      defaultValue:() => []
    }

    await queryInterface.addColumn(MissionTableName, "skills", attributes);


  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(MissionTableName, "skills");
  }
};