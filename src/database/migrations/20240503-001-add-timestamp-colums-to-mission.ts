import {DataTypes, QueryInterface} from "sequelize";
import {MissionTableName} from "../models/definitions/mission";
import {sequelize} from "../sequelize";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize){
    const createdAt = {
      type: DataTypes.DATE,
      defaultValue:() => sqlz.literal("CURRENT_TIMESTAMP")
    }

    const date = {type: DataTypes.DATE};

    const description = await queryInterface.describeTable(MissionTableName)

    if (!("createdAt" in description))
      await queryInterface.addColumn(MissionTableName, "createdAt", createdAt);

    if (!("startedAt" in description))
      await queryInterface.addColumn(MissionTableName, "startedAt", date);

    if (!("finishedAt" in description))
      await queryInterface.addColumn(MissionTableName, "finishedAt", date);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(MissionTableName, "createdAt");
    await queryInterface.removeColumn(MissionTableName, "startedAt");
    await queryInterface.removeColumn(MissionTableName, "finishedAt");
  }
};