import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionTableName} from "../models/definitions/mission";

module.exports = {
  async up(queryInterface: QueryInterface){
    const attributes = {
      type: DataTypes.STRING,
      defaultValue:() => ""
    }

    const description = await queryInterface.describeTable(MissionTableName);

    if (!("pointOfContact" in description))
      await queryInterface.addColumn(MissionTableName, "pointOfContact", attributes);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(MissionTableName, "pointOfContact");
  }
};