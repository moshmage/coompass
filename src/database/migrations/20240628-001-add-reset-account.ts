import {QueryInterface} from "sequelize";
import {ResetAccountModelDefinition, ResetAccountTableName} from "../models/definitions/reset-account";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.createTable(ResetAccountTableName, ResetAccountModelDefinition);
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(ResetAccountTableName)
  }
};