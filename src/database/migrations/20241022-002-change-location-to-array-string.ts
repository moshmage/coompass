import {DataTypes, QueryInterface} from "sequelize";
import {CompanyTableName} from "../models/definitions/company";

module.exports = {
  async up(queryInterface: QueryInterface) {
    await queryInterface.addColumn(CompanyTableName, 'location_tmp', {
      type: DataTypes.ARRAY(DataTypes.INTEGER),
      allowNull: false,
      defaultValue: []
    });

    await queryInterface.removeColumn(CompanyTableName, 'location');

    await queryInterface.renameColumn(CompanyTableName, 'location_tmp', 'location');
  },
};