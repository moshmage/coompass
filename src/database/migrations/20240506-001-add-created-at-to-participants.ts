import {DataTypes, QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionTableName} from "../models/definitions/mission";
import {MissionParticipantsTableName} from "../models/definitions/mission-participants";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize){
    const createdAt = {
      type: DataTypes.DATE,
      defaultValue:() => sqlz.literal("CURRENT_TIMESTAMP")
    }

    const description = await queryInterface.describeTable(MissionParticipantsTableName)

    if (!("createdAt" in description))
      await queryInterface.addColumn(MissionParticipantsTableName, "createdAt", createdAt);

    if (!("updatedAt" in description))
      await queryInterface.addColumn(MissionParticipantsTableName, "updatedAt", createdAt);

  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.removeColumn(MissionParticipantsTableName, "createdAt");
    await queryInterface.removeColumn(MissionParticipantsTableName, "updatedAt");
  }
};