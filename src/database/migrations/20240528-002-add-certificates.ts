import {QueryInterface} from "sequelize";
import {sequelize} from "../sequelize";
import {CertificatesModelDefinition, CertificatesTableName} from "../models/definitions/certificates";

module.exports = {
  async up(queryInterface: QueryInterface, sqlz: typeof sequelize) {
    await queryInterface.createTable(CertificatesTableName, CertificatesModelDefinition)
  },

  down: async (queryInterface: QueryInterface) => {
    await queryInterface.dropTable(CertificatesTableName)
  }
};