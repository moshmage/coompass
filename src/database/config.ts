import {configDotenv} from "dotenv";

configDotenv();

const {
  POSTGRES_USERNAME: username,
  POSTGRES_PASSWORD: password,
  POSTGRES_DB: database,
  POSTGRES_PORT: port,
  POSTGRES_HOST: host,
  POSTGRES_CA_PRIVATE_KEY: key,
  POSTGRES_CA_CERTIFICATE: ca,
  POSTGRES_CA_CLIENT_CERTIFICATE: cert,
  POSTGRES_USE_SSL: useSSL,
} = process.env;

const config = {
  database, username, password,
  dialect: "postgres", host, port: +(port || 5432),
  ... !useSSL ? {} :
    {
      ssl: true,
      dialectOptions: {
        dialect: "postgres",
        ssl: {
          require: true,
          rejectUnauthorized: false,
          key, ca, cert
        },
      }}
}

export default config;

// @ts-ignore
// module.exports = config;