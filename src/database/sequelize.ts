import {Sequelize} from "sequelize";
import * as pg from "pg";
import config from "./config";

const _sequelize = new Sequelize({
  ...config as any,
  dialectModule: pg,
});

export const sequelize = _sequelize;