import {Model} from "sequelize";
import {CompanyDistrictsModelDefinition, CompanyDistrictsTableName} from "./definitions/company-districts";
import {sequelize} from "../sequelize";

class CompanyDistricts extends Model {
    public company_id!: string;
    public district_id!: number;
}

const CompanyDistrictsModel = CompanyDistricts.init(CompanyDistrictsModelDefinition, {sequelize, tableName: CompanyDistrictsTableName, modelName: CompanyDistrictsTableName, timestamps: false})

export {CompanyDistrictsModel as CompanyDistricts};