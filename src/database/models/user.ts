import {Model, NonAttribute} from "sequelize";
import {MissionLog} from "./mission-log";
import {HourLog} from "./hour-log";
//@ts-ignore
import {UserTypes} from "../../types/user/user";
import {sequelize} from "../sequelize";
import {UserModelDefinition, UserModelTable} from "./definitions/user";


class User extends Model {
  public id!: string;
  public signature!: string;
  public key?: string;
  public address?: string;
  public email!: string;
  public type!: 'user' | 'ngo' | 'company' | 'admin';
  public username?: string;
  public mobilePhone?: string;
  public profileImage?: string;
  public bio?: string;
  public role?: keyof typeof UserTypes;
  public companies!: string[];

  public skills!: string[];

  public readonly createdAt!: Date;
  public readonly lastLogin!: Date;

  public readonly missionLogs?: NonAttribute<typeof MissionLog[]>;
  public readonly hours?: NonAttribute<typeof HourLog[]>;
}

const UserModel = User.init(UserModelDefinition, {sequelize, modelName: UserModelTable, tableName: UserModelTable, timestamps: false});

export { UserModel as User };