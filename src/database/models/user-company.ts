import {Model} from "sequelize";
import {UserCompanyModelDefinition, UserCompanyTableName} from "./definitions/user-company";
import {sequelize} from "../sequelize";

class UserCompanyModel extends Model {}

UserCompanyModel.init(UserCompanyModelDefinition, {
  sequelize,
  modelName: UserCompanyTableName,
  tableName: UserCompanyTableName,
  indexes: [
    {
      unique: true,
      fields: ["userId", "companyId"],
    },
  ],
  timestamps: false})

export {UserCompanyModel as UserCompany}