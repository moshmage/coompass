import {Model} from "sequelize";
import {sequelize} from "../sequelize";
import {HourLogModelDefinition, HourLogTableName} from "./definitions/hour-log";

class HourLog extends Model {
  public id!: string;
  public amount!: number;
  public participantId!: string;
  public missionLogId!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

const HourLogModel = HourLog.init(HourLogModelDefinition, {sequelize, modelName: HourLogTableName,});

export { HourLogModel as HourLog };