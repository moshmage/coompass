import {Model} from "sequelize";
import {ResetAccountModelDefinition, ResetAccountTableName} from "./definitions/reset-account";
import {sequelize} from "../sequelize";

class ResetAccount extends Model {
  public email!: string;
  public confirmationKey!: string;
  public expires!: Date;
}

const ResetAccountModel = ResetAccount.init(ResetAccountModelDefinition,{
  sequelize,
  tableName: ResetAccountTableName,
  modelName: ResetAccountTableName,
  timestamps: false,
})

export {ResetAccountModel as ResetAccount}