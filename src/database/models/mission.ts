import {Model, NonAttribute} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionLog} from "./mission-log";
import {MissionModelDefinition, MissionTableName} from "./definitions/mission";
import {Company} from "./company";

class Mission extends Model {
  public id!: string;
  public title!: string;
  public reward!: number;
  public state!: 'started' | 'finished' | 'paused';
  public creator!: string;
  public participants!: string[];
  public companyId!: string;

  public image!: string;
  public description!: string;

  public causes!: string[];
  public skills!: string[];

  public createdAt!: Date;
  public startedAt?: Date;
  public finishedAt?: Date;
  public when_start!: Date;
  public when_finish!: Date;

  public pointOfContact!: string;

  public readonly company!: NonAttribute<typeof Company>;
  public readonly logs?: NonAttribute<typeof MissionLog[]>;
}

const MissionModel = Mission.init(MissionModelDefinition, {sequelize, modelName: MissionTableName, tableName: MissionTableName, timestamps: false});

MissionModel.beforeCreate((instance, options) => {
  instance.createdAt = new Date();
})

export { MissionModel as Mission };