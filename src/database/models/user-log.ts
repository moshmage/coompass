import {Model} from "sequelize";
import {sequelize} from "../sequelize";
import {UserLogDefinition, UserLogTableName} from "./definitions/user-log";

class UserLog extends Model {
  public id!: string;
  public amount?: number;
  public userId!: string;
  public missionId?: string;
  public action!: 'start-mission' | 'login' | 'register' | "joined";
}

const UserLogModel = UserLog.init(UserLogDefinition, {sequelize, modelName: UserLogTableName, tableName: UserLogTableName, timestamps: true });

export {UserLogModel as UserLog}