import {Model} from "sequelize";
import {DistrictModelDefinition, DistrictsTableName} from "./definitions/districts";
import {sequelize} from "../sequelize";

class Districts extends Model {
    public id!: string;
    public pt!: string;
    public en!: string;
}

const DistrictModel = Districts.init(DistrictModelDefinition, {sequelize, modelName: DistrictsTableName, timestamps: false});

export {DistrictModel as Districts};