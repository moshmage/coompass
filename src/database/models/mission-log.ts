import {Model} from "sequelize";
import {HourLog} from "./hour-log";
import {sequelize} from "../sequelize";
import {MissionLogModelDefinition, MissionLogTableName} from "./definitions/mission-log";

class MissionLog extends Model {
  public id!: string;
  public action!: 'start' | 'finished' | 'hour-input';
  public hourLog?:  typeof HourLog;
  public userId!: string;
  public missionId!: string;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

const MissionLogModel = MissionLog.init(MissionLogModelDefinition, {sequelize, modelName: MissionLogTableName,});

export { MissionLogModel as MissionLog };