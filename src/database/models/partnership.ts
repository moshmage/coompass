import {Model} from "sequelize";
import {PartnershipsModelDefinition, PartnershipsTableName} from "./definitions/partnerships";
import {sequelize} from "../sequelize";

class Partnership extends Model {
  public companyId!: string;
  public partnerOrgId!: string;
}

const PartnershipModel = Partnership.init(PartnershipsModelDefinition, {sequelize, modelName: PartnershipsTableName, tableName: PartnershipsTableName, timestamps: false});

export {PartnershipModel as Partnership};