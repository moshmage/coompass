import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const HourLogTableName = "HourLog"

export const HourLogModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  amount: {
    type: DataTypes.INTEGER,
  },
  participantId: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  missionLogId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
};