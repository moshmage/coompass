import {DataTypes} from "sequelize";

export const DistrictsTableName = "Districts";
export const DistrictModelDefinition = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    pt: {
        type: DataTypes.STRING,
        allowNull: false
    },
    en: {
        type: DataTypes.STRING,
        allowNull: false
    }
}