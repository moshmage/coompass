import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const CompanyTableName = "company";

export const CompanyModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  creator: {
    type: DataTypes.STRING(18),
  },
  name: {
    type: DataTypes.STRING,
  },
  description: {
    type: DataTypes.TEXT,
  },
  image: {
    type: DataTypes.TEXT
  },
  employees: {
    type: DataTypes.ARRAY(DataTypes.STRING(18)),
    defaultValue: () => []
  },
  type: {
    type: DataTypes.STRING,
    defaultValue: () => ""
  },
  sdg: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue: () => []
  },
  website: {
    type: DataTypes.STRING
  },
  xcom: {
    type: DataTypes.STRING
  },
  linkedin: {
    type: DataTypes.STRING
  },
  facebook: {
    type: DataTypes.STRING
  },
  youtube: {
    type: DataTypes.STRING
  },
  instagram: {
    type: DataTypes.STRING
  },
}