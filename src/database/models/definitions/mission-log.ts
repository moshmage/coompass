import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const MissionLogTableName = "MissionLog";

export const MissionLogModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  action: {
    type: DataTypes.ENUM('start', 'finished', 'hour-input'),
    allowNull: false,
  },
  userId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
  missionId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
}
