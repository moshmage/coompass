import {DataTypes} from "sequelize";
import uniqid from "uniqid";
import {sequelize} from "../../sequelize";
export const MissionTableName = "Mission"
export const MissionModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  reward: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  state: {
    type: DataTypes.ENUM('started', 'finished', 'paused'),
    allowNull: false,
  },
  creator: {
    type: DataTypes.STRING(18),
  },
  companyId: {
    type: DataTypes.STRING(18),
  },
  description: {
    type: DataTypes.TEXT,
  },
  image: {
    type: DataTypes.TEXT
  },
  participants: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue: () => []
  },
  causes: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue: () => []
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: () => sequelize.literal('CURRENT_TIMESTAMP')
  },
  startedAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  finishedAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  pointOfContact: {
    type: DataTypes.STRING,
    defaultValue: () => ""
  },
  skills: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue:() => []
  },
  requirements: {
    type: DataTypes.TEXT,
    defaultValue: () => ""
  },
  when_start: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  when_finish: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  location: {
    type: DataTypes.STRING,
    defaultValue: () => ""
  },
}