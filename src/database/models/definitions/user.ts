import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const UserModelTable = "User"

export const UserModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  signature: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  key: {
    type: DataTypes.STRING,
  },
  address: {
    type: DataTypes.STRING(42),
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  type: {
    type: DataTypes.ENUM('user', 'ngo', 'company'),
  },
  username: {
    type: DataTypes.STRING,
  },
  mobilePhone: {
    type: DataTypes.STRING(15),
  },
  profileImage: {
    type: DataTypes.TEXT,
  },
  bio: {
    type: DataTypes.TEXT,
  },
  role: {
    type: DataTypes.ARRAY(DataTypes.ENUM('user', 'ngo', 'admin')),
    defaultValue: () => ["user"]
  },
  companies: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue: () => []
  },
  skills: {
    type: DataTypes.ARRAY(DataTypes.STRING),
    defaultValue: () => []
  },
  createdAt: {
    type: DataTypes.DATE,
  },
  lastLogin: {
    type: DataTypes.DATE,
  }
}