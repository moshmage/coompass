import {DataTypes} from "sequelize";

export const UserCompanyTableName = "UserCompany"

export const UserCompanyModelDefinition = {
  userId: {
    type: DataTypes.STRING(18),
    references: {
      model: "User",
      key: 'id'
    }
  },
  companyId: {
    type: DataTypes.STRING(18),
    references: {
      model: "Company",
      key: 'id',
    }
  },
  pending: {
    type: DataTypes.BOOLEAN,
  },
  accepted: {
    type: DataTypes.BOOLEAN,
  }
}