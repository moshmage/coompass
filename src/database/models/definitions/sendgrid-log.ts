import {DataTypes} from "sequelize";
import {v4 as uuidv4} from "uuid";

export const SendGridLogTableName = "Sendgrid";
export const SendGridLogModelDefinition = {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true,
        defaultValue: () => uuidv4(),
    },
    event: {
        type: DataTypes.STRING,
    }
}