import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const CalendarTableName = "Calendar";

export const CalendarModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  userId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
  entryType: {
    type: DataTypes.ENUM("mission-created", "mission-start", "mission-end", "mission-started", "mission-ended"),
    allowNull: false,
  },
  date: {
    type: DataTypes.DATE,
  },
  missionId: {
    type: DataTypes.STRING
  },
}