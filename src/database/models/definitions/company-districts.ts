import {DataTypes} from "sequelize";

export const CompanyDistrictsTableName = "CompanyDistricts";
export const CompanyDistrictsModelDefinition = {
    companyId: {
        type: DataTypes.STRING(18),
        allowNull: false
    },
    districtId: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}