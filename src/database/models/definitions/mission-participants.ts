import {DataTypes} from "sequelize";
import uniqid from "uniqid";
import {sequelize} from "../../sequelize";

export const MissionParticipantsTableName = "MissionParticipants";

export const MissionParticipantsDefinition = {
  id: {
    type: DataTypes.STRING(18),
    defaultValue: () => uniqid(),
    primaryKey: true,
  },
  missionId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
  userId: {
    type: DataTypes.STRING(18),
    allowNull: false,
  },
  state: {
    type: DataTypes.ENUM('accepted', 'rejected', 'pending'),
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: () => sequelize.literal('CURRENT_TIMESTAMP')
  },
  updatedAt: {
    type: DataTypes.DATE,
  }
}