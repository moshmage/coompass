import {DataTypes} from "sequelize";
import {v4 as uuidv4} from 'uuid';

export const ResetAccountTableName = "ResetAccount";
export const ResetAccountModelDefinition = {
  confirmationKey: {
    type: DataTypes.STRING(36),
    primaryKey: true,
    defaultValue: () => uuidv4(),
  },
  email: {
    type: DataTypes.STRING,
  },
  expires: {
    type: DataTypes.DATE,
  }
}