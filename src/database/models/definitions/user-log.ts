import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const UserLogTableName = "UserLog";

export const UserLogDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  amount: {
    type: DataTypes.INTEGER,
  },
  userId: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  missionId: {
    type: DataTypes.STRING(18),
  },
  action: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE
  },
  updatedAt: {
    type: DataTypes.DATE
  },
};