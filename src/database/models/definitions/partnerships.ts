import {DataTypes} from "sequelize";

export const PartnershipsTableName = "Partnerships";
export const PartnershipsModelDefinition = {
  companyId: {
    type: DataTypes.STRING(18),
  },
  partnerOrgId: {
    type: DataTypes.STRING(18),
  }
}