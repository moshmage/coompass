import {DataTypes} from "sequelize";
import uniqid from "uniqid";

export const CertificatesTableName = "Certificates";
export const CertificatesModelDefinition = {
  id: {
    type: DataTypes.STRING(18),
    primaryKey: true,
    defaultValue: () => uniqid(),
  },
  createdDate: {
    type: DataTypes.DATE,
  },
  collaborators: {
    type: DataTypes.INTEGER,
  },
  hoursProvided: {
    type: DataTypes.INTEGER,
  },
  initiatives: {
    type: DataTypes.INTEGER,
  },
  causes: {
    type: DataTypes.ARRAY(DataTypes.STRING)
  },
  missions: {
    type: DataTypes.ARRAY(DataTypes.STRING)
  },
  txHash: {
    type: DataTypes.STRING,
  },
  companyId: {
    type: DataTypes.STRING
  },
  tokenId: {
    type: DataTypes.STRING
  },
  imageCid: {
    type: DataTypes.STRING,
  },
  metadataCid: {
    type: DataTypes.STRING
  }
}