import {Company} from "./company";
import {Mission} from "./mission";
import {MissionParticipants} from "./mission-participants";
import {User} from "./user";
import {UserLog} from "./user-log";
import {Calendar} from "./calendar";
import {Certificate} from "./certificates";
import {UserCompany} from "./user-company";
import {ResetAccount} from "./reset-account";
import {SendgridLog} from "./sendgrid-log";
import {Districts} from "./districts";
import {CompanyDistricts} from "./company-districts";
import {Partnership} from "./partnership";

User.hasMany(UserLog, {foreignKey: 'userId', sourceKey: "id"});
User.hasMany(Calendar, {foreignKey: "userId", sourceKey: "id"});
User.belongsToMany(Company, { through: UserCompany, foreignKey: 'userId', as: "associatedCompanies" });
User.belongsToMany(Mission, { through: MissionParticipants, sourceKey: "id", foreignKey: "userId", as: "missions"});

UserLog.hasOne(User, {foreignKey: "id", sourceKey: "userId", as: "user"});
UserLog.hasOne(Mission, {foreignKey: "id", sourceKey: "missionId", as: "mission"});

Company.hasMany(Mission, {foreignKey: 'companyId', sourceKey: "id", as: "companyMissions"});
Company.belongsToMany(User, { through: UserCompany, foreignKey: 'companyId', as: "associatedUsers" });
Company.hasOne(User, {foreignKey: "id", sourceKey: "creator", as: "companyCreator"})
Company.belongsToMany(Districts, { through: CompanyDistricts, foreignKey: 'companyId', as: "districts" });
Company.belongsToMany(Company, {through: Partnership, foreignKey: 'companyId', as: 'partnerOrgs', scope: { type: 'ngo'}});
Company.belongsToMany(Company, {through: Partnership, foreignKey: 'partnerOrgId', as: 'partnerCompanies', scope: { type: 'company'},});

Mission.hasMany(MissionParticipants, {foreignKey: 'missionId', as: 'participantsState'});
Mission.hasMany(Calendar, {foreignKey: "missionId", sourceKey: "id"});
Mission.hasMany(UserLog, {foreignKey: "missionId", sourceKey: "id"});
Mission.hasOne(Company, {foreignKey: 'id', sourceKey: "companyId"});
Mission.belongsToMany(User, {through: MissionParticipants, foreignKey: "missionId"});

MissionParticipants.hasOne(User, {foreignKey: "id", sourceKey: "userId", as: "user"});
MissionParticipants.hasOne(Mission, {foreignKey: "id", sourceKey: "missionId", as: "mission"});

Calendar.hasOne(Mission, {foreignKey: "id", sourceKey: "missionId", as: "mission"});

Certificate.hasOne(Company, {foreignKey: "id", sourceKey: "companyId"});

ResetAccount.hasOne(User, {foreignKey: "email", sourceKey: "email"})

Districts.belongsToMany(Company, {through: CompanyDistricts, foreignKey: "districtId", as: "companies"});

export {Company, Mission, MissionParticipants, User, Calendar, UserLog, Certificate, ResetAccount, SendgridLog, Districts, Partnership, UserCompany}