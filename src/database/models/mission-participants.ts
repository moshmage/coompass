import {Model} from "sequelize";
import {sequelize} from "../sequelize";
import {MissionParticipantsDefinition, MissionParticipantsTableName} from "./definitions/mission-participants";

class MissionParticipants extends Model {
  public id!: string;
  public state!: 'accepted' | 'rejected' | 'pending';
  public userId!: string;
  public missionId!: string;

  public createdAt!: Date;
  public updatedAt!: Date;
}

const MissionParticipantsModel =
  MissionParticipants.init(MissionParticipantsDefinition, {
    sequelize,
    tableName: MissionParticipantsTableName,
    modelName: MissionParticipantsTableName,
    timestamps: false
  });

MissionParticipantsModel.beforeCreate((instance, options) => {
  instance.createdAt = new Date();
})

export { MissionParticipantsModel as MissionParticipants };