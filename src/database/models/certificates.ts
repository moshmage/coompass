import {Model} from "sequelize";
import {sequelize} from "../sequelize";
import {CertificatesModelDefinition, CertificatesTableName} from "./definitions/certificates";

class Certificates extends Model {
  public id!: string;
  public createdDate!: Date;
  public collaborators!: number;
  public hoursProvided!: number;
  public initiatives!: number;
  public causes!: string[];
  public txHash!: string;
  public companyId!: string;

}

const CertificateModel = Certificates.init(CertificatesModelDefinition, {
  sequelize,
  tableName: CertificatesTableName,
  modelName: CertificatesTableName,
  timestamps: false,
})

export {CertificateModel as Certificate}