import {Model} from "sequelize";
import {CalendarModelDefinition, CalendarTableName} from "./definitions/calendar";
import {sequelize} from "../sequelize";

class Calendar extends Model {
  public id!: string;
  public userId!: string;
  public createdAt!: string;
  public updatedAt!: string;
  public entryType!: "mission-start" | "mission-end" | "mission-started" | "mission-ended";
  public missionId!: string;
}

const CalendarModel = Calendar.init(CalendarModelDefinition, {
  sequelize,
  modelName: CalendarTableName,
  tableName: CalendarTableName,
  timestamps: false
});

export {CalendarModel as Calendar}