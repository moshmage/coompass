import {Model} from "sequelize";
import {SendGridLogModelDefinition, SendGridLogTableName} from "./definitions/sendgrid-log";
import {sequelize} from "../sequelize";

class SendgridLog extends Model {
    public id!: string;
    public event!: string;
}

const SendgridLogModel = SendgridLog.init(SendGridLogModelDefinition, {sequelize, modelName: SendGridLogTableName, tableName: SendGridLogTableName, timestamps: false});

export {SendgridLogModel as SendgridLog};