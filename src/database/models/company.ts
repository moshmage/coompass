import {Model, NonAttribute} from "sequelize";
import {CompanyModelDefinition, CompanyTableName} from "./definitions/company";
import {sequelize} from "../sequelize";
import {User} from "./user";

class Company extends Model {
  public id!: string;
  public name!: string;
  public description!: string;
  public creator!: string;
  public employees!: string[];
  public image!: string;
  public type!: "ngo" | "company"
  public sdg!: string[];

  public readonly companyCreator?: NonAttribute<typeof User>;
  public readonly associatedUsers?: NonAttribute<typeof User[]>;
  public readonly partnerOrgs?: NonAttribute<Company[]>;
  public readonly partnerCompanies?: NonAttribute<Company[]>;
}

const CompanyModel = Company.init(CompanyModelDefinition, {sequelize, modelName: CompanyTableName, tableName: CompanyTableName, timestamps: false});

export { CompanyModel as Company }