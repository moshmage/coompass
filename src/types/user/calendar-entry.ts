import {Mission} from "@/types/mission/mission";

export type CalendarEntry = {
  id: string;
  userId: string;
  entryType: "mission-start" | "mission-end" | "mission-started" | "mission-ended";
  missionId: string;
  mission: Mission;
  date: Date;
}