//@ts-ignore
import {HourLog, Mission, MissionLog} from "@/types/mission/mission";
//@ts-ignore
import {Company} from "@/types/company/company";

export enum UserTypes {
  user=  "user",
  ngo = "ngo",
  company = "company",
  admin = "admin",
  org = "organization"
}

export type User = {
  id: string;
  username: string;
  email: string;
  mobilePhone?: string;
  profileImage?: string;
  bio?: string;
  type: UserTypes;
  missionLogs?: MissionLog[];
  missions?: Mission[];
  hours?: HourLog[];
  role: UserTypes[];
  companies: string[];
  skills: string[];
  createdAt: Date;
  lastLogin: Date;
  associatedCompanies: Company[];
}

export type TokenizedUser = User & {  };