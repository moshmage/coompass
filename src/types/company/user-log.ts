import {User} from "@/types/user/user";
import {Mission} from "@/types/mission/mission";

export type UserLog = {
  createdAt: Date,
  user: User,
  action: string;
  missionId: string;
  mission?: Mission;
}