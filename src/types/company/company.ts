import {Mission} from "@/types/mission/mission";
import {District} from "@/types/districts/district";

export type Company = {
  id: string;
  name: string;
  description: string;
  image: string;
  sdg: string[];
  website?: string;
  xcom?: string;
  linkedin?: string;
  facebook?: string;
  youtube?: string;
  instagram?: string;
  companyMissions?: Mission[]
  creator: string;
  districts: District[]
  partnerOrgs: Company[];
  partnerCompanies: Company[];
  UserCompany?: {pending: boolean, accepted: boolean};
  employees: string[];
}