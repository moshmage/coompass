export type District = {
    id: number;
    pt: string;
    en: string;
}