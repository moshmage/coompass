export type AllowedActionTypes =
  "start-mission"
  | "login"
  | "register"
  | "joined_company"
  | "joined_mission"
  | "was_rejected"
  | "pending_join_mission"
  | "reset_account"
  | "created_mission"
  | "pending_join_company"
  | "accepted_join_company"
  | "denied_join_company"
  ;