import {ReactNode} from "react";

export type TableRow = { [columnName: string]: string | ReactNode; };
export type TableColumns = string[];
export type TableRows = TableRow[];