import {NextRequest, NextResponse} from "next/server";

export type PageMiddleware = (request: NextRequest) => Promise<NextResponse>