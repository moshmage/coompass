import type {Session} from "@auth/core/types";
import type {TokenizedUser} from "@/types/user/user";

export type CustomSession = Session & {user?: TokenizedUser}