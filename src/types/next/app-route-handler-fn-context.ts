export type AppRouteHandlerFnContext = {
  params?: Record<string, string | string[]>
}