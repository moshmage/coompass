import {PageMiddleware} from "@/types/next/page-middleware";

export type CustomMiddleware = {[page: string]: PageMiddleware[]}