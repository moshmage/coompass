import {Company} from "@/types/company/company";

export type CertificateData = {
  collaborators: number,
  hoursProvided: number,
  initiatives: number,
  causes: string[],
  missions: string[],
  createdDate: Date;
  companyId: string;
  Company: Company;
  sdgs: string[];
}