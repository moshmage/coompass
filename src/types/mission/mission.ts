import {Company} from "@/types/company/company";

export enum MissionState {
  started = "started",
  finished = "finished",
  paused = "paused"
}

export enum MissionLogAction {
  start = "start",
  finished = "finished",
  "hour-input" = "hour-input"
}

export type HourLog = {
  id: string;
  amount: number;
  participantId: number;
  missionLogId: string;
}

export type MissionLog = {
  id: string;
  action: MissionLogAction;
  userId: string;
  hourLog?: HourLog;
  missionId: string;
}

export type ParticipantsState = {
  missionId: string;
  userId: string;
  state: "accepted" | "rejected" | "pending";
  user: any;
  createdAt: Date;
  updatedAt: Date;
  mission: Mission
}

export type Mission = {
  id: string;
  title: string;
  reward: number;
  state: MissionState;
  creator: string;
  participants: string[];
  logs: MissionLog[];
  description: string;
  image: string;
  company: Company;
  causes: string[];
  skills: string[];
  participantsState?: ParticipantsState[];
  createdAt: Date;
  startedAt: Date;
  finishedAt: Date;
  requirements: string;
  location: string;
  when_start: Date;
  when_finish: Date;
}