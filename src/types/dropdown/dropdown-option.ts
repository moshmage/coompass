export type DropdownOption = {
  title: string;
  active?: boolean;
  value?: string|number;
};