import {createInstance} from 'i18next';
import resourcesToBackend from 'i18next-resources-to-backend';
import {initReactI18next} from 'react-i18next/initReactI18next';
import {FALLBACK_LOCALE, getOptions, LANGUAGE_COOKIE, Locales} from './settings';
import {cookies} from 'next/headers';

async function initI18next(lang: Locales) {
  const i18nInstance = createInstance();
  await i18nInstance
    .use(initReactI18next)
    .use(
      resourcesToBackend(
        (lang: string,) => import(`./locales/${lang}/translation.json`),
      ),
    )
    .init(getOptions(lang));

  return i18nInstance;
}

export async function createTranslation() {
  const lang = await getLocale();
  const i18nextInstance = await initI18next(lang);

  return {
    t: i18nextInstance.getFixedT(lang),
  };
}

export async function getLocale() {
  return (cookies().get(LANGUAGE_COOKIE)?.value ?? FALLBACK_LOCALE) as Locales;
}

