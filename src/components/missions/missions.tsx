"use client";

import {SearchBar} from "@/components/search/search-bar/search-bar";
import {Grid, LinearProgress} from "@mui/material";
import {MissionCard} from "@/components/mission/mission-card/mission-card";
import {useEffect, useState} from "react";
import {Mission} from "@/types/mission/mission";

import searchStyles from "@/components/search/search.module.css";
import {useSelf} from "@/components/hooks/use-self";
import {useMissions} from "@/components/hooks/queries/use-missions";
import {useTranslation} from "@/i18n/client";
import Structural from "@/components/structural/structural";

export function Missions() {
  const [availableMissions, setAvailableMissions] = useState<Mission[]>([]);
  const {userData} = useSelf();
  const {t} = useTranslation();

  const search = (title: string, location: string, causes: string[], skills: string[], virtual: boolean, inPerson: boolean) => {
    if (!title && !location && !causes.length && !skills.length && !virtual && !inPerson)
      setAvailableMissions(missions);
    else {

      const hasHit = ({title: t, skills: s, causes: c, location: l}: Mission) => {
        const hit = [!title, !skills.length, !causes.length, !location, !virtual, !inPerson];

        if (title)
          hit[0] = t.toLowerCase().includes(title!.toLowerCase());
        if (skills?.length)
          hit[1] = skills.every(e => s?.includes(e))
        if (causes?.length)
          hit[2] = causes.every(e => c?.includes(e))
        if (location)
          hit[3] = l?.toLowerCase()?.includes(location.toLowerCase())
        if (virtual)
          hit[4] = l?.toLowerCase()?.includes("virtual")
        if (inPerson)
          hit[5] = !l?.toLowerCase()?.includes("virtual")

        return hit.every(e => e);
      }

      setAvailableMissions(missions.filter(hasHit));
    }
  }

  const {data: missions, isFetching: loading} = useMissions();

  useEffect(() => {
    console.log(missions);
    setAvailableMissions(missions);
  }, [missions]);

  return <>
    <SearchBar onSearch={search}/>
    <Grid container>
      <Grid item xs={12} className={searchStyles.searchResults_subitle}>
        {t('missions.availableOpportunities', {n: availableMissions.length.toLocaleString()})}
      </Grid>
    </Grid>
    <Structural.If condition={loading}>
      <Grid container>
        <Grid item xs={12}>
          {loading ? <Grid item><LinearProgress/></Grid> : null}
        </Grid>
      </Grid>
    </Structural.If>
    <Grid container spacing={3}>

      {(availableMissions).map(({title, image, description, id, location, company, state, participantsState, createdAt }, i: any) =>
        (<Grid key={i} item xs={12} sm={6} md={4} lg={4} xl={3}>
          <MissionCard state={state} participatingState={participantsState?.find(({userId}: any) => userId === userData?.id)?.state}
                       name={title}
                       createdDate={createdAt??new Date()}
                       location={location}
                       missionImage={image}
                       orgName={company?.name}
                       description={description}
                       id={id} />
        </Grid>))}
    </Grid>
  </>
}