"use client";

import styles from "@/components/window/badge-perk/bade-perk.module.css";
import WindowBox from "@/components/window/window-box";

import trophy from "@/assets/trophy.svg"
import {Grid} from "@mui/material";
import {useTranslation} from "@/i18n/client";

export default function BadePerk({missions = 0, pass = 0, merch = 0}) {
  const {t} = useTranslation()
  const totalSum = missions + pass + merch;

  const percentages = [
    [missions, styles.badgePerk_bar_yellow],
    [pass, styles.badgePerk_bar_blue],
    [merch, styles.badgePerk_bar_green]
  ].map(([v, _className], i) => {
    return <div key={i} className={`${styles.badgePerk_bar} ${_className}`} style={{width: `${(+v / totalSum) * 100}%`}}></div>
  });

  const legend = [
    [missions, styles.badgePerk_bar_yellow, t('common.mission')],
    [pass, styles.badgePerk_bar_blue, t('dashboard.user.score.pass')],
    [merch, styles.badgePerk_bar_green, t('dashboard.user.score.merch')]
  ].map(([v, _className, text], i) => {
    return <Grid container key={i} style={{display: "flex", alignItems: "center"}}>
      <Grid item xs={12} sm={3} md={12} lg={12} xl={3} className={`${styles.badgePerk_bar_small} ${_className}`}></Grid>
      <Grid item xs={12} sm={3} md={12} lg={12} xl={3} className={`${styles.badgePerk_legend_typo} ${styles.badgePerk_legend_amount}`}>{v}</Grid>
      <Grid item xs={12} sm={6} md={12} lg={12} xl={6} className={`${styles.badgePerk_legend_typo} ${styles.badgePerk_legend_span}`}>{text}</Grid>
    </Grid>
  })


  return <WindowBox comingSoon icon={trophy} title={t('dashboard.user.badges.badgeAndPerks')} actionLabel={t('dashboard.user.badges.thisYear')}>
    <div className={styles.badgePerk_wrapper}>
      <div style={{marginBottom: 22}}>
        <span className={styles.badgePerk_title_amount}>{totalSum}</span>
        <span className={styles.badgePerk_title_span}>{t('dashboard.user.badges.awardsInTotal')}</span></div>
      <div style={{display: "flex", gap: 6, marginBottom: 20}}>{percentages}</div>
      <Grid container>
        <Grid item></Grid>
        <Grid item xs={12} sm={12} md={2}></Grid>
        <Grid item xs={12} sm={12} md={3}></Grid>
      </Grid>
      <div style={{display: "flex", justifyContent: "start", gap: 10}}>{legend}</div>
    </div>
  </WindowBox>
}