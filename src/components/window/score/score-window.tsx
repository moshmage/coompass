"use client";
import WindowBox from "@/components/window/window-box";
import Gauge from "@/components/gauge/gauge";

import star from "@/assets/star.svg";

import styles from "@/components/window/score/score.module.css";
import {useTranslation} from "@/i18n/client";

export default function ScoreWindow({earned = 0, level = 0, gaugeValue = 0}) {
  const {t} = useTranslation();
  return <WindowBox comingSoon={true} icon={star} title={t('dashboard.user.score.currentScore')} actionLabel={t('dashboard.user.score.claimPrize')} primaryAction={true}>
    <div className={`row ${styles.score_wrapper}`}>
      <div className="column no-grow">
        <Gauge value={gaugeValue} />
      </div>
      <div className="column">
        <div>
          <div className={styles.score_small_title}>{t('dashboard.user.score.impactLevel')}</div>
          <div className={`${styles.score_box} ${styles.score_box_primary}`}>
            <span>{t('dashboard.user.score.level', {level})}</span>
            <span>{t('dashboard.user.score.levels.basic')}</span>
          </div>
        </div>
        <div className={styles.score_box}>
          <span>{t('dashboard.user.score.pass')}</span>
          <strong>${earned}</strong>
        </div>
      </div>
    </div>
  </WindowBox>
}