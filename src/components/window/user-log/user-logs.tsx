"use client";
import WindowBox from "@/components/window/window-box";

import note from "@/assets/note.svg";
import {LogRow} from "@/components/window/user-log/log-row";

import style from "@/components/window/user-log/logs.module.css"
import {useUserLogs} from "@/components/hooks/queries/use-user-logs";
import {LinearProgress} from "@mui/material";
import {NoData} from "@/components/no-data/no-data";
import {useSelf} from "@/components/hooks/use-self";
import {useTranslation} from "@/i18n/client";

export function UserLogs({ companyId }: {companyId: string,}) {
  const {t} = useTranslation()
  const {userData} = useSelf();
  const {data: logs, isFetching: loading} = useUserLogs(companyId);

  return <WindowBox icon={note} title={t('user.logs.latestActivity')}>
    <div className={style.logs_wrapper}>
      {!logs.length && !loading ? <NoData/> : null}
      {
        loading ? <LinearProgress/> : null
      }
      {logs.map((log, i) => <LogRow key={i} log={log} self={userData} />)}
    </div>
  </WindowBox>
}