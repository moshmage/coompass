import {formatDistanceToNow} from "date-fns";
import style from "@/components/window/user-log/logs.module.css"
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {UserLog} from "@/types/company/user-log";
import {User} from "@/types/user/user";
import {useTranslation} from "@/i18n/client";
import Structural from "@/components/structural/structural";
import Link from "next/link";

export function LogRow({log = {} as UserLog, self = {} as User|null}) {
  const {t} = useTranslation();
  const logAction = () =>
    log.action === "joined"
      ? !log.missionId
        ? "joined_company"
        : "joined_mission"
      : log.action

  return <div className={style.log_wrapper}>
    <Base64Image className={style.log_image} src={log.user.profileImage || self?.id === log.user.id && self.associatedCompanies?.[0]?.image || ""}/>
    <div className={style.log_name}>{log.user.username || self?.id === log.user.id && t('user.logs.you') || t('user.logs.noUsername')}</div>
    <div className={style.log_action}>
      {t(`user.logs.action.${logAction()}`, {mission: log.mission?.title || "unknown mission"})}
    </div>
    <Structural.If condition={!!log.mission?.title && !!log.missionId}>
      <Link className={style.log_href} href={`/missions/${log.missionId}`} target="_blank">&nbsp; &#8599;</Link>
    </Structural.If>
    <div className={style.log_date}><span>{t('common.dates.distance', {distance: formatDistanceToNow(log.createdAt)})}</span></div>
  </div>
}