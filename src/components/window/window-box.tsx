import styles from "@/components/window/window.module.scss";
import Image from "next/image";
import {ReactNode} from "react";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Structural from "@/components/structural/structural";


type WindowBoxProps = {
  title?: string|ReactNode;
  icon?: string;
  actionLabel?: string;
  primaryAction?: boolean;
  children: React.ReactNode;
  style?: any;
  comingSoon?: boolean;
  action?: ReactNode
}

export default function WindowBox({
                                    title = "",
                                    icon = "",
                                    actionLabel = "",
                                    action,
                                    primaryAction = false,
                                    children,
                                    style = {},
                                    comingSoon = false
                                  }: WindowBoxProps) {
  return <div className={styles.window_wrapper} style={{...style, ... comingSoon ? {position: "relative"} : {}}}>
      <Structural.If condition={comingSoon}>
          <div style={{height: "100%", width: "100%", backgroundColor: "rgba(0,0,0,.6)", position: "absolute", zIndex: 2, top: 0, left: 0, borderRadius: "16px"}}></div>
          <div
              style={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)",
                  backgroundColor: "rgba(39, 39, 42, 1)",
                  padding: "10px",
                  borderRadius: "5px",
                  fontSize: "small",
                  zIndex: 3
              }}
          >
              Coming soon
          </div>
      </Structural.If>

    <div className={styles.window_head}>
      <div style={{display: "flex", alignSelf: "center"}}>
        {icon ? <Image src={icon} alt={"icon"}/> : null}
        <span className={styles.window_title}>{title}</span>
      </div>
      {actionLabel ? <SimpleButton small label={actionLabel} style={primaryAction ? "primary" : "secondary"}/> : null}
      {action ? action : null}
    </div>
    <div className={styles.window_body}>

        {children}
    </div>
  </div>
}