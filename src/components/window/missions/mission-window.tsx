import WindowBox from "@/components/window/window-box";

import rocketLaunch from "@/assets/RocketLaunch.svg";
import MissionRow from "@/components/window/missions/mission-row";

import style from "@/components/window/missions/mission-window.module.css"
import {Mission, ParticipantsState} from "@/types/mission/mission";
import {LinearProgress} from "@mui/material";
import {NoData} from "@/components/no-data/no-data";
import {useTranslation} from "@/i18n/client";

export default function MissionWindow({missions = [] as ParticipantsState[]|Mission[]|any, loading = false}) {
  const {t} = useTranslation()
  return <div>
    <WindowBox icon={rocketLaunch} title={t('organizations.missions')}>
      <div className={style.missionsWrapper}>
        {!missions.length && !loading ? <NoData /> : null}
        {missions.map((entry: any, i: number) =>
          <MissionRow key={i}
                      title={(entry.mission || entry).title}
                      userState={entry.mission ? entry.state : null}
                      state={(entry.mission || entry).state}
                      img={(entry.mission || entry).image}
                      id={(entry.mission || entry).id} />
        )}
        {
          loading ? <LinearProgress/> : null
        }
      </div>
    </WindowBox>
  </div>
}