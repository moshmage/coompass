import styles from "@/components/window/missions/mission-window.module.css";
import Link from "next/link";
import {MissionState} from "@/components/mission/mission-state/mission-state";
import {ParticipatingState} from "@/components/mission/particiapting-state/particiapting-state";

export default function MissionRow({state = "", title = "", img = "", id = "", userState = ""}) {
  return <div className={styles.missionWindow_row}>
    {/*<Base64Image className={styles.missionWindow_row_image} src={img} alt={"mission image"}/>*/}
    <div className={styles.missionWindow_row_title}>
      <Link href={`/missions/${id}`}>{title}</Link>
    </div>
    {userState ? <ParticipatingState state={userState} /> : null}
    <MissionState state={state} />
  </div>
}