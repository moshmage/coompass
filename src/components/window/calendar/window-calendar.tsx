"use client";

import WindowBox from "@/components/window/window-box";
import Calendar from "@/components/calendar/calendar";

import calendarBlank from "@/assets/calendarBlank.svg"
import CalendarCard from "@/components/calendar/calendar-card";
import {useUserCalendar} from "@/components/hooks/queries/use-user-calendar";
import Link from "next/link";
import {useEffect, useState} from "react";
import {isSameDay} from "date-fns";
import {CalendarEntry} from "@/types/user/calendar-entry";
import {NoData} from "@/components/no-data/no-data";
import {CircularProgress} from "@mui/material";
import {useTranslation} from "@/i18n/client";


export default function WindowCalendar() {
  const [selectedDate, setSelectedDate] = useState<Date | null>(null);
  const {data: calendarEntries, isFetching: loading} = useUserCalendar();
  const [entries, setEntries] = useState<CalendarEntry[]>([]);
  const {t} = useTranslation();

  useEffect(() => {
    setEntries(calendarEntries?.filter(({date}) => selectedDate && isSameDay(date, selectedDate)) || [])
  }, [selectedDate])

  return <WindowBox title={t('common.calendar.schedule')} icon={calendarBlank} style={{minHeight: "calc(100vh - 244px)"}}>
    <Calendar onSelectedDate={setSelectedDate}/>
    <div style={{marginTop: 8}}></div>
    <div style={{padding: "16px 0", textAlign: "center", borderBottom: "1px solid rgba(255,255,255,.6)", fontSize: 14, color: "rgba(255,255,255,.6)", marginBottom: 16}}>{t('common.calendar.yourAppointments')}</div>
    {
      loading ? <div style={{display: "flex", justifyContent: "center"}}><CircularProgress/></div> : null
    }
    {
      !entries.length && !loading
        ? <CalendarCard><NoData message={t('common.calendar.noAppointmentsForThisDay')}/></CalendarCard>
        : null
    }
    {
      entries.map(({mission, missionId, entryType}, i) =>
        <CalendarCard key={i}>
          <Link href={`/missions/${missionId}`} style={{fontSize: 14}}><strong>{t(`common.calendar.entryType.${entryType}`)}</strong> {mission.title}</Link>
        </CalendarCard>
      )
    }
  </WindowBox>
}