"use client";

import WindowBox from "@/components/window/window-box";

import note from "@/assets/note.svg"
import LineGraph from "@/components/line-graph/line-graph";
import {addDays, differenceInDays, format, isAfter, subDays, subMonths, subWeeks, subYears,} from "date-fns";
import {Grid} from "@mui/material";

import clock from "@/assets/clock.svg"
import Image from "next/image";

import style from "@/components/window/work-graph/work-graph.module.css"
import {SwitcherButton} from "@/components/switcher/switcher-button";
import {useEffect, useState} from "react";
import {useUserMissions} from "@/components/hooks/queries/use-user-missions";
import {useTranslation} from "@/i18n/client";

export default function WorkGraph() {
  const {t} = useTranslation()
  const [data, setData] = useState<number[]>([]);
  const [labels, setLabels] = useState<string[]>([]);
  const [hours, setHours] = useState<number>();

  const options = [
    {label: t('dashboard.user.workGraph.days', {n:5})},
    {label: t('dashboard.user.workGraph.weeks', {n: 2})},
    {label: t('dashboard.user.workGraph.months', {n: 1})},
    {label: t('dashboard.user.workGraph.months', {n: 6})},
    {label: t('dashboard.user.workGraph.years', {n: 1})},
  ]

  const onOptionToggle = (label: string) => {
    let date = new Date();

    if (label === t('dashboard.user.workGraph.days', {n:5}))
      date = subDays(date, 5);
    if (label === t('dashboard.user.workGraph.weeks', {n: 2}))
      date = subWeeks(date, 2);
    if (label === t('dashboard.user.workGraph.months', {n: 1}))
      date = subMonths(date, 1);
    if (label === t('dashboard.user.workGraph.months', {n: 6}))
      date = subMonths(date, 6);
    if (label === t('dashboard.user.workGraph.years', {n: 1}))
      date = subYears(date, 1);

    const reduced =
      missions
        .filter(({state}) => state === "finished")
        .filter(({finishedAt}) => isAfter(finishedAt, date))
        .map(({finishedAt, reward}) => ({finishedAt, reward}))
        .reduce((p, c) => {
          const date = format(c.finishedAt, "yyyy-MM-dd");
          return {[date]: ((p as any)[date]||0)+(+c.reward)}
        }, {});


    let currentDate = date;
    const today = new Date();

    while (differenceInDays(today, currentDate) > 0) {
      const formatted = format(currentDate, "yyyy-MM-dd");
      if (!reduced.hasOwnProperty(formatted))
        (reduced as any)[formatted] = 0;

      currentDate = addDays(currentDate, 1);
    }

    const sorted: {[d: string]: number} =
      Object
        .keys(reduced)
        .sort()
        .reduce((acc, date) => {
          (acc as any)[date] = (reduced as any)[date];
          return acc;
        }, {})

    setData(Object.values(sorted));
    setLabels(Object.keys(sorted).map(date => format(new Date(date), "dd/MM/yyyy")));
    setHours(Object.values(sorted).reduce((acc, value) => acc+value,0));
  }

  const iconStyle = {
    borderRadius: "100%",
    height: 40,
    width: 40,
    backgroundColor: "rgba(192, 132, 252, 0.1)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
  };

  const {data: missions, isFetching: loading} = useUserMissions();

  useEffect(() => { onOptionToggle(t('dashboard.user.workGraph.days', {n:5})) }, [missions]);

  return <WindowBox icon={note} title={t('dashboard.user.workGraph.workHoursAnalysis')} style={{height: "100%"}}>
    <Grid container style={{paddingBottom: "20px"}}>
      <Grid item xs={12} sm={1} md={1}>
        <div style={iconStyle}>
          <Image src={clock} alt={"clock"} width={15} height={15}></Image>
        </div>
      </Grid>
      <Grid item xs={12} sm={11} md={11} sx={{textAlign: {xs: "center", sm: "left"}, paddingLeft: {xs: "0px", sm: "10px"}}}>
        <div className={style.workGraph_total_hours}>{t('dashboard.user.workGraph.totalWork')}</div>
        <div className={style.workGraph_hours_subtitle}>{t('dashboard.user.workGraph.hours',{n: hours})}</div>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12} style={{textAlign: "center"}}>
        <SwitcherButton options={options} activeLabel={t('dashboard.user.workGraph.days', {n:5})} onToggle={onOptionToggle}/>
      </Grid>
    </Grid>
    <LineGraph data={data} labels={labels} loading={loading} />
  </WindowBox>
}