"use client";

import tableStyle from "@/components/table/simple-table/simple-table.module.css";
import {TableColumns, TableRow, TableRows} from "@/types/simple-table/simple-table";
import Structural from "@/components/structural/structural";
import {NoData} from "@/components/no-data/no-data";
import {Grid, LinearProgress} from "@mui/material";
import {ReactNode, useEffect, useMemo, useState} from "react";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {useTranslation} from "@/i18n/client";

type SimpleTableProps = {
  columns: TableColumns;
  rows: TableRows;
  onRowClick?: ((_row: TableRow) => void);
  isLoading?: boolean;
  enableSearch?: boolean;
  defaultSearch?: string;
};

export function SimpleTable({
                              columns = [] as TableColumns,
                              rows = [] as TableRows,
                              onRowClick, isLoading = false,
                              enableSearch = false,
                              defaultSearch = ""
}: SimpleTableProps) {
  const [search, setSearch] = useState("");
  const {t} = useTranslation();

  const searchRowColumns = useMemo(() => {
    if (!enableSearch || !search)
      return rows;

    const hasColumns = search.split(":").length > 1;
    const searchColumnIndexes =
      hasColumns
        ? search.toLowerCase().split(":")[0].split(",").map(name => columns.findIndex(cname => cname.toLowerCase() === name))
        : [];

    return rows.filter(row => {
      console.log(`ROW`, row, Object.values(row), `hasColumns`, hasColumns, searchColumnIndexes);
      if (!hasColumns)
        return Object.values(row)
          .filter(v => typeof v === "string")
          .some((rowValue: string|ReactNode) =>
            (rowValue as string).toLowerCase().indexOf(search.toLowerCase()) > -1)

      return searchColumnIndexes
        .some((index: number) => {
          const values = Object.values(row);
          return typeof values[index] === "string" && (values[index] as string).toLowerCase().indexOf(search.toLowerCase()) > -1
        });
    })
  }, [search, rows, columns, enableSearch]);

  useEffect(() => {
    if (defaultSearch)
      setSearch(defaultSearch);

  }, [defaultSearch]);

  return <>
    <Structural.If condition={enableSearch}>
      <SimpleTextInput label={t('company.employeesTable.searchLabel')}
                       placeHolder={t('company.employeesTable.searchPlaceholder')}
                       onValueChange={v => { setSearch(v.target.value); }} />
    </Structural.If>

    <table className={tableStyle.table}>
      <thead>
      <tr>
        {columns.map((column, i) => <th key={i}>{column}</th>)}
      </tr>
      </thead>
      <tbody>
      <Structural.If condition={isLoading}>
        <Grid container>
          <Grid item xs={12}>
            <LinearProgress style={{marginTop: 16}}/>
          </Grid>
        </Grid>
      </Structural.If>

      {
        searchRowColumns.map((row, i) =>
          <tr key={i} className={onRowClick ? tableStyle.clickable : ""} onClick={() => onRowClick?.(row)}>
            {
              columns.map((column, ci) =>
                <td key={ci}>{row[column]}</td>)
            }
          </tr>)
      }
      <Structural.If condition={!rows.length && !isLoading}>
        <tr>
          <td><NoData/></td>
        </tr>
      </Structural.If>

      </tbody>
    </table>
  </>
}