import styles from "@/components/buttons/button.module.css"

export default function SmallButton({label = "", primary = false}) {
  const _className = `${styles.smallButton} ${primary ? styles.smallButton_primary : ""}`
  return <button className={_className}>{label}</button>
}