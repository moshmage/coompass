"use client";

import Image from "next/image";
import caretDown from "@/assets/caretDown.svg";
import {Menu, MenuItem} from "@mui/material";
import {useState} from "react";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {CheckBox} from "@/components/inputs/check-box/check-box";
import {useTranslation} from "@/i18n/client";

type DropdownButtonProps = {
  label: string;
  options: DropdownOption[];
  onSelect: (options: DropdownOption[]) => void;
  multiple?: boolean;
  sx?: any;
  buttonStyle?: "secondary" | "black" | "primary";
  translatedOptions?: boolean;
}

export function DropdownButton({
                                 label = "",
                                 options = [] as DropdownOption[],
                                 onSelect, translatedOptions = false,
                                 multiple, buttonStyle = "secondary",
                                 sx = {},
}: DropdownButtonProps) {
  const [anchorElement, setAnchorElement] = useState<null|HTMLElement>(null);
  const open = Boolean(anchorElement);

  const {t} = useTranslation();

  const selectOption = (option: DropdownOption) => {
    if (!multiple)
      for (const entry of options)
        if (option.title !== entry.title)
          entry.active = false;

    option.active = !option.active;
    onSelect(options.filter(({active}) => active));

    if (!multiple)
      setAnchorElement(null);
  }

  const tr = (title: string) => translatedOptions ? title : t(`common.tagsFiltersOptions.${title}`)

  return <>
    <SimpleButton label={<><span>{label}</span><Image src={caretDown} alt={""}/></>}
                  small style={buttonStyle} sx={{display: "flex", justifyContent: "space-between", ...sx}}
                  onClick={(e) => {setAnchorElement(e.currentTarget)}}/>
    <Menu open={open} anchorEl={anchorElement} onClose={() => setAnchorElement(null)}>
      {options.map((option, i) =>
        <MenuItem key={i} onClick={() => selectOption(option)} sx={{backgroundColor: "black", color: "white"}}>
          {multiple ? <CheckBox isChecked={!!option.active} label={tr(option.title)}/> : tr(option.title)}
        </MenuItem> )}
    </Menu>
  </>
}