import styles from "@/components/buttons/button.module.css";
import {MouseEventHandler, ReactNode} from "react";

type ButtonProps = {
  label: string|ReactNode;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  disabled?: boolean

  style?: "primary"|"secondary"|"black";
  type?: "button"|"submit"|"reset";
  small?: boolean;
  block?: boolean;
  sx?: any;
};

export function SimpleButton({label, onClick, disabled, style = "primary", type = "button", small = false, sx = {}, block}: ButtonProps) {

  const buttonSize = small ? styles.smallButton : styles.simpleButton
  const buttonType = style === "primary"
    ? styles.simpleButton_primary
    : style === "secondary"
      ? styles.simpleButton_secondary
      : styles.simpleButton_black;

  return <button disabled={disabled}
                 style={sx}
                 onClick={onClick}
                 type={type}
                 className={`${buttonSize} ${buttonType} ${block ? styles.block : ""}`}>{label}</button>
}