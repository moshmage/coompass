"use client";

import styles from "@/components/buttons/button.module.css";
import Image from "next/image";

export default function IconButton({icon = "", onClick = () => {}}) {
  return <button className={[styles.simpleButton, styles.iconButton].join(" ")} style={{width: "auto", marginTop: 0}} onClick={onClick}>
    <Image src={icon} alt={"icon"}/>
  </button>
}