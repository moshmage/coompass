import {useTranslation} from "@/i18n/client";

export function NoData({message = ""}) {
  const {t} = useTranslation();
  return <div style={{textAlign: "center", fontSize: 14}}>{message || t('common.noData')}</div>
}