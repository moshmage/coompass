import logo from "@/assets/logo.svg"
import Image from "next/image";
export function Logo({height = 40, width = 205.71}) {
  return <Image width={width} height={height} src={logo} alt="logo" />
}