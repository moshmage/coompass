import logo from "@/assets/logo-small.svg";
import Image from "next/image";

export function LogoSmall({height = 64, width = 64}) {
  return <Image width={width} height={height} src={logo} alt="logo" />
}