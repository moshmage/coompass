"use client";

import {chartsGridClasses, LineChart} from "@mui/x-charts";

import styles from "@/components/line-graph/line-graph.module.css"
import {CircularProgress} from "@mui/material";

export default function LineGraph({data = [] as number[], labels = [] as string[], loading = false}) {

  return (
    <div className={styles.lineGraph_wrapper}>
      {loading ? <div style={{display: "flex", justifyContent: "center", alignItems: "center", marginTop: 8}}><CircularProgress/></div> : null}
      {
        !loading
          ? <LineChart margin={{top: 8, left: 8, right: 8, bottom: 8}}
                       leftAxis={null}
                       bottomAxis={null}
                       grid={{horizontal: true}}
                       xAxis={[{scaleType: "point", data: labels}]}
                       sx={{
                         [`& .${chartsGridClasses.horizontalLine}`]: {
                           "stroke-dasharray": "5,5",
                           "stroke": "rgba(255, 255, 255, 0.12)!important",
                         }
                       }}
                       series={[{ curve: "linear", data, showMark: false, }]} />
          : null
      }
    </div>
  );
}