"use server";

import React from "react";
import SideBarMenu from "@/components/side-bar/menu/side-bar-menu";
import TopBar from "@/components/top-bar/top-bar";
import {Grid} from "@mui/material";
import Providers from "@/app/providers";
import {auth} from "@/handlers/server/credentials/auth";
import {redirect} from "next/navigation";

import menuStyles from "@/components/side-bar/menu/menu.module.css";

export async function LayoutApp({children,}: { children: React.ReactNode, }) {
  return <Providers>
    <Grid container>
      <Grid item xs={2} className={menuStyles.fixedMenu}>
        <SideBarMenu />
      </Grid>
      <Grid item xs={10} style={{marginLeft: "16.666667%"}}>
        <Grid container>
          <Grid item xs={12}>
            <TopBar />
          </Grid>
        </Grid>
        <Grid container spacing={0} style={{padding: "16px"}} className="black-page">
          {children}
        </Grid>
      </Grid>
    </Grid>
  </Providers>
}