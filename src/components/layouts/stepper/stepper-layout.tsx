import {Grid} from "@mui/material";
import {StepTopBar} from "@/components/top-bar/step-top-bar";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import {ReactNode} from "react";
import Providers from "@/app/providers";

export function StepperLayout({
                                companyId = "",
                                steps = [] as { label: string, url: string }[],
                                active = 0,
                                backLink = "",
                                children = null as ReactNode|null,
                              }) {
  return <Providers>
    <Grid container>
      <Grid item xs={12}>
        <StepTopBar companyId={companyId} steps={steps} active={active}/>
      </Grid>
    </Grid>
    <div className="gradient-page h-100">
      <Grid container style={{marginTop: 32}}>
        <Grid item xs={0} sm={1} md={2} />
        <Grid item xs={12} sm={11} md={10}><Link href={backLink}><SimpleButton label="back" small={true} style="secondary" /></Link></Grid>
      </Grid>
      <Grid container style={{paddingBottom: 30}}>
        <Grid item xs={0} sm={1} md={2} lg={4} />
        <Grid item xs={12} sm={10} md={8} lg={4}>
          {children}
        </Grid>
        <Grid item xs={0} sm={1} md={2} lg={4} />
      </Grid>

    </div>


  </Providers>
}