import {Logo} from "@/components/logo/logo";
import Link from "next/link";
import ChangeLocale from "@/components/locale/change-locale";

export async function LayoutSimple({children, link = "", linkText = ""}: any) {
  return <div className="h-100 gradient-page">
    <div className="row">
      <div className="column" style={{display: "flex", alignItems: "center", position: "relative", zIndex: 2}}>
        <div><Logo/></div>
        <div style={{marginLeft: 16}}>
          <ChangeLocale />
        </div>
      </div>
      {
        link ? <>
          <div className="column">
            <div className="row justify-end">
              <Link href={link}><span>{linkText}</span></Link>
            </div>
          </div>
        </>
          : null
      }
    </div>
    <div className="row">
      {children}
    </div>
  </div>
}