import styles from "@/components/box/box.module.css"
export function Box({gradient = true, children}: {gradient: boolean, children: React.ReactNode}) {
  return <div className={`${styles.box} ${gradient && "gradient-page" || ""}`}>{children}</div>
}