"use client";

import {Grid} from "@mui/material";
import style from "@/app/missions/[id]/mission.module.css";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {MissionDetails} from "@/components/mission/mission-details/mission-details";
import axios from "axios";
import {useRouter} from "next/navigation";
import {useSelf} from "@/components/hooks/use-self";
import {UserTypes} from "@/types/user/user";
import {useEffect, useState} from "react";
import {ConfirmationModal} from "@/components/modals/confirmation/confirmation-modal";
import MarkedRender from "@/components/marked-render/marked-render";
import {useSnackbar} from "notistack";
import {ParticipantsState} from "@/types/mission/mission";
import {format} from "date-fns";
import {InformationModal} from "@/components/modals/info-modal/info-modal";
import {useTranslation} from "@/i18n/client";
import Structural from "@/components/structural/structural";
import Link from "next/link";

export function MissionPage({
                                header = "", id = "",
                                title = "Mission title that is very very very very very very very very very long",
                                orgName = "ORGANIZATION NAME",
                                totalUsers = 12,
                                totalPendingUsers = 12,
                                description = "Here be dragons",
                                causes = ["cause 1", "cause 2", "cause 3"],
                                datePosted = new Date(),
                                dateStarted = new Date(),
                                dateFinished = new Date(),
                                skills = ["skill 1", "skill 2", "skill 3", "skill 4"],
                                state = "",
                                participation = null as ParticipantsState | null,
                                requirements = "",
                                reward = 0,
                                location = "",
                                whenStart = new Date(),
                                whenFinish = new Date(), companyId = "",
                            }) {
    const {t} = useTranslation()
    const router = useRouter();
    const {userData} = useSelf();

    const [isModalOpen, setModalOpen] = useState<boolean>(false);
    const [isPendingOpen, setPendingOpen] = useState<boolean>(false);

    const [isUser, setIsUser] = useState<boolean>(false);
    const [isNGO, setIsNGO] = useState<boolean>(false);
    const [isNGOOwner, setIsNGOOwner] = useState<boolean>(false);
    const [NGOButtonLabel, setNGOButtonLabel] = useState<string>("");

    const [partnership, setPartnership] = useState<boolean>(false);

    const {enqueueSnackbar} = useSnackbar()

    const _joinMission = async () => {
        setModalOpen(false);
        enqueueSnackbar(t('common.snackbars.info.joiningMission'), {variant: "info"});

        axios.post(`/api/missions/${id}/participants-state`)
            .then(() => {
                enqueueSnackbar(t('common.snackbars.success.requestedParticipation'), {variant: "success"});
                router.refresh();
                setPendingOpen(true);
            })
            .catch(e => {
                console.log(e?.message);
                enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToRequestParticipation'), {variant: "warning"});
            });
    }

    const _changeMissionState = async () => {
        const _state = state === "paused" ? "started" : "finished";
        enqueueSnackbar(t('common.snackbars.info.changingMissionState'), {variant: "info"});

        if (_state === "started" && !totalUsers) {
          enqueueSnackbar(t('common.snackbars.info.noAcceptedUsers'), {variant: "warning"});
          return;
        }

        axios.patch(`/api/missions/${id}`, {state: _state})
            .then(() => {
                enqueueSnackbar(t('common.snackbars.success.changedMissionState'), {variant: "success"});
                setTimeout(() => router.refresh(), 1000);
            })
            .catch(e => {
                console.log(e?.message);
                enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToChangeMissionState'), {variant: "warning"});
            });

    }

    const deleteMission = async () => {
        enqueueSnackbar(t('common.snackbars.info.deletingMission'), {variant: "info"});
        axios.delete(`/api/missions/${id}`)
            .then(() => {
                enqueueSnackbar(t('common.snackbars.success.deletedMission'), {variant: "success"});
                setTimeout(() => router.push(`/organization/${companyId}`), 1000);
            })
            .catch(e => {
                console.log(e?.message);
                enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToDeleteMission'), {variant: "warning"});
            });
    }

    const getParticipationState = () => {
        if (!participation?.state)
            return ``;

        if (participation?.state === "pending")
            return t('missions.participationText.pending');

        return t(`missions.participationText.${participation?.state}`);
    }

    const participationText = getParticipationState();
    const when = <>{format(whenStart || new Date(), "dd/MM/yyyy")} &#x2192; {format(whenFinish || new Date(), "dd/MM/yyyy")}</>

    useEffect(() => {
        setIsUser(userData?.type === UserTypes.user);
        setIsNGO(userData?.type === UserTypes.ngo);
        setIsNGOOwner(userData?.associatedCompanies?.[0]?.id === companyId && userData?.associatedCompanies?.[0]?.creator === userData.id);
        setNGOButtonLabel(state === "started" ? t('missions.finishMission') : state === "paused" ? t('missions.startMission') : "");

        const partnerOrgs = userData?.associatedCompanies?.[0]?.partnerOrgs;
        setPartnership(!partnerOrgs?.length ? true : partnerOrgs.findIndex(({id}) => companyId === id) > -1,)
    }, [userData])

    return <>
        {/*<Grid container style={{paddingBottom: "16px"}}>*/}
        {/*  <Grid item xs={12} className={style.mission_header}>*/}
        {/*    <Base64Image src={header} alt={"mission header"} backgroundSize="cover" />*/}
        {/*  </Grid>*/}
        {/*</Grid>*/}
        <Grid container spacing={2}>
            <Grid item xs={9}>
                <Grid container spacing={1} style={{borderBottom: "1px solid rgba(255,255,255,.12)", paddingBottom: 8}}>
                    <Grid item xs={9}>
                        <div className={style.mission_header_title}>{title}</div>
                        <span className={style.mission_header_org}>{t('common.organization')}: </span>
                        <span className={style.mission_header_orgname}>{orgName}</span>
                        <Structural.If condition={isNGOOwner && state !== "started"}>
                            <div style={{marginTop: 8, display: "flex", gap: 6}}>
                                <SimpleButton label={t('missions.deleteMission')} onClick={deleteMission}
                                              style="secondary" small/>
                                <Link href={`/create-mission/details/?edit=${id}`}>
                                    <SimpleButton label={t('missions.editMission')} style="secondary" small/>
                                </Link>
                            </div>
                        </Structural.If>
                    </Grid>
                    <Grid item xs={3} style={{textAlign: "center", display: "flex", alignItems: "end"}}>
                        <div>
                            <div style={{display: "flex", justifyContent: "center"}}>
                                <Structural.If condition={partnership && isUser && state === "paused"}>
                                    <Structural.If condition={!participation}>
                                        <SimpleButton small label={t('missions.iWantToHelp')}
                                                      onClick={() => setModalOpen(true)}
                                                      style="primary"/>
                                    </Structural.If>
                                    <Structural.If condition={!!participation}>
                                        <span style={{fontSize: 14}}>{participationText}</span>
                                    </Structural.If>
                                </Structural.If>
                                <Structural.If condition={isNGOOwner && !!NGOButtonLabel}>
                                    <SimpleButton small label={NGOButtonLabel}
                                                  onClick={_changeMissionState}
                                                  style="primary"/>
                                </Structural.If>
                            </div>

                            <div style={{
                                paddingTop: 8,
                                marginTop: 8,
                                width: "100%",
                                borderTop: "1px solid rgba(255,255,255,.12)",
                                fontSize: 12
                            }}>
                                <div>
                                    <span>{t('missions.usersJoined', {total: totalUsers})}{isNGO ? `, ${t('missions.usersPending', {total: totalPendingUsers})}` : ""}</span>
                                </div>
                            </div>
                        </div>

                    </Grid>
                </Grid>
                <Grid container style={{padding: "8px 0"}}>
                    <Grid item xs={12}>
                        <MarkedRender source={description}/>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={3}>
                <MissionDetails when={when}
                                causes={causes}
                                skills={skills}
                                where={location}
                                reward={reward}
                                datePosted={datePosted}
                                dateStarted={dateStarted}
                                dateFinished={dateFinished}
                                requirements={requirements}/>
            </Grid>
        </Grid>

        <ConfirmationModal isOpen={isModalOpen} title={t('missions.areYouSure')}
                           onConfirm={_joinMission}
                           onCancel={() => setModalOpen(false)}>
            <>{t('missions.youAreAboutToJoinMission', {title})}</>
        </ConfirmationModal>

        <InformationModal isOpen={isPendingOpen}
                          confirmLabel={t('missions.backToMissions')}
                          onClose={() => {
                              setPendingOpen(false);
                              router.refresh();
                              router.push(`/missions`);
                          }}>
            <>
                <p style={{fontSize: 14}}>{t('missions.orgWasNotifiedWithYourInterest', {orgName})}</p>
                <p style={{fontSize: 12, marginTop: 16}}>{t('missions.anEmailHasBeenSentToYou')}</p>
            </>
        </InformationModal>
    </>
}