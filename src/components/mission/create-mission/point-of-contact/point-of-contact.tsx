"use client";

import createMissionStyles from "@/components/mission/create-mission/create-mission.module.css";
import {CreateMissionHeader} from "@/components/mission/create-mission/create-mission-header/create-mission-header";
import {Grid} from "@mui/material";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import React, {useEffect, useState} from "react";

import envelope from "@/assets/envelopeSimple.svg";
import Link from "next/link";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {isEmail} from "validator";
import {useRouter, useSearchParams} from "next/navigation";
import rocket from "@/assets/rocket.svg";
import {useTranslation} from "@/i18n/client";

export function PointOfContact() {
  const {t} = useTranslation()
  const [email, setEmail] = useState<string>("");
  const [_tmp, setTemp] = useState<any>(null);

  const router = useRouter();
  const searchParams = useSearchParams();

  const validForm = () => email && isEmail(email);

  const nextStep = () => {
    const tmp = JSON.parse(localStorage.getItem("new-mission") || "{}");
    tmp.pointOfContact = email;
    localStorage.setItem("new-mission", JSON.stringify(tmp));

    router.push(`/create-mission/review${searchParams.has("edit") ? `?edit=${searchParams.get("edit")}` : ''}`);
  }

  useEffect(() => {
    if (!localStorage.getItem("new-mission")) {
      router.push(`/create-mission/details${searchParams.has("edit") ? `?edit=${searchParams.get("edit")}` : ''}`);
      return;
    }

    const tmp = JSON.parse(localStorage.getItem("new-mission")!)
    setEmail(tmp.pointOfContact)
    setTemp(tmp);

  }, []);

  return <div className={createMissionStyles.createMission}>
    <CreateMissionHeader title={t('create-mission.createNewMission')}
                         stepLabel={t('create-mission.step')}
                         subtitle={t('create-mission.point-of-contact.pointOfContact')}
                         stepNumber={2} image={rocket} />
    <Grid container>
      <Grid item xs={12} style={{paddingBottom: "16px"}}>
        <SimpleTextInput label={t('common.Email')}
                         defaultValue={_tmp?.pointOfContact}
                         beforeIcon={envelope}
                         placeHolder={t('create-mission.point-of-contact.pointOfContactEmail')}
                         helperInfo={t('create-mission.point-of-contact.thisWillBeShownToVolunteers')}
                         name="pointofcontact"
                         type="email"
                         onValueChange={e => setEmail(e?.target.value)} />
      </Grid>
    </Grid>
    <Grid container style={{marginTop: 16}}>
      <Grid item xs={12} style={{display: "flex", justifyContent: "space-between"}}>
        <Link href={`/create-mission/details`}><SimpleButton label={t('common.buttons.back')} small style="secondary" /></Link>
        <SimpleButton disabled={!validForm()} label={t('common.buttons.nextStep')} style="primary" small onClick={nextStep} />
      </Grid>
    </Grid>
  </div>
}