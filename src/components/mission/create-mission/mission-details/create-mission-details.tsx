"use client";

import {CreateMissionHeader} from "@/components/mission/create-mission/create-mission-header/create-mission-header";
import rocket from "@/assets/rocket.svg";
import {Grid} from "@mui/material";
import inputStyle from "@/components/inputs/text-input/text-input.module.css";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {MissionTags} from "@/constants/mission-tags";
import {Skills} from "@/constants/skills";
import React, {useEffect, useState} from "react";
import {useRouter, useSearchParams} from "next/navigation";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import MDEditor from "@uiw/react-md-editor";

import createMissionStyles from "@/components/mission/create-mission/create-mission.module.css"
import {NumberInput} from "@/components/inputs/number-input/number-input";
import map from "@/assets/map-trifold.svg";
import {CheckBox} from "@/components/inputs/check-box/check-box";
import {DateInput} from "@/components/inputs/date-input/date-input";
import {isAfter, isBefore, isValid} from "date-fns";
import {useTranslation} from "@/i18n/client";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {DropdownButton} from "@/components/buttons/dropdown/dropdown-button";
import {getMissions} from "@/handlers/client/missions/get-missions";
import {getMission} from "@/handlers/client/missions/get-mission";

export function CreateMissionDetails({companyId = ""}) {
  const {t} = useTranslation();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [requirements, setRequirements] = useState("");
  const [image, setImage] = useState("");
  const [reward, setReward] = useState<number>(0);
  const [validImage, setValidImage] = useState<boolean>(false);
  const [causes, setCauses] = useState<DropdownOption[]>(MissionTags);
  const [skills, setSkills] = useState<DropdownOption[]>(Skills);
  const [_tmp, setTemp] = useState<any>(null);
  const [whenStart, setWhenStart] = useState<Date|null>(null);
  const [whenFinish, setWhenFinish] = useState<Date|null>(null);
  const [where, setWhere] = useState<string>("");
  const [isVirtual, setIsVirtual] = useState<boolean>(false);

  const router = useRouter();
  const searchParams = useSearchParams();

  const nextStep = () => {
    const removeTitle = ({title}: {title: string}) => title;
    localStorage.setItem("new-mission", JSON.stringify({
      title, description, image, reward, validImage, requirements,
      whenStart: whenStart?.toISOString(), whenFinish: whenFinish?.toISOString(),
      where, isVirtual,
      causes: causes.filter(c => c.active).map(removeTitle),
      skills: skills.filter(s => s.active).map(removeTitle)
    }))

    router.push(`/create-mission/point-of-contact${searchParams.has("edit") ? `?edit=${searchParams.get("edit")}` : ''}`);
  }

  const validForm = () => {
    console.log(whenStart, whenFinish)
    console.log(
        title,
        description,
        reward,
        skills.length, causes.length,
        isValid(whenStart), isValid(whenFinish),
        (!isVirtual ? !!where : true),
        requirements.length > 0 && requirements.length <= 200,
    )
    return !!(
        title &&
        description &&
        // image &&
        // validImage &&
        reward &&
        skills.length && causes.length &&
        isValid(whenStart) && isValid(whenFinish)
        && (!isVirtual ? !!where : true) &&
        requirements.length > 0 && requirements.length <= 200
    );
  }

  const updateSkillState = (options: DropdownOption[]) => {
    setSkills(
      Skills.map(s => ({...s, active: !!options.find(o => o.title === s.title)}))
    )
  }

  const updateCausesState = (options: DropdownOption[]) => {
    setCauses(
      MissionTags.map(s => ({...s, active: !!options.find(o => o.title === s.title)}))
    )
  }

  useEffect(() => {

    (async () => {
      if (searchParams.has("edit")) {
        const mission = await getMission(searchParams.get("edit") as string)

        if (mission?.company.id !== companyId)
          return;

        if (mission) {
          (mission as any).isVirtual = mission.location.indexOf("virtual") > -1;
          (mission as any).where = mission.location;
          (mission as any).whenStart = mission.when_start;
          (mission as any).whenFinish = mission.when_finish;

          localStorage.setItem("new-mission", JSON.stringify(mission));
        }

      }

      if (!localStorage.getItem("new-mission"))
        return;

      const tmp = JSON.parse(localStorage.getItem("new-mission")!);

      setTitle(tmp.title)
      setDescription(tmp.description)
      setReward(tmp.reward);

      // setImage(tmp.image);
      // setValidImage(tmp.validImage || false);

      console.log(tmp.whenStart, tmp.whenFinish,tmp.when_start, tmp.when_finish);

      setCauses(MissionTags.map(c => ({...c, active: tmp.causes.includes(c.title)})));
      setSkills(Skills.map(s => ({...s, active: tmp.skills.includes(s.title)})));
      setWhenStart(new Date(tmp.whenStart));
      setWhenFinish(new Date(tmp.whenFinish));
      setIsVirtual(tmp.isVirtual);
      setRequirements(tmp.requirements)

      if (!tmp.isVirtual)
        setWhere(tmp.where);

      setTemp(tmp);
    })();

  }, [])

  return <div className={createMissionStyles.createMission}>
    <CreateMissionHeader title={t('create-mission.createNewMission')}
                         stepLabel={t('create-mission.step')}
                         stepNumber={1} subtitle={t('create-mission.details.missionDetails')} image={rocket} />

    {/*<Grid container className={inputStyle.horizontal_input}>*/}
    {/*  <Grid item xs={6} className={inputStyle.textInput}>*/}
    {/*    <label htmlFor="imageUpload">{t('create-mission.details.missionHeader')}</label>*/}
    {/*    <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.minImage', {dimension: "519x83px"})}</div>*/}
    {/*  </Grid>*/}
    {/*  <Grid item xs={6} >*/}
    {/*    <ImageInput img={_tmp?.image || ""} onChange={(image, valid) => {setImage(image); setValidImage(valid)}} />*/}
    {/*  </Grid>*/}
    {/*</Grid>*/}

    <Grid container>
      <Grid item xs={12} style={{paddingBottom: "16px"}}>
        <SimpleTextInput label={t('create-mission.details.missionName')}
                         defaultValue={_tmp?.title}
                         placeHolder={t('create-mission.details.theNameOfTheMission')}
                         name="missionName"
                         type="text"
                         onValueChange={e => setTitle(e?.target.value)} />
      </Grid>
    </Grid>

    <Grid container style={{paddingBottom: "16px"}}>
      <Grid item xs={12} className={inputStyle.textInput}>
        <label htmlFor="description">{t('create-mission.details.missionDescription')}</label>

        <MDEditor preview="edit" value={description} onChange={v => setDescription(v || "")}/>
        {/*<MDEditor.Markdown source={value} style={{ whiteSpace: 'pre-wrap' }} />*/}

      </Grid>
    </Grid>

    <Grid container style={{paddingBottom: "16px"}}>
      <Grid item xs={12}>
        <NumberInput label={t('create-mission.details.hoursRequired')}
                     name="hoursRequired"
                     defaultValue={_tmp?.reward}
                     onValueChange={value => setReward(value)} />
      </Grid>
    </Grid>

    <Grid container className={inputStyle.textInput} style={{paddingBottom: "16px"}} spacing={1}>
      <Grid item xs={12} sm={6}>
        <DateInput label={t('common.dates.whenItStarts')} onValidDate={setWhenStart} defaultDate={_tmp?.whenStart} />
      </Grid>
      <Grid item xs={12} sm={6}>
        <DateInput label={t('common.dates.whenItFinishes')} onValidDate={setWhenFinish} defaultDate={_tmp?.whenFinish} />
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <div style={{color: "red", fontSize: 12, paddingLeft: 16}}>
            {
              isValid(whenStart) && isValid(whenFinish)
                ? !isAfter(whenFinish as Date, whenStart!)
                  ? <>{t('common.dates.finishHasToBeAfterStart')}</>
                  : !searchParams.has("edit") && (isBefore(whenFinish as Date, new Date()) || isBefore(whenStart as Date, new Date()))
                    ? <>{t('common.dates.datesHaveToBeInTheFuture')}</>
                    : null
                : null
            }
          </div>
        </Grid>
      </Grid>
    </Grid>

    <Grid container spacing={1}>
      <Grid item xs={12} className={inputStyle.textInput}>
        <SimpleTextInput beforeIcon={map} disabled={isVirtual}
                         placeHolder={t('common.location.enterLocation')} label={t('common.location.where')}
                         onValueChange={e => setWhere(e.target.value)} />
      </Grid>
    </Grid>
    <Grid item xs={12} style={{display: "flex", alignItems: "center", paddingBottom: 16, paddingTop: 8}}>
      <CheckBox onChange={setIsVirtual} isChecked={isVirtual} label="Virtual" />
    </Grid>

    <Grid container>
      <Grid item xs={12} className={inputStyle.textInput}>
        <label>{t('common.causes.selectCauses')}</label>
        <DropdownButton multiple label={t('common.causes.availableCauses')} options={causes} onSelect={updateCausesState}/>

      </Grid>
    </Grid>

    <Grid container>
      <Grid item xs={12} className={inputStyle.textInput}>
        <label>{t('common.skills.selectRequiredSkills')}</label>
        <DropdownButton multiple label={t('common.skills.availableSkills')} options={skills} onSelect={updateSkillState}/>
      </Grid>
    </Grid>

    <Grid container>
      <Grid item xs={12} style={{paddingBottom: "16px"}} className={inputStyle.textInput}>
        <label htmlFor="requirements">{t('create-mission.details.missionRequirements')}</label>
        <div className={inputStyle.textInput_input} style={{height: "auto"}}>
          <textarea placeholder={t('create-mission.details.missionRequirementsPlaceholder')}
                    defaultValue={_tmp?.requirements}
                    onChange={e => setRequirements(e.target.value)}/>
        </div>
        <div className={inputStyle.horizontal_input_textarea_length}
             style={{color: requirements.length > 200 ? "red" : ""}}>{requirements.length} / 200
        </div>
      </Grid>
    </Grid>

    <Grid container style={{marginTop: 16}}>
      <Grid item xs={12} style={{display: "flex", justifyContent: "space-between"}}>
        <Link href={`/organization/${companyId}`}><SimpleButton label={t('common.buttons.back')} small style="secondary" /></Link>
        <SimpleButton disabled={!validForm()} label={t('common.buttons.nextStep')} style="primary" small onClick={nextStep} />
      </Grid>
    </Grid>

  </div>
}