"use client";

import {Grid} from "@mui/material";
import Image from "next/image";

export function CreateMissionHeader({
                                      title = "",
                                      stepLabel = "",
                                      stepNumber = 0,
                                      subtitle = "",
                                      image = ""
}) {
  return <>
    <Grid container style={{marginBottom: 32}}>
      <Grid item xs={12} style={{textAlign: "center", marginTop: 56}}>
        <div style={{width: 64, height: 64, borderRadius: "100%", padding: 16, border: "1px solid rgba(255, 255, 255, 0.12)", margin: "0 auto"}}>
          <Image src={image} alt={""} height={32} width={32} />
        </div>
        <div style={{fontSize: 24, fontWeight: 600, color: "white", marginTop: 8, marginBottom: 24}}>{title}</div>
      </Grid>
      <Grid item xs={12}><div className="hr"></div></Grid>
      <Grid item xs={12} style={{textAlign: "center", marginTop: 32}}>
        <div style={{fontSize: 13, fontWeight: 400}}>{stepLabel} {stepNumber}</div>
        <div style={{fontSize: 20, fontWeight: 500}}>{subtitle}</div>
      </Grid>
    </Grid>
  </>
}