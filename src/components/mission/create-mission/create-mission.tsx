"use client";

import {Grid} from "@mui/material";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import React, {useState} from "react";
import axios from "axios";
import {useParams, useRouter} from "next/navigation";
import {SettingsBox} from "@/components/settings/settings-box";
import inputStyle from "@/components/inputs/text-input/text-input.module.css";
import createOrgStyle from "@/components/organization/create-org/create-org.module.css";
import {ImageInput} from "@/components/inputs/image/image-input";

import rocket from "@/assets/rocket.svg";
import {TagSelector} from "@/components/tags/tag-selector/tag-selector";
import {MissionTags} from "@/constants/mission-tags";
import {Skills} from "@/constants/skills";
import {useTranslation} from "@/i18n/client";

export function CreateMission() {

  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")
  const [image, setImage] = useState("")
  const [reward, setReward] = useState<number>(0);
  const [validImage, setValidImage] = useState<boolean>(false);
  const [causes, setCauses] = useState<{title: string}[]>([]);
  const [skills, setSkills] = useState<{title: string}[]>([]);

  const params = useParams();
  const router = useRouter();

  const {t} = useTranslation();

  const createMission = async () => {

    const removeTitle = ({title}: {title: string}) => title;

    const postData = {
      title, reward, description, image,
      companyId: params.id,
      causes: causes.map(removeTitle),
      skills: skills.map(removeTitle),
    }

    const {data} = await axios.post("/api/missions", postData);
    if (data)
      router.push(`/missions/${data.id}/`);
  }

  const validForm = !!(title && description && image && validImage && reward);

  return <SettingsBox title="Create new mission" icon={rocket}>

    <Grid item xs={12}>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={6} className={inputStyle.textInput}>
          <label htmlFor="imageUpload">Mission header</label>
          <div className={createOrgStyle.createOrg_form_subtitle}>Min 519x83px, PNG or JPEG formats.</div>
        </Grid>
        <Grid item xs={6} >
          <ImageInput onChange={(image, valid) => {setImage(image); setValidImage(valid)}} />
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} style={{paddingBottom: "16px"}}>
          <SimpleTextInput label="Mission name"
                           name="missionName"
                           type="text"
                           onValueChange={e => setTitle(e?.target.value)} />
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} style={{paddingBottom: "16px"}} className={inputStyle.textInput}>
          <label htmlFor="description">Mission description</label>
          <div className={inputStyle.textInput_input} style={{height: "auto"}}>
            <textarea onChange={e => setDescription(e.target.value)}/>
          </div>

        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12} style={{paddingBottom: "16px"}}>
          <SimpleTextInput label="Hours required"
                           type="number"
                           name="hoursRequired"
                           onValueChange={e => setReward(+e.target.value)} />
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12}>
          <TagSelector label="Select causes"
                       popupTitle="Available causes"
                       tags={MissionTags}
                       onSelect={setCauses} />
        </Grid>
      </Grid>

      <Grid container>
        <Grid item xs={12}>
          <TagSelector label="Select required skills"
                       popupTitle="Available causes"
                       tags={Skills}
                       onSelect={setSkills} />
        </Grid>
      </Grid>

      <div className={createOrgStyle.createOrg_submit}>
        <SimpleButton disabled={!validForm} label="Save changes" style="primary" onClick={createMission}/>
      </div>
    </Grid>

  </SettingsBox>
}