"use client";

import React, {useEffect, useState} from "react";
import rocket from "@/assets/rocket.svg";
import {CreateMissionHeader} from "@/components/mission/create-mission/create-mission-header/create-mission-header";
import WindowBox from "@/components/window/window-box";
import {Collapse, Grid} from "@mui/material";

import style from "@/components/mission/mission-details/mission-details.module.css";
import {TagGroup} from "@/components/tag-group/tag-group";
import Link from "next/link";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import axios from "axios";
import {useRouter, useSearchParams} from "next/navigation";
import MarkedRender from "@/components/marked-render/marked-render";
import {CaretDownSvg} from "@/components/svg/caret-down-svg";
import {useSnackbar} from "notistack";
import {format} from "date-fns";
import {useTranslation} from "@/i18n/client";
import {useUserCompany} from "@/components/hooks/queries/use-user-company";

export function Review({orgName = "", companyId = ""}) {
  const {t} = useTranslation();
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")
  const [requirements, setRequirements] = useState("")
  // const [image, setImage] = useState("")
  const [reward, setReward] = useState<number>(0);
  const [causes, setCauses] = useState<{title: string}[]>([]);
  const [skills, setSkills] = useState<{title: string}[]>([]);
  const [email, setEmail] = useState<string>("");
  const [checked, setChecked] = useState<boolean>(false);
  const [whenFinish, setWhenFinish] = useState<Date|null>(null);
  const [whenStart, setWhenStart] = useState<Date|null>(null);
  const [location, setLocation] = useState<string>("");
  const {data: companyData} = useUserCompany();

  const router = useRouter();
  const searchParams = useSearchParams();
  const {enqueueSnackbar} = useSnackbar();

  const removeTitle = ({title}: {title: string}) => title;

  const createMission = async () => {

    const removeTitle = ({title}: {title: string}) => title;

    const postData = {
      title, reward, description, image: "", email,
      companyId: companyData?.id, requirements, location, when_start: whenStart, when_finish: whenFinish,
      causes: causes.map(removeTitle),
      skills: skills.map(removeTitle),
    }

    enqueueSnackbar(t(`common.snackbars.info.${searchParams.has("edit") ? 'editingMission' : 'creatingMission'}`), {variant: "info"});

    (searchParams.has("edit") ? axios.patch : axios.post)(`/api/missions${searchParams.has("edit") ? `/${searchParams.get("edit")}` : ""}`, postData)
      .then(response => {
        enqueueSnackbar(t('common.snackbars.success.createdMission'), {variant: "success"});
        router.push(`/missions/${response.data.id}/`);
        localStorage.removeItem("new-mission");
      })
      .catch(e => {
        console.log(`Error`, e.message, e?.response?.data?.error);
        enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToCreateMission'), {variant: "warning"});
      })
  }

  useEffect(() => {
    if (!localStorage.getItem("new-mission")) {
      router.push("/create-mission/details");
      return;
    }

    const tmp = JSON.parse(localStorage.getItem("new-mission")!);

    if (!tmp.pointOfContact) {
      router.push("/create-mission/point-of-contact");
      return;
    }

    const mapTitle = (title: string) => ({title});

    setTitle(tmp.title);
    setDescription(tmp.description);
    setRequirements(tmp.requirements);
    // setImage(tmp.image);
    setReward(tmp.reward);
    setCauses(tmp.causes.map(mapTitle));
    setSkills(tmp.skills.map(mapTitle));
    setEmail(tmp.pointOfContact);
    setWhenFinish(new Date(tmp.whenFinish));
    setWhenStart(new Date(tmp.whenStart));
    setLocation(tmp.isVirtual ? t('common.location.onlineVirtual') : tmp.where);

  }, [])

  return <>
    <CreateMissionHeader
      title={t('create-mission.createNewMission')}
      stepLabel={t('create-mission.step')}
      subtitle={t('create-mission.review.review')}
      stepNumber={3}
      image={rocket} />
    <WindowBox title={t('create-mission.review.missionOverview')}>
      <>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <div>
              <div className={style.missionDetails_label}>{t('common.mission')}</div>
              <div className={style.missionDetails_entry}>{title}</div>
            </div>
            {/*<div>*/}
            {/*  <div className={style.missionDetails_label}>Date posted</div>*/}
            {/*  <div className={style.missionDetails_entry}>{formatDistanceToNow(new Date())}</div>*/}
            {/*</div>*/}
          </Grid>
          <Grid item xs={12} sm={6} style={{textAlign: "right"}}>
            <div className={style.missionDetails_label}>{t('common.organization')}</div>
            <div className={style.missionDetails_entry}>{orgName}</div>
          </Grid>
        </Grid>
        <Grid container style={{paddingTop: 16}} >
          <Grid item xs={12}>
            <div className={style.missionDetails_label}>{t('missions.requiredHours')}</div>
            <div className={style.missionDetails_entry}>{reward}h</div>
          </Grid>
        </Grid>
        <Grid container style={{paddingTop: 16}} spacing={1}>
          <Grid item xs={12} sm={6}>
            <div className={style.missionDetails_label}>{t('common.dates.whenItStarts')}</div>
            <div className={style.missionDetails_entry}>{whenStart ? format(whenStart, "dd/MM/yyy") : "-"}</div>
          </Grid>
          <Grid item xs={12} sm={6} style={{textAlign: "right"}}>
            <div className={style.missionDetails_label}>{t('common.dates.whenItFinishes')}</div>
            <div className={style.missionDetails_entry}>{whenFinish ? format(whenFinish, "dd/MM/yyy") : "-"}</div>
          </Grid>
        </Grid>
        <Grid container style={{paddingTop: 16}} >
          <Grid item xs={12}>
            <div className={style.missionDetails_label}>{t('common.location.where')}</div>
            <div className={style.missionDetails_entry}>{location}</div>
          </Grid>
        </Grid>
        <Grid container style={{paddingTop: 16}}>
          <Grid item xs={12}>
            <TagGroup label={t('common.causes.causeAreas')} tags={causes.map(removeTitle)}/>
          </Grid>
        </Grid>
        <Grid container style={{paddingTop: 16, paddingBottom: 16}}>
          <Grid item xs={12}>
            <TagGroup label={t('common.skills.skills')} tags={skills.map(removeTitle)}/>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12}>
            <div style={{display: "flex", justifyContent: "space-between", cursor: "pointer"}} className={style.missionDetails_label} onClick={() => setChecked(!checked)}>
              <span>{t('create-mission.details.missionDetails')}</span>
              <div style={{transform: `rotate(${checked ? "180deg" : "0deg"})`}}><CaretDownSvg /></div>
            </div>
            <Collapse in={checked}>
              {/*<Grid item xs={12} className={style.mission_header}>*/}
              {/*  <Base64Image src={image} alt={"mission header"} backgroundSize="cover" style={{width: "100%", height: "83px"}} />*/}
              {/*</Grid>*/}
              <div style={{paddingTop: 16}}></div>
              <MarkedRender source={description}/>
              <div style={{paddingBottom: 8}}></div>
            </Collapse>
          </Grid>
        </Grid>

        <Grid container style={{paddingTop: 16,}}>
          <Grid item xs={12}>
            <div className={style.missionDetails_label}>{t('create-mission.details.missionRequirements')}</div>
            <div className={style.missionDetails_entry}>{requirements}</div>
          </Grid>
        </Grid>

      </>
    </WindowBox>

    <Grid container style={{marginTop: 16}}>
      <Grid item xs={12} style={{display: "flex", justifyContent: "space-between"}}>
        <Link href={`/create-mission/point-of-contact`}><SimpleButton label={t('common.buttons.back')} small style="secondary" /></Link>
        <SimpleButton label={t('create-mission.review.launchMission')} style="primary" small onClick={createMission} />
      </Grid>
    </Grid>
  </>
}