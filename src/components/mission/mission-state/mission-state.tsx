import missionWindowStyles from "@/components/window/missions/mission-window.module.css";
import {useTranslation} from "@/i18n/client";


export function MissionState({state= "", display = "flex"}) {
  const {t} = useTranslation()
  const backgroundColor = state === "started" ? "rgba(73, 222, 129, 0.1)" : state === "finished" ? "rgba(58, 189, 248, 0.1)" : "rgba(255, 255, 255, 0.09)"
  const color = state === "started" ? "rgba(255, 255, 255, 1)" : state === "finished" ? "rgba(56, 189, 248, 1)" : "rgba(255, 255, 255, 0.6)";
  const useBall = state === "started";

  return <div className={missionWindowStyles.missionWindow_row_state} style={{display, backgroundColor}}>
    { useBall ? <div className={missionWindowStyles.missionWindow_row_state_circle}></div> : null }
    <div style={{color}}>{t(`missions.state.${state}`)}</div>
  </div>
}