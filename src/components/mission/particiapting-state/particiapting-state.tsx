import missionWindowStyles from "@/components/window/missions/mission-window.module.css";
import {useTranslation} from "@/i18n/client";

export function ParticipatingState({state = ""}) {
  const {t} = useTranslation();
  const backgroundColor = state === "accepted" ? "rgba(73, 222, 129, 0.1)" : state === "rejected" ? "rgba(58, 189, 248, 0.1)" : "rgba(255, 255, 255, 0.09)"
  const color = state === "accepted" ? "rgba(255, 255, 255, 1)" : state === "rejected" ? "rgba(56, 189, 248, 1)" : "rgba(255, 255, 255, 0.6)";
  const useBall = state === "accepted";

  return <div className={missionWindowStyles.missionWindow_row_state} style={{backgroundColor, marginRight: 8}}>
    { useBall ? <div className={missionWindowStyles.missionWindow_row_state_circle}></div> : null }
    <div style={{color}}>{t(`missions.participationText.small.${state}`)}</div>
  </div>
}