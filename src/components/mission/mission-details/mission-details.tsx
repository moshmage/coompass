"use client";

import style from "@/components/mission/mission-details/mission-details.module.css";
import {TagGroup} from "@/components/tag-group/tag-group";
import {format} from "date-fns";
import {ReactNode} from "react";
import {useTranslation} from "@/i18n/client";


export function MissionDetails({
                                 causes = [] as string[],
                                 when = null as string|ReactNode|null,
                                 where = "",
                                 datePosted = new Date(),
                                 dateStarted = null as Date|null,
                                 dateFinished = null as Date|null,
                                 skills = [] as string[],
                                 requirements = "", reward = 0
}) {

  const {t} = useTranslation();

  const dateDiv = (label: string, date: Date) =>
    <div>
      <div className={style.missionDetails_label}>{label}</div>
      <div className={style.missionDetails_entry}>{format(date, "dd/MM/yyyy")}</div>
    </div>

  return <div className={style.missionDetails_wrapper}>
    <TagGroup label={t('common.causes.causeAreas')} tags={causes}/>
    <div>
      <div className={style.missionDetails_label}>{t('common.dates.whenItStarts')}</div>
      <div className={style.missionDetails_entry}>{when}</div>
    </div>
    <div>
      <div className={style.missionDetails_label}>{t('missions.requiredHours')}</div>
      <div className={style.missionDetails_entry}>{reward}</div>
    </div>
    <div>
      <div className={style.missionDetails_label}>{t('common.location.where')}</div>
      <div className={style.missionDetails_entry}>{where}</div>
    </div>
    {dateDiv(t('missions.postedOn'), datePosted || new Date())}
    <TagGroup label={t('common.skills.skills')} tags={skills}/>
    {dateStarted ? dateDiv(t('common.dates.dateStarted'), dateStarted) : null}
    {dateFinished ? dateDiv(t('common.dates.dateFinished'), dateFinished) : null}
    <div>
      <div className={style.missionDetails_label}>{t('create-mission.details.missionRequirements')}</div>
      <div className={style.missionDetails_entry}>{requirements || "-"}</div>
    </div>
  </div>
}