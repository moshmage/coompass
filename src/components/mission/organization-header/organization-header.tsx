import style from "@/components/mission/organization-header/org-header.module.css";
import {Grid} from "@mui/material";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {createTranslation} from "@/i18n/server";

export async function OrganizationHeader({ id = "",
                                     image = "",
                                     orgName = "Organization name",
                                     description = "Here be dragons", missionId = ""}) {
  const {t} = await createTranslation();

  return <Grid container style={{paddingBottom: 32, marginBottom: 32, borderBottom: "1px solid rgba(255, 255, 255, 0.12)"}}>
    <Grid item xs={2} className={style.organizationHeader_logo}>
      <Base64Image src={image} alt={"org logo"} />
    </Grid>
    <Grid item xs={10}>
      <Grid container style={{marginBottom: 16}}>
        <Grid item xs={12} style={{display: "flex", justifyContent: "space-between"}}>
          <h3 className={style.organizationHeader_title}>{orgName}</h3>
          <Link href={`/organization/${id}/mission-list`}><SimpleButton small style="secondary" label={t('missions.viewAllMissions')} /></Link>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <span className={style.organizationHeader_description}>{description}</span>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12} className={style.organizationHeader_learn_more}>
          <Link href={`/organization/${id}/overview?back=${missionId}`}>{t('missions.learnMore')}</Link>
        </Grid>
      </Grid>
    </Grid>
  </Grid>
}