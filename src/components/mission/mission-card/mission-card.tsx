import {Grid} from "@mui/material";
import Image from "next/image";

import style from "@/components/mission/mission-card/mission-card.module.css";
import Link from "next/link";
import {format,} from "date-fns";
import {cleanMarkdown} from "@/handlers/utils/clean-markdown";

import locationImage from "@/assets/location.svg";
import {MissionState} from "@/components/mission/mission-state/mission-state";
import {MissionState as MissionStateType} from "@/types/mission/mission";
import {useTranslation} from "@/i18n/client";
import {ParticipatingState} from "@/components/mission/particiapting-state/particiapting-state";

export function MissionCard({
                              name = "Mission name",
                              id = "",
                              missionImage = "",
                              location = "online",
                              orgName = "",
                              createdDate = new Date(),
                              description = "Here be dragons",
                              state = "" as MissionStateType,
                              participatingState = "", fromOrg = ""
                            }) {
  const {t} = useTranslation()
  const cleaned = cleanMarkdown(description);

  return <Link href={`/missions/${id}${fromOrg ? `?fromOrg=${fromOrg}` : ""}`}>
    <Grid container className={style.missionCard}>
      {/*<Grid container>*/}
      {/*  <Grid item xs={12} className={style.missionCard_image}>*/}
      {/*    <Base64Image src={missionImage} alt={"mission image"} backgroundSize="cover"/>*/}
      {/*  </Grid>*/}
      {/*</Grid>*/}
      <Grid container style={{display: "flex", justifyContent: "space-between", alignItems: "center", marginBottom: 16}} spacing={1}>
        <Grid item xs={12} className={style.missionCard_title_location}>
          <div className={style.missionCard_title}>
            <span>{name}</span>
          </div>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <span className={style.missionCard_org}>{t('common.organization')}: </span>
          <span className={style.missionCard_org_name}>{orgName}</span>
        </Grid>
      </Grid>
      <Grid container className={style.missionCard_description}>
        <Grid item xs={12} title={cleaned}>
          <span>{cleaned}</span>
        </Grid>
      </Grid>
      <Grid container style={{marginBottom: 16, marginTop: 8}}>
        <Grid item xs={12} style={{display: "flex", alignItems: "center", height: 20}}>
          { participatingState ? <ParticipatingState state={participatingState!} /> : null }
          <MissionState state={state} />
        </Grid>
      </Grid>
      <Grid container style={{alignItems: "end"}}>
        <Grid item xs={12} sm={12} md={6} style={{display: "flex",}}>
          <div className={style.missionCard_location}><Image src={locationImage} alt={""} style={{marginRight: 8}} /> {location?.toLowerCase()?.includes("virtual") ? t('common.location.virtual') : t('common.location.inPerson')}</div>
        </Grid>
        <Grid item xs={12} sm={12} md={6} style={{display: "flex", justifyContent: "end", alignItems: "center", height: 20}}>
          <div className={style.missionCard_date}>{t('missions.postedOn')}</div>
          <div className={style.missionCard_date_time}>{format(createdDate, "dd/MM/yyyy")}</div>
        </Grid>
      </Grid>
    </Grid>
  </Link>

}