"use client";

import missionWindowStyles from "@/components/window/missions/mission-window.module.css";
import {useState} from "react";
import {PendingUsersModal} from "@/components/mission/mission-pending-users/modal/pending-users-modal";
import {useRouter} from "next/navigation";

export function MissionPendingUsers({missionId = "", pendingUsers = 0, display = "flex"}) {
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const onClose = () => {
    setOpen(false);
    router.refresh();
  }

  return <>
    <div className={missionWindowStyles.missionWindow_row_state} style={{display, cursor: "pointer"}} onClick={() => setOpen(true)}>
      <div>{pendingUsers}</div>
      <div>Pending</div>
    </div>
    <PendingUsersModal missionId={missionId} open={open} onClose={onClose} />
  </>
}