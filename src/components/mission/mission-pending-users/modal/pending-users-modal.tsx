"use client";

import {useEffect, useState} from "react";
import {User} from "@/types/user/user";
import axios from "axios";
import {Modal} from "@mui/material";
import WindowBox from "@/components/window/window-box";
import {PendingUserRow} from "@/components/mission/mission-pending-users/pending-user-row/pending-user-row";

export function PendingUsersModal({missionId = "", open = false, onClose = () => {}}) {
  const [pendingUsers, setPendingUsers] = useState<User[]>([]);
  const getPendingUsers = () =>
    axios.get(`/api/missions/${missionId}/participants-state?state=pending`)
      .then(response => response.data.map(({user}: {user: User}) => user).flat())
      .then(users => {
        setPendingUsers(users);
        if (!users.length)
          onClose();
      })
      .catch(reason => console.log(reason));

  useEffect(() => {
    if (open)
      getPendingUsers();
  }, [open])

  return <Modal open={open} onClose={onClose}>
    <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: "786px"}}>
      <WindowBox title={`Pending users for ${missionId}`}>
        {pendingUsers.map((user, key) =>
          <PendingUserRow key={key} missionId={missionId} user={user} missionTitle={""} refresh={() => getPendingUsers()} />)}
      </WindowBox>
    </div>
  </Modal>
}