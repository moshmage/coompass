"use client";

import {User} from "@/types/user/user";
import axios from "axios";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {formatDistanceToNow} from "date-fns";
import styles from "@/components/mission/mission-pending-users/pending-user-row/pending-users-row.module.css"
import Link from "next/link";
import {useSnackbar} from "notistack";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {useTranslation} from "@/i18n/client";

type PendingUserRowType = {
  user: User,
  missionId: string;
  missionTitle: string;
  refresh: () => void;
  createdAt?: Date;
}

export function PendingUserRow({
                                 user, refresh, missionId = "",
                                 createdAt = new Date(), missionTitle
}: PendingUserRowType) {

  const {t} = useTranslation()
  const {enqueueSnackbar} = useSnackbar();

  const changePendingState = (state: "accepted" | "rejected", userId: string) => {
    enqueueSnackbar(`${state === "accepted" ? "Approving" : "Rejecting"} ${userId}`, {variant: "info"})
    axios.patch(`/api/missions/${missionId}/participants-state`, {state, userId})
      .then(() => {
        refresh()
        enqueueSnackbar(`${state === "accepted" ? "Approved" : "Rejected"} ${userId}`, {variant: "success"})
      })
      .catch(e => {
        console.log(e?.message)
        enqueueSnackbar(e?.response?.data?.message || "Failed to change state", {variant: "warning"})
      })
  }

  return <div className={styles.pendingUsersRow_wrapper}>
    <div className={styles.pendingUsersRow_content}>
      <div><Base64Image src={user.profileImage} style={{borderRadius: "100%", marginTop: 3, height: 16, width: 16, backgroundColor: "gray", marginRight: 8}}/></div>
      <div className={styles.pendingUsersRow_title}>{user.username || user.email}</div>
      <div style={{marginLeft: "auto"}} className={styles.pendingUsersRow_action}>{t('organization.pending.requestToJoin')}</div>
      <div className={styles.pendingUsersRow_action}>{t('common.dates.distance', {distance: formatDistanceToNow(createdAt)})}</div>
      <div style={{marginRight: 8}}>
        <SimpleButton small style="secondary" label={t('common.buttons.approve')} onClick={() => changePendingState("accepted", user.id)}/>
      </div>
      <div><SimpleButton small style="secondary" label={t('common.buttons.reject')} onClick={() => changePendingState("rejected", user.id)}/></div>
    </div>
    <div className={styles.pendingUsersRow_action}>
      <Link href={`/missions/${missionId}/`}>{missionTitle}</Link>
    </div>
  </div>
}