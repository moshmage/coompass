"use client";

import {Grid} from "@mui/material";
import {SimpleTable} from "@/components/table/simple-table/simple-table";
import {format} from "date-fns";
import {MissionState} from "@/components/mission/mission-state/mission-state";
import {useMissionsWithParticipantsOf} from "@/components/hooks/queries/use-missions-with-participants-of";
import {useEffect, useState} from "react";
import Link from "next/link";
import {useTranslation} from "@/i18n/client";
import {useLocale} from "@/i18n/hooks/locale-provider";

export function CompanyMissionsTable({companyId}: { companyId: string }) {
  const [rows, setRows] = useState<any[]>([]);
  const [columns, setColumns] = useState<string[]>([]);
  const {data: missions, isFetching: loading} = useMissionsWithParticipantsOf(companyId);
  const {t} = useTranslation();
  const lng = useLocale();

  const row = (missionId: string, missionTitle: string, orgId: string, orgName: string, createdDate: Date, startDate: Date, finishedDate: Date, State: string) =>
    ({
      [t('company.missionsTable.columns.mission')]: <Link href={`/missions/${missionId}/`}>{missionTitle}</Link>,
      [t('company.missionsTable.columns.organization')]: <Link href={`/organization/${orgId}/overview`}>{orgName}</Link>,
      [t('company.missionsTable.columns.createdDate')]: format(createdDate || new Date(), "dd/MM/yyyy"),
      [t('company.missionsTable.columns.startedDate')]: startDate ? format(startDate, "dd/MM/yyyy") : "-",
      [t('company.missionsTable.columns.finishedDate')]: finishedDate ? format(finishedDate, "dd/MM/yyyy") : "-",
      [t('company.missionsTable.columns.state')]: <MissionState display="inline-flex" state={State}/>
    });


  useEffect(() => {
    if (!missions.length)
      return;

    setColumns([
      t('company.missionsTable.columns.mission'),
      t('company.missionsTable.columns.organization'),
      t('company.missionsTable.columns.createdDate'),
      t('company.missionsTable.columns.startedDate'),
      t('company.missionsTable.columns.finishedDate'),
      t('company.missionsTable.columns.state')
    ])

    setRows(
      missions.map(({id, title, createdAt, startedAt, finishedAt, state, company}) =>
        row(id, title, company?.id!, company?.name!, createdAt, startedAt, finishedAt, state))
    )
  }, [missions, lng])


  return <>
    <Grid container>
      <Grid item xs={12}>
        <SimpleTable columns={columns} rows={rows} isLoading={loading}/>
      </Grid>
    </Grid>
  </>
}