"use client";

import {Collapse, Grid} from "@mui/material";
import {useTranslation} from "@/i18n/client";
import {useCompanies} from "@/components/hooks/queries/use-companies";
import {UserTypes} from "@/types/user/user";
import React, {useEffect, useState} from "react";
import {Company} from "@/types/company/company";
import {useCompany} from "@/components/hooks/queries/use-company";
import {addPartnership} from "@/handlers/client/company/add-partnership";
import {useSnackbar} from "notistack";
import {deletePartnership} from "@/handlers/client/company/delete-partnership";
import {OrganizationsList} from "@/components/organizations/organizations-list";
import {CaretDownSvg} from "@/components/svg/caret-down-svg";

export function CompanyPartnerships({companyId = ""}) {
  const {t} = useTranslation();
  const {data: organizations, isFetching: loadingOrgs} = useCompanies({type: UserTypes.ngo});
  const {data: company, refetch, isFetching: loadingCompany} = useCompany(companyId);
  const {enqueueSnackbar} = useSnackbar();

  const [availableOrgs, setAvailableOrgs] = useState<Company[]>([]);
  const [partnerOrgs, setPartnerOrgs] = useState<Company[]>([]);
  const [availableListOpen, setAvailableListOpen] = useState<boolean>(false);

  const updateAvailableAndPartnerOrgs = (organizations: Company[], company: Company) => {
    if (!organizations || !company)
      return;

    const partnerOrgsIds = company.partnerOrgs.map(({id}) => id);

    setAvailableOrgs(organizations.filter(({id}) => !partnerOrgsIds.includes(id)));
    setPartnerOrgs(company.partnerOrgs);
  }

  const removePartner = (id: string) => {
    enqueueSnackbar(t('common.snackbars.info.removingPartnership'), {variant: "info"});
    deletePartnership(companyId, id)
      .then(() => {
        refetch();
        enqueueSnackbar(t('common.snackbars.success.partnershipRemoved'), {variant: "success"})
      })
      .catch((err) => {
        console.error(err);
        enqueueSnackbar(t('common.snackbars.warning.failedToRemovePartnership', {message: err?.response?.data?.message || "Failed add partnership"}), {variant: "warning"})
      })
  }

  const addPartner = (id: string) => {
    enqueueSnackbar(t('common.snackbars.info.creatingPartnership'), {variant: "info"});

    addPartnership(companyId, id)
      .then(() => {
        refetch();
        enqueueSnackbar(t('common.snackbars.success.partnershipCreated'), {variant: "success"})
      })
      .catch((err) => {
        console.error(err);
        enqueueSnackbar(t('common.snackbars.warning.failedToCreatePartnership', {message: err?.response?.data?.message || "Failed add partnership"}), {variant: "warning"})
      });
  }

  useEffect(() => { updateAvailableAndPartnerOrgs(organizations, company!); }, [organizations, company]);

  return <>
    <Grid container>
      <Grid item xs={12}>
        <h1>{t('partnerships.yourCompanyPartnerships')}</h1>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12}>
        <OrganizationsList companies={partnerOrgs} loading={loadingOrgs} removePartnerButton={removePartner} />
      </Grid>
    </Grid>
    <Grid container marginTop={"32px"}>
      <Grid item xs={12} display={"flex"} className={"cursor-pointer"}
            justifyContent={"start"}
            onClick={_ => setAvailableListOpen(!availableListOpen)}>
        <h2>{t('partnerships.availableOrgsForPartnerships')}</h2>
        <div style={{transform: `rotate(${availableListOpen ? "180deg" : "0deg"})`, alignSelf: "end", marginLeft: "1rem"}}><CaretDownSvg/></div>
      </Grid>
    </Grid>
    <Collapse in={availableListOpen}>
      <Grid container>
        <Grid item xs={12}>
          <OrganizationsList companies={availableOrgs} loading={loadingCompany||loadingOrgs} addPartnerButton={addPartner} />
        </Grid>
      </Grid>
    </Collapse>
  </>
}