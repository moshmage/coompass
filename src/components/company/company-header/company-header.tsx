"use client";

import {Grid} from "@mui/material";
import {CompanyHeaderLogo} from "@/components/company/comany-header-logo/company-header-logo";
import {CompanyHeaderEmployees} from "@/components/company/company-header-employees/company-header-employees";
import {useMissionsWithParticipantsOf} from "@/components/hooks/queries/use-missions-with-participants-of";
import {useCompany} from "@/components/hooks/queries/use-company";
import {useCompanyUsers} from "@/components/hooks/queries/use-company-users";

export function CompanyHeader({companyId = ""}) {

  const {data: missions} = useMissionsWithParticipantsOf(companyId);
  const {data: company} = useCompany(companyId);
  const {data: users} = useCompanyUsers(companyId);

  return <>
    <Grid container style={{alignItems: "center"}} spacing={1}>
      <Grid item xs={12} sm={12} md={5} lg={4} xl={3}>
        <CompanyHeaderLogo image={company?.image} companyName={company?.name}/>
      </Grid>
      <Grid item xs={0} sm={0} md={2} lg={4} xl={6}/>
      <Grid item xs={12} sm={12} md={5} lg={4} xl={3}>
        <CompanyHeaderEmployees missions={missions.length}
                                companyId={company?.id}
                                employees={users.length ? users.length - 1 : 0} />
      </Grid>
    </Grid>
  </>
}