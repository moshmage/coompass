import WindowBox from "@/components/window/window-box";

import usersThree from "@/assets/usersThree.svg";

import style from "@/components/company/company-header-employees/company-header-employees.module.css";
import Link from "next/link";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {useTranslation} from "@/i18n/client";

export function CompanyHeaderEmployees({employees = 0, missions = 0, companyId = ""}) {
  const {t} = useTranslation()
  return <WindowBox icon={usersThree}
                    title="Employees"
                    action={<Link href={`/company/${companyId}/employees`}><SimpleButton label={t('company.header.viewAll')} small style="secondary" /></Link>}>
    <div style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
      <div>
        <span className={style.companyHeaderEmployees_number}>{missions}</span>
        <span className={style.companyHeaderEmployees_text}>{t('company.header.employees.activeMissions')}</span>
      </div>
      <div>
        <span className={style.companyHeaderEmployees_number}>{employees}</span>
        <span className={style.companyHeaderEmployees_text}>{t('company.header.employees.employeesInTotal')}</span>
      </div>
    </div>
  </WindowBox>
}