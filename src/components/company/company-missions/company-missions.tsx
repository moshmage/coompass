"use client";
import {useMissionsWithParticipantsOf} from "@/components/hooks/queries/use-missions-with-participants-of";
import MissionWindow from "@/components/window/missions/mission-window";

export function CompanyMissions({companyId = ""}) {
  const {data: missions} = useMissionsWithParticipantsOf(companyId);

  return <MissionWindow missions={missions}/>
}