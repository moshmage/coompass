"use client";

import {useQuery, useQueryClient} from "@tanstack/react-query";
import axios from "axios";
import {User} from "@/types/user/user";
import {format} from "date-fns";
import {Grid} from "@mui/material";
import {SimpleTable} from "@/components/table/simple-table/simple-table";
import {QueryKeys} from "@/constants/query-keys";
import {useSelf} from "@/components/hooks/use-self";
import {useTranslation} from "@/i18n/client";
import {useEffect, useState} from "react";
import {useLocale} from "@/i18n/hooks/locale-provider";
import {useSearchParams} from "next/navigation";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Structural from "@/components/structural/structural";
import {manageMembership} from "@/handlers/client/company/manage-membership";
import {enqueueSnackbar} from "notistack";
import {getErrorMessage} from "@/handlers/utils/get-error-message";

export function EmployeesList({companyId = ""}) {
  const {t} = useTranslation();
  const {userData} = useSelf();
  const [rows, setRows] = useState<any[]>([]);
  const [columns, setColumns] = useState<string[]>();
  const lng = useLocale();
  const query = useSearchParams();
  const queryClient = useQueryClient();

  const getEmployees = () =>
    axios.get<User[]>(`/api/company/${companyId}/users`)
      .then(({data}) => data)

  const {data: users, isFetching: loadingUsers } =
    useQuery({queryKey: QueryKeys.companyUsers, queryFn: getEmployees, initialData: [], staleTime: 0});

  const manageCompanyMembership = (userId: string, accept: boolean) => {
    enqueueSnackbar(t('common.snackbars.info.managingCompanyMembership'), {variant: "info"});

    manageMembership(companyId, {userId, accept})
      .then(() => {
        enqueueSnackbar(t(`common.snackbars.success.${accept ? "manageCompanyMembershipAccepted" : "manageCompanyMembershipRejected"}`), {variant: "success"});
        queryClient.invalidateQueries({queryKey: QueryKeys.companyUsers});
      })
      .catch(e => {
        enqueueSnackbar(t('common.snackbars.warning.failedToManageMembershipCompany', getErrorMessage(e)), {variant: "warning"});
      });
  }

  const row = (Employee: string, joinDate: string, lastLogin: string, active: number, complete: number, pending: boolean, userId: string) =>
    ({
      [t('company.employeesTable.columns.Employee')]: Employee,
      [t('company.employeesTable.columns.joinDate')]: joinDate,
      [t('company.employeesTable.columns.lastLogin')]: lastLogin,
      [t('company.employeesTable.columns.activeMissions')]: active,
      [t('company.employeesTable.columns.completeMissions')]: complete,
      [t('common.actions')]:
        <Structural>
          <Structural.If condition={pending}>
            <div style={{display: "flex", gap: 6}}>
              <SimpleButton label={t('common.buttons.approve')} small onClick={() => manageCompanyMembership(userId, true)}/>
              <SimpleButton label={t('common.buttons.reject')} small style="secondary" onClick={() => manageCompanyMembership(userId, false)}/>
            </div>
          </Structural.If>
          <Structural.Else>
            <SimpleButton label={t('common.buttons.remove')} small style="secondary" onClick={() => manageCompanyMembership(userId, false)}/>
          </Structural.Else>
        </Structural>
    });

  useEffect(() => {
    setColumns([
      t('company.employeesTable.columns.Employee'),
      t('company.employeesTable.columns.joinDate'),
      t('company.employeesTable.columns.lastLogin'),
      t('company.employeesTable.columns.activeMissions'),
      t('company.employeesTable.columns.completeMissions'),
      t('common.actions')
    ])

    setRows(
      users
        .filter(({id}) => id !== userData?.id)
        .map(({username, createdAt, lastLogin, missions, associatedCompanies, id}) => {
          return row(
            username || t('user.logs.noUsername'),
            createdAt ? format(createdAt, "dd/MM/yyyy") : "-",
            lastLogin ? format(lastLogin, "dd/MM/yyyy") : "-",
            missions?.filter(({state}) => state === "started")?.length || 0,
            missions?.filter(({state}) => state === "finished")?.length || 0,
            !!associatedCompanies?.[0]?.UserCompany?.pending,
            id
          )
        })
    )
  }, [users, lng])

  return <>
    <Grid container>
      <Grid item xs={12}>
        <SimpleTable enableSearch
                     defaultSearch={query.get("search") || ""}
                     columns={columns!}
                     rows={rows}
                     isLoading={loadingUsers}/>
      </Grid>
    </Grid>
  </>
}