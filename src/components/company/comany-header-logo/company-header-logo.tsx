import style from "@/components/company/comany-header-logo/company-header.module.css";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import Link from "next/link";
import {CircularProgress} from "@mui/material";
import {useTranslation} from "@/i18n/client";
import {HourGlass} from "@/components/svg/hour-glass";
import Structural from "@/components/structural/structural";
import {CheckCircle} from "@/components/svg/check-circle";

export function CompanyHeaderLogo({
                                    image = "",
                                    alignRight = false,
                                    companyName = "",
                                    pending = false,
                                    link = false}) {

  const {t} = useTranslation();

  return <div>
    <div className={style.companyHeader_title} style={{textAlign: alignRight ? "right" : "left"}}>
      {
        !link ? t('organization.yourCompany') : <Link href="/company/join">{t('dashboard.user.company.header.joinCompany')}</Link>
      }
    </div>
    <div className={style.companyHeader_wrapper}>
      {!image ? <CircularProgress/> : <Base64Image className={style.companyHeader_image} src={image} alt="company logo"/>}
      <div className={style.companyHeader_name}>
        <div style={{position: "absolute", top: 10, left: "90%"}}
             title={pending ? "Membership pending" : "Membership accepted"}>
          <Structural>
            <Structural.If condition={pending}><HourGlass/></Structural.If>
            <Structural.Else><CheckCircle/></Structural.Else>
          </Structural>
        </div>
    {companyName}
  </div>
  <div className={style.companyHeader_kpi}>{t('company.header.viewKPI')}</div>
    </div>
  </div>
}