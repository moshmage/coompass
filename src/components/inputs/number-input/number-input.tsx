"use client";

import {MinusSvg} from "@/components/svg/minus-svg";
import {PlusSvg} from "@/components/svg/plus-svg";
import React, {useRef} from "react";

import styles from "@/components/inputs/text-input/text-input.module.css"

type NumberInputProps = {
  label?: string;
  defaultValue?: number;
  onValueChange?: (value: number) => void;
  min?: number;
  max?: number;
  name?: string;
}

export function NumberInput({
                              defaultValue = 0,
                              label = "",
                              name = "",
                              onValueChange,
                              min,
                              max}: NumberInputProps) {
  const inputRef = useRef<HTMLInputElement>(null);

  const onChange = (value: string) => {
    let _value = +value.replace(/\D/g, "");

    if (min !== undefined && _value < min)
      _value = min;
    if (max !== undefined && _value > max)
      _value = max;

    if (inputRef.current)
      inputRef.current.value = _value.toString();

    onValueChange?.(_value);
  }

  return <div className={styles.textInput}>
    {label && <label htmlFor={name}>{label}</label> || ""}
    <div className={styles.textInput_input}>
      <div style={{cursor: "pointer", zIndex: 1}} className={styles.textInput_icon}
           onClick={() => onChange((+(inputRef.current?.value || 0) - 1).toString())}><MinusSvg/></div>
      <input defaultValue={defaultValue}
             style={{textAlign: "center", margin: "-8px -40px"}}
             ref={inputRef}
             id={name}
             name={name}
             onChange={e => onChange(e.target.value)}/>
      <div style={{cursor: "pointer", zIndex: 1}} className={styles.textInput_icon}
           onClick={() => onChange((+(inputRef.current?.value || 0) + 1).toString())}><PlusSvg/></div>
    </div>

  </div>
}