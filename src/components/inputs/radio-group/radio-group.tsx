import elementStyle from "@/components/inputs/radio-group/radio-group.module.css";
import {useState} from "react";

type RadioGroupOption = {
  label: string;
  value: string;
};

type RadioGroupProps = {
  onChange: (option: RadioGroupOption) => void;
  options: RadioGroupOption[];
  name: string;
};

export function RadioGroup({ options, onChange, name }: RadioGroupProps) {
  const [selectedValue, setSelectedValue] = useState<string | null>(null);

  const handleChange = (value: string) => {
    const selectedOption = options.find(option => option.value === value);
    if (selectedOption) {
      setSelectedValue(value);
      onChange(selectedOption);
    }
  };

  return (
    <div style={{display: "flex", gap: 16}}>
      {options.map(option => (
        <div key={option.value}
             className={elementStyle.radio}
             onClick={() => handleChange(option.value)}>
          <input type="radio"
                 name={name}
                 value={option.value}
                 checked={selectedValue === option.value}
                 onChange={() => handleChange(option.value)}/>
          <span className={elementStyle.radio_checkmark}></span>
          <span className={elementStyle.radio_labelText}>{option.label}</span>
        </div>
      ))}
    </div>
  );
}