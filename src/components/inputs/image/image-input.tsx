import NextImage from "next/image";
import user from "@/assets/user.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import inputStyle from "@/components/inputs/text-input/text-input.module.css";
import React, {useEffect, useRef, useState} from "react";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {useTranslation} from "@/i18n/client";

export function ImageInput({
                             validWidth = 0,
                             validHeight = 0, maxSizeKB = 500,
                             onChange = ((_image: string, _valid: boolean) => {}),
                             img = "",
                             hidePreview = false,
}) {
  const [image, setImage] = useState(img);
  const [validImage, setValidImage] = useState<boolean>(!!img);
  const fileInputRef = useRef<HTMLInputElement>(null);
  const [validKb, setValidKb] = useState(true);
  const {t} = useTranslation();
  const handleFileChange = (file?: File) => {
    if (!file)
      return;

    // const sizeInKB = file.size / 1024;
    // setValidKb(sizeInKB <= maxSizeKB);

    const reader = new FileReader();
    reader.onload = () => {
      const _image = new Image();

      _image.onload = () => {
        // const {naturalHeight, naturalWidth} = _image;
        // const isImageValid = naturalHeight >= validHeight && naturalWidth >= validWidth && !(sizeInKB > maxSizeKB);
        setValidImage(true)
        onChange(reader.result as string, true);
      }

      _image.src = reader.result as string;
      setImage(reader.result as string);
    }

    reader.readAsDataURL(file);
  }

  useEffect(() => {
    if (image)
      return;

    setImage(img);
    setValidImage(!!img);
  }, [img]);

  return <>
    <div style={{display: "flex", alignItems: "center"}}>
      <input hidden
             id="imageUpload"
             ref={fileInputRef}
             type="file"
             accept=".png, .jpg, .jpeg"
             onChange={e => handleFileChange(e.target.files?.[0])}/>
      <SimpleButton label="Upload" style="secondary" onClick={() => fileInputRef?.current?.click()}/>
      {!hidePreview
        ? image
          ? image.startsWith("data:image")
            ? <Base64Image src={image} backgroundSize={"cover"} alt={""} style={{width: 60, height: 60, borderRadius: "100%", backgroundColor: "gray", marginLeft: "1rem"}}/>
            : <NextImage src={image} width={40} height={60} alt={""} style={{width: 60, height: 60, borderRadius: "100%", backgroundColor: "gray", marginLeft: "1rem"}}/>
          : <NextImage src={user} width={28} height={28} alt={""}/>
        : null
      }
    </div>
    {
      !validImage && image
        ?
        <div className={inputStyle.horizontal_input_textarea_length} style={{color: "red"}}>
          {!validKb
            ? t('common.image.imageSizeShouldBeLessThan', {value:maxSizeKB})
            : t('common.image.providedImageIsLowerThan', {width: validWidth, height: validHeight})}
        </div>
        : ""}
  </>
}