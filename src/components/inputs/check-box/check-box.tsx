import elementStyle from "@/components/inputs/check-box/checkbox.module.css";

type CheckBoxProp = {
  label: string;
  onChange?: (v: boolean) => void;
  isChecked?: boolean;
}

export function CheckBox({onChange, label = "", isChecked}: CheckBoxProp) {

  return <div className={elementStyle.checkbox} onClick={_ => onChange?.(!isChecked)}>
    <input
      type="checkbox"
      defaultChecked={isChecked} />
    <span className={elementStyle.checkbox_checkmark}></span>
    <span className={elementStyle.checkbox_labelText}>{label}</span>
  </div>
}