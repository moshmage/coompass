import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {useEffect, useState} from "react";
import {format, parseISO} from "date-fns";
import {useMask} from "@react-input/mask";

export function DateInput({defaultDate, label, onValidDate}: {defaultDate?: Date, label?: string; onValidDate: (date: Date|null) => void}) {
    const [validDate, setIsValidDate] = useState<boolean|null>(null);
    const [value, setValue] = useState<string>("");

  const mask = useMask({
    mask: "DD / MM / YYYY",
    replacement: {D: /\d/, M: /\d/, Y: /\d/},
  })

  const handleInputChange = (e: any) => {
    setValue(e.target.value);

    if (e.target.value.replace(/\D/g, "").length === 8) {
      const date = e.target.value.replace(/\s/g, "").split("/").reverse().join("-");
      const _validDate = !Number.isNaN(parseISO(date)?.getDate())

      if (_validDate)
        onValidDate(new Date(date));

      setIsValidDate(_validDate);
    } else {
      setIsValidDate(null);
      onValidDate(null)
    }
  }

  useEffect(() => {
    if (!mask || !mask.current || !defaultDate)
      return;

    setValue(format(defaultDate, "dd / MM / yyyy"));
  }, [defaultDate]);

  return <>
    <SimpleTextInput reference={mask} type="date"
                     onValueChange={handleInputChange}
                     label={label}
                     value={value}
                     placeHolder="DD / MM / YYYY" />

    <div style={{color: "red", fontSize: 12}}>{validDate === false ? "Date is invalid" : ""}</div>
  </>;
}