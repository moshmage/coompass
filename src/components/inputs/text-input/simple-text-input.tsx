import React, {ChangeEventHandler, FocusEvent, ReactNode} from 'react';
import styles from "@/components/inputs/text-input/text-input.module.css"
import Image from "next/image";

import info from "@/assets/info.svg";

export type InputProps = {
  defaultValue?: string | readonly string[] | number;
  label?: string;
  placeHolder?: string;
  type?: string;
  pattern?: string;
  name?: string;

  beforeIcon?: string | ReactNode;
  beforeIconIsNode?: boolean;
  afterIcon?: string;
  beforeText?: string;

  onValueChange?: ChangeEventHandler<HTMLInputElement>;

  // @ts-ignore
  helperInfo?: string;

  value?: string;

  onFocus?: (v: boolean) => void;
  onBlur?: (e: FocusEvent<HTMLInputElement>) => void;

  required?: boolean;

  small?: boolean;
  disabled?: boolean;

  reference?: any;
}

export function SimpleTextInput({
                                  label = "",
                                  placeHolder = "",
                                  beforeIcon,
                                  beforeIconIsNode,
                                  afterIcon,
                                  beforeText,
                                  type = "text",
                                  pattern,
                                  onValueChange,
                                  defaultValue,
                                  name = "",
                                  helperInfo,
                                  onFocus, onBlur, value, required, small = false, disabled = false, reference,
                                }: InputProps) {

  const size = small ? styles.textInput_input_small : styles.textInput_input;

  const onBeforeTextClick = beforeText
    ? () => (document.querySelector(`input[name="${name}"]`) as HTMLInputElement)?.select()
    : undefined

  return (
    <div className={styles.textInput}>
      {label && <label htmlFor="inputField" className={`${required ? "required" : ""}`}>{label}</label> || ""}
      <div className={size}>
        {
          beforeIcon ?
            !beforeIconIsNode
              ? <Image className={styles.textInput_icon} src={beforeIcon as string} alt={"before icon"}/>
              : beforeIcon
            : ""
        }
        {beforeText ?
          <div className={styles.textInput_before_text} onClick={onBeforeTextClick}>{beforeText}</div> : null}
        <input
          id={name || "inputField"}
          defaultValue={defaultValue}
          placeholder={placeHolder}
          type={type}
          pattern={pattern}
          onChange={onValueChange}
          name={name}
          value={value}
          disabled={disabled}
          {...reference ? {ref: reference} : {}}
          {...onFocus ? {onFocus: () => onFocus(true)} : {}}
          {...(onFocus || onBlur) ? {onBlur: (e) => {
              onFocus?.(false); onBlur?.(e)
            }} : {}}
        />
        {afterIcon && <Image className={styles.textInput_icon} src={afterIcon} alt={"before icon"}/> || ""}
      </div>
      {helperInfo
        // ? <div className={styles.textInput_helper}> <Image src={info} alt={"info"} width={18} height={18}/> {helperInfo} </div>
        ? <div className={styles.textInput_helper}> {helperInfo} </div>
        : null
      }

    </div>
  );
}