"use client";

import styles from "@/components/switcher/switcher-button.module.css"
import {useEffect, useState} from "react";
import {useRouter} from "next/navigation";
import {useTranslation} from "@/i18n/client";
import {useLocale} from "@/i18n/hooks/locale-provider";

type SwitcherOption = {
  label: string,
  url?: string,
  value?: string,
}

export function SwitcherButton({
                                 label = "", options = [] as SwitcherOption[],
                                 activeLabel = "",
                                 name = "",
                                 onToggle = (label: string, value?: string) => {}}) {

  const [active, setActive] = useState(activeLabel);
  const [value, setValue] = useState<string>();
  const router = useRouter();
  const {t} = useTranslation();
  const lng = useLocale();

  const isActive = (label: string) => active === label && styles.switcherButton_button_active || "";
  const classFor = (label: string) => `${styles.switcherButton_button} ${isActive(label)}`;

  const toggleActive = (label: string, index: number) => {
    setActive(label);
    onToggle(label, options[index].value !== undefined ? options[index].value : undefined);

    if (options[index].value)
      setValue(options[index].value);

    if (options[index].url)
      router.push(options[index].url!)

    console.log(`options[index].value`,options[index].value)
  }

  useEffect(() => {
    console.log(`ACTIVE LABEL`, activeLabel)
    setActive(activeLabel);
  }, [lng])

  return <>
    <input name={name} value={value || active?.toLowerCase()} hidden/>
    {label ? <div className={styles.switcherButton_label}>{label}</div> : null }
    <div className={styles.switcherButton_button_wrapper}>
      {
        options.map((option, i) =>
          <div key={i} className={classFor(option.label)} onClick={() => toggleActive(option.label, i)}>{option.label}</div>
        )
      }
    </div>
  </>
}