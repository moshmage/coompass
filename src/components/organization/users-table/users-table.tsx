"use client";
import {Grid} from "@mui/material";
import {OrgPagesHeader} from "@/components/organization/org-pages-header/org-pages-header";
import {SimpleTable} from "@/components/table/simple-table/simple-table";
import {useCompanyParticipants} from "@/components/hooks/queries/use-company-participants";
import {format} from "date-fns";
import {ReactNode, useEffect, useState} from "react";
import {useTranslation} from "@/i18n/client";
import {useLocale} from "@/i18n/hooks/locale-provider";

type UserTableRow = {
  User: string;
  Company: ReactNode;
  "Join date": string;
  "Last login": string;
  "Active missions": number;
  "Complete missions": number;
}

export function UsersTable({companyId}: {companyId: string}) {
  const {t} = useTranslation();
  const {data: users, isFetching: loadingUsers} = useCompanyParticipants(companyId);

  const [rows, setRows] = useState<UserTableRow[]>([]);
  const [columns, setColumns] = useState<string[]>([]);
  const lng = useLocale();

  const row = (User: string, Company: string, joinDate: string, lastLogin: string, active: number, complete: number) =>
    ({
      [t('organization.users.columns.user')]: User,
      [t('organization.users.columns.company')]: Company,
      [t('organization.users.columns.joinDate')]: joinDate,
      [t('organization.users.columns.lastLogin')]: lastLogin,
      [t('organization.users.columns.activeMissions')]: active,
      [t('organization.users.columns.completeMissions')]: complete
    });

  useEffect(() => {

    setColumns([
      t('organization.users.columns.user'),
      t('organization.users.columns.company'),
      t('organization.users.columns.joinDate'),
      t('organization.users.columns.lastLogin'),
      t('organization.users.columns.activeMissions'),
      t('organization.users.columns.completeMissions')
    ])

    setRows(
      users
        .map(({username, email, createdAt, lastLogin, associatedCompanies, missions}) =>
          row(
            username || email || "No username",
            associatedCompanies?.[0]?.name || "No company yet",
            createdAt ? format(createdAt, "dd/MM/yyyy") : "-",
            lastLogin ? format(lastLogin, "dd/MM/yyyy") : "-",
            missions?.filter(({state}) => state === "started")?.length || 0,
            missions?.filter(({state}) => state === "finished")?.length || 0
          )
        ) as any
    )
  }, [users, lng]);

  return <>
    <Grid container>
      <Grid item xs={12}>
        <OrgPagesHeader number={users.length} title={t('menu.engagedUsers')} />
      </Grid>
    </Grid>

    <Grid container>
      <Grid item xs={12}>
        <SimpleTable columns={columns} rows={rows} isLoading={loadingUsers} />
      </Grid>
    </Grid>
  </>
}