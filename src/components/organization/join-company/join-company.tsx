"use client";

import style from "@/components/session/session-box.module.css";
import styles from "@/components/session/session-box.module.css";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import envelope from "@/assets/envelopeSimple.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {useState} from "react";
import axios from "axios";
import {useRouter} from "next/navigation";
import {signOut} from "next-auth/react";
import {useSelf} from "@/components/hooks/use-self";
import Link from "next/link";
import {useTranslation} from "@/i18n/client";
import {useSnackbar} from "notistack";


export function JoinCompanyBox() {
  const {t} = useTranslation();
  const [companyEmail, setCompanyEmail] = useState("");

  const {userData} = useSelf();
  const router = useRouter();

  const {enqueueSnackbar} = useSnackbar();

  const joinCompany = async () => {
    enqueueSnackbar(t('common.snackbars.info.requestingToJoinCompany'), {variant: "info"});

    axios.post(`/api/company/join/request`, {companyEmail})
      .then(() => {
        enqueueSnackbar(t('common.snackbars.success.requestedToJoinCompany'), {variant: "success"});
        router.push(`/dashboard/user`);
      })
      .catch((e: any) => {
        enqueueSnackbar(t('common.snackbars.warning.failedToRequestToJoinCompany', {message: e.response?.data?.error}), {variant: "warning"});
        console.error(`Error requesting to join company`, e);
    })
  }

  const _logout = async () => {
    await signOut({redirect: true, callbackUrl: "/"})
  }

  return <div className={style.sessionBox}>
    <div className={styles.sessionBox_title}>{t('company.join.joinYourCompany')}</div>
    <div className={styles.sessionBox_subtitle}>{t('company.join.joinSharedEnvironmentWhereTeamsVanWorkOnProjects')}</div>

    <div className={styles.sessionBox_subtitle_small}>{t('company.join.loggedInAs', {email: userData?.email})}</div>

    <SimpleTextInput label={t('company.join.companyEmailAddress')}
                     type="email"
                     name="company_email"
                     beforeIcon={envelope}
                     placeHolder="email@domain.tld"
                     onValueChange={e => setCompanyEmail(e?.target?.value)}/>

    <div className={styles.sessionBox_buttonWrapper}>
      <SimpleButton block label={t('company.join.joinCompany')} style="primary" onClick={joinCompany}/>
    </div>

    <div className={styles.sessionBox_buttonWrapper}>
      <SimpleButton block label={t('common.buttons.logout')} style="secondary" onClick={_logout}/>
    </div>

    <div className={styles.sessionBox_buttonWrapper}>
      <Link href="/dashboard/user"><SimpleButton block label={t('common.buttons.goBack')} style="black" /></Link>
    </div>
  </div>
}