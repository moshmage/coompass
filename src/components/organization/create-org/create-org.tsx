"use client";
import {CircularProgress, Collapse, Grid} from "@mui/material";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {useRouter} from "next/navigation";
import {useSelf} from "@/components/hooks/use-self";
import {UserTypes} from "@/types/user/user";
import buildings from "@/assets/buildings.svg";

import inputStyle from "@/components/inputs/text-input/text-input.module.css";
import createOrgStyle from "@/components/organization/create-org/create-org.module.css";
import {SettingsBox} from "@/components/settings/settings-box";
import {ImageInput} from "@/components/inputs/image/image-input";
import {SDG} from "@/constants/sdg";
import {useSnackbar} from "notistack";
import {CaretDownSvg} from "@/components/svg/caret-down-svg";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {useUserCompany} from "@/components/hooks/queries/use-user-company";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {FacebookSvg} from "@/components/svg/facebook-svg";
import {XComSvg} from "@/components/svg/x-com-svg";
import {LinkedinSvg} from "@/components/svg/linkedin-svg";
import {WebsiteSvg} from "@/components/svg/website-svg";
import {useTranslation} from "@/i18n/client";
import {YoutubeSvg} from "@/components/svg/youtube-svg";
import {InstagramSvg} from "@/components/svg/instagram-svg";
import {DropdownButton} from "@/components/buttons/dropdown/dropdown-button";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {useDistricts} from "@/components/hooks/queries/use-districts";
import {useLocale} from "@/i18n/hooks/locale-provider";
import {dataUrlToFile} from "@/handlers/utils/data-url-to-file";
import {UploadType} from "@/types/upload/upload-types";

type Matcher = {
  website: string;
  xcom: string;
  linkedin: string;
  facebook: string;
  instagram: string;
  youtube: string;
}

export function CreateOrg() {
  const [name, setName] = useState<string>("");
  const [description, setDescription] = useState<string>("")
  const [image, setImage] = useState<string>("");
  const [validImage, setValidImage] = useState<boolean>(true);
  const [sdg, setSDG] = useState<DropdownOption[]>([]);
  const [checked, setChecked] = useState<boolean>(false);
  const [districtOptions, setDistrictOptions] = useState<DropdownOption[]>([]);
  const [website, setWebsite] = useState<string>("");
  const [xcom, setXCom] = useState<string>("");
  const [linkedin, setLinkedin] = useState<string>("");
  const [facebook, setFacebook] = useState<string>("");
  const [youtube, setYoutube] = useState<string>("");
  const [instagram, setInstagram] = useState<string>("");
  const [validLink, setValidLink] = useState({
    website: true,
    xcom: true,
    facebook: true,
    linkedin: true,
    youtube: true,
    instagram: true,
  })

  const {data: org, isFetching} = useUserCompany();
  const {data: districts,} = useDistricts();

  const router = useRouter();
  const {userData, refetch} = useSelf();

  const {t} = useTranslation();
  const locale = useLocale();

  const companyOrNGO = userData?.type === UserTypes.ngo ? t('company.create.type.organization') : t('company.create.type.company')
  const validForm = !!(
    name &&
    description &&
    image &&
    validImage &&
    (website && validLink.website || true) &&
    (xcom && validLink.xcom || true) &&
    (linkedin && validLink.linkedin || true) &&
    (facebook && validLink.facebook || true) &&
    (instagram && validLink.instagram || true) &&
    (youtube && validLink.youtube || true)
  );

  const {enqueueSnackbar} = useSnackbar();

  const registerCompany = async () => {
    if (!validForm)
      return;

    enqueueSnackbar(t('common.snackbars.info.updatingCompanyInformation', {type: companyOrNGO}), {variant: "info"});

    try {

      const data = await  axios.post("/api/company", {
        name,
        description,
        website, xcom, linkedin, facebook, instagram, youtube,
        image,
        sdg: sdg.filter(s => s.active).map(({title}) => title),
        id: org?.id,
        districts:
          districtOptions
            .filter(({active}) => active)
            .map(({value}) => value)
      }).then(d => d.data)

      if (image && (image !== userData?.associatedCompanies?.[0]?.image)) {
        const form = new FormData();
        form.append("file", dataUrlToFile(image, "company_header"));
        form.append("id", data.id);
        form.append("type", UploadType.company_header);
        await axios.post(`/api/upload`, form);
      }

      enqueueSnackbar(t('common.snackbars.success.updatedCompanyInformation', {type: companyOrNGO}), {variant: "success"});

      await refetch?.();

      if (data.data)
        router.push(`/${userData?.type === UserTypes.ngo ? "organization" : "company"}/${data.data.id}/`)

    } catch (e: any) {
      console.log(`Error creating company`, e);
      enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.success.failedToUpdateCompanyInformation'), {variant: "warning"});
    }
  }

  const loader = () =>
    isFetching && <CircularProgress style={{width: 10, height: 10}}/> || null

  const matcher = {
    website: /https?:\/\/(?:www\.)?[a-zA-Z0-9-]+(?:\.[a-zA-Z]{2,})(?:\/[^\s]*)?/,
    xcom: /https?:\/\/(?:www\.)?(twitter|x)\.com\/([a-zA-Z0-9_]+)/,
    facebook: /https?:\/\/(?:www\.)?facebook\.com\/([a-zA-Z0-9\.]+)/,
    linkedin: /https?:\/\/(?:www\.)?linkedin\.com\/in|company\/([a-zA-Z0-9-]+)/,
    instagram: /https?:\/\/(?:www\.)?instagram\.com\/([a-zA-Z0-9_\.]+)/,
    youtube: /https?:\/\/(?:www\.)?youtube\.com\/(?:channel\/|user\/)?([a-zA-Z0-9_\-@]+)/,
  }

  const _setValidLink = (kind: keyof Matcher, value: string) => {

    console.log(matcher[kind].test(value), value, kind)

    if (!matcher[kind])
      return;

    setValidLink({
      ...validLink,
      [kind]: !value ? true : matcher[kind].test(value)
    })
  }

  const updateSDG = (options: DropdownOption[]) => {
    setSDG(SDG.map(o => {
      return ({...o, active: !!options.find(sel => sel.title === o.title)})
    }))
  }

  const updateDistricts = (options: DropdownOption[]) => {
    setDistrictOptions(
      districts.map(({id, ...data}) =>
        ({title: data[locale], value: id, active: !!options.find(o => o.value === id)?.active}))
    );
  }

  useEffect(() => {

    setSDG(SDG.map(o => ({...o, active: org?.sdg?.includes(o.title)})));

    if (!org)
      return;

    setName(org?.name || "");
    setDescription(org?.name || "");
    setImage(org?.image || "");

    setWebsite(org?.website || "");
    setXCom(org?.xcom || "");
    setLinkedin(org?.linkedin || "");
    setInstagram(org?.instagram || "");
    setYoutube(org?.youtube || "");

  }, [org])

  useEffect(() => {
    if (!districts)
      return;

    setDistrictOptions(
        districts.map(({id, ...data}) =>
            ({title: data[locale], value: id, active: org?.districts?.some(({id: pid}) => pid === id)})
        )
    )

  }, [districts, org, locale]);

  return <SettingsBox title={t('company.create.companySettings', {type: companyOrNGO})}
                      subtitle={t('common.customizeEditEssentialProfileDetails')}
                      icon={buildings}>
    <Grid item xs={12}>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
          <label style={{marginTop: "0"}} htmlFor="companyName">{t('company.create.forms.companyName', {type:companyOrNGO})} {loader()}</label>
        </Grid>
        <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
          <div className={inputStyle.textInput_input}>
            <input
              id="companyName"
              placeholder={t('company.create.forms.yourCompanyName', {type: companyOrNGO})}
              type="text"
              onChange={e => setName(e?.target.value)}
              name="name"
              value={name}
            />
          </div>
        </Grid>
      </Grid>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid container>
          <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
            <label htmlFor="imageUpload">{t('company.create.forms.companyLogo', {type:companyOrNGO})} {loader()}</label>
            <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.minImage', {dimension: "400x400"})}</div>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <ImageInput validHeight={400}
                        validWidth={400} hidePreview
                        img={org?.image}
                        onChange={(image, valid) => {
                          setImage(image);
                          setValidImage(valid);
                          setChecked(true)
                        }}/>
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          <Grid item xs={12}
                style={{display: "flex", justifyContent: "space-between", marginTop: 8,}}
                className={`cursor-pointer ${inputStyle.textInput}`} onClick={() => setChecked(!checked)}>
            <label className="cursor-pointer">{t('company.create.forms.previewYourLogo')}</label>
            <div style={{transform: `rotate(${checked ? "180deg" : "0deg"})`}}><CaretDownSvg/></div>
          </Grid>
          <Grid item xs={12}>

            <Collapse in={checked}>
              <Grid container>
                <Grid item xs={12} sm={12} md={4}>
                  <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.dashboard')}</div>
                  <Base64Image src={image} backgroundSize="contain"
                               style={{width: 40, height: 40, borderRadius: 0, marginTop: 8}}/>
                </Grid>
                <Grid item xs={12} sm={12} md={4}>
                  <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.profilePicture')}</div>
                  <Base64Image src={image} backgroundSize="contain"
                               style={{width: 24, height: 24, borderRadius: "100%", marginTop: 8}}/>
                </Grid>
                <Grid item xs={12} sm={12} md={4}>
                  <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.missionPage')}</div>
                  <Base64Image src={image} backgroundSize="contain"
                               style={{width: 110, height: 110, borderRadius: "100%", marginTop: 8}}/>
                </Grid>
              </Grid>
            </Collapse>

          </Grid>
        </Grid>
      </Grid>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
          <label htmlFor="companyDescription">{t('company.create.forms.companyDescription', {type: companyOrNGO})} {loader()}</label>
        </Grid>
        <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
          <div className={inputStyle.textInput_input} style={{height: "auto"}}>
            <textarea
              id="companyDescription"
              placeholder={t('company.create.forms.companyDescription', {type: companyOrNGO})}
              onChange={e => setDescription(e?.target.value)}
              name="description"
              defaultValue={org?.description}
            />
          </div>
          <div className={inputStyle.horizontal_input_textarea_length}
               style={{color: description.length > 500 ? "red" : ""}}>{description.length} / 500
          </div>
        </Grid>
      </Grid>

      {
        userData?.type === UserTypes.ngo
          ? <Grid container className={inputStyle.horizontal_input}>
            <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
              <label>{t('common.sdg.organizationSustainableDevelopmentGoals')} {loader()}</label>
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
              <DropdownButton label={t('common.sdg.availableSDG')}
                              options={sdg} multiple
                              onSelect={updateSDG} />
            </Grid>
          </Grid>
          : ""
      }

      <Grid container style={{marginTop: 16}} spacing={1}>
        <Grid item xs={12} sm={12} md={6} className={inputStyle.textInput}>
          <label>{t('common.location.select')} {loader()}</label>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <DropdownButton onSelect={updateDistricts}
                          label={t('common.location.available')} translatedOptions multiple
                          options={districtOptions}/>
        </Grid>
      </Grid>

      <Grid container style={{marginTop: 16}} spacing={1}>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://wwww.yourwebsite.com"}
                           beforeIcon={<WebsiteSvg/>} beforeIconIsNode
                           defaultValue={org?.website}
                           helperInfo={!validLink.website ? t('common.errors.notAValidLink', {type: "website", example: "https://wwww.yourwebsite.com"}) : ""}
                           onBlur={(_) => {_setValidLink("website", website)}}
                           onValueChange={e => setWebsite(e.target.value)}/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://www.facebook.com/"}
                           beforeIcon={<FacebookSvg />} beforeIconIsNode
                           defaultValue={org?.facebook}
                           onBlur={(_) => {_setValidLink("facebook", facebook)}}
                           helperInfo={!validLink.facebook ? t('common.errors.notAValidLink', {type: "facebook", example: "https://www.facebook.com/username"}) : ""}
                           onValueChange={e => setFacebook(e.target.value)}/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://www.twitter.com/"}
                           beforeIcon={<XComSvg/>} beforeIconIsNode
                           defaultValue={org?.xcom}
                           onBlur={(_) => {_setValidLink("xcom", xcom)}}
                           helperInfo={!validLink.xcom ? t('common.errors.notAValidLink', {type: "x.com", example: "https://www.x.com/username"}) : ""}
                           onValueChange={e => setXCom(e.target.value)}/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://www.linkedin.com/in/"}
                           beforeIcon={<LinkedinSvg/>} beforeIconIsNode
                           defaultValue={org?.linkedin}
                           onBlur={(_) => {_setValidLink("linkedin", linkedin)}}
                           helperInfo={!validLink.linkedin ? t('common.errors.notAValidLink', {type: "linkedin", example: "https://www.linkedin.com/in/username"}) : ""}
                           onValueChange={e => setLinkedin(e.target.value)}/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://www.youtube.com/"}
                           beforeIcon={<YoutubeSvg/>} beforeIconIsNode
                           defaultValue={org?.youtube}
                           onBlur={(_) => {_setValidLink("youtube", youtube)}}
                           helperInfo={!validLink.youtube ? t('common.errors.notAValidLink', {type: "youtube", example: "https://www.youtube.com/@username"}) : ""}
                           onValueChange={e => setYoutube(e.target.value)}/>
        </Grid>
        <Grid item xs={12} sm={6}>
          <SimpleTextInput placeHolder={"https://www.instagram.com/"}
                           beforeIcon={<InstagramSvg/>} beforeIconIsNode
                           defaultValue={org?.instagram}
                           onBlur={(_) => {_setValidLink("instagram", instagram)}}
                           helperInfo={!validLink.instagram ? t('common.errors.notAValidLink', {type: "instagram", example: "https://www.instagram.com/in/username"}) : ""}
                           onValueChange={e => setInstagram(e.target.value)}/>
        </Grid>
      </Grid>

      <div className="hr" style={{marginTop: 24}}></div>

      <div className={createOrgStyle.createOrg_submit}>
        <SimpleButton disabled={!validForm} label={t('common.buttons.saveChanges')} style="primary" onClick={registerCompany}/>
      </div>
    </Grid>
  </SettingsBox>
}