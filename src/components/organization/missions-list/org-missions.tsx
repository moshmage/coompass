import {Grid, LinearProgress} from "@mui/material";
import {MissionCard} from "@/components/mission/mission-card/mission-card";
import {Mission} from "@/types/mission/mission";

export function OrgMissions({missions = [] as Mission[], loading = false, orgId = ""}) {
  return <>
    {
      loading
        ? <>
          <Grid container>
            <Grid item xs={12}>
              <LinearProgress/>
            </Grid>
          </Grid>
        </>
        : null
    }
    <Grid container spacing={1}>
      {
        missions.map(({title, image, description, id, location, state, company: {name}, createdAt}: any, i: any) =>
          (<Grid key={i} item xs={12} sm={6} md={4} lg={4} xl={3}>
            <MissionCard fromOrg={orgId}
                         name={title}
                         missionImage={image}
                         orgName={name}
                         description={description}
                         id={id}
                         location={location}
                         state={state}
                         createdDate={createdAt}/>
          </Grid>))
      }
    </Grid>
  </>
}