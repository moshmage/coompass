import {Mission} from "@/types/mission/mission";
import {Grid, LinearProgress} from "@mui/material";
import {MissionCard} from "@/components/mission/mission-card/mission-card";

import styles from "@/components/organization/organization-box/org-box.module.css"
import {NumberDisplay} from "@/components/number/display/number-display";

export function OrgMissionsOverview({missions = [] as Mission[], loading = false}) {
  return <>
    <Grid container>
      <Grid item xs={12} style={{marginBottom: 28, marginTop: 38}}>
        <div className={styles.orgBox_title}>Missions <NumberDisplay number={missions.length}/></div>
      </Grid>
    </Grid>
    {
      loading
        ? <>
          <Grid container>
            <Grid item xs={12}>
              <LinearProgress/>
            </Grid>
          </Grid>
        </>
        : null
    }
    <Grid container spacing={4}>
      {
        missions.map(({title, image, description, id, location, company: {name}, state}: any, i: any) =>
          (<Grid key={i} item xs={12} sm={6} md={6}>
            <MissionCard location={location} state={state} name={title} missionImage={image} orgName={name} description={description} id={id}/>
          </Grid>))
      }
    </Grid>
  </>
}