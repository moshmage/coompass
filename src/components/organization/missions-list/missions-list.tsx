"use client";
import {useCompany} from "@/components/hooks/queries/use-company";
import {Grid} from "@mui/material";
import {useOrganizationsMissions} from "@/components/hooks/queries/use-organizations-missions";
import {OrgMissions} from "@/components/organization/missions-list/org-missions";
import {useTranslation} from "@/i18n/client";

export function MissionsList({companyId}: {companyId: string}) {
  const {t} = useTranslation()
  const {data: missions, isFetching: loading} = useOrganizationsMissions(companyId);
  const {data: company} = useCompany(companyId);
  return <>
    <Grid container style={{paddingBottom: "16px"}}>
      <Grid item xs={12}>
        <h2>{t('organization.mission-list.missionsForOrg', {org: company?.name || "..."})}</h2>
      </Grid>
    </Grid>

    <OrgMissions orgId={companyId} missions={missions} loading={loading} />
  </>
}