import {Grid} from "@mui/material";
import {SimpleButton} from "@/components/buttons/simple/simple-button";

import style from "@/components/organization/organization-box/org-box.module.css";
import Image from "next/image";
import Link from "next/link";
import {SDG_TO_COLOR, SDG_TO_ICON} from "@/constants/sdg";
import {useTranslation} from "@/i18n/client";
import Structural from "@/components/structural/structural";

type OrgBoxProps = {
  name: string;
  imageUrl: string;
  activeMissions?: number;
  totalMissions?: number;
  description: string;
  id: string;
  sdg: string[];
  addPartnerButton?: null|((_id: string) => void);
  removePartnerButton?: null|((_id: string) => void);
}

export function OrgBox({
                         name = "",
                         imageUrl = "",
                         activeMissions = 0, totalMissions = 0,
                         description = "",
                         id = "",
                         sdg = [] as string[],
                         addPartnerButton = null,
                         removePartnerButton = null,
}: OrgBoxProps) {

  const {t} = useTranslation();

  let _description = description.substring(0,151);
  if (description.length > 151)
    _description += "...";

  return <Grid container className={style.orgBox}>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={5} md={4} className={style.orgBox_image}>
        <Image src={imageUrl} width={10} height={10} alt={"org logo"}></Image>
      </Grid>
      <Grid item xs={12} sm={7} md={8} title={name}>
        <h3 className={style.orgBox_title}>{name}</h3>
        {/*<span className={style.orgBox_subTitle}>{t('company.employeesTable.columns.activeMissions')} </span><span className={style.orgBox_subTitle_total}>{activeMissions}</span>*/}
        <span className={style.orgBox_subTitle}>{t('company.employeesTable.columns.allMissions')} </span><span className={style.orgBox_subTitle_total}>{totalMissions}</span>
      </Grid>
    </Grid>
    <Grid container className={style.orgBox_description}>
      <Grid item xs={12}>
        <div title={description} >{_description}</div>
      </Grid>
    </Grid>
    <Grid container className={style.orgBox_related}>
      <Grid item xs={12}>
        <div>{t('organizations.relatedGoals')}:</div>
      </Grid>
    </Grid>
    <div className={style.orgBox_sdg}>
      {sdg.map((entry, i) => (
        <div key={i} className={style.orgBox_sdg_entry} style={{backgroundColor: SDG_TO_COLOR[entry]}}>{(SDG_TO_ICON[entry]||null)?.replace("goal_", "")}</div>
      ))}&nbsp;
    </div>
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6} md={6}>
        <Link style={{width: "100%"}} href={`/organization/${id}/overview`}>
          <SimpleButton sx={{width: "inherit"}} small style="secondary" label={t('organizations.overview')} />
        </Link>
      </Grid>
      <Grid item xs={12} sm={6} md={6}>
        <Link style={{width: "100%"}} href={`/organization/${id}/mission-list`}>
          <SimpleButton sx={{width: "inherit"}} small style="secondary" label={t('organizations.missions')} /></Link>
      </Grid>
    </Grid>
    <Structural.If condition={!!(addPartnerButton||removePartnerButton)}>
      <Grid container spacing={2} marginTop={0}>
        <Grid item xs={12}>
          <Structural.If condition={!!addPartnerButton}>
            <SimpleButton block small style="primary"
                          label={t('partnerships.addPartnerButton')}
                          onClick={() => addPartnerButton?.(id)} />
          </Structural.If>
          <Structural.If condition={!!removePartnerButton}>
            <SimpleButton block small style="primary"
                          label={t('partnerships.removePartnerButton')}
                          onClick={() => removePartnerButton?.(id)} />
          </Structural.If>
        </Grid>
      </Grid>
    </Structural.If>
  </Grid>
}