"use client";
import {CircularProgress, Grid} from "@mui/material";
import {useOrganizationsMissions} from "@/components/hooks/queries/use-organizations-missions";
import {OrgMissionsOverview} from "@/components/organization/missions-list/org-missions-overview";
import {useCompany} from "@/components/hooks/queries/use-company";
import {Base64Image} from "@/components/base-64-image/base-64-image";

import orgPageStyles from "@/components/organization/page/organization-page.module.css";
import {SDG_TO_ICON} from "@/constants/sdg";
import Image from "next/image";
import Link from "next/link";
import {WebsiteSvg} from "@/components/svg/website-svg";
import {XComSvg} from "@/components/svg/x-com-svg";
import {LinkedinSvg} from "@/components/svg/linkedin-svg";
import {FacebookSvg} from "@/components/svg/facebook-svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {CaretLeftSvg} from "@/components/svg/caret-left-svg";
import {useTranslation} from "@/i18n/client";
import {InstagramSvg} from "@/components/svg/instagram-svg";
import {YoutubeSvg} from "@/components/svg/youtube-svg";

export function OrgOverview({companyId = ""}) {
  const {t} = useTranslation()
  const {data: company, isFetching} = useCompany(companyId);
  const {data: missions, isFetching: loading} = useOrganizationsMissions(companyId);

  const searchParams = new URLSearchParams(window.location.search);
  const link = searchParams.get("back") ? `/missions/${searchParams.get("back")}` : `/organizations/`
  const linkText =
    searchParams.get("back")
      ? t('organization.overview.backToMission')
      : t('organization.overview.backToOrganizations');

  return <>
    <Grid container>
      <Grid item xs={12} className={orgPageStyles.orgPage_back}>
        <Link href={link}>
          <SimpleButton label={<> <CaretLeftSvg width={20} height={20}/> {linkText}</>} style="black" small />
        </Link>
      </Grid>
    </Grid>
    <Grid container spacing={3}>
      <Grid item xs={12} sm={12} md={4}>
        <Grid container>
          <Grid item xs={12}>
            {isFetching && !company?.image
              ? <CircularProgress style={{width: 100, height: 100}}/>
              : <Base64Image src={company?.image} style={{width: 100, height: 100, borderRadius: "100%"}} />}
          </Grid>
        </Grid>
        <Grid container style={{marginTop: 24}}>
          <Grid item xs={12}>
            <div className={orgPageStyles.orgPage_headerBox_subtitle}>{t('common.ngo')}</div>
            <div className={orgPageStyles.orgPage_headerBox_total}>{company?.name}</div>
          </Grid>
        </Grid>
        <Grid container style={{marginTop: 24}}>
          <Grid item xs={12}>
            <div className={orgPageStyles.orgPage_overview_company_description}>{company?.description}</div>
          </Grid>
        </Grid>

        <Grid container style={{marginTop: 24, marginBottom: 24}}>
          <Grid item xs={12}>
            {company?.website ? <Link style={{margin: 3}} target="_blank" href={company.website}><WebsiteSvg/></Link> : null}
            {company?.xcom ? <Link style={{margin: 3}} target="_blank" href={company.xcom}><XComSvg/></Link> : null}
            {company?.linkedin ? <Link style={{margin: 3}} target="_blank" href={company.linkedin}><LinkedinSvg/></Link> : null}
            {company?.facebook ? <Link style={{margin: 3}} target="_blank" href={company.facebook}><FacebookSvg/></Link> : null}
            {company?.instagram ? <Link style={{margin: 3}} target="_blank" href={company.instagram}><InstagramSvg/></Link> : null}
            {company?.youtube ? <Link style={{margin: 3}} target="_blank" href={company.youtube}><YoutubeSvg/></Link> : null}
          </Grid>
        </Grid>

        <div style={{marginTop: 108, gap: 16, display: "flex", flexWrap: "wrap"}}>
          {
            (company?.sdg || []).map((label, i) => (
              <>
                <Image key={i} style={{boxSizing: "border-box"}} height={80} width={80} src={`/sdg/${SDG_TO_ICON[label]}.svg`} alt={""} />
              </>
            ))
          }
        </div>
      </Grid>

      <Grid item xs={12} sm={12} md={8}>
        <OrgMissionsOverview missions={missions} loading={loading}/>
      </Grid>
    </Grid>
  </>
}