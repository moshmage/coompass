"use client";
import {Grid} from "@mui/material";
import {OrganizationPageHeader} from "@/components/organization/page/org-page-header";
import {OrganizationHeaderBox} from "@/components/organization/page/org-header-box";
import Link from "next/link";
import {PendingUsersHeader} from "@/components/organization/page/pending-users-header";
import {OrganizationPageEngage} from "@/components/organization/page/engagement";
import {OrgGraph} from "@/components/organization/page/org-graph";
import {OrganizationPageEngagedUsers} from "@/components/organization/page/engaged-users";
import MissionWindow from "@/components/window/missions/mission-window";
import {useOrganizationsMissions} from "@/components/hooks/queries/use-organizations-missions";
import {useCompany} from "@/components/hooks/queries/use-company";
import {useEffect, useState} from "react";
import {useEngagedGraph} from "@/components/hooks/queries/use-engaged-graph";
import orgPageStyle from "@/components/organization/page/organization-page.module.css";
import {useEngagement} from "@/components/hooks/queries/use-engagement";
import {useTranslation} from "@/i18n/client";

export function OrgPage({companyId}: {companyId: string}) {
  const {t} = useTranslation()
  const [totalUsers, setTotalUsers] = useState<number>(0);
  const [totalWorkedHours, setTotalWorkedHours] = useState<number>(0);

  const {data: missions, isFetching: loadingMissions} = useOrganizationsMissions(companyId);
  const {data: company} = useCompany(companyId);
  const {data: {data, labels}, isFetching: loadingGraph} = useEngagedGraph(companyId);
  const {data: engagement} = useEngagement(companyId);

  useEffect(() => {
    setTotalUsers(missions.reduce((prev, mission) => prev + (mission.participantsState?.length || 0), 0));
    setTotalWorkedHours(
      missions
        .filter(({state}) => state === "finished")
        .reduce((prev, {reward}) => prev + reward,0))

  }, [missions, setTotalUsers, setTotalWorkedHours])

  return <>
    <Grid item xs={12}>
      <Grid container style={{display: "flex", alignItems: "center", paddingBottom: "24px"}} spacing={3}>
        <Grid item xs={12} sm={12} md={12} lg={6}>
          <div style={{display: "flex", justifyContent: "space-between", marginBottom: 8}}>
            <div className={orgPageStyle.orgPage_image_title}>{t('organization.yourCompany')}</div>
            <div className={orgPageStyle.orgPage_image_subtitle}>
              <Link href="/company/create">{t('organization.editProfile')}</Link>
            </div>
          </div>
          <OrganizationPageHeader image={company?.image} companyName={company?.name}/>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={6}>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <OrganizationHeaderBox label={t('organization.yourMissions')}
                                     total={missions.length} />
            </Grid>
            <Grid item xs={6}>
              <PendingUsersHeader companyId={companyId} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid container style={{paddingBottom: "24px"}}>
        <OrganizationPageEngage totalEngagedUsers={totalUsers} loading={loadingMissions}
                                totalMissions={missions.length}
                                workedHours={totalWorkedHours} />
      </Grid>

      <OrgGraph data={data} labels={labels} loading={loadingGraph} />

      <Grid container style={{paddingBottom: "24px", paddingTop: 24}}>
        <OrganizationPageEngagedUsers thirtyDays={engagement.thirtyDays}
                                      sevenDays={engagement.sevenDays}
                                      newUsers={engagement.all} />
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <MissionWindow missions={missions}/>
        </Grid>
      </Grid>
    </Grid>
  </>
}