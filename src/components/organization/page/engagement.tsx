import orgPageStyle from "@/components/organization/page/organization-page.module.css"
import {CircularProgress, Grid} from "@mui/material";
import {useTranslation} from "@/i18n/client";

export function OrganizationPageEngage({totalEngagedUsers = 0, totalMissions = 0, workedHours = 0, loading = false}) {

  const {t} = useTranslation()

  const content = (title: string, value: number) => <>
    <div className={orgPageStyle.orgPage_headerBox_subtitle}>{title}</div>
    <div className={orgPageStyle.orgPage_headerBox_total}>{loading ? <CircularProgress /> :value}</div>
  </>

  return <Grid item xs={12} className={orgPageStyle.orgPage_headerBox}>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={4}>
        {content(t('organization.engagement.totalEngagedUsers'), totalEngagedUsers)}
      </Grid>
      <Grid item xs={12} sm={4}>
        {content(t('organization.engagement.totalMissions'), totalMissions)}
      </Grid>
      <Grid item xs={12} sm={4}>
        {content(t('organization.engagement.workedHours'), workedHours)}
      </Grid>
    </Grid>
  </Grid>
}