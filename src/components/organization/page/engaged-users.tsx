import orgPageStyle from "@/components/organization/page/organization-page.module.css"
import {Grid} from "@mui/material";
import {CaretDownSvg} from "@/components/svg/caret-down-svg";
import {useTranslation} from "@/i18n/client";

export function OrganizationPageEngagedUsers({
                                         thirtyDays = 0,
                                         sevenDays = 0,
                                         newUsers = 0
}) {
  const {t} = useTranslation()
  return <Grid item xs={12} className={orgPageStyle.orgPage_headerBox}>
    <Grid container style={{paddingBottom: 16}}>
      <Grid item xs={12} style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
        <div className={orgPageStyle.orgPage_headerBox_title}>{t('organization.engagement.engagement')}</div>
        <div className={orgPageStyle.orgPage_headerBox_title_select}>{t('organization.engagement.allMembers')} <CaretDownSvg /></div>
      </Grid>
    </Grid>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={4}>
        <div className={orgPageStyle.orgPage_headerBox_total}>{thirtyDays}</div>
        <div className={orgPageStyle.orgPage_headerBox_subtitle}>{t('organization.engagement.engagedInTheLastDays', {n: 30})}</div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className={orgPageStyle.orgPage_headerBox_total}>{sevenDays}</div>
        <div className={orgPageStyle.orgPage_headerBox_subtitle}>{t('organization.engagement.engagedInTheLastDays', {n: 7})}</div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className={orgPageStyle.orgPage_headerBox_total}>{newUsers}</div>
        <div className={orgPageStyle.orgPage_headerBox_subtitle}>{t('organization.engagement.newUsers')}</div>
      </Grid>
    </Grid>
  </Grid>
}