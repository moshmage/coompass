"use client";

import {OrganizationHeaderBox} from "@/components/organization/page/org-header-box";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {useEffect, useState} from "react";
import {ParticipantsState} from "@/types/mission/mission";
import axios from "axios";
import WindowBox from "@/components/window/window-box";
import {PendingUserRow} from "@/components/mission/mission-pending-users/pending-user-row/pending-user-row";
import {Modal} from "@mui/material";
import {useTranslation} from "@/i18n/client";

export function PendingUsersHeader({companyId = ""}) {
  const {t} = useTranslation()
  const [loading, setLoading] = useState(false);
  const [pending, setPending] = useState<ParticipantsState[]>([]);
  const [open, setOpen] = useState<boolean>(false)

  const getPendingUsers = () => {
    // todo use react-query to not repeat requests
    if (loading)
      return;
    setLoading(true);
    axios.get<ParticipantsState[]>(`/api/company/${companyId}/participants-state?state=pending`)
      .then(response => {
        if (response.data) {
          if (!response.data.length)
            setOpen(false);
          setPending(response.data)
        }
      })
      .finally(() => {
        setLoading(false);
      })
  }

  const onClose = async () => {
    setOpen(false)
  };

  useEffect(getPendingUsers, []);

  return <>
    <OrganizationHeaderBox label={t('organization.pending.pendingUsers')}
                           total={pending.length}
                           action={<SimpleButton small label={t('organization.pending.manage')} style="secondary" disabled={pending.length === 0}
                                                 onClick={() => setOpen(true)}/>}/>

    <Modal open={open} onClose={onClose}>
      <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: "786px"}}>
        <WindowBox title={t('organization.pending.pendingUsersList')}>
          {pending.map((state, key) =>
            <PendingUserRow key={key}
                            missionId={state.missionId}
                            missionTitle={state.mission.title}
                            user={state.user}
                            createdAt={state.createdAt || new Date()}
                            refresh={() => getPendingUsers()}/>)}
        </WindowBox>
      </div>
    </Modal>
  </>
}