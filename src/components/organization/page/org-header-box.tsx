import {ReactNode} from "react";

import orgPageStyle from "@/components/organization/page/organization-page.module.css"
import {Grid} from "@mui/material";

export function OrganizationHeaderBox({
                                        label = "",
                                        total = 0,
                                        action = null as ReactNode|null,
                                        style = "primary" as "primary" | "secondary",
                                      }) {
  return <Grid container sx={{padding: {xs: "16px", md: "32px"}}} className={orgPageStyle.orgPage_headerBox}>
    <Grid container>
      <Grid item className={orgPageStyle.orgPage_headerBox_subtitle} xs={12}>{label}</Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12} sm={12} md={3} className={orgPageStyle.orgPage_headerBox_total}>{total}</Grid>
      <Grid item xs={12} sm={12} md={9} style={{display: "flex", justifyContent: "end"}}>{action}</Grid>
    </Grid>
  </Grid>
}