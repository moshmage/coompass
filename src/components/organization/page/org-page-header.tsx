import orgPageStyle from "@/components/organization/page/organization-page.module.css";
import {Base64Image} from "@/components/base-64-image/base-64-image";

export function OrganizationPageHeader({image = "", companyName = ""}) {
  return <div className={orgPageStyle.orgPage_image}>
    <Base64Image src={image} alt="org logo" backgroundSize="contain" />
    <div>{companyName}</div>
  </div>
}