"use client";

import {areaElementClasses, chartsGridClasses, LineChart,} from "@mui/x-charts";
import orgPageStyle from "@/components/organization/page/organization-page.module.css";
import {CircularProgress, Grid,} from "@mui/material";
import {CaretDownSvg} from "@/components/svg/caret-down-svg";
import {format, subMonths} from "date-fns";
import {useTranslation} from "@/i18n/client";


export function OrgGraph({
                           data = [] as number[],
                           labels = [] as string[],
                           loading = false
                         }) {

  const {t} = useTranslation()

  const today = new Date();

  return <Grid item xs={12} className={orgPageStyle.orgPage_headerBox}>
    <Grid container>
      <Grid item xs={12} style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
        <div className={orgPageStyle.orgPage_headerBox_title}>{t('organization.totalMembers')} <CaretDownSvg/></div>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={12} style={{width: "100%"}}>
        <div style={{height: "300px", width: "100%",}}>
          {loading ? <div style={{display: "flex", justifyContent: "center", alignItems: "center"}}><CircularProgress/>
          </div> : null}
          {!loading
            ? <LineChart series={[{data, color: "#ff01c7", area: true, showMark: false, curve: "linear"}]}
                         xAxis={[{scaleType: 'point', data: labels}]}
                         leftAxis={null}
                         bottomAxis={null}
                         margin={{left: 1, right: 1, top: 0, bottom: 5}}
                         grid={{vertical: true}}
                         sx={{
                           [`& .${areaElementClasses.root}`]: {
                             fill: 'rgba(255,1,199,0.3)', zIndex: 3
                           },
                           [`& .${chartsGridClasses.verticalLine}`]: {
                             "stroke-dasharray": "5,5",
                             "stroke": "rgba(255, 255, 255, 0.12)!important",
                           },
                         }}/>
            : null}
          {
            !loading
              ? <>
                <div className={orgPageStyle.orgPage_graph_date}>
                  <div>{format(subMonths(today, 1), "dd MMM yyyy")}</div>
                  <div>{format(today, "dd MMM yyyy")}</div>
                </div>
              </>
              : null
          }
        </div>

      </Grid>
    </Grid>
  </Grid>
}