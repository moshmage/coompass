"use client";
import {Grid} from "@mui/material";
import {OrgPagesHeader} from "@/components/organization/org-pages-header/org-pages-header";
import Link from "next/link";
import {SimpleTable} from "@/components/table/simple-table/simple-table";
import {Id} from "@/components/id/id";
import {XOfTotal} from "@/components/number/x-of-total/x-of-total";
import {MissionState} from "@/components/mission/mission-state/mission-state";
import {format} from "date-fns";
import {useOrganizationsMissions} from "@/components/hooks/queries/use-organizations-missions";
import {ReactNode, useEffect, useState} from "react";
import {useTranslation} from "@/i18n/client";
import {useLocale} from "@/i18n/hooks/locale-provider";

type MissionsTableRows = {
  id: ReactNode,
  Mission: ReactNode,
  State: ReactNode,
  Participants: ReactNode,
  "Start date": string,
  "End date": string,
}

export function MissionsTable({companyId}: { companyId: string }) {
  const {data: missions, isFetching: loading} = useOrganizationsMissions(companyId);
  const [rows, setRows] = useState<MissionsTableRows[]>([])
  const [columns, setColumns] = useState<string[]>([])
  const lng = useLocale();
  const {t} = useTranslation();

  const row = (
    id: string,
    Mission: string,
    State: string,
    Participants: number,
    startedAt: Date | null,
    finishedAt: Date | null,
    totalParticipants: number
  ) =>
    ({
      id: <Id id={id}/>,
      [t('organization.missions.columns.mission')]: <Link href={`/missions/${id}`}>{Mission}</Link>,
      [t('organization.missions.columns.startedAt')]: startedAt ? format(startedAt, "dd/MM/yyyy") : "-",
      [t('organization.missions.columns.endDate')]: finishedAt ? format(finishedAt, "dd/MM/yyyy") : "-",
      [t('organization.missions.columns.participants')]: <XOfTotal x={Participants} total={totalParticipants}/>,
      [t('organization.missions.columns.state')]: <MissionState display="inline-flex" state={State}/>,
    });

  useEffect(() => {

    setColumns([
      t('organization.missions.columns.mission'),
      t('organization.missions.columns.startedAt'),
      t('organization.missions.columns.endDate'),
      t('organization.missions.columns.participants'),
      t('organization.missions.columns.state')
    ])

    setRows(
      missions.map(({id, title, participants, state, when_start, when_finish, participantsState}) =>
        row(id, title, state, participants.length, when_start, when_finish, participantsState?.length || 0)) as any
    )
  }, [missions, lng]);

  return <>
    <Grid container spacing={1}>
      <Grid item xs={12} sm={3} md={3}>
        <OrgPagesHeader title={t('organization.yourMissions')} number={missions.length}/>
      </Grid>
      <Grid item xs={0} sm={6} md={6}/>
    </Grid>

    <Grid container>
      <Grid item xs={12}>
        <SimpleTable columns={columns} rows={rows} isLoading={loading}/>
      </Grid>
    </Grid>
  </>
}