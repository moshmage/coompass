import styles from "@/components/organization/org-pages-header/org-pages-header.module.css";
import {NumberDisplay} from "@/components/number/display/number-display";

export function OrgPagesHeader({number = 0, title = ""}) {
  return <div className={styles.missionPageHeader_title}>{title} <NumberDisplay number={number}/></div>
}