"use client";

import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";

import tagsStyles from "@/components/tags/tags.module.css";
import {useEffect, useRef, useState} from "react";
import {useTranslation} from "@/i18n/client";

type SimpleTag = {title: string};

function Tag({label = "", onDelete = () => {}}: any) {
  return <span className={tagsStyles.tag_entry} onClick={onDelete}>{label} <span className={tagsStyles.closer}>x</span></span>
}

export function TagSelector({
                              tags = [] as SimpleTag[],
                              label = "Select tags",
                              popupTitle = "Select multiple tags",
                              placeHolder = "Search tags",
                              onSelect = (_tags: SimpleTag[]) => {},
                              defaultValues = [] as string[], required = false,
                            }) {
  const [isFocused, setIsFocused] = useState<boolean>(false);
  const [selectedTags, setSelectedTags] = useState<SimpleTag[]>([]);
  const [search, setSearch] = useState<string>("");
  const [defaultLoaded, setDefaultLoaded] = useState<boolean>(false)
  const [available, setAvailable] = useState<SimpleTag[]>([]);
  const wrapperRef = useRef(null);

  const {t} = useTranslation()

  const selectTag = (tag: SimpleTag) => {
    if (selectedTags.includes(tag))
      unselectTag(selectedTags.findIndex(t => t === tag))
    else
      setSelectedTags(prevState => ([...prevState, tag]));

    setTimeout(() => { setSearch(""); }, 100);
  }

  const unselectTag = (i: number) =>
    setSelectedTags(prevState => prevState.filter((_k, index) => i !== index))

  const focusOpen = (value: boolean) => {
    if (value)
      setIsFocused(true);
  }

  const handleClickOutside = (event: any) => {
    if (wrapperRef.current && !(wrapperRef.current as any).contains(event.target))
      setIsFocused(false);
  };

  useEffect(() => {
    if (!search)
      setAvailable(tags);
    else
      setAvailable(tags.filter(({title}) => title.toLowerCase().includes(search)));
  }, [search]);

  useEffect(() => {
    // console.log(`DEFAULTS.changed`, defaultValues)
    if (defaultLoaded || !defaultValues.length)
      return;
    setDefaultLoaded(true);
    setSelectedTags(tags.filter(({title}) => defaultValues.includes(title)))
  }, [defaultValues]);

  useEffect(() => {
    // console.log(`SELECTED.changed`, selectedTags)
    onSelect(selectedTags);
    }, [selectedTags]);

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  return <div ref={wrapperRef}>
    <div>
      <SimpleTextInput label={label}
                       required={required}
                       placeHolder={placeHolder}
                       onFocus={focusOpen}
                       value={search}
                       onValueChange={e => setSearch(e.target.value)} />
    </div>
    <div className={tagsStyles.tag_entry_wrapper}>
      {(selectedTags).map(({title}, i) =>
        <Tag key={i} label={t(`common.tagsFiltersOptions.${title}`)} onDelete={() => unselectTag(i)} />)}
    </div>
    {
      <div className={tagsStyles.tags_popup} style={{display: isFocused ? "block" : "none"}}>
        <div>{popupTitle}</div>
        <ul>
          {(available).map((option, i) =>
            <li key={i}
                data-tag={i}
                aria-selected={selectedTags.includes(option)}
                onClick={(e) => {
                  console.log(`CLICKED`, e);
                  selectTag(option)
                }}>
              <span>{t(`common.tagsFiltersOptions.${option.title}`)}</span>
            </li>
          )}
        </ul>
      </div>
    }
  </div>
}