import {Grid, Modal} from "@mui/material";
import WindowBox from "@/components/window/window-box";
import {SimpleButton} from "@/components/buttons/simple/simple-button";

import styles from "@/components/certificate/certificate.module.css";
import {TagGroup} from "@/components/tag-group/tag-group";
import {format, subMonths} from "date-fns";
import {useCertificateData} from "@/components/hooks/queries/use-certificate-data";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {CertificateData} from "@/types/certificate/certificate-data";
import {useRef} from "react";
import html2canvas from "@jsplumb/html2canvas";
import {LogoSmall} from "@/components/logo/logo-small";
import {useSnackbar} from "notistack";
import axios from "axios";
import {useTranslation} from "@/i18n/client";

import {SDG_TO_ICON} from "@/constants/sdg";
import Image from "next/image";


type CertificateModalProps = {
  isOpen: boolean;
  onConfirm?: () => void;
  onCancel: () => void;
  companyId: string;
  mint?: (data: CertificateData) => void
}

export function CertificateModal({isOpen, onCancel, onConfirm, mint, companyId}: CertificateModalProps) {
  const {data: qData} = useCertificateData(companyId);
  const ref = useRef<HTMLDivElement|null>(null);
  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  const download = (image: string) => {
    const link = document.createElement('a');
    link.href = image;
    link.download = 'captured-image.png';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  const _onConfirm = async () => {
    if (!ref.current)
      return;

    enqueueSnackbar(t('common.snackbars.info.creatingImage'), {variant: "info"});

    const loadAllImages = (element: any) => {
      const images = element.querySelectorAll('img');
      return Promise.all(
        Array.from(images).map(
          (img: any) =>
            new Promise((resolve: any) => {
              if (img.complete) {
                resolve();
              } else {
                img.onload = resolve;
                img.onerror = resolve; // Resolve even if the image fails to load
              }
            })
        )
      );
    };

    const inlineSVGWithDimensions = async (element: any) => {
      const images = element.querySelectorAll('img[src$=".svg"]');

      const fetchSVG = async (url: any) => {
        const response = await fetch(url);
        return response.text();
      };

      const parseAndInlineStyles = (img: any, svgElement: any) => {
        // Extract <style> rules from the SVG
        const styleElement = svgElement.querySelector('style');
        const styleRules: any = {};

        if (styleElement) {
          const styleContent = styleElement.textContent;
          const rules = styleContent.split('}');

          rules.forEach((rule: any) => {
            const [selectors, properties] = rule.split('{');
            if (selectors && properties) {
              selectors.trim().split(',').forEach((selector: any) => {
                styleRules[selector.trim()] = properties.trim();
              });
            }
          });

          styleElement.remove(); // Remove <style> after processing
        }

        // Apply class-based styles inline
        const elements = svgElement.querySelectorAll('*');
        elements.forEach((el: any) => {
          const classList = el.getAttribute('class');
          if (classList) {
            classList.split(' ').forEach((cls: any) => {
              const style = styleRules[`.${cls}`];
              if (style) {
                style.split(';').forEach((prop: any) => {
                  const [key, value] = prop.split(':');
                  if (key && value) {
                    el.style[key.trim()] = value.trim();
                  }
                });
              }
            });
            el.removeAttribute('class'); // Clean up classes
          }
        });

        // Copy all computed styles from the original image to the SVG element
        const computedStyle = window.getComputedStyle(img);
        const stylesToCopy = [
          'margin', 'padding', 'border', 'display', 'box-sizing', 'float', 'position',
          'width', 'height', 'overflow', 'flex', 'align-items', 'justify-content', 'gap',
        ];

        stylesToCopy.forEach((prop: any) => {
          if (computedStyle[prop]) {
            svgElement.style[prop] = computedStyle[prop];
          }
        });

        // Preserve width and height
        const imgWidth = img.getAttribute('width') || window.getComputedStyle(img).width;
        const imgHeight = img.getAttribute('height') || window.getComputedStyle(img).height;

        if (imgWidth && imgHeight) {
          svgElement.setAttribute('width', imgWidth);
          svgElement.setAttribute('height', imgHeight);
        } else if (svgElement.hasAttribute('viewBox')) {
          // Fallback: calculate width and height from viewBox
          const viewBox = svgElement.getAttribute('viewBox').split(' ');
          const vbWidth = parseFloat(viewBox[2]);
          const vbHeight = parseFloat(viewBox[3]);
          if (!svgElement.hasAttribute('width')) svgElement.setAttribute('width', vbWidth);
          if (!svgElement.hasAttribute('height')) svgElement.setAttribute('height', vbHeight);
        }
      };

      await Promise.all(
        Array.from(images).map(async (img: any) => {
          const svgText = await fetchSVG(img.src);
          const parser = new DOMParser();
          const svgElement = parser.parseFromString(svgText, 'image/svg+xml').documentElement;

          parseAndInlineStyles(img, svgElement);

          img.replaceWith(svgElement);
        })
      );
    };

    await loadAllImages(ref.current);
    await inlineSVGWithDimensions(ref.current);

    const image =
      await html2canvas(ref.current, {backgroundColor: null, scale: 2})
        .then(canvas => {
          enqueueSnackbar(t('common.snackbars.success.imageCreated'), {variant: "success"})
          console.log(`${canvas.toDataURL("image/png")}`)
          return canvas.toDataURL("image/png")
        })
        .catch((e) => {
          console.log(`Failed to create image`, e);
          enqueueSnackbar(t('common.snackbars.warning.failedToCreateImage'), {variant: "warning"})
          return "";
        });

    if (!image)
      return;

    enqueueSnackbar(t('common.snackbars.info.creatingCertificate'), {variant: "info"});

    await axios.post(`/api/company/${companyId}/certificate`, {image})
      .then(() => {
        download(image);
        enqueueSnackbar(t('common.snackbars.success.certificateCreated'), {variant: "success"});
      })
      .catch((e) => {
        console.log(`Failed to create certificate`, e);
        enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToCreateCertificate'), {variant: "warning"});
      })

    onConfirm?.();
  }

  return <Modal open={isOpen} onClose={onCancel}>
    <Grid container style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: "729px"}}>
      <Grid item xs={12}>
        <WindowBox title={t('company.requestCertificate.modal.title')}>
          <div className={styles.certificateWrapper} ref={ref}>
            <div style={{marginBottom: 32, width: "100%", textAlign: "center"}}>
              <Base64Image src={qData?.company?.image || ""} style={{height: 59, margin: "0 auto"}}/>
            </div>
            <div className={styles.certificateTitleWrapper}>
              <div className={styles.certificateTitle}>{t('company.requestCertificate.modal.yourCompanysImpact')}</div>
              <div className={styles.certificateDateWrapper}>
                <div className={styles.certificateDateEntry}>
                  <span className={styles.certificateSubtitle}>{t('common.dates.from')}</span>
                  <span className={styles.certificateDate}>{format(subMonths(qData?.data?.createdDate || new Date(), 6), "dd/MM/yyyy")}</span>
                </div>
                <div className={styles.certificateDateEntry}>
                  <span className={styles.certificateSubtitle}>{t('common.dates.to')}</span>
                  <span className={styles.certificateDate}>{format(qData?.data?.createdDate || new Date(), "dd/MM/yyyy")}</span>
                </div>
              </div>
            </div>
            <div className={styles.certificateDataWrapper}>
              <div style={{display: "flex"}}>
                <div className={styles.certificateDataEntry}>
                  <div className={styles.certificateSubtitle}>{t('company.requestCertificate.modal.collaboratorsOnMissions')}</div>
                  <div className={styles.certificateDataEntryValue}>{qData?.data?.collaborators}</div>
                </div>
                <div className="vr"></div>
                <div className={styles.certificateDataEntry}>
                  <div className={styles.certificateSubtitle}>{t('company.requestCertificate.modal.volunteerHoursProvided')}</div>
                  <div className={styles.certificateDataEntryValue}>{qData?.data?.hoursProvided}</div>
                </div>
                <div className="vr"></div>
                <div className={styles.certificateDataEntry}>
                  <div className={styles.certificateSubtitle}>{t('company.requestCertificate.modal.initiativesSupported')}</div>
                  <div className={styles.certificateDataEntryValue}>{qData?.data?.initiatives}</div>
                </div>
              </div>
              <div style={{marginTop: 16}}>
                <div className={styles.certificateSubtitle} style={{marginBottom: 8}}>{t('company.requestCertificate.modal.supportedCauses')}</div>
                <TagGroup label="" tags={qData?.data?.causes || []}/>
              </div>
              <div style={{marginTop: 16}}>
                {qData?.data?.sdgs.map((entry, i) => (
                    <Image key={i} style={{boxSizing: "border-box", margin: "6px", width: 80, height: 80}} height={80} width={80} src={`/sdg/${SDG_TO_ICON[entry]}.svg`} alt={""} />
                ))}&nbsp;
              </div>
            </div>
            <div style={{textAlign: "center"}}>
              <div className={styles.certificateTitle}
                   style={{marginTop: 28, marginBottom: 16}}>{t('company.requestCertificate.modal.thankYouForYourSupport')}</div>
              <LogoSmall height={32} width={32} />
            </div>
          </div>
          <Grid container style={{marginTop: "16px"}}>
          <Grid item xs={12} style={{justifyContent: "end", display: "flex"}}>
              <div style={{marginRight: "8px"}}><SimpleButton label={"Download"} onClick={_onConfirm}/></div>
              <SimpleButton label={t('common.buttons.ok')} style="secondary" onClick={onCancel}/>
            </Grid>
          </Grid>
        </WindowBox>
      </Grid>
    </Grid>
  </Modal>
}