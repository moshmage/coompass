"use client";

import {useEffect, useState} from "react";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {CertificateModal} from "@/components/certificate/certificate-modal";
import {ConfirmationModal} from "@/components/modals/confirmation/confirmation-modal";
import {useCertificates} from "@/components/hooks/queries/use-certificates";
import styles from "@/components/certificate/certificate.module.css";
import {useTranslation} from "@/i18n/client";

export function RequestCertificate({companyId = ""}) {
  const [isOpen, setIsOpen] = useState(false);
  const [countOpen, setCountIsOpen] = useState<boolean>(false);
  const [allowedCerts, setAllowedCerts] = useState<number>(0);

  const {data, refetch: refetchData} = useCertificates(companyId);

  const {t} = useTranslation();

  useEffect(() => {
    setAllowedCerts(2 - (data?.length || 0))
  }, [data])

  return <>
    <SimpleButton label={t('company.requestCertificate.button')}
                  style="secondary" onClick={() => setCountIsOpen(true)} />
    <CertificateModal isOpen={isOpen} companyId={companyId}
                      onCancel={() => {
                        setIsOpen(false);
                        refetchData()
                      }}
                      onConfirm={() => {
                        setIsOpen(false)
                        refetchData();
                      }} />

    <ConfirmationModal isOpen={countOpen} title={t('company.requestCertificate.button')}
                       confirmLabel={t('company.requestCertificate.confirmationModal.confirmLabel')}
                       confirmDisabled={(data?.length || 0) >= 99}
                       onCancel={() => setCountIsOpen(false)}
                       onConfirm={() => { setCountIsOpen(false); setIsOpen(true); }}>
      <div style={{textAlign: "center"}}>
        <div className={styles.certificateSubtitle}>{t('company.requestCertificate.confirmationModal.modalSubtitle')}</div>
        <div style={{marginTop: 32}}>{allowedCerts}</div>
        <div>{t('company.requestCertificate.confirmationModal.totalRequestsLeft')}</div>
      </div>
    </ConfirmationModal>

  </>
}