import {marked} from "marked";
import sanitizeHtml from "sanitize-html";

export default function MarkedRender({ className = "", source = "_loading..._" }) {

  const sanitizedOptions = {
    allowedTags: sanitizeHtml.defaults.allowedTags.concat(["img"]),
    transformTags: {
      "a": (_tagName: string, attribs: any) => {
        return {
          tagName: "a",
          attribs: {
            ...attribs,
            target: "_blank",
            rel: "noopener noreferrer"
          }
        };
      }
    }
  }

  const markedOptions = {
    "async": false,
    "breaks": false,
    // newline: true,
    "extensions": null,
    "gfm": true,
    "hooks": null,
    "pedantic": false,
    "silent": false,
    "tokenizer": null,
    "walkTokens": null
  }

  const innerHtml = {__html: sanitizeHtml(marked(source, markedOptions) as string, sanitizedOptions)}

  return <div className={`marked-render markdown-body ${className}`} dangerouslySetInnerHTML={innerHtml}></div>
}