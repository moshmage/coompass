"use client";

import Image from "next/image";
import {useState} from "react";

import styles from "@/components/tabbed/tabbed.module.css";

type Tab = {
  title: string;
  icon: string;
  content(): React.ReactNode
}

type TabbedProps = {
  tabs: Tab[]
}

export default function Tabbed({tabs}: TabbedProps) {

  const [active, setActive] = useState(0);

  const Tab = ({title, icon}: Partial<Tab>, i: number) => {
    return <div key={i} onClick={() => setActive(i)}
                className={`${active === i ? styles.tabbed_tab_active : ""} ${styles.tabs_tab}`}>
      <Image className={styles.tabs_tab_icon} src={icon!} alt={"tab icon"}/>
      <span>{title}</span>
    </div>
  }

  return <>
    <div className={styles.tabbed_tabs}>
      {tabs.map(Tab)}
    </div>
    <div className={styles.tabs_content}>
      {tabs[active]?.content?.()}
    </div>
  </>

}