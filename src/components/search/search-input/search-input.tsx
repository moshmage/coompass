"use client";

import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";

import magnifyGlass from "@/assets/magnify-glass.svg";

type SearchInputProps = {
  onChange: (search: string) => void;
  disabled?: boolean;
}

export function SearchInput({onChange, disabled}: SearchInputProps) {
  return <SimpleTextInput beforeIcon={magnifyGlass} small disabled={disabled}
                          placeHolder="Search by keyword..."
                          onValueChange={e => onChange(e.target.value)}/>
}