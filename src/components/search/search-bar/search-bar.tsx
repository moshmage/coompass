"use client";

import {Grid} from "@mui/material";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";

import map from "@/assets/map-trifold.svg"

import elementStyle from "@/components/search/search.module.css";
import {DropdownButton} from "@/components/buttons/dropdown/dropdown-button";
import {Skills} from "@/constants/skills";
import {Dispatch, SetStateAction, useEffect, useState} from "react";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {MissionTags} from "@/constants/mission-tags";
import {CheckBox} from "@/components/inputs/check-box/check-box";
import {SDG} from "@/constants/sdg";
import {useTranslation} from "@/i18n/client";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Structural from "@/components/structural/structural";
import {useDistricts} from "@/components/hooks/queries/use-districts";
import {useLocale} from "@/i18n/hooks/locale-provider";

type SearchBarProps = {
  hideLocation?: boolean;
  hideSkills?: boolean;
  useSDG?: boolean;
  useOrgLocation?: boolean;
  onSearch: (search: string, location: string, causes: string[], skills: string[], virtual: boolean, inPerson: boolean, districts: number[]) => void;
}

export function SearchBar({onSearch, hideLocation, hideSkills, useSDG, useOrgLocation}: SearchBarProps) {
  const [search, setSearch] = useState("");
  const [location, setLocation] = useState("");
  const [causes, setCauses] = useState<string[]>([]);
  const [skills, setSkills] = useState<string[]>([]);
  const [isVirtual, setIsVirtual] = useState(false);
  const [isInPerson, setisInPerson] = useState(false);
  const [selectedDistricts, setSelectedDistricts] = useState<number[]>([]);
  const [districtOptions, setDistrictOptions] = useState<DropdownOption[]>([]);

  const {t} = useTranslation();
  const locale = useLocale();

  const onSelect = (options: DropdownOption[], cb: Dispatch<SetStateAction<string[]>>) => {
    cb(options.map(({title}) => title));
  }

  const _setIsVirtual = (b: boolean) => {
    setIsVirtual(b);

    if (isInPerson)
      setisInPerson(false);
  }

  const _setIsInPerson = (b: boolean) => {
    setisInPerson(b);

    if (isVirtual)
      setIsVirtual(false);
  }

  const _setSelectedDistricts = (options: DropdownOption[]) => {
    setSelectedDistricts(
      options
        .filter(({active}) => active)
        .map(({value}) => value as number)
    )
  }

  const {data: districts} = useDistricts();

  useEffect(() => {
    onSearch(search, location, causes, skills, isVirtual, isInPerson, selectedDistricts);
    }, [search, location, causes, skills, isVirtual, isInPerson, selectedDistricts]);

  useEffect(() => {
    setDistrictOptions(
      districts.map(({id, ...data}) => ({title: data[locale], value: id}))
    )
  }, [districts, locale]);

  return <Grid container className={elementStyle.searchBar} style={{display: "flex", alignItems: "center", margin: "32px 0"}} spacing={1}>
    {
      !hideLocation
        ? <>
          <Grid item xs={12} sm={12} md={7}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={7} md={6}>
                <SimpleTextInput beforeIcon={map} small disabled={isVirtual}
                                 placeHolder={t('common.location.enterLocation')}
                                 value={location}
                                 onValueChange={e => setLocation(e.target.value)} />
              </Grid>
              <Grid item xs={12} sm={6} md={6} style={{display: "flex", alignItems: "center", gap: 8}}>
                <CheckBox onChange={_setIsVirtual} isChecked={isVirtual} label={t('common.location.virtual')} />
                <CheckBox onChange={_setIsInPerson} isChecked={isInPerson} label={t('common.location.inPerson')} />
              </Grid>
            </Grid>
          </Grid>
        </>
        : null
    }
    <Structural.If condition={!!useOrgLocation}>
      <Grid item xs={12} sm={12} md={7}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={7} md={6}>
            <DropdownButton multiple label={t('common.location.select')} options={districtOptions} translatedOptions onSelect={_setSelectedDistricts}/>
          </Grid>
        </Grid>
      </Grid>
    </Structural.If>
    <Grid item xs={12} sm={12} md={useOrgLocation ? 5 : hideLocation ? 12 : 5}>
      <Grid container spacing={1}>
        {/*<Grid item xs={12} sm={12} md={6} lg={7}><SearchInput onChange={setSearch}/></Grid>*/}
        <Grid item xs={12} style={{justifyContent: "end", display: "flex", alignItems: "center"}}>
          {hideLocation ? <div style={{margin: "0 auto"}}></div> : <div style={{margin: "0 8px"}}></div>}
          <DropdownButton multiple sx={{minWidth: "fit-content"}}
                          label={useSDG ? t('common.sdg.goals') : t('common.causes.causeAreas')}
                          options={useSDG ? SDG : MissionTags}
                          onSelect={(opts) => onSelect(opts, setCauses) } />
          {
            !hideSkills
              ? <>
                <div style={{margin: "0 8px"}}></div>
                <DropdownButton multiple
                                label={t('common.skills.skills')}
                                options={Skills}
                                onSelect={(opts) => onSelect(opts, setSkills)}/>
              </>
              : null
          }
        </Grid>
      </Grid>
    </Grid>
  </Grid>
}