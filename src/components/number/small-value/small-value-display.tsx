
export function SmallValueDisplay({number, symbol, separator = "."}: {number: string, symbol?: string, separator?: string}) {
  const [_number, _decimal] = number.toString().split(separator);
  const regex = /0(0+)/
  const matcher = _decimal.match(regex);
  const zerosAmount = matcher ? matcher[1].length : 0;

  const decimal = zerosAmount ? _decimal.replace(regex, "") : _decimal;
  return <>{symbol}{_number}{separator}{zerosAmount ? <sub>{zerosAmount}</sub> : ""}{decimal}</>
}