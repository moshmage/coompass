
import styles from "@/components/number/x-of-total/x-of-total.module.css"

export function XOfTotal({x = 0, total = 0}) {
  return <span className={styles.xOfTotal_actual}>{x}<span>/{total}</span></span>
}