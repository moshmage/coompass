
import styles from "@/components/number/display/number-display.module.css"
import {SmallValueDisplay} from "@/components/number/small-value/small-value-display";

export function NumberDisplay({number = 0}) {
  return <div className={styles.numberDisplay_wrapper}>
    <span className={styles.numberDisplay_value}>{number < 0 ? <SmallValueDisplay number={number.toString()} /> : number}</span>
  </div>
}