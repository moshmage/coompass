"use client";
import {useState} from "react";
import {getLocaleClient, useTranslation} from "@/i18n/client";
import {enqueueSnackbar, SnackbarProvider} from "notistack";
import {isEmail, normalizeEmail} from "validator";
import axios from "axios";
import styles from "@/components/session/session-box.module.css";
import {LogoSmall} from "@/components/logo/logo-small";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import envelope from "@/assets/envelopeSimple.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import {PasswordBox} from "@/components/session/password-box/password-box";
import {useRouter} from "next/navigation";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";

export function ResetAccount({searchParams}: any) {
  const [email, setEmail] = useState("");
  const [confirmationKey, setConfirmationKey] = useState(searchParams?.key);
  const [newPassword, setNewPassword] = useState("");
  const [valid, setValid] = useState(false);

  const {t} = useTranslation();
  const router = useRouter()

  const resetAccount = async () => {
    enqueueSnackbar(t("common.snackbars.info.resettingPassword"), {variant: "info"});
    if (!isEmail(email)) {
      enqueueSnackbar(t("common.snackbars.warning.invalidEmailValue"), {variant: "warning"});
      return;
    }

    axios.post(`/api/register/reset`, {email: normalizeEmail(email), confirmationKey, newPassword})
      .then(_ => {
        enqueueSnackbar(t("common.snackbars.success.passwordWasReset"), {variant: "success"})
        router.push("/")
      })
      .catch(e => {
        console.log(`Failed to request reset password`, e);
        enqueueSnackbar(t('common.snackbars.warning.failedToRequestPasswordReset'), {variant: "warning"})
      })
  }

  const onPasswordChange = (p: string, _c: string, v: boolean) => {
    setValid(v);

    if (!v)
      return;

    setNewPassword(p);
  }

  return <SnackbarProvider maxSnack={3}>
    <LocaleProvider value={getLocaleClient()}>
    <div className={styles.sessionBox}>
      <LogoSmall/>

      <div className={styles.sessionBox_title}>{t('session.reset.requestResetAccount')}</div>
      <div className={styles.sessionBox_subtitle}>{t('session.reset.enterDetails')}</div>

      <div>
        <SimpleTextInput label={t('session.emailAddress')}
                         type="email"
                         name="username"
                         value={email}
                         onValueChange={e => setEmail(e.target.value)}
                         beforeIcon={envelope}
                         placeHolder="email@domain.tld"/>

        <SimpleTextInput label={t('session.confirmationKey')}
                         name="confirmationKey"
                         value={confirmationKey}
                         onValueChange={e => setConfirmationKey(e.target.value)}
                         placeHolder="email@domain.tld"/>

        <PasswordBox onChange={onPasswordChange} isReset={true}/>

        <div className={styles.sessionBox_buttonWrapper}>
          <SimpleButton disabled={!valid} block label={t('session.reset.resetPassword')} style="primary"
                        onClick={resetAccount}/>
        </div>
        <div style={{marginTop: "8px"}}>
          <Link href="/session/register" className={styles.sessionBox_buttonWrapper}>
            <SimpleButton block label={t('session.registerAnAccount')} style="secondary"/>
          </Link>
        </div>
        <div style={{marginTop: "8px"}}>
          <Link href="/" className={styles.sessionBox_buttonWrapper}>
            <SimpleButton block label={t('session.register.loginWithAnExistingAccount')} style="secondary"/>
          </Link>
        </div>
      </div>

    </div>
    </LocaleProvider>
  </SnackbarProvider>
}