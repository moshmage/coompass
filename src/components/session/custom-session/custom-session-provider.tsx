"use client";

import {getSession} from "next-auth/react";
import {useSelfStore} from "@/components/hooks/use-self";
import axios from "axios";
import {User} from "@/types/user/user";
import {useQuery} from "@tanstack/react-query";
import {useEffect} from "react";
import {QueryKeys} from "@/constants/query-keys";

export function CustomSessionProvider({ children }: any) {

  const setSession = useSelfStore((state) => state.setSession);
  const setUserData = useSelfStore((state) => state.setUserData);
  const setRefetchUser = useSelfStore((state) => state.setRefetchUser);
  const setIsFetching = useSelfStore((state) => state.setIsFetching);

  const getUser = () =>
    axios.get<User|null>("/api/users/self")
      .then(data => data.data)
      .catch(_ => null) as Promise<User|null>;

  const {data: userData, refetch, isFetching} =
    useQuery({queryKey: QueryKeys.userData, queryFn: getUser, staleTime: 10, initialData: null});

  const {data: sessionData} =
    useQuery({queryKey: QueryKeys.userSession, queryFn: () => getSession({broadcast: false}), staleTime: 10, initialData: null})

  useEffect(() => {

    if (userData)
      setUserData(userData);
    else setUserData(null);

    if (sessionData)
      setSession(sessionData)
    else setSession(null);


  }, [sessionData, userData])

  useEffect(() => { setRefetchUser(refetch) }, [refetch]);
  useEffect(() => { setIsFetching(isFetching) }, [isFetching]);

  return <>{children}</>
}