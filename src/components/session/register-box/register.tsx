"use client";

import {LogoSmall} from "@/components/logo/logo-small";
import styles from "@/components/session/session-box.module.css";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import envelope from "@/assets/envelopeSimple.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {useRouter} from "next/navigation";
import {PasswordBox} from "@/components/session/password-box/password-box";
import Link from "next/link";
import {useState} from "react";
import {enqueueSnackbar, SnackbarProvider} from "notistack";
import axios from "axios";
import {getLocaleClient, useTranslation} from "@/i18n/client";
import {RegisterTypeSelection} from "@/components/session/register-type-selection/register-type-selection";
import Structural from "@/components/structural/structural";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";

export function RegisterBox() {
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [type, setType] = useState("");
  const [valid, setValid] = useState(false);
  const [roleSelected, setRoleSelected] = useState(false);

  const {t} = useTranslation();
  const router = useRouter();

  const _register = () => {
    if (!valid)
      return;

    enqueueSnackbar(t("common.snackbars.info.registering"), {variant: "info"});

    axios.post(`/api/register/user`, {type, username, password})
      .then(() => {
        router.push(`/?message=${t("common.snackbars.success.registered")}`);
      })
      .catch((e: any) => {
        console.log(`Failed to register`, e);
        enqueueSnackbar(t("common.snackbars.warning.failedToRegister", {message: e?.response?.data?.error}), {variant: "warning"})
      })

  }

  return <SnackbarProvider maxSnack={3}>
    <LocaleProvider value={getLocaleClient()}>

    <Structural>
      <Structural.If condition={!roleSelected}>
        <RegisterTypeSelection onNext={(v) => { setType(v); setRoleSelected(true) }} />
      </Structural.If>
      <Structural.Else>
        <div className={styles.sessionBox}>
          <LogoSmall/>

          <div className={styles.sessionBox_title}>{t('session.registerAnAccount')}</div>
          <div className={styles.sessionBox_subtitle}>{t('session.register.enterYourCredentials')}</div>

          <div>

            <SimpleTextInput label={t('session.emailAddress')}
                             type="email"
                             value={username}
                             onValueChange={e => setUsername(e.target.value)}
                             name="username"
                             beforeIcon={envelope}
                             placeHolder="email@domain.tld"/>

            <PasswordBox onChange={(p, c, v) => {
              setValid(v);
              setPassword(p);
            }}/>

            <div className={styles.sessionBox_buttonWrapper}>
              <SimpleButton disabled={!valid || !username} block label={t('session.register.registerAs', {type: t(`session.register.as.${type}`)})} style="primary"
                            onClick={_register}/>
            </div>

            <div className={styles.sessionBox_buttonWrapper}>
              <SimpleButton block label={t('session.register.roleSelection')} style="secondary"
                            onClick={() => setRoleSelected(false)}/>
            </div>

            <div>
              <Link href="/" className={styles.sessionBox_buttonWrapper}>
                <SimpleButton block label={t('session.register.loginWithAnExistingAccount')} style="secondary"/>
              </Link>
            </div>
          </div>

        </div>
      </Structural.Else>
    </Structural>

    </LocaleProvider>
  </SnackbarProvider>
}