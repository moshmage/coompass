"use client";

import {UserTypes} from "@/types/user/user";
import {Grid} from "@mui/material";
import {useState} from "react";
import Image from "next/image";
import {useTranslation} from "@/i18n/client";
import {SimpleButton} from "@/components/buttons/simple/simple-button";

type RegisterTypeSelectionProps = {
  onNext: (type: UserTypes) => void;
}

export function RegisterTypeSelection({onNext}: RegisterTypeSelectionProps) {
  const [type, setType] = useState<UserTypes>(UserTypes.user);
  const {t} = useTranslation();

  const types = [
    {title: () => t("session.register.company"), value: UserTypes.company, action: () => setType(UserTypes.company)},
    {title: () => t("session.register.organization"), value: UserTypes.ngo, action: () => setType(UserTypes.ngo)},
    {title: () => t("session.register.uUserser"), value: UserTypes.user, action: () => setType(UserTypes.user)},
  ]

  const images: any = {
    [UserTypes.user]: "/home-pic-user.jpg",
    [UserTypes.company]: "/home-pic-company.jpg",
    [UserTypes.ngo]: "/home-pic-organization.jpg",
  }

  const optionStyle = (value: UserTypes) => ({
    ... value === type ? {border: "solid 1px rgba(135, 210, 197, 1)"} : {},
    ... value === type ? {margin: "-1px -1px 15px -1px"} : {marginBottom: 16},
    backgroundColor: "rgba(255, 255, 255, 0.04)",
    borderRadius: 16,
    padding: 16,
    cursor: "pointer"
  })

  return <Grid container>
    <Grid item xs={5} style={{position: "relative", overflow: "hidden", height: "100vh", margin: "-80px 0 0 -6px"}} sx={{display: {xs: "none", sm: "block"}}}>
      <img style={{objectFit: "cover", width: "41vw", height: "100vh"}} src={images[type]} alt="home-pic"/>
    </Grid>
    <Grid item xs={12} sm={7} md={7}>
      <Grid container>
        <Grid item xs={1} sm={1} md={1} lg={3} />
        <Grid item xs={10} sm={10} md={10} lg={6}>
          <Grid container>
            <Grid item xs={12} style={{textAlign: "center", marginTop: 56}}>
              <div style={{width: 64, height: 64, borderRadius: "100%", padding: 16, border: "1px solid rgba(255, 255, 255, 0.12)", margin: "0 auto"}}>
                <Image src={"/userGear.png"} alt={""} height={32} width={32} />
              </div>
              <div style={{fontSize: 24, fontWeight: 600, color: "white", marginTop: 8, marginBottom: 16}}>{t("session.register.roleSelection")}</div>
              <div style={{fontSize: 14, fontWeight: 500, color: "rgba(255, 255, 255, 0.6)", marginTop: 8, marginBottom: 24}}>{t("session.register.chooseYourRole")}</div>
            </Grid>
          </Grid>
          {
            types.map(({title, action, value}, i) => <>
              <Grid container key={`type-${i}`}>
                <Grid item xs={12} style={optionStyle(value)} onClick={action}>
                  <div style={{fontWeight: 500, fontSize: 16, color: "white"}}>{title()}</div>
                </Grid>
              </Grid>
            </>)
          }
          <Grid container>
            <Grid item xs={12}>
              <SimpleButton disabled={!type} block style="primary" label={t('common.buttons.continue')} onClick={() => onNext(type!)}></SimpleButton>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={1} sm={1} md={1} lg={3} />
      </Grid>
    </Grid>
  </Grid>
}