"use client";

import styles from "@/components/session/session-box.module.css";
import {LogoSmall} from "@/components/logo/logo-small";
import {getLocaleClient, useTranslation} from "@/i18n/client";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import envelope from "@/assets/envelopeSimple.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import Link from "next/link";
import {useState} from "react";
import {enqueueSnackbar, SnackbarProvider} from "notistack";
import axios from "axios";
import {isEmail, normalizeEmail} from "validator";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";

export function ForgotPasswordBox() {
  const [email, setEmail] = useState("")
  const {t} = useTranslation();

  const requestPasswordRequest = async () => {
    enqueueSnackbar(t("common.snackbars.info.requestingPasswordReset"), {variant: "info"});
    if (!isEmail(email)) {
      enqueueSnackbar(t("common.snackbars.warning.invalidEmailValue"), {variant: "warning"});
      return;
    }

    axios.post(`/api/register/forgot`, {email: normalizeEmail(email)})
      .then(_ => {
        enqueueSnackbar(t("common.snackbars.success.requestedPasswordRequest"), {variant: "success"})
      })
      .catch(e => {
        console.log(`Failed to request reset password`, e);
        enqueueSnackbar(t('common.snackbars.warning.failedToRequestPasswordReset'), {variant: "warning"})
      })
  }

  return <SnackbarProvider maxSnack={3}>
    <LocaleProvider value={getLocaleClient()}>
      <div className={styles.sessionBox}>
        <LogoSmall/>

        <div className={styles.sessionBox_title}>{t('session.reset.requestResetAccount')}</div>
        <div className={styles.sessionBox_subtitle}>{t('session.reset.enterYourEmail')}</div>

        <div>
          <SimpleTextInput label={t('session.emailAddress')}
                           type="email"
                           name="username"
                           value={email}
                           onValueChange={e => setEmail(e.target.value)}
                           beforeIcon={envelope}
                           placeHolder="email@domain.tld"/>

          <div className={styles.sessionBox_buttonWrapper}>
            <SimpleButton block label={t('session.reset.resetAccount')} style="primary"
                          onClick={requestPasswordRequest}/>
          </div>
          <div style={{marginTop: "8px"}}>
            <Link href="/session/register" className={styles.sessionBox_buttonWrapper}>
              <SimpleButton block label={t('session.registerAnAccount')} style="secondary"/>
            </Link>
          </div>
          <div style={{marginTop: "8px"}}>
            <Link href="/" className={styles.sessionBox_buttonWrapper}>
              <SimpleButton block label={t('session.register.loginWithAnExistingAccount')} style="secondary"/>
            </Link>
          </div>
        </div>

      </div>
    </LocaleProvider>
  </SnackbarProvider>
}