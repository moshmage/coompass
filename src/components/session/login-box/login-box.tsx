import {auth, signIn} from "@/handlers/server/credentials/auth";
import {isRedirectError} from "next/dist/client/components/redirect";
import {LogoSmall} from "@/components/logo/logo-small";
import styles from "@/components/session/session-box.module.css";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import envelope from "@/assets/envelopeSimple.svg";
import lock from "@/assets/lock.svg";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {redirect} from "next/navigation";
import {Alert, Snackbar} from "@mui/material";
import {AlertColor} from "@mui/material/Alert/Alert";
import Link from "next/link";
import {createTranslation} from "@/i18n/server";
import {getUserRedirectPath} from "@/handlers/utils/get-user-redirect-path";
import {fetchServerData} from "@/handlers/server/fetch/fetch-data";
import {User} from "@/types/user/user";

export async function LoginBox({searchParams}: any) {
  const {t} = await createTranslation();
  let warningOpen = !!searchParams?.message;
  let warningMessage = searchParams?.message ? searchParams?.message : "";
  let warningType: AlertColor = searchParams?.error ? "error" : "success";

  const _auth = await auth();
  if ((_auth?.user as User))
    redirect(getUserRedirectPath(_auth?.user as User));

  async function _signIn(formData: FormData) {
    "use server";
    let redirection = "";

    try {
      await signIn("credentials", formData);
    } catch (e: any) {
      if (isRedirectError(e)) {
        const user = await fetchServerData(`/api/users?email=${formData.get("username")}`);
        redirection = getUserRedirectPath(user);
      } else {
        redirection = `/?message=${e?.cause?.err?.message || e?.message || "No message provided"}&error=1`;
      }
    }

    redirect(redirection);
  }

  return <div className={styles.sessionBox}>
    <LogoSmall/>

    <div className={styles.sessionBox_title}>{t('session.sign-in.loginToYourAccount')}</div>
    <div className={styles.sessionBox_subtitle}>{t('session.sign-in.enterLoginDetails')}</div>

    <form action={_signIn}>
      <SimpleTextInput label={t('session.emailAddress')}
                       type="email"
                       name="username"
                       beforeIcon={envelope}
                       placeHolder="email@domain.tld"/>

      <SimpleTextInput label={t('session.password')} type="password" name="password" beforeIcon={lock}/>

      <div className={styles.sessionBox_buttonWrapper}>
        <SimpleButton block label="Login" style="primary" type="submit"/>
        <div id="spinnerContainer" className={styles.spinnerContainer}>
          <div className={styles.spinner}></div>
        </div>
      </div>
      <div style={{marginTop: "8px"}}>
        <Link href="/session/register" className={styles.sessionBox_buttonWrapper}>
          <SimpleButton block label={t('session.registerAnAccount')} style="secondary" type="submit"/>
        </Link>
      </div>
      <div style={{marginTop: 16, fontSize: 14, color: "rgba(255,255,255,.6)"}}>
        <Link href="/session/forgot-password">
          {t('session.sign-in.forgotYourPassword')}
        </Link>
      </div>
    </form>

    <Snackbar open={warningOpen} autoHideDuration={3000}>
      <Alert
        severity={warningType}
        variant="filled"
        sx={{width: '100%'}}>
        {warningMessage}
      </Alert>
    </Snackbar>

    <div dangerouslySetInnerHTML={{
      __html: `
        <script>
          
          document.addEventListener("DOMContentLoaded", function() {
            const form = document.querySelector("form");
            const spinnerContainer = document.getElementById("spinnerContainer");
            let interval = null;
            
            if (form) {
              form.addEventListener("submit", function() {
                if (spinnerContainer) {
                  spinnerContainer.style.display = "flex";
                }

                if (interval)
                    clearInterval(interval);

                interval = setInterval(() => {
                  const spinnerContainer = document.getElementById("spinnerContainer");
                  const urlParams = new URLSearchParams(window.location.search);
                  const error = urlParams.get("error");
              
                  if (spinnerContainer) {
                    if (error === "1") {
                      spinnerContainer.style.display = "none";
                      clearInterval(interval);
                    }
                  }
                }, 100);
                
              });
            }
          });
        </script>
      `
    }}/>

  </div>
}