"use client";

import {useSelf} from "@/components/hooks/use-self";
import {useRouter} from "next/navigation";
import {getUserRedirectPath} from "@/handlers/utils/get-user-redirect-path";
import {useEffect} from "react";

export function Redirector() {
  const {userData, isFetching} = useSelf();
  const router = useRouter();
  let redirecting = false;

  const _redirect = () => {
    if (redirecting)
      return;

    redirecting = true;

    router.push(getUserRedirectPath(userData!))
  }

  useEffect(() => {
    console.log(`USER DATA`, userData);
    if (!isFetching && userData)
      _redirect();

  }, [userData, isFetching]);

  return <></>
}