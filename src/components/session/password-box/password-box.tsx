"use client";

import {useEffect, useState} from "react";
import {CheckCircle} from "@/components/svg/check-circle";
import lock from "@/assets/lock.svg";
import {SimpleTextInput} from "@/components/inputs/text-input/simple-text-input";
import {useTranslation} from "@/i18n/client";
import {PasswordSpecialChars} from "@/constants/password-restrictions";

type PasswordBoxProps = {
  onChange?: (password: string, confirmedPassword: string, passwordMatch: boolean) => void;
  isReset?: boolean;
}

export function PasswordBox({onChange, isReset}: PasswordBoxProps) {
  const {t} = useTranslation()
  const [password, setPassword] = useState<string>("");
  const [confirmPassword, setConfirmPassword] = useState<string>("");

  const passwordValidator =
    ({
      match: password && confirmPassword && password === confirmPassword,
      correctLength: password.length >= 12,
      hasLowerCase: /[a-z]/.test(password),
      hasUppercase: /[A-Z]/.test(password),
      hasDecimal: /\d/.test(password),
      hasSpecialChar: new RegExp(`[${PasswordSpecialChars}]`).test(password),
      onlySupportedChar: !(new RegExp(`[^A-Za-z\\d${PasswordSpecialChars}]`).test(password)),
    })

  const validator = [
    [t('session.register.passwordMustMatch'), passwordValidator.match],
    [t('session.register.haveAtLeastCharacters'), passwordValidator.correctLength],
    [t('session.register.oneLowercaseLetter'), passwordValidator.hasLowerCase],
    [t('session.register.oneUppercaseLetter'), passwordValidator.hasUppercase],
    [t('session.register.oneNumber'), passwordValidator.hasDecimal],
    [t('session.register.oneSpecialChar'), passwordValidator.hasSpecialChar],
    [t('session.register.onlySupportedChars'), password && passwordValidator.onlySupportedChar],
  ]

  useEffect(() => {
    const valid = Object.values(passwordValidator).every(v => v === true);
    onChange?.(password, confirmPassword, valid);
  }, [password, confirmPassword])

  return <>
    <div style={{paddingBottom: "16px"}}>
      <SimpleTextInput label={t(`session${isReset ? ".reset" : ""}.password`)}
                       type="password"
                       name="password"
                       onValueChange={e => setPassword(e?.target.value)}
                       beforeIcon={lock}/>

      <SimpleTextInput label={t(`session.${isReset ? "reset" : "register"}.confirmPassword`)}
                       type="password"
                       name="confirm-password"
                       onValueChange={e => setConfirmPassword(e?.target.value)}
                       beforeIcon={lock}/>
    </div>

    {
      validator.map(([text, value], i) =>
        <div key={i} style={{display: "flex", alignItems: "center", paddingBottom: "8px"}}>
          <CheckCircle fillColor={value ? "rgba(135, 210, 197, 1)" : ""} opacity={value ? 1 : .3}/>
          <span style={{marginLeft: "8px"}}>{text}</span>
        </div>
      )
    }
  </>
}