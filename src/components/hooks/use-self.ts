"use client";

import {User} from "@/types/user/user";
import {create} from "zustand";
import {Session} from "@auth/core/types";

type UseSelfStoreProps = {
  sessionData: Session | null,
  userData: User | null,
  isFetching: boolean,
  setSession(sessionData: Session | null): void,
  setUserData(userData: User | null): void,
  setRefetchUser(fn: any): void;
  refetchUser?(): Promise<void>;
  setIsFetching(v: boolean): void;
}

export const useSelfStore = create<UseSelfStoreProps>((set, get) => ({
  sessionData: null,
  userData: null,
  isFetching: false,
  setSession: (sessionData: Session) => set({sessionData}),
  setUserData: (userData: User) => set({userData}),
  setRefetchUser: (refetchUser) => set({refetchUser}),
  setIsFetching:(isFetching: boolean) => set({isFetching}),
}))

export function useSelf() {
  const {sessionData, userData, refetchUser, isFetching} =
    useSelfStore((state) => ({
      sessionData: state.sessionData,
      userData: state.userData,
      refetchUser: state.refetchUser,
      isFetching: state.isFetching
    }));

  return {userData, sessionData, refetch: refetchUser, isFetching}
}