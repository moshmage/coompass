import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getDistrictsList} from "@/handlers/client/districts/get-districts-list";
import {District} from "@/types/districts/district";

export function useDistricts() {
    return useQuery({queryKey: QueryKeys.districts, queryFn: () => getDistrictsList(), initialData: [] as District[], staleTime: 0})
}