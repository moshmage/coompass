import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyCertificates} from "@/handlers/client/company/get-company-certificates";

export function useCertificates(companyId = "") {
  return useQuery({
    queryKey: QueryKeys.certificates(companyId),
    queryFn: () => getCompanyCertificates(companyId),
    enabled: !!companyId,
    initialData: [],
    staleTime: 10,
  })
}