"use client";

import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getMissionsWithParticipantsOf} from "@/handlers/client/company/get-missions-with-participants-of";

export function useMissionsWithParticipantsOf(companyId = "") {

  return useQuery({queryKey: QueryKeys.companyMissions(companyId), queryFn: () => getMissionsWithParticipantsOf(companyId), initialData: [], staleTime: 0})
}