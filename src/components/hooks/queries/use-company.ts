"use client";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompany} from "@/handlers/client/company/get-company";
import {Company} from "@/types/company/company";

export function useCompany(companyId = "") {
  return useQuery({queryKey: QueryKeys.company(companyId), queryFn: () => getCompany(companyId), initialData: null, staleTime: 10, enabled: !!companyId})
}