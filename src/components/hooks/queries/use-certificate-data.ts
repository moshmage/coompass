import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyCertififacesData} from "@/handlers/client/company/get-company-certififaces-data";

export function useCertificateData(companyId = "") {
  return useQuery({
    queryKey: QueryKeys.certificatesData(companyId),
    enabled: !!companyId,
    queryFn: () => getCompanyCertififacesData(companyId),
    initialData: null,
    staleTime: 10
  })
}