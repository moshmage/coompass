import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyParticipantsState} from "@/handlers/client/company/get-company-participants-state";

export function useCompanyParticipantsState(companyId: string) {
  return useQuery({queryKey: QueryKeys.companyParticipantsState(companyId), queryFn: () => getCompanyParticipantsState(companyId), staleTime: 3000, initialData: []})
}