import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import axios from "axios";

export function useEngagement(companyId = "") {
  const _getCompanyEngagement = () =>
    axios.get(`/api/company/${companyId}/engagement`)
      .then(({data}) => data)
      .catch((e) => {
        console.log(`Error fetching engagement`, e)
        return {
          all: 0,
          thirtyDays: 0,
          sevenDays: 0,
        }
      });
  return useQuery({
    queryKey: QueryKeys.companyEngagement(companyId),
    queryFn: _getCompanyEngagement,
    enabled: !!companyId,
    staleTime: 5,
    initialData: {
      all: 0,
      thirtyDays: 0,
      sevenDays: 0,
    }
  })
}