"use client";

import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyUserLogs} from "@/handlers/client/company/get-company-user-logs";

export function useUserLogs(companyId: string) {
  return useQuery({queryKey: QueryKeys.userLogs, initialData: [], staleTime: 5, queryFn: () => getCompanyUserLogs(companyId)})
}