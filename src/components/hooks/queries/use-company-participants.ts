import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyParticipants} from "@/handlers/client/company/get-company-participants";

export function useCompanyParticipants(companyId: string) {
  return useQuery({
    queryKey: QueryKeys.companyParticipants(companyId),
    queryFn: () => getCompanyParticipants(companyId),
    enabled: !!companyId,
    staleTime: 1,
    initialData: []})
}