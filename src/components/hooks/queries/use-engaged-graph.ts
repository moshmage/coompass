import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getOrgEngagement} from "@/handlers/client/company/get-org-engagement";

export function useEngagedGraph(companyId: string) {
  return useQuery({
    queryKey: QueryKeys.companyEngagementGraph(companyId),
    initialData: {data: [], labels: []},
    queryFn: () => getOrgEngagement(companyId), staleTime: 5,
    enabled: !!companyId,
  })
}