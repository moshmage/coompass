import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getMissions} from "@/handlers/client/missions/get-missions";

export function useMissions() {
  return useQuery({
    queryKey: QueryKeys.missions(),
    queryFn: getMissions,
    initialData: [],
    staleTime: 10,
  })
}