import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanies} from "@/handlers/client/company/get-companies";

export const useCompanies = ({type = "", search = "", companyId = ""}) =>
  useQuery({queryKey: QueryKeys.companies({type, search, companyId}), queryFn: () => getCompanies({type, search, companyId}), initialData: [], staleTime: 1})