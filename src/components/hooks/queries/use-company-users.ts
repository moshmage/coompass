"use client";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompanyUsers} from "@/handlers/client/company/get-company-users";

export function useCompanyUsers(companyId = "") {
  return useQuery({queryKey: QueryKeys.companyUsers, queryFn: () => getCompanyUsers(companyId), initialData: [], staleTime: 0});
}