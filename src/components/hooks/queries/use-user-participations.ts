"use client";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {useSelf} from "@/components/hooks/use-self";
import {getUserParticipations} from "@/handlers/client/missions/get-user-participations";

export function useUserParticipations() {
  const {userData: user} = useSelf();

  return useQuery({queryKey: QueryKeys.userParticipations(user?.id!), queryFn: () => getUserParticipations(user?.id!), initialData: [], enabled: !!user?.id, staleTime: 10})
}