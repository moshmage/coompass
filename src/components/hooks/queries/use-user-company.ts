import {useSelf} from "@/components/hooks/use-self";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCompany} from "@/handlers/client/company/get-company";
import {useEffect} from "react";

export function useUserCompany() {
  const {userData} = useSelf();

  useEffect(() => {
    console.log(`USER DATA`, userData);
  }, [userData]);

  return useQuery({
    queryKey: QueryKeys.company(userData?.companies?.[0]!),
    queryFn: () => getCompany(userData?.companies?.[0]),
    enabled: !!userData?.companies?.[0],
    initialData: null,
    staleTime: 10
  })
}