import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getOrgMissions} from "@/handlers/client/company/get-org-missions";

export function useOrganizationsMissions(companyId: string) {
  return useQuery({queryKey: QueryKeys.companyMissions(companyId), queryFn: () => getOrgMissions(companyId), initialData: [], staleTime: 5})
}