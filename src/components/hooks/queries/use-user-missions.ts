"use client";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getUserMissions} from "@/handlers/client/missions/get-user-missions";
import {useSelf} from "@/components/hooks/use-self";

export function useUserMissions() {
  const {userData: user} = useSelf();

  return useQuery({queryKey: QueryKeys.userMissions, queryFn: () => getUserMissions(user?.id!), initialData: [], enabled: !!user?.id, staleTime: 10})
}