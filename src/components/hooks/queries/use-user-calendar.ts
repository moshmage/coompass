import {useSelf} from "@/components/hooks/use-self";
import {useQuery} from "@tanstack/react-query";
import {QueryKeys} from "@/constants/query-keys";
import {getCalendar} from "@/handlers/client/calendar/get-calendar";

export function useUserCalendar() {
  const {userData} = useSelf();

  return useQuery({
    queryKey: QueryKeys.calendar(userData?.id!),
    enabled: !!userData?.id,
    queryFn: () => getCalendar(userData?.id!),
    initialData: [],
    staleTime: 10
  })
}