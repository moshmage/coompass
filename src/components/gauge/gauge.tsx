import {CSSProperties} from "react";

export default function Gauge({min = 0, max = 100, value = 0}) {

  const percentage = ((value - min) / (max-min)) * 100;

  const size = 101;
  const valueColor = "rgba(100, 198, 183, 1)";
  const backgroundColor = "rgba(100, 198, 183, .1)"

  const radius = (size - 10) / 2;
  const circumference = 2 * Math.PI * radius;
  const dashoffset = circumference - (percentage / 100) * circumference;

  const meterStyle: CSSProperties = {
    width: size + 'px',
    height: size + 'px',
    position: 'relative'
  };

  const circleStyle: CSSProperties = {
    fill: 'none',
    stroke: backgroundColor || '#ccc',
    strokeWidth: 10,
    strokeLinecap: 'round'
  };

  const valueStyle: CSSProperties = {
    fill: 'none',
    stroke: valueColor || 'blue',
    strokeWidth: 10,
    strokeLinecap: 'round',
    strokeDasharray: circumference,
    strokeDashoffset: dashoffset,
    transform: 'rotate(-90deg)',
    transformOrigin: '50% 50%'
  };

  const textStyle: CSSProperties = {
    fontSize: "18px",
    fontWeight: '500',
    fill: "white",
    textAnchor: 'middle',
  };

  return (
    <svg className="circular-meter" style={meterStyle}>
      <circle cx={size / 2} cy={size / 2} r={radius} style={circleStyle} />
      <circle cx={size / 2} cy={size / 2} r={radius} style={valueStyle} />
      <text x={size / 2} y={55.5} style={textStyle}>{value}%</text>
    </svg>
  );
}