"use client";

import style from "@/components/tag-group/tag-group.module.css";
import {Tag} from "@/components/tag-group/tag";
import {useTranslation} from "@/i18n/client";

export function TagGroup({tags = [] as string[], label = ""}) {
  const {t} = useTranslation();
  return <div>
    {label ? <div className={style.tagGroup_label}>{label}</div> : null}
    <div>
      {tags.map((tag: string, i) => <Tag key={i} label={t(`common.tagsFiltersOptions.${tag}`)} />)}
    </div>
  </div>
}