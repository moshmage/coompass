import style from "@/components/tag-group/tag.module.css";

export function Tag({label = ""}) {
  return <div className={style.tag}>{label}</div>
}