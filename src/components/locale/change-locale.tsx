'use client';
import React from 'react';
import {switchLocaleAction} from '@/i18n/actions/switch-locale';
import {getLocaleClient, useTranslation} from "@/i18n/client";
import {DropdownButton} from "@/components/buttons/dropdown/dropdown-button";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {LocaleProvider} from "@/i18n/hooks/locale-provider";

export default function ChangeLocale() {
  const handleLocaleChange = ([value]: DropdownOption[]) =>
    switchLocaleAction(value.value! as string);

  const {t} = useTranslation();
  const options = [
    {title: t('languages.english'), value: "en"},
    {title: t('languages.portuguese'), value: "pt"},
  ]

  return <LocaleProvider value={getLocaleClient()}>
    <DropdownButton onSelect={handleLocaleChange} translatedOptions label={t('languages.select')} options={options} />
  </LocaleProvider>
}
