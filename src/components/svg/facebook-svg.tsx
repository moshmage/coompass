export function FacebookSvg({fill = "white", fillOpacity = .6}) {
  return <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M21.9961 12C21.9961 6.48 17.5161 2 11.9961 2C6.47609 2 1.99609 6.48 1.99609 12C1.99609 16.84 5.43609 20.87 9.99609 21.8V15H7.99609V12H9.99609V9.5C9.99609 7.57 11.5661 6 13.4961 6H15.9961V9H13.9961C13.4461 9 12.9961 9.45 12.9961 10V12H15.9961V15H12.9961V21.95C18.0461 21.45 21.9961 17.19 21.9961 12Z"
      style={{fill, fillOpacity}}/>
  </svg>
}