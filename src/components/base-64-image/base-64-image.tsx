export function Base64Image({src = "", className = "", alt = "", style = {}, backgroundSize = "contain"}) {
  const _style = {
    display: "inline-block",
    ...style,
    backgroundImage: `url(${src})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize,
  }

  // empty 1x1 px
  const _src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"

  return <img style={_style} src={_src} className={className} alt={!src ? alt : ""} />
}