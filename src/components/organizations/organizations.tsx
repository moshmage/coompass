"use client";
import {Grid, LinearProgress} from "@mui/material";
import {OrgBox} from "@/components/organization/organization-box/org-box";
import {useEffect, useState} from "react";
import {Company} from "@/types/company/company";
import {SearchBar} from "@/components/search/search-bar/search-bar";
import {UserTypes} from "@/types/user/user";
import {useCompanies} from "@/components/hooks/queries/use-companies";
import Structural from "@/components/structural/structural";
import {NoData} from "@/components/no-data/no-data";
import searchStyles from "@/components/search/search.module.css";
import {useTranslation} from "@/i18n/client";

export function Organizations({companyId = "", usePartners = false}) {
  const [availableResults, setAvailableResults] = useState<Company[]>([]);
  const {t} = useTranslation();

  const defaultCompaniesList = () =>
    !usePartners ? companies : companies?.[0]?.partnerOrgs || []

  const search = (name: string, location: string, causes: string[], _skills: string[], _virtual: boolean, _inPerson: boolean, districts: number[]) => {
    if (!name && !location && !causes.length && !districts.length)
      setAvailableResults(defaultCompaniesList());
    else {

      const hasHit = ({name: t, sdg: s, districts: d}: Company) => {
        const hit = [!name, !causes.length, !districts.length];

        if (name)
          hit[0] = t.toLowerCase().includes(name!.toLowerCase());
        if (causes?.length)
          hit[1] = causes.every(e => s?.includes(e));
        if (districts?.length)
          hit[2] = districts.every(e => d?.find(({id}) => id === +e))

        return hit.every(e => e);
      }

      setAvailableResults(defaultCompaniesList().filter(hasHit));
    }
  }

  const {data: companies, isFetching: loading} =
    useCompanies({type: !usePartners ? UserTypes.ngo : "", companyId});

  useEffect(() => {
    setAvailableResults(!usePartners ? companies : companies?.[0]?.partnerOrgs || []);
  }, [companies, setAvailableResults]);

  return <>
    <SearchBar useOrgLocation hideLocation hideSkills useSDG onSearch={search}/>
    <Grid container>
      <Grid item xs={12} className={searchStyles.searchResults_subitle}>
        {t('missions.availableOrgs', {n: defaultCompaniesList().length.toLocaleString()})}
      </Grid>
    </Grid>
    <Structural.If condition={loading}>
      <Grid container>
        <Grid item xs={12}>
          {loading ? <Grid item><LinearProgress/></Grid> : null}
        </Grid>
      </Grid>
    </Structural.If>
    <Structural.If condition={!loading && !defaultCompaniesList().length}>
      <Grid container marginTop={6}>
        <Grid item xs={12} alignItems={"center"}>
          <NoData />
        </Grid>
      </Grid>
    </Structural.If>
    <Grid container spacing={3} alignItems={"stretch"}>
      {
        availableResults.map((company, i) =>
          <Grid key={i} item xs={12} sm={6} md={6} lg={4} xl={3}>
            <OrgBox name={company.name}
                    description={company.description}
                    imageUrl={company.image}
                    totalMissions={company.companyMissions!.filter(mission => ["paused", "started"].includes(mission.state)).length}
                    id={company.id}
                    sdg={company.sdg}/>
          </Grid>)
      }
    </Grid>
  </>
}