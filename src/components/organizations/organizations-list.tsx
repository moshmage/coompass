import {Grid, LinearProgress} from "@mui/material";
import {OrgBox} from "@/components/organization/organization-box/org-box";
import {Company} from "@/types/company/company";
import Structural from "@/components/structural/structural";
import {NoData} from "@/components/no-data/no-data";

type OrganizationsListProps = {
  companies: Company[];
  addPartnerButton?: null|((_id: string) => void);
  removePartnerButton?: null|((_id: string) => void);
  loading?: boolean;
}

export function OrganizationsList({
                                    companies = [] as Company[],
                                    addPartnerButton = null,
                                    removePartnerButton = null,
                                    loading = false
}: OrganizationsListProps) {
  return <>
    <Grid container marginTop={6}>
      <Grid item xs={12}>
        {loading ? <Grid item><LinearProgress/></Grid> : null}
      </Grid>
    </Grid>
    <Structural.If condition={!loading && !companies.length}>
      <Grid container marginTop={1} marginBottom={6}>
        <Grid item xs={12} alignItems={"center"}>
          <NoData />
        </Grid>
      </Grid>
    </Structural.If>
    <Grid container spacing={3} alignItems={"stretch"}>
      {
        companies.map((company, i) =>
          <Grid key={i} item xs={12} sm={6} md={6} lg={4} xl={3}>
            <OrgBox name={company.name}
                    description={company.description}
                    imageUrl={company.image}
                    totalMissions={company.companyMissions!.filter(mission => ["paused", "started"].includes(mission.state)).length}
                    id={company.id}
                    sdg={company.sdg}
                    addPartnerButton={addPartnerButton}
                    removePartnerButton={removePartnerButton}/>
          </Grid>)
      }
    </Grid>
  </>
}