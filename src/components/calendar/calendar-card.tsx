import styles from "@/components/calendar/calendar.module.css";

export default function CalendarCard({children}: {children: React.ReactNode}) {
  return <div className={styles.calendar_card}>
    {children}
  </div>
}