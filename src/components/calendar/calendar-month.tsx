"use client";
import styles from "@/components/calendar/calendar.module.css";
import IconButton from "@/components/buttons/icon/icon-button";

import caretLeft from "@/assets/caretLeft.svg";
import caretRight from "@/assets/caretRight.svg";

export default function CalendarMonth({year = 0, month = "", onMonthChange = (n: number) => {}}) {
  return <div className={styles.calendar_month}>
    <div className={styles.calendar_arrow}>
      <IconButton icon={caretLeft} onClick={() => onMonthChange(-1)} />
    </div>
    <span className={styles.calendar_month_title}>{month}, {year}</span>
    <div className={styles.calendar_arrow}>
      <IconButton icon={caretRight} onClick={() => onMonthChange(+1)} />
    </div>
  </div>
}