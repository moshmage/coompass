import styles from "@/components/calendar/calendar.module.css";

import {getDate, isSameDay} from "date-fns";

export default function CalendarDay({date = new Date(), selected = new Date(), onSelected = ((d: Date) => {})}) {
  const now = new Date();
  const day = getDate(date);
  const weekDay = new Date(date).toLocaleDateString("en", {weekday: "short"});

  const _className = [
    styles.calendar_day,
    ... isSameDay(date, now)
      ? [styles.calendar_day_today]
      : isSameDay(date, selected)
        ? [styles.calendar_day_selected]
        : []
  ].join(" ")

  return <div className={_className} onClick={() => onSelected(date)}>
    <div className={styles.calendar_day_weekday}>{weekDay}</div>
    <div className={styles.calendar_day_number}>{day}</div>
  </div>
}