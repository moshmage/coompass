import styles from "@/components/calendar/calendar.module.css";
import CalendarDay from "@/components/calendar/calendar-day";
import {addDays} from "date-fns";
import IconButton from "@/components/buttons/icon/icon-button";

import caretLeft from "@/assets/caretLeft.svg";
import caretRight from "@/assets/caretRight.svg";

export default function CalendarWeek({
                                       selected = new Date(),
                                       start = new Date(),
                                       onWeekChange = ((n: number) => {}),
                                       onSelectedChange = ((d: Date) => {})
                                     }) {
  const _dates = [];
  for (let day = 0; day < 5; day++) {
    _dates.push(addDays(start, day));
  }

  return <div className={styles.calendar_week}>
    <div className={styles.calendar_arrow}><IconButton icon={caretLeft} onClick={() => onWeekChange(-5)}/></div>
    {_dates.map((date, i) => <CalendarDay key={i} selected={selected} date={date} onSelected={d => onSelectedChange(d)}/>)}
    <div className={styles.calendar_arrow}><IconButton icon={caretRight} onClick={() => onWeekChange(+5)}/></div>
  </div>
}