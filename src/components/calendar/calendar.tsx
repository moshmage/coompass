"use client";

import {useEffect, useState} from "react";
import {addDays, addMonths, format, subDays, subMonths} from "date-fns";
import CalendarMonth from "@/components/calendar/calendar-month";
import CalendarWeek from "@/components/calendar/calendar-week";

type CalendarProps = {
  onSelectedDate?: (date: Date) => void,
  defaultDate?: Date;
}

export default function Calendar({onSelectedDate, defaultDate}: CalendarProps) {
  const date = new Date();

  const [month, setMonth] = useState(date);
  const [selectedDate, setSelectedDate] = useState(defaultDate || date);

  const changeMonth = (by: number) => {
    const _date = (by > 0 ? addMonths : subMonths)(month, Math.abs(by));
    _date.setDate(1);
    setMonth(_date);
  }

  const changeWeek = (by: number) => {
    setMonth((by > 0 ? addDays : subDays)(month, Math.abs(by)));
  }

  useEffect(() => { onSelectedDate?.(selectedDate) }, [selectedDate]);
  useEffect(() => {
    if (!defaultDate)
      return;

    setMonth(defaultDate);
    setSelectedDate(defaultDate);

  }, [defaultDate]);

  return <div>

    <CalendarMonth month={format(month, "MMM")}
                   year={+format(month, "yyyy")}
                   onMonthChange={by => changeMonth(by)} />

    <CalendarWeek start={month}
                  selected={selectedDate}
                  onSelectedChange={d => setSelectedDate(d)}
                  onWeekChange={by => changeWeek(by)} />
  </div>
}