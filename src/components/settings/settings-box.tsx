import {Grid} from "@mui/material";
import NextImage from "next/image";
import React from "react";

import settingStyles from "@/components/settings/settings-box.module.css";

export function SettingsBox({title = "", subtitle = "", icon = "", children}: any) {
  return <Grid container style={{paddingBottom: 82}}>
    <Grid item xs={12} className={settingStyles.settingsBox_wrapper}>
      <div className={settingStyles.settingsBox_image}>
        <NextImage src={icon} alt="" height={32} width={32} />
      </div>
      <div>
        <h2>{title}</h2>
        <div className={settingStyles.settingsBox_subtitle}>{subtitle}</div>
      </div>
    </Grid>
    <Grid container>
      {children}
    </Grid>
  </Grid>
}