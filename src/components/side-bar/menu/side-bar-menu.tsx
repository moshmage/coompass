"use client";

import {Logo} from "@/components/logo/logo";
import SideBarLink from "@/components/side-bar/menu-link/side-bar-link";

import styles from "@/components/side-bar/menu/menu.module.css";
import {UserTypes} from "@/types/user/user";
import {ReactNode} from "react";
import {HouseSvg} from "@/components/svg/house-svg";
import {MissionsSvg} from "@/components/svg/missions-svg";
import {UsersThreeSvg} from "@/components/svg/users-three-svg";
import {CogWheelSvg} from "@/components/svg/cog-wheel-svg";
import {Grid} from "@mui/material";
import {LogoSmall} from "@/components/logo/logo-small";
import {useSelf} from "@/components/hooks/use-self";
import {OrgsSvg} from "@/components/svg/orgs-svg";
import {LogoutSvg} from "@/components/svg/logout-svg";
import {signOut} from "next-auth/react";
import {HelpSvg} from "@/components/svg/help-svg";
import {TandC} from "@/components/svg/t-and-c";
import {XComSvg} from "@/components/svg/x-com-svg";
import {MediumSvg} from "@/components/svg/medium-svg";
import {TelegramSvg} from "@/components/svg/telegram-svg";
import {LinkedinSvg} from "@/components/svg/linkedin-svg";
import Link from "next/link";
import {useTranslation} from "@/i18n/client";
import {useLocale} from "@/i18n/hooks/locale-provider";
import {Handshake} from "@/components/svg/handshake";

export default function SideBarMenu() {
  const {t} = useTranslation();
  const {userData} = useSelf();

  const logoutAction = async () => {
    await signOut({callbackUrl: "/", redirect: true});
  }

  const menuEntry = (link: string|null, label: string, icon: ReactNode, onClick?: () => void) =>
    ({link, label, icon, onClick});

  const createCompanyLink = () =>
    menuEntry(`/company/create`, t('menu.settings'), <CogWheelSvg/>);

  const userSettings = () =>
    menuEntry(`/user/settings`, t('menu.settings'), <CogWheelSvg/>)

  const ngoMenu = (companyId?: string) => [
    ... companyId ? [
      menuEntry(`/organization/${companyId}`, t('menu.dashboard'), <HouseSvg fillColor="white" opacity={.6}/>),
      menuEntry(`/organization/${companyId}/missions`, t('menu.yourMissions'), <MissionsSvg/>),
      menuEntry(`/organization/${companyId}/users`, t('menu.engagedUsers'), <UsersThreeSvg/>),
      menuEntry("/organizations", t('menu.organizations'), <OrgsSvg/>),
    ] : [],
  ]

  const companyMenu = (companyId?: string) => [
    ... companyId ? [
      menuEntry(`/company/${companyId}`, t('menu.dashboard'), <HouseSvg fillColor="white" opacity={.6}/>),
      menuEntry(`/company/${companyId}/employees`, t('menu.employees'), <UsersThreeSvg/>),
      menuEntry(`/company/${companyId}/partnerships`, t('menu.partnerships'), <Handshake/>),
      menuEntry("/organizations", t('menu.organizations'), <OrgsSvg/>),
      menuEntry(`/company/${companyId}/missions`, t('menu.missions'), <MissionsSvg/>),
    ] : [],
  ]

  const usersMenu = () => [
    menuEntry(`/dashboard/user`, t('menu.dashboard'), <HouseSvg fillColor="white" opacity={.6}/>),
    menuEntry(`/dashboard/user/partnerships`, t('menu.partnerships'), <Handshake/>),
    menuEntry("/organizations", t('menu.organizations'), <OrgsSvg/>),
    menuEntry("/missions", t('menu.missions'), <MissionsSvg/>),
  ]

  const bottomMenu = [
    menuEntry("https://coompass.notion.site/Coompass-Help-Center-5ddadd49c79649538e22493018ed4243", t('menu.helpCenter'), <HelpSvg />),
    menuEntry(null, t('menu.legal'), <TandC />),
    userData?.type === UserTypes.user ? userSettings() : createCompanyLink(),
    menuEntry(null, t('menu.logout'), <LogoutSvg/>, logoutAction),
  ]

  const socialMenu = [
    {link: "https://x.com/coompassio", icon: <XComSvg/>},
    {link: "https://coompass.medium.com/", icon: <MediumSvg/>},
    {link: "https://t.me/compassio", icon: <TelegramSvg/>},
    {link: "https://www.linkedin.com/company/coompass", icon: <LinkedinSvg/>},
  ]

  return <Grid container className={`h-100 ${styles.sidebarMenu}`}>
      <Grid item xs={12}>
        <Grid container sx={{padding: {xs: "8px 0 0 8px", md: "16px 0 0 16px"}}}>
          <Grid sx={{display:{xs: "none", lg: "block"}}} item xs={0} sm={0} md={0} lg={12}><Logo height={32} width={165}/></Grid>
          <Grid sx={{display:{xs: "block", lg: "none"}, textAlign: {xs: "center"}}}
                item xs={12} sm={0} md={0}><LogoSmall height={39}/></Grid>
        </Grid>

        <div className="hr dashed"></div>

        {(
          userData?.type === UserTypes.ngo
            ? ngoMenu(userData?.companies?.[0])
            : userData?.type === UserTypes.company
              ? companyMenu(userData?.companies?.[0])
              : userData?.type === UserTypes.user
                ? usersMenu()
                : []
        ).map(({link, label, icon}, i) =>
          <Grid container key={i} sx={{padding: {xs: "0 8px", lg: "0 16px"}}}>
            <SideBarLink link={link!} label={label} icon={icon}/>
          </Grid>
        )}

        <div className={styles.sidebarMenu_bottom}>
          {bottomMenu.map(({link, label, icon, onClick}, i) => (
            <Grid container key={i} sx={{padding: {xs: "0 8px", lg: "0 16px"}}}>
              <SideBarLink label={label} link={link} icon={icon} onAction={onClick}/>
            </Grid>
          ))}
          <div className="hr"></div>
          <Grid container spacing={1} sx={{padding: {xs: "0 8px", lg: "0 16px"}}}>
            {socialMenu.map(({link, icon}, i) => (
              <Grid item xs={6} sm={6} md={3} key={i} style={{textAlign: "center"}}>
                <Link target="_blank" prefetch={false} href={link}>{icon}</Link>
              </Grid>
            ))}
          </Grid>
          <Grid container>
            <Grid item xs={12}>
              <div className={styles.sidebarMenu_trademark}>
                @ Coompass 2024
              </div>
            </Grid>
          </Grid>
        </div>
      </Grid>
  </Grid>
}