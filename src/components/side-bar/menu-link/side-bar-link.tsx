"use client";

import styles from "@/components/side-bar/menu-link/side-bar-link.module.css";
import Link from "next/link";
import {usePathname} from "next/navigation";
import {ReactNode} from "react";
import {Grid} from "@mui/material";

type SideBarProps = {
  label: string;
  link: string|null;
  icon: ReactNode;
  onAction?: () => void;
}

export default function SideBarLink({label, link = null, icon, onAction}: SideBarProps) {
  const pathname = usePathname();

  const _className = `${styles.sideBarLinkButton} ${ pathname === link ? styles.sideBarLinkButton_active : ""}`

  const linkStyle = {display: "flex", width: "100%", justifyContent: "start", alignItems: "center"}
  const isSoon = !link && !onAction;

  return <Grid container className={_className}>
    <Link prefetch={false} target={link?.startsWith("http") ? "_blank" : ""} href={link || "#"} style={linkStyle} onClick={onAction}>
      <Grid sx={{textAlign: {xs: "center", sm: "center"}}} item xs={12} sm={12} md={2} style={{marginTop: 4}}>{icon}</Grid>
      <Grid sx={{display: {xs: "none", md: "block"}}} style={{marginLeft: 8}} item xs={0} sm={0} md={10} lg={10}>
        <span className={isSoon ? "soon" : ""}>{label}</span>
      </Grid>
    </Link>
  </Grid>
}