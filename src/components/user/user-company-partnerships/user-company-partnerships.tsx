"use client";

import {Grid} from "@mui/material";
import {CompanyHeader} from "@/components/user/company-header/company-header";
import {Organizations} from "@/components/organizations/organizations";
import {useSelf} from "@/components/hooks/use-self";
import {useTranslation} from "@/i18n/client";

export function UserCompanyPartnerships() {
  const {userData: user} = useSelf();
  const {t} = useTranslation();

  return <>
    <Grid container spacing={1} style={{display: "flex", alignItems: "center", marginBottom: 8}}>
      <Grid item xs={12} sm={6} md={7} lg={6}>
        <h2>{t('partnerships.yourCompanyPartnerships')}</h2>
      </Grid>
      <Grid item xs={0} sm={0} md={0} lg={3} style={{paddingLeft: 0}}/>
    </Grid>
    <Grid container style={{borderTop: "1px solid rgba(255, 255, 255, 0.12)", marginTop: "32px"}}>
      <Organizations usePartners companyId={user?.associatedCompanies?.[0]?.id} />
    </Grid>
  </>
}