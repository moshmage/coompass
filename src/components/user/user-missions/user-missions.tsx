"use client";

import MissionWindow from "@/components/window/missions/mission-window";
import {useUserParticipations} from "@/components/hooks/queries/use-user-participations";

export function UserMissions() {
  const {data: missions, isFetching} = useUserParticipations();

  return <MissionWindow missions={missions} loading={isFetching} />
}