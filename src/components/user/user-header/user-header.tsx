"use client";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {useSelf} from "@/components/hooks/use-self";
import Structural from "@/components/structural/structural";
import userIcon from "@/assets/user.svg";
import NextImage from "next/image";
import React from "react";

export function UserHeader() {
  const {userData: user} = useSelf();

  return <div style={{display: "flex", alignItems: "center"}}>
    <div style={{marginRight: 8}}>
      <Structural>
        <Structural.If condition={!!user?.profileImage}>
          <Base64Image backgroundSize={"cover"} src={user?.profileImage || ""} style={{width: 60, height: 60, borderRadius: "100%", backgroundColor: "gray"}} />
        </Structural.If>
        <Structural.Else>
          <NextImage src={userIcon} width={60} height={60} alt={""}/>
        </Structural.Else>
      </Structural>
    </div>
    <h3>{user?.username || user?.email}</h3>
  </div>
}