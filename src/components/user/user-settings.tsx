"use client";

import {SettingsBox} from "@/components/settings/settings-box";

import userGear from "@/assets/userGear.svg";
import {Grid} from "@mui/material";
import inputStyle from "@/components/inputs/text-input/text-input.module.css";
import createOrgStyle from "@/components/organization/create-org/create-org.module.css";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import React, {useEffect, useState} from "react";
import {ImageInput} from "@/components/inputs/image/image-input";
import axios from "axios";
import {useSelf} from "@/components/hooks/use-self";
import {User} from "@/types/user/user";
import {useRouter} from "next/navigation";
import {Skills} from "@/constants/skills";
import {useSnackbar} from "notistack";
import {useTranslation} from "@/i18n/client";
import {DropdownButton} from "@/components/buttons/dropdown/dropdown-button";
import {DropdownOption} from "@/types/dropdown/dropdown-option";
import {getUserRedirectPath} from "@/handlers/utils/get-user-redirect-path";
import {dataUrlToFile} from "@/handlers/utils/data-url-to-file";
import {UploadType} from "@/types/upload/upload-types";

export function UserSettings() {
  const {t} = useTranslation()
  const {userData, refetch} = useSelf();

  const [name, setName] = useState<string>();
  const [image, setImage] = useState<string>();
  const [skills, setSkills] = useState<DropdownOption[]>([]);
  const [validImage, setValidImage] = useState<boolean>(true);

  const router = useRouter()
  const validForm = name && skills.filter(({active}) => active).length;

  const {enqueueSnackbar} = useSnackbar();

  const editUsernameProfile = async () => {
    if (!validForm)
      return;

    enqueueSnackbar(t('common.snackbars.info.updatingUserSettings'), {variant: "info"})

    const calls = [];
    calls.push(
      axios.put<User>(`/api/users/${userData?.id}`, {username: name, skills: skills.filter(({active}) => active).map(({title}) => title)})
    )

    if (image && userData?.profileImage !== image) {
      const form = new FormData();
      form.append("file", dataUrlToFile(image, "profile"));
      form.append("id", userData?.id!);
      form.append("type", UploadType.user_profile);
      calls.push(axios.post(`/api/upload`, form))
    }

    Promise
      .all(calls)
      .then(async (responses) => {
        await refetch?.();
        enqueueSnackbar(t('common.snackbars.success.updatedUserSettings'), {variant: "success"})
        router.push(getUserRedirectPath(responses[0].data))
      })
      .catch(e => {
        enqueueSnackbar(e?.response?.data?.error || t('common.snackbars.warning.failedToUpdateUser'), {variant: "warning"});
      })
  }

  const updateSkills = (options: DropdownOption[]) =>
    setSkills(Skills.map(({title}) => ({title, active: options?.findIndex(s => s.title === title) > -1})))


  useEffect(() => {
    if (!userData)
      return;

    if (!name)
      setName(userData.username || "");
    if (!image) {
      setImage(userData.profileImage || "")
      setValidImage(!!userData.profileImage);
    }

    if (!skills.length)
      setSkills(Skills.map(({title}) => ({title, active: userData.skills?.findIndex(s => s === title) > -1})))


  }, [userData]);

  return <SettingsBox title={t('user.settings.profileSettings')} subtitle={t('common.customizeEditEssentialProfileDetails')} icon={userGear}>
    <Grid item xs={12}>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={6} className={inputStyle.textInput}>
          <label htmlFor="userName" className="required">{t('user.settings.firstAndLastName')}</label>
        </Grid>
        <Grid item xs={6} className={inputStyle.textInput}>
          <div className={inputStyle.textInput_input}>
            <input
              id="userName"
              placeholder={t('user.settings.yourName')}
              defaultValue={name}
              type="text"
              onChange={e => setName(e?.target.value)}
              name="name"
            />
          </div>
        </Grid>
      </Grid>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={6} className={inputStyle.textInput}>
          <label htmlFor="imageUpload">{t('common.image.profilePicture')}</label>
          <div className={createOrgStyle.createOrg_form_subtitle}>{t('common.image.minImage', {dimension: "400x400"})}</div>
        </Grid>
        <Grid item xs={6}>
          <ImageInput img={image}
                      validWidth={400}
                      validHeight={400}
                      onChange={(image, valid) => {
                        setImage(image);
                        setValidImage(valid)
                      }}/>
        </Grid>
      </Grid>

      <Grid container className={inputStyle.horizontal_input}>
        <Grid item xs={6} className={inputStyle.textInput}>
          <label htmlFor="skills" className="required">{t('user.settings.selectYourSkills')}</label>
          <div className={createOrgStyle.createOrg_form_subtitle}>{t('user.settings.selectYourSkillsTagline')}</div>
        </Grid>
        <Grid item xs={6}>
          <DropdownButton multiple label={t('user.settings.selectYourSkills')} options={skills} onSelect={updateSkills}/>
        </Grid>
      </Grid>

      <div className={createOrgStyle.createOrg_submit}>
        <SimpleButton disabled={!validForm} label={t('common.buttons.saveChanges')} style="primary" onClick={editUsernameProfile}/>
      </div>
    </Grid>
  </SettingsBox>
}