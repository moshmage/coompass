"use client";

import {CompanyHeaderLogo} from "@/components/company/comany-header-logo/company-header-logo";
import {useSelf} from "@/components/hooks/use-self";

export function CompanyHeader() {

  const {userData: user} = useSelf();

  return <>
    <CompanyHeaderLogo image={user?.associatedCompanies?.[0]?.image}
                       alignRight
                       pending={user?.associatedCompanies?.[0]?.UserCompany?.pending}
                       link={!user?.associatedCompanies?.[0]}/>
  </>
}