export function Id({id = ""}) {
  const style = {
    backgroundColor: "rgba(255,255,255,.9)", padding: "8px", fontSize: "12px", color: "black",
    display: "inline",
    borderRadius: "8px"
  }
  return <div style={style}>{id}</div>
}