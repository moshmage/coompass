"use client";

import {CaretRightSvg} from "@/components/svg/caret-right-svg";
import {useTranslation} from "@/i18n/client";

export function Step({label = "", active = false, index = 0, showCaret = true, showCheck = false}) {
  const backgroundColor = !showCheck ? active ? "rgba(135, 210, 197, 1)" : "rgba(255, 255, 255, 0.04)" : "rgba(74, 222, 128, 1)";
  const ballBorder = active ? "" : "1px solid rgba(255, 255, 255, 0.12)";
  const ballColor = showCheck || active ? "black" : "rgba(255, 255, 255, 0.6)";
  const labelColor = active ? "white" : "rgba(255, 255, 255, 0.6)";

  const ballStyle = {
    borderRadius: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 13,
    fontWeight: 500,
    height: 20,
    width: 20,
    backgroundColor, border: ballBorder, color: ballColor,
    marginRight: 8,
  }

  const {t} = useTranslation();

  return <div style={{display: "flex", alignItems: "center"}}>
    <div style={ballStyle}>{showCheck ? "\u2713" : index}</div>
    <div style={{color: labelColor}}>{t(label)}</div>
    {showCaret ? <div style={{margin: "0 16px", display: "flex", alignItems: "center"}}><CaretRightSvg /></div> : null }
  </div>
}