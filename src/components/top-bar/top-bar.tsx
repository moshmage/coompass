"use client";

import styles from "@/components/top-bar/top-bar.module.css";
import TopBarDropdown from "@/components/top-bar/top-bar-dropdown";
import {useSelf} from "@/components/hooks/use-self";
import {UserTypes} from "@/types/user/user";
import {WebsiteSvg} from "@/components/svg/website-svg";
import Link from "next/link";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import ChangeLocale from "@/components/locale/change-locale";
import {useTranslation} from "@/i18n/client";

export default function TopBar() {

  const {userData, isFetching} = useSelf();
  const {t} = useTranslation();

  return <div className={styles.topBar}>
    <div className={`row`}>
      {/*<div className={`column ${styles.topBar_urlTitle}`}>Dashboard</div>*/}
      {/*<div className="column"><SimpleTextInput beforeIcon={magnify} /></div>*/}
      <div className={`column ${styles.topBar_lastColumn}`}
           style={{display: "flex", gap: "24px", alignItems: "center"}}>
        {/*<IconButton icon={lightning} />*/}
        {/*<IconButton icon={bell} />*/}
        <ChangeLocale />
        {
          userData?.type === UserTypes.ngo
            ? <Link href="/create-mission/details"><SimpleButton label={t('common.buttons.createMission')} small /></Link>
            : null
        }
        {
          userData?.type !== UserTypes.user
            ? userData?.associatedCompanies?.[0]?.website
              ? <Link title={userData?.associatedCompanies?.[0]?.website} target="_blank"
                      href={userData?.associatedCompanies?.[0]?.website}><WebsiteSvg/></Link>
              : null
            : null
        }

        <TopBarDropdown loading={isFetching}
                        userName={userData?.username || userData?.associatedCompanies?.[0]?.name}
                        userImage={userData?.profileImage || userData?.associatedCompanies?.[0]?.image}/>
      </div>
    </div>
  </div>
}