import {Grid} from "@mui/material";
import {LogoSmall} from "@/components/logo/logo-small";
import {CloseSvg} from "@/components/svg/close-svg";
import Link from "next/link";
import {Step} from "@/components/top-bar/top-bar-step";

export function StepTopBar({steps = [] as { label: string, url: string }[], active = 0, companyId = ""}) {
  return <Grid container style={{height: 60, display: "flex", alignItems: "center", padding: "0 44px", backgroundColor: "rgba(24, 24, 27, 1)"}}>
    <Grid item xs={1}><LogoSmall height={32} width={32}/></Grid>
    <Grid item xs={10} style={{display: "flex", justifyContent: "center"}}>
      {steps.map(({label, url}, key) =>
        <Link key={key} href={url}><Step label={label} active={key === active} index={key+1} showCaret={key < steps.length - 1} showCheck={key < active} /></Link>)}
    </Grid>
    <Grid item xs={1} style={{display: "flex", justifyContent: "end"}}><Link href={`/organization/${companyId}`}><CloseSvg /></Link></Grid>
  </Grid>
}