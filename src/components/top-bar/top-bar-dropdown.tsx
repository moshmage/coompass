"use client";

import styles from "@/components/top-bar/top-bar.module.css";
import {LinearProgress} from "@mui/material";
import {Base64Image} from "@/components/base-64-image/base-64-image";
import {useTranslation} from "@/i18n/client";

export default function TopBarDropdown({userImage = "", userName = "", loading = false}) {
  const {t} = useTranslation();
  return <>
    <button className={styles.topBar_dropdown}>

      {
        (loading && !userImage && <LinearProgress style={{width: 24}}/>) || <Base64Image style={{width: 24, height: 24, borderRadius: "50%", margin: "8px 4px"}} src={userImage} alt={""}/>
      }

      <span>
        {
          loading
            ? userName || "loading"
            : userName || t('user.logs.noUsername')
        }
      </span>

    </button>
  </>
}