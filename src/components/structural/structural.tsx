import {Children, cloneElement, FC, isValidElement, ReactNode} from "react";

type StructuralProps = { children: ReactNode };
type ConditionalProps = { condition: boolean; children: ReactNode };

type IfElseIfElse = {
  If: FC<ConditionalProps>,
  Else: FC<StructuralProps>
}

/**
 * Conditional component is exported as `Structural.If`
 * @example Without "Structural" parent:
 * <Structural.If condition={a === 1}>A is 1</Structural.If>
 * <Structural.If condition={a === 2}>A is 2</Structural.If>
 */
const Conditional: FC<ConditionalProps> = ({condition, children}: ConditionalProps) => {
  return condition ? <>{children}</> : null;
};

const Else: FC<StructuralProps> = ({children}) => {
  return <>{children}</>;
};

/**
 *
 * Structural will render the first true condition, falling back to the Else if none is found.
 * If <Structural.Else> is used, it must come last!
 *
 * @example Simple If, ElseIf, Else
 * <Structural>
 *   <Structural.If condition={a === 1}>A is 1</Structural.If>
 *   <Structural.Else>A is not 1</Structural.Else>
 * </Structural>
 * @example Multiple If conditions
 * <Structural>
 *   <Structural.If condition={a === 1}>A is 1</Structural.If>
 *   <Structural.If condition={a === 2}>A is 2</Structural.ElseIf>
 *   <Structural.Else>A is not 1 or 2</Structural.Else>
 * </Structural>
 * @example No Else statement
 * <Structural>
 *   <Structural.If condition={a === 1}>A is 1</Structural.If>
 *   <Structural.If condition={a === 2}>A is 2</Structural.ElseIf>
 * </Structural>
 * */
const Structural: FC<StructuralProps> & IfElseIfElse = ({children}) => {

  const keyedChildren = Children.map(children, (child, index) =>
    isValidElement(child) ? cloneElement(child, { key: child.key ?? index }) : child
  );

  const element =
    keyedChildren?.find((child: ReactNode) => {
      if (!isValidElement(child))
        return false;

      if ((child.type === Conditional) && child?.props?.condition)
        return true;

      return child.type === Else;
    });

  return <>{element}</>;
};

/** Multiple "If" will behave like "else if" */
Structural.If = Conditional;

/** Use Else as a fallback if needed */
Structural.Else = Else;

export default Structural;