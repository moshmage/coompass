import {ReactNode} from "react";
import {Grid, Modal} from "@mui/material";
import WindowBox from "@/components/window/window-box";
import {SimpleButton} from "@/components/buttons/simple/simple-button";

type ConfirmationModalType = {
  isOpen: boolean;

  title?: string;
  subtitle?: string;
  children?: ReactNode;
  onClose?: () => void;
  confirmLabel?: string;
}

export function InformationModal({
                                    title = "Information",
                                    isOpen = false,
                                    children = null,
                                    onClose, confirmLabel = "Ok"
                                  }: ConfirmationModalType) {
  return <Modal open={isOpen} onClose={onClose}>
    <Grid container
          style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: "33%"}}>
      <Grid item xs={12}>
        <WindowBox title={title}>
          {children}
          <Grid container style={{marginTop: "16px"}}>
            <Grid item xs={12} style={{justifyContent: "center", display: "flex", alignItems: "center"}}>
              <SimpleButton label={confirmLabel} small style="secondary" onClick={onClose}/>
            </Grid>
          </Grid>
        </WindowBox>
      </Grid>
    </Grid>
  </Modal>
}