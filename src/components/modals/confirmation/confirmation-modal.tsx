"use client";

import {Grid, Modal} from "@mui/material";
import {ReactNode} from "react";
import WindowBox from "@/components/window/window-box";
import {SimpleButton} from "@/components/buttons/simple/simple-button";
import {useTranslation} from "@/i18n/client";

type ConfirmationModalType = {
  isOpen: boolean;

  title?: string;
  subtitle?: string;
  children?: ReactNode;
  onConfirm?: () => void;
  onCancel: () => void;
  confirmLabel?: string;
  cancelLabel?: string;
  confirmDisabled?: boolean;
}

export function ConfirmationModal({
                                    title = "Are you sure?",
                                    isOpen = false,
                                    children = null,
                                    onConfirm,
                                    onCancel,
                                    confirmLabel,
                                    cancelLabel, confirmDisabled,
                                  }: ConfirmationModalType) {

  const {t} = useTranslation();

  return <Modal open={isOpen} onClose={onCancel}>
    <Grid container
          style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: "33%"}}>
      <Grid item xs={12}>
        <WindowBox title={title}>
          {children}
          <Grid container style={{marginTop: "16px"}}>
            <Grid item xs={12} style={{justifyContent: "end", display: "flex"}}>
              <div style={{marginRight: "8px"}}>
                {
                  onConfirm
                    ? <SimpleButton disabled={confirmDisabled} label={confirmLabel || t('common.buttons.yes')} onClick={onConfirm}/>
                    : null
                }
              </div>
              <SimpleButton label={cancelLabel || t('common.buttons.cancel')} style="secondary" onClick={onCancel}/>
            </Grid>
          </Grid>
        </WindowBox>
      </Grid>
    </Grid>
  </Modal>
}