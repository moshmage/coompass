export const CreateMissionSteps = [
  {label: "create-mission.details.missionDetails", url: "/create-mission/details"},
  {label: "create-mission.point-of-contact.pointOfContact", url: "/create-mission/point-of-contact"},
  {label: "create-mission.review.review", url: "/create-mission/review"},
]