import {tagTitle} from "@/constants/tag-title";

export const MissionTags = [
  tagTitle("healthAndWellness"),
  tagTitle("education"),
  tagTitle("environment"),
  tagTitle("humanRights"),
  tagTitle("communityDevelopment"),
  tagTitle("povertyAlleviation"),
  tagTitle("animalWelfare"),
  tagTitle("artsAndCulture"),
  tagTitle("disasterAndRelief"),
  tagTitle("childrenAndYouth"),
  tagTitle("technologyAndInnovation"),
  tagTitle("internationalDevelopment"),
  tagTitle("organizationalDevelopment"),
]