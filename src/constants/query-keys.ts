export const QueryKeys = {
  userData: ["user-data"],
  userSession: ["user-session"],
  userCompanies: ["user-companies"],
  userCompany: ["user-company"],
  userMissions: ["user-missions"],
  companyUsers: ["company-users"],
  ngoList: ["ngo-list"],
  company: (id: string) => ["company", id],
  userLogs: ["user-logs"],
  companies:({type = "", search = "", companyId = ""}) => {
    const filter = [];
    if (type)
      filter.push(type);
    if (search)
      filter.push(search);
    if (companyId)
      filter.push(companyId);

    return ["companies", ...filter];
  },
  companyMissions: (orgId: string) => ["company-missions", orgId],
  companyParticipantsState: (companyId: string) => ["company-participants-state", companyId],
  companyParticipants: (companyId: string) => ["company-participants", companyId],
  companyEngagementGraph: (companyId: string) => ["company-engagement-graph", companyId],
  companyEngagement:(companyId: string) => ["engagement", companyId],
  calendar: (id: string) => ["user-calendar", id],
  certificatesData: (companyId: string) => ["certificate-data", companyId],
  certificates:(companyId: string) => ["certificates", companyId],
  missions:() => ["missions"],
  userParticipations: (id: string) => ["user-participations", id],
  districts: ["districts"],
}