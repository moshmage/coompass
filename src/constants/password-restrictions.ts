export const PasswordMinimumLength = 12;
export const PasswordSpecialChars = "@$!%*?&^#_"
export const PasswordRegex =
  new RegExp(`^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[${PasswordSpecialChars}])[A-Za-z\\d${PasswordSpecialChars}]{${PasswordMinimumLength},}$`)