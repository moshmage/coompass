export const ApiRoutes = {
  SignIn: "/api/auth/signin",
  SignOut: "/api/auth/signout",
}