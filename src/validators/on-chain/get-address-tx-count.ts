import Web3 from "web3";

const {DEFAULT_CHAIN_RPC: web3Host} = process.env;

export async function getAddressTxCount(address: string, connection?: Web3) {
  connection = connection ? connection : new Web3(web3Host!);
  return connection.eth.getTransactionCount(address);
}