import type {UserTypes} from "@/types/user/user";
import type {Session} from "@auth/core/types";

export const sessionHasRole = (token: Session, needsRole: UserTypes) => {
  return (token?.user as any)?.role?.includes(needsRole);
}