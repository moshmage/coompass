export function isValidPrivateKey(key: string) {
  return /^(0x)?[0-9A-Fa-f]{64}$/.test(key);
}