export function isBase64Image(payload: string) {
  try {

    const decodedData = atob(payload.split(',')[1]);

    const dataArray = new Uint8Array(decodedData.length);
    for (let i = 0; i < decodedData.length; i++) {
      dataArray[i] = decodedData.charCodeAt(i);
    }

    new Blob([dataArray]);

    return true;

  } catch (error: any) {

    console.log(`ERROR`, error.message)

    return false;
  }
}