export class BaseApiError extends Error {
  status: number;

  constructor(message = "unknown error", status = 500) {
    super(message);

    Error.captureStackTrace(this, this.constructor);

    this.status = status;
  }
}