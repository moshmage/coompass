import {BaseApiError} from "@/classes/errors/http/base-api-error";
import {
  BadRequestErrors,
  ConflictErrors,
  ExpiredErrors,
  ForbiddenErrors,
  NotAcceptableErrors,
  NotFoundErrors,
  UnauthorizedErrors
} from "@/classes/errors/http/types";

export class HttpNotFoundError extends BaseApiError {
  constructor(message: string = NotFoundErrors.NotFound) {
    super(message, 404);
  }
}

export class HttpBadRequestError extends BaseApiError {
  constructor(message: string = BadRequestErrors.BadRequest) {
    super(message, 400);
  }
}

export class HttpConflictError extends BaseApiError {
  constructor(message: string = ConflictErrors.Conflict) {
    super(message, 409);
  }
}

export class HttpUnauthorizedError extends BaseApiError {
  constructor(message: string = UnauthorizedErrors.Unauthorized) {
    super(message, 401);
  }
}

export class HttpForbiddenError extends BaseApiError {
  constructor(message: string = ForbiddenErrors.Forbidden) {
    super(message, 403);
  }
}

export class HttpNotAcceptableError extends BaseApiError {
  constructor(message: string = NotAcceptableErrors.NotAcceptable) {
    super(message, 406);
  }
}

export class HttpExpiredError extends BaseApiError {
  constructor(message: string = ExpiredErrors.Expired) {
    super(message, 410);
  }
}

export class HttpServerError extends BaseApiError {
  constructor(message: string) {
    super(message, 500);
  }
}