export enum Errors {
  WalletNotConnected = "Wallet not connected"
}

export enum MetamaskErrors {
  UserRejected = 4001,
  ExceedAllowance = -32603
}

export enum EmailConfirmationErrors {
  ALREADY_CONFIRMED = "already-confirmed",
  INVALID_LINK = "invalid-link",
  EXPIRED_LINK = "expired-link"
}

export enum OriginLinkErrors {
  Banned = "banned",
  Invalid = "invalid"
}


export enum BadRequestErrors {
  BadRequest = "bad request",
  MissingParameters = "missing parameters",
  WrongParameters = "wrong parameters",
  WrongParamsNotAnAddress = "expected and address",
  WrongParamsNotANumber = "expected a number",
  WrongParamsNotUUID = "expected a UUID",
  WrongParamsNotAnEmail = "expected an email",
  WrongParamsNotANormalizedEmail = "failed to normalize email",
  WrongParamsPassword = "password does not match allowed pattern",
  InvalidPrivateKey = "private key is not a valid one",
  SignaturesDoNotMatch = "wrong password",
  WrongParamsNotAnImage = "image is not base-64 encoded",
  WrongDomain = "user and company domains do not match",
  MissingParametersUUID = "missing a uuid",
  CompaniesCannotJoinCompanies = "companies cannot join companies",
  UserCannotMakeCompany = "users cannot make companies",
  WrongParamsNotAKnownAction = "action is not a known action",
  CompanyAndUserCannotBeTogether = "user id and company id cannot be used together",
  WrongParamsUploadType = "upload type not recognized",
}

export enum ForbiddenErrors {
  Forbidden = "forbidden",
  NotTheOwner = "entry is owned by another user",
  NoUserOrWrongCredentials = "No user found with that email or the wrong credentials were used",
  NotAuthorized = "not authorized",
  DomainNotAuthorized = "domain not authorized",
}

export enum NotFoundErrors {
  NotFound = "not found",
  NotificationNotFound = "notification not found",
  CompanyNotFound = "company not found",
  UserNotFound = "user not found",
  MissionNotFound = "mission not found",
  ParticipationNotFound = "participation not found",
  ConfirmationLinkNotFound = "this confirmation link does not exist anymore",
  CompanyOwnerNotFound = "company owner not found",
  RelationNotFound = "company and user relation not found",
}

export enum ConflictErrors {
  Conflict = "conflict",
  ConflictEmail = "email conflict",
  ConflictCompanyNameExists = "company/ngo name already exists",
  DomainAlreadyExists = "company/ngo domain already exists",
  ParticipationAlreadyExists = "participation already exists",
  MissionWithSameTitle = "mission with same title already exists",
  CannotDeleteMissionStarted = "mission cannot be deleted because it's running",
  AlreadyCompanyMember = "already a company member",
  RelationAlreadyExists = "relation already exists",
  CanOnlyJoinOneCompany = "can only join one company"
}

export enum UnauthorizedErrors {
  Unauthorized = "unauthorized",
  UnauthorizedNoRole = "missing proper user role",
}

export enum NotAcceptableErrors {
  NotAcceptable = "not acceptable",
  TooManyCertificates = "allotted amount of requests given"
}

export enum ExpiredErrors {
  Expired = "Expired",
  PasswordResetConfirmationLinkExpired = "the link for password reset has expired"
}