import Web3 from "web3";
import {getAddressTxCount} from "@/validators/on-chain/get-address-tx-count";

const {SKIP_ADDRESS_TX_COUNT: skippAddressTxCount} = process.env;

export class AddressCreator {
  static async create(): Promise<{address: string, privateKey: string}> {
    const {address, privateKey, encrypt} = new Web3().eth.accounts.create();
    if (!skippAddressTxCount && +(await getAddressTxCount(address)))
      return AddressCreator.create();

    return {address, privateKey}
  }

  static sign(data: string, privateKey: string) {
    return new Web3().eth.accounts.sign(data, privateKey);
  }

  static verify(data: string, signature: string, expectedAddress: string) {
    return new Web3().eth.accounts.recover(signature, data) === expectedAddress;
  }
}