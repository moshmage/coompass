import {createHash, createSign, pbkdf2Sync, randomBytes, sign, verify} from "crypto"

const {CREDENTIALS_ITERATIONS: iterations = 10000,} = process.env;

export class Signer {

  static signMessage(message: string, password: string) {
    // Use password as a salt
    const salt = randomBytes(16);

    // Derive a key from the password using PBKDF2
    const key = pbkdf2Sync(password, salt, +iterations, 32, 'rsa-sha256');

    // Create a Sign object and sign the message digest with RSA
    const signer = createSign('RSA-SHA256');
    signer.update(message);

    // Sign the message digest using the private key
    const signature = signer.sign({key}, 'base64');

    return {
      message,
      signature,
      salt: salt.toString('base64')
    };
  }

  static verifyMessage(signedData: { salt: string; signature: string, message: string }) {
    // Decode base64 strings
    const salt = Buffer.from(signedData.salt, 'base64');
    const signature = Buffer.from(signedData.signature, 'base64');
    const messageDigest = createHash('sha256').update(signedData.message).digest();

    // Derive a key from the password using PBKDF2
    const key = pbkdf2Sync(signedData.message, salt, 100000, 32, 'sha256'); // Adjust iterations as needed

    return verify('RSA-SHA256', messageDigest, {key}, signature);
  }

}