import {createHash, randomBytes, createCipheriv, createDecipheriv} from "node:crypto";

const CIPHER_ALGO = "aes256";
const HASH_ALGO = "sha256";
const INPUT_ENCODE = "utf-8";

const bufferedSalt = (salt: string|Buffer) =>
  Buffer.isBuffer(salt) ? salt : Buffer.from(salt, 'base64')

export class Encryptor {
  static encrypt(message: string, key: string, salt: string|Buffer = randomBytes(16)) {
    const cipher =
      createCipheriv(CIPHER_ALGO, createHash(HASH_ALGO).update(key).digest(), bufferedSalt(salt));

    const encoded =
      cipher.update(message, INPUT_ENCODE, "base64") + cipher.final("base64");

    return {
      encoded,
      salt: typeof salt === "string" ? salt : salt.toString("base64"),
    };
  }

  static decode(data: string, key: string, salt: string | Buffer) {
    const decipher = createDecipheriv(CIPHER_ALGO, createHash(HASH_ALGO).update(key).digest(), bufferedSalt(salt));

    // Use 'binary' encoding for the input (data) and 'utf-8' for the output
    const decrypted = decipher.update(data, 'base64', INPUT_ENCODE) + decipher.final(INPUT_ENCODE);

    return decrypted;
  }
}