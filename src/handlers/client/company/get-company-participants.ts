import axios from "axios";
import {ParticipantsState} from "@/types/mission/mission";
import {User} from "@/types/user/user";

export const getCompanyParticipants = (companyId: string) =>
  axios.get<User[]>(`/api/company/${companyId}/participants`)
    .then(({data}) => data)