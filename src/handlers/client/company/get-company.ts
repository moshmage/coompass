"use client";

import axios from "axios";
import {Company} from "@/types/company/company";

export const getCompany = (companyId?: string) => {
  if (!companyId)
    return null;

  return axios.get<Company>(`/api/company/${companyId}`)
    .then(({data}) => data)
}