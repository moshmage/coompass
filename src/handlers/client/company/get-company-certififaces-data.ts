import axios from "axios";
import {CertificateData} from "@/types/certificate/certificate-data";
import {Company} from "@/types/company/company";

export const getCompanyCertififacesData = (companyId = "") =>
  axios.get<{data: CertificateData, company: Company}>(`/api/company/${companyId}/certificate/data`)
    .then(({data}) => data)
    .catch(e => {
      console.log(`Failed to fetch certificates data`, e);
      return null;
    })