import axios from "axios";
import {ParticipantsState} from "@/types/mission/mission";

export const getCompanyParticipantsState = (companyId: string) =>
  axios.get<ParticipantsState[]>(`/api/company/${companyId}/participants-state`)
    .then(({data}) => data)