"use client";
import axios from "axios";
import {Mission} from "@/types/mission/mission";

export const getMissionsWithParticipantsOf = (companyId = "") =>
  axios.get<Mission[]>(`/api/company/${companyId}/missions`)
    .then(({data}) => data)