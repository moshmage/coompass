"use client";
import axios from "axios";
import {User} from "@/types/user/user";

export const getCompanyUsers = (companyId = "") =>
  axios.get<User[]>(`/api/company/${companyId}/users`)
    .then(({data}) => data);