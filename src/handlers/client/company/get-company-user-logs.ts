"use client";

import axios from "axios";
import {UserLog} from "@/types/company/user-log";

export const getCompanyUserLogs = (companyId = "") =>
  axios.get<UserLog[]>(`/api/company/${companyId}/users/logs`)
    .then(({data}) => data);