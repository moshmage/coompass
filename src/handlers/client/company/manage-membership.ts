import axios from "axios";

export const manageMembership = (companyId: string, {userId = "", accept}: {userId: string, accept: boolean}) =>
  axios.post(`/api/company/${companyId}/membership`, {userId, accept})