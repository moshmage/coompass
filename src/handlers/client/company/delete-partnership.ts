import axios from "axios";

export const deletePartnership = async (companyId: string, partnerOrgId: string): Promise<void> =>
  axios.delete(`/api/company/${companyId}/partnerships/${partnerOrgId}`)