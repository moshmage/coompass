import axios from "axios";
import {Mission} from "@/types/mission/mission";

export const getOrgMissions = (companyId: string) =>
  axios.get<Mission[]>(`/api/missions/?companyId=${companyId}`)
    .then(({data}) => data)