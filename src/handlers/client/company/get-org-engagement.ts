import axios from "axios";

export const getOrgEngagement = (companyId: string) =>
  axios.get<{data: number[], labels: string[]}>(`/api/company/${companyId}/graph`)
    .then(({data}) => data)