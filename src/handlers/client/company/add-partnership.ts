import axios from "axios";

export const addPartnership = (companyId: string, partnerOrgId: string) =>
  axios.post(`/api/company/${companyId}/partnerships/${partnerOrgId}/`)