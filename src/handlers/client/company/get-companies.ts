import axios from "axios";
import {Company} from "@/types/company/company";

export const getCompanies = async ({type = "", search = "", companyId = ""}) => {

  const params = new URLSearchParams();
  if (type)
    params.append("type", type);
  if (search)
    params.append("search", search);
  if (companyId)
    params.append("companyId", companyId);

  const paramsQuery = params.size ? `?${params.toString()}` : ``;

  return axios
    .get<Company[]>(`/api/company${paramsQuery}`).then(({data}) => data)
    .catch((error) => {
      console.log(`Failed to get companies`, error);
      return [] as Company[];
    })
}