import axios from "axios";
import {CertificateData} from "@/types/certificate/certificate-data";

export const getCompanyCertificates = (companyId = "") =>
  axios.get<CertificateData[]>(`/api/company/${companyId}/certificate`)
    .then(({data}) => data)
    .catch(e => {
      console.log(`Failed to fetch certificate`, e);
      return null;
    })