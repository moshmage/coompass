import axios from "axios";
import {Mission} from "@/types/mission/mission";

export const getMissions = () =>
  axios.get<Mission[]>(`/api/missions`)
    .then(({data}) => data)
    .catch(e => {
      console.log(`Failed to get missions`, e);
      return []
    })