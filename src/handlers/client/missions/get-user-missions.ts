"use client";

import axios from "axios";
import {Mission} from "@/types/mission/mission";

export const getUserMissions = (userId: string) =>
  axios.get<Mission[]>(`/api/users/${userId}/missions`)
    .then(({data}) => data);