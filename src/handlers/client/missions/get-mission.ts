import axios from "axios";
import {Mission} from "@/types/mission/mission";

export const getMission = (missionId: string) =>
    axios.get<Mission[]>(`/api/missions/${missionId}/`)
        .then(({data}) => data[0]??null)