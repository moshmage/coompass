"use client";

import axios from "axios";
import {ParticipantsState} from "@/types/mission/mission";

export const getUserParticipations = (userId: string) =>
  axios.get<ParticipantsState[]>(`/api/users/${userId}/participations`)
    .then(({data}) => data);