import axios from "axios";
import {CalendarEntry} from "@/types/user/calendar-entry";

export const getCalendar = (userId: string) =>
  axios.get<CalendarEntry[]>(`/api/users/${userId}/calendar`)
    .then(({data}) => data)
    .catch(e => {
      console.log(`Failed to get calendar`, e);
      return null;
    })