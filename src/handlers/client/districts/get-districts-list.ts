import axios from "axios";
import {District} from "@/types/districts/district";

export function getDistrictsList() {
    return axios.get<District[]>(`/api/districts/`)
        .then(({data}) => data)
        .catch(e => {
            console.error(`Failed to fetch district list`, e)
            return [];
        })
}