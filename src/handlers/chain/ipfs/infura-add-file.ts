import uniqid from "uniqid";
import axios from "axios";
import FormData from "form-data";

export async function infuraAddFile(file: Buffer | string | JSON | Record<string, unknown>,
                                    pin = true,
                                    fileType: string,
                                    ext: string): Promise<{ hash: string; fileName: string; size: string }> {

  if (!file)
    return Promise.reject();

  const auth =
    "Basic " +
    Buffer.from(process.env.INFURA_PROJECT_ID + ":" + process.env.INFURA_SECRET_KEY).toString("base64");

  const form = new FormData();
  const isBuffer = Buffer.isBuffer(file);

  let content = file;

  if (isBuffer) content = Buffer.from(file)
  else if (typeof content === "object") content = JSON.stringify(file)

  if (isBuffer) {
    const options = {
      filename: `${uniqid()}.${ext}`,
      contentType: fileType
    };

    form.append("file", content, options);
  } else {
    form.append("file", content, `${uniqid()}.${ext}`);
  }

  const headers = {
    "Content-Type": `multipart/form-data; boundary=${form.getBoundary()}`,
    Accept: "*/*",
    Connection: "keep-alive",
    authorization: auth
  };

  const { data } =
    await axios.post(`${process.env.INFURA_BASE_URL}/api/v0/add?stream-channels=true&progress=false&pin=${pin}`, form, {headers})
      .catch(e => {
        console.error("Failed to upload file", e);
        return {data: {hash: "",}}
      });
  return { hash: data.Hash, fileName: data.Name, size: data.Size, };
}