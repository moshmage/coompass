import {infuraAddFile} from "@/handlers/chain/ipfs/infura-add-file";

export async function uploadJsonFileToInfura(jsonObject: Record<string, unknown>) {
  const jsonString = JSON.stringify(jsonObject);
  const fileType = 'application/json';
  const ext = 'json';

  const result = await infuraAddFile(jsonString, true, fileType, ext);
  return result.hash;
}