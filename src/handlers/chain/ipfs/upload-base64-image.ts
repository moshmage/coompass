import {infuraAddFile} from "@/handlers/chain/ipfs/infura-add-file";

export async function uploadBase64Image(base64String: string) {
  const base64Data = base64String.split(',')[1];
  const buffer = Buffer.from(base64Data, 'base64');
  const fileType = base64String.split(';')[0].replace("data:", "");
  const ext = base64String.split(';')[0].split('/')[1];

  const result = await infuraAddFile(buffer, true, fileType, ext);

  return result.hash;
}