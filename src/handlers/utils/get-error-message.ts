export const getErrorMessage = (e: any, defaultMessage = "") =>
  ({message: e?.response?.data?.error || defaultMessage || ""});