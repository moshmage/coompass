export function deployInfo(event: any) {
  const name = event.payload.deployment.name;
  const state = event.type.split('.')?.[1] || "unknown";

  const branch = event.payload.deployment.meta["gitlabCommitRef"];
  const org = event.payload.deployment.meta["gitlabProjectNamespace"];
  const commitRepo = event.payload.deployment.meta["gitlabProjectRepo"];
  const commitSha = event.payload.deployment.meta["gitlabCommitSha"];
  const commitUrl = `https://gitlab.com/${org}/${commitRepo}/commit/${commitSha}`
  const deployUrl = event.payload.links.deployment;
  const deployId = event.payload.deployment.id.replace("dpl_", "");
  const branchAlias = event.payload.deployment.meta.branchAlias;
  const message = event.payload.deployment.meta["gitlabCommitMessage"];
  const target = event.payload.target;

  let color = 15158332;
  if (["succeeded", "promoted"].includes(state))
    color = 3066993;
  if (["created", "ready"].includes(state))
    color = 11393254;
  if (["canceled"].includes(state))
    color = 8421504;

  return {
    branch, org, commitRepo, commitSha, commitUrl, name, state, deployUrl, color, message, target, branchAlias, deployId
  }
}