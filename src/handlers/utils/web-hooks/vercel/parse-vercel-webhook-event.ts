import {deployInfo} from "@/handlers/utils/web-hooks/vercel/deploy-info";

export function parseVercelWebhookEvent(event: any) {

  const {
    name,
    color,
    target,
    branchAlias,
    branch,
    state,
    commitSha,
    commitUrl,
    deployUrl,
    message,
    deployId
  } = deployInfo(event);


  return {
    content: null,
    embeds: [
      {
        title: `Deployment of ${name} from ${branch} ${target ? `on ${target} ` : ""}${state}.`,
        url: null,
        description: null,
        color,
        fields: [
          ["promoted", "succeeded"].includes(state)
            ? {
              name: "Website URL",
              value: state === "promoted" || target === "production"
                ? `[app.coompass.io](https://app.coompass.io/)`
                : `[${branchAlias}](https://${branchAlias})`
            }
            : {},
          {
            name: 'Vercel Deploy',
            value: `[${deployId}](${deployUrl})`
          },
          {
            name: 'Commit',
            value: `[${commitSha}](${commitUrl})`
          },
          {
            name: 'Commit Message',
            value: message
          },
        ]
      }
    ]
  };
}