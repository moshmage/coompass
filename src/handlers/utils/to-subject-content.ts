export const toSubjectContent =
  (to: string, subject: string, content: string) =>
    ({to, subject, content})