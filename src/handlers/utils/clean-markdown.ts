export function cleanMarkdown(text: string) {
  // Remove links
  text = text.replace(/\[([^\]]+)\]\([^\)]+\)/g, '$1');

  // Remove inline code
  text = text.replace(/`([^`]+)`/g, '$1');

  // Remove headers
  text = text.replace(/^#+\s+(.*)$/gm, '$1');

  // Remove bold and italic
  text = text.replace(/\*\*([^*]+)\*\*/g, '$1');
  text = text.replace(/\*([^*]+)\*/g, '$1');
  text = text.replace(/_([^_]+)_/g, '$1');

  // Remove blockquote
  text = text.replace(/^\s*>\s*(.*)$/gm, '$1');

  // Remove horizontal rule
  text = text.replace(/^\s*---+\s*$/gm, '');

  // Remove lists
  text = text.replace(/^\s*([-+*]|\d+\.)\s+/gm, '');

  return text;
}