import {User, UserTypes} from "@/types/user/user";

export const getUserRedirectPath = (_user: User) => {

  if ([UserTypes.ngo, UserTypes.company].includes(_user.type) && !_user.companies?.length)
    return `/company/create`

  if ([UserTypes.company, UserTypes.ngo].includes(_user.type))
    return `/${UserTypes.ngo === _user.type ? "organization" : "company"}/${_user.companies?.[0]}`;

  if (_user.type === UserTypes.user) {
    if (!_user.username)
      return `/user/settings`;

    if (!_user.associatedCompanies?.length)
      return `/company/join`;

    return `/dashboard/user`;
  }

  return `/`;
}