import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {Calendar, Mission} from "@/database/models";
import {Op} from "sequelize";

export async function getCalendarForUser({userId = "",}) {
  if (!userId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  return await Calendar.findAll({
    where: {userId: {[Op.eq]: userId}},
    include: [
      {model: Mission, as: "mission", attributes: {exclude: ["image"]}}
    ]
  });
}