import {NextRequest} from "next/server";
import {Op} from "sequelize";
import {Company, Mission, User} from "@/database/models";
import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {uploadFileToS3} from "@/handlers/server/upload/upload-to-s3";
import {UploadType} from "@/types/upload/upload-types";
import {s3Service} from "@/handlers/server/storage/s3-service";

export const uploadRouteHandler = async (request: NextRequest, requester: string) => {
  const formData = await request.formData();
  if (!formData.get("id") || !formData.get("type") || !formData.get("file"))
    throw new HttpBadRequestError(BadRequestErrors.WrongParameters);

  const {s3Path} = await uploadFileToS3(formData.get("file") as File);

  const where = {
    where: {
      id: {[Op.eq]: requester},
      ... formData.get("type") !== UploadType.user_profile
        ? {creator: {[Op.eq]: requester}}
        : {}
    }
  };

  let prevImage: null|string|undefined = null;
  let updated = null;

  switch (formData.get("type")) {
    case UploadType.company_header:
      prevImage = (await Company.findOne(where))?.image;
      updated = await Company.update({image: s3Path}, where);
      break;
    case UploadType.user_profile:
      prevImage = (await User.findOne(where))?.profileImage;
      updated = await User.update({profileImage: s3Path}, where)
      break;
    case UploadType.mission_header:
      prevImage = (await Mission.findOne(where))?.image;
      updated = await Mission.update({image: s3Path}, where);
      break;
    default:
      throw new HttpBadRequestError(BadRequestErrors.WrongParamsUploadType);
  }

  let deleted = null;
  let prevDeleted = false;

  console.log(prevImage, prevImage?.startsWith("http"), updated, s3Path);

  if (prevImage && prevImage.startsWith("http"))
    prevDeleted = !!(await s3Service.delete(new URL(prevImage).pathname))?.DeleteMarker;

  if (!updated[0])
    deleted = (await s3Service.delete(new URL(s3Path).pathname));


  return {path: s3Path, updated, deleted, prevDeleted}
}