import {s3Service} from "@/handlers/server/storage/s3-service";


export const uploadFileToS3 = async (file: File) => {

  // @ts-ignore
  const fileBuffer = await file!.arrayBuffer();
  // @ts-ignore
  const fileName = file!.name;

  console.log(`FILENAME`, fileName);

  // @ts-ignore
  return s3Service.upload(fileName, fileBuffer, file!.type);
}