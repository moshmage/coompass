import {Districts} from "@/database/models";

export async function getDistricts() {
    return Districts.findAll();
}