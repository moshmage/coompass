import {BadRequestErrors} from "@/classes/errors/http/types";
import {Company, Mission, User, UserLog} from "@/database/models/";
import {Op} from "sequelize";

export async function getUserLogs({userId = "", ofCompanyId = "", missionId = ""}) {
  if (userId && ofCompanyId)
    throw new Error(BadRequestErrors.CompanyAndUserCannotBeTogether)

  const where = {
    ...userId ? {userId: {[Op.eq]: userId}} : {},
    ...missionId ? {missionId: {[Op.eq]: missionId}} : {},
  }

  return await UserLog.findAll({
    where,
    include: [
      {
        model: User,
        as: "user",
        attributes: {exclude: ["key", "signature", "address"]},
        ...ofCompanyId
          ? {
            required: true,
            include: [{
              model: Company,
              required: true,
              as: "associatedCompanies",
              where: {id: {[Op.eq]: ofCompanyId}},
              attributes: []
            }]
          }
          : {}
      },
      {
        model: Mission,
        as: "mission",
        attributes: ["title"]
      }
    ],
    order: [['createdAt', 'DESC']]
  });
}