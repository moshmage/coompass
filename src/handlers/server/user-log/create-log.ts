import {BadRequestErrors} from "@/classes/errors/http/types";
import {UserLog} from "@/database/models/";
import {AllowedActionTypes} from "@/types/log/allowed-action-types";

export async function createLog({
                            userId = "",
                            action = "" as AllowedActionTypes,
                            amount = 0,
                            missionId = ""
}) {
  if (!userId || !action)
    throw new Error(BadRequestErrors.MissingParameters);

  if (amount && isNaN(amount))
    throw new Error(BadRequestErrors.WrongParamsNotANumber);

  const log = await UserLog.create({userId, amount, missionId, action});

  return log.dataValues;
}