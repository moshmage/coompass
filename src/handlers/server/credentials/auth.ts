import type {NextAuthConfig} from "next-auth"
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import {userLogin} from "@/handlers/server/credentials/user-login";
import {HttpForbiddenError} from "@/classes/errors/http/http-errors";
import {ForbiddenErrors} from "@/classes/errors/http/types";
import GoogleProvider from "next-auth/providers/google";
import {JWT} from "@auth/core/jwt";
import {Session} from "@auth/core/types";

const {
  GOOGLE_CLIENT_ID: clientId,
  GOOGLE_CLIENT_SECRET: clientSecret,
  AUTH_SECRET,
  API
} = process.env;

// @ts-ignore
const config: NextAuthConfig = {
  providers: [
    CredentialsProvider({name: "credentials", credentials: {username: {type: "email", label: "Email"}, password: {type: "password",}},
      async authorize(credentials) {
        const login = await userLogin(credentials as Record<"username"|"password", string>)
        if (!login?.authorized)
          throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized);

        return login.user as any;
      },
    }),
    ... clientId && clientSecret ? [GoogleProvider({clientId, clientSecret,})] : []
  ],
  callbacks: {
    authorized({ auth}) {
      return !!auth;
    },
    jwt: async ({ user, token }) => {
      if (user) {
        token.uid = user.id;
        token.user = user;
        token.role = (user as any).role.concat((user as any).type);
      }
      // console.log(`JWT`, user, token)
      return token;
    },
    session: async ({ session, token }: { session: Session; token?: JWT }) => {
      if (session?.user) {
        (session.user as any) = token?.user;
        (session.user as any).role = token?.role || [];
      }
      // console.log(`SESSION`, session, token)
      return session;
    },
  }
}

export const {handlers: AuthHandlers, auth, signIn, signOut} = NextAuth(config);