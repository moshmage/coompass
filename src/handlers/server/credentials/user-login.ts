import {User} from "@/database/models/";
import {Op} from "sequelize";
import {HttpBadRequestError, HttpForbiddenError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ForbiddenErrors} from "@/classes/errors/http/types";

import {emailPassValidation} from "@/handlers/server/credentials/helpers";
import {AddressCreator} from "@/classes/credentials/address-creator";
import {Encryptor} from "@/classes/credentials/encryptor";
import {isValidPrivateKey} from "@/validators/is-valid-private-key";
import {createLog} from "@/handlers/server/user-log/create-log";

export type CredentialsProps = {username: string, password: string}

const {CREDENTIALS_SALT: salt} = process.env;

export async function userLogin(credentials: CredentialsProps): Promise<{authorized: boolean; user: typeof User|null}> {
  const {email, password} = emailPassValidation(credentials);

  const user = await User.findOne({where: {email: {[Op.eq]: email}}, attributes: {exclude: ["profileImage"]}, raw: true});
  if (!user)
    throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized);

  // Decrypt the user's private key using the provided password and salt
  let privateKey = ``;
  try {
    privateKey = Encryptor.decode(user.key!, password, salt!)
  } catch (e: any) {
    throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized)
  }

  if (!isValidPrivateKey(privateKey))
    throw new HttpBadRequestError(BadRequestErrors.InvalidPrivateKey);

  // Generate a signature using the decrypted private key and the provided password
  const signature = AddressCreator.sign(password, privateKey).signature;
  if (signature !== user.signature)
    throw new HttpBadRequestError(BadRequestErrors.SignaturesDoNotMatch);

  await createLog({userId: user.id, action: "login"});
  await User.update({lastLogin: new Date()}, {where: {id: user.id}});

  // @ts-ignore
  delete user.signature;
  delete user.key;
  delete user.address;

  return {authorized: true, user: user as any as typeof User};
}