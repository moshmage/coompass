import {
  HttpBadRequestError,
  HttpExpiredError,
  HttpNotFoundError,
  HttpUnauthorizedError
} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ExpiredErrors, NotFoundErrors, UnauthorizedErrors} from "@/classes/errors/http/types";
import {ResetAccount} from "@/database/models/reset-account";
import {Op} from "sequelize";
import {User} from "@/database/models";
import {emailPassValidation} from "@/handlers/server/credentials/helpers";
import {isAfter} from "date-fns";
import {AddressCreator} from "@/classes/credentials/address-creator";
import {Encryptor} from "@/classes/credentials/encryptor";

const {CREDENTIALS_SALT: salt} = process.env;

export async function resetPassword({confirmationKey  = "", email = "", newPassword = ""}) {
  if (!confirmationKey || !email || !newPassword)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const {password, email: nEmail} = emailPassValidation({username: email, password: newPassword});

  const resetAccountEntry = await ResetAccount.findOne({
    where: {
      confirmationKey: {[Op.eq]: confirmationKey},
      email: {[Op.eq]: email}
    },
    include: [
      {model: User, attributes: {exclude: ["image"]}}
    ]
  })

  console.log(resetAccountEntry);

  if (!resetAccountEntry)
    throw new HttpNotFoundError(NotFoundErrors.ConfirmationLinkNotFound);

  if (resetAccountEntry.confirmationKey !== confirmationKey || resetAccountEntry.email !== nEmail)
    throw new HttpUnauthorizedError(UnauthorizedErrors.Unauthorized);

  if (isAfter(new Date(), resetAccountEntry.expires)) {
    await resetAccountEntry.destroy();
    throw new HttpExpiredError(ExpiredErrors.PasswordResetConfirmationLinkExpired);
  }

  const user = await User.findOne({where: {email: {[Op.eq]: nEmail}}});
  if (!user)
    throw new HttpNotFoundError(NotFoundErrors.UserNotFound);

  // Return an address and a private key for that address
  const {address, privateKey} = await AddressCreator.create();

  // use the private key to sign the password the user provided
  const signature = AddressCreator.sign(password, privateKey).signature;

  // encrypt the private key using the signature and our own salt
  const {encoded} = Encryptor.encrypt(privateKey, password, salt!);

  await user.update({signature, address, key: encoded});
  await resetAccountEntry.destroy();

  return {}
}