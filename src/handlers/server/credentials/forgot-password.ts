import {HttpBadRequestError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {User} from "@/database/models";
import {Op} from "sequelize";
import {emailService} from "@/handlers/server/email/email-service"
import {v4 as uuidv4} from "uuid";
import {addHours} from "date-fns";
import {ResetAccount} from "@/database/models/reset-account";
import {isEmail, normalizeEmail} from "validator";
import {createLog} from "@/handlers/server/user-log/create-log";
import {ForgotPasswordSubject, ForgotPasswordTemplate} from "@/handlers/server/templates/email/forgot-password";

export async function forgotPassword({email = ""}) {
  if (!email)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  if (!isEmail(email))
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotAnEmail);

  const normalizedEmail = normalizeEmail(email);
  if (!email)
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotANormalizedEmail);

  const user = await User.findOne({where: {email: {[Op.eq]: email}}, attributes: {exclude: ["image", "key", "signature"]}});
  if (!user)
    throw new HttpNotFoundError(NotFoundErrors.UserNotFound);

  const confirmationKey = uuidv4();
  const expires = addHours(new Date(), 1);

  const previousEntry = await ResetAccount.findOne({where: {email: {[Op.eq]: normalizedEmail}}})
  if (previousEntry)
    await previousEntry.destroy();

  await emailService.sendEmail(ForgotPasswordSubject, user.email, ForgotPasswordTemplate(user.username||user.email, confirmationKey))

  await ResetAccount.create({email: normalizedEmail, expires, confirmationKey});

  await createLog({userId: user.id, action: "reset_account"});

  return {}
}