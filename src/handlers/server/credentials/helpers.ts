import {UserTypes} from "@/types/user/user";
import {isEmail, normalizeEmail} from "validator";
import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {PasswordRegex} from "@/constants/password-restrictions";

export function isUserType(userType: UserTypes) {
  return Object.values(UserTypes).includes(userType);
}

export function emailPassValidation(credentials: {username: string, password: string}) {
  if (!credentials.username || !credentials.password)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  if (!isEmail(credentials.username))
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotAnEmail);

  const email = normalizeEmail(credentials.username);
  if (!email)
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotANormalizedEmail);

  const password = credentials.password.match(PasswordRegex);

  if (!password)
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsPassword);

  return {email, password: password[0]}
}
