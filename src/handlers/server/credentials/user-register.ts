import {HttpConflictError, HttpForbiddenError} from "@/classes/errors/http/http-errors";
import {ConflictErrors, ForbiddenErrors} from "@/classes/errors/http/types";
import {Op} from "sequelize";
import {UserTypes} from "@/types/user/user";
import {emailPassValidation} from "@/handlers/server/credentials/helpers";
import {AddressCreator} from "@/classes/credentials/address-creator";
import {Encryptor} from "@/classes/credentials/encryptor";
import {User} from "@/database/models/";
import {createLog} from "@/handlers/server/user-log/create-log";


const {CREDENTIALS_SALT: salt, ALLOWED_EMAIL_DOMAINS: allowedEmailDomains = ""} = process.env;

type UserRegisterProps = {
  username: string;
  password: string;
  type: UserTypes;
}

export async function userRegister(credentials: UserRegisterProps) {
  if (!salt)
    throw new Error("Not enough salt.");

  const {email, password} = emailPassValidation(credentials);

  const user = await User.findOne({where: {email: {[Op.eq]: email}}});

  if (user)
    throw new HttpConflictError(ConflictErrors.ConflictEmail);

  if (allowedEmailDomains && !allowedEmailDomains.toLowerCase().split(",").includes(email.toLowerCase().split("@")[1]))
    throw new HttpForbiddenError(ForbiddenErrors.DomainNotAuthorized);

  const credentialType = credentials.type as UserTypes;

  if ([UserTypes.company, UserTypes.ngo].includes(credentialType)) {
    if (await User.findOne({where: {email: {[Op.iLike]: email.split("@")[1]}}}))
      throw new HttpConflictError(ConflictErrors.DomainAlreadyExists)
  }

  // Return an address and a private key for that address
  const {address, privateKey} = await AddressCreator.create();

  // use the private key to sign the password the user provided
  const signature = AddressCreator.sign(password, privateKey).signature;

  // encrypt the private key using the signature and our own salt
  const {encoded} = Encryptor.encrypt(privateKey, password, salt!);

  if (credentialType === UserTypes.company)
    credentials.type = UserTypes.company;
  else if (credentialType === UserTypes.ngo)
    credentials.type = UserTypes.ngo;
  else credentials.type = UserTypes.user;

  // save the signature, address and encoded private key to the database
  const newUser = await User.create({email, signature, address, key: encoded, type: credentials.type, role: [], createdAt: new Date()})

  await createLog({userId: newUser.id, action: "register"});

  return {user: newUser.dataValues, created: true};
}