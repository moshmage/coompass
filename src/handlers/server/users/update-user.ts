import {BadRequestErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {User} from "@/database/models/";
import {Op} from "sequelize";

export async function updateUser({id = "", username = "", image = "", skills = [] as string[]}) {
  if (!id)
    throw new Error(BadRequestErrors.MissingParameters);

  const user = await User.findOne({where: {id: {[Op.eq]: id}}});
  if (!user)
    throw new Error(NotFoundErrors.UserNotFound);

  const profileImage = image || user.profileImage;

  await user.update({username: username || user.username, profileImage, skills})
  await user.save();

  return user.dataValues;
}