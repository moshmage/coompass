import {Company, User} from "@/database/models/";
import {Op} from "sequelize";

export async function getUsers({companyId = "", userId = "", email = ""}, associatedCompanies = true, partners = true,) {
  const where = {
    ... companyId ? {companies: {[Op.contains]: [companyId]}} : {},
    ... userId ? {id: {[Op.eq]: userId}} : {},
    ... email ? {email: {[Op.eq]: email}} : {},
  }

  const include = [];
  const associatedCompany = {
    model: Company,
    as: "associatedCompanies",
    include: [] as any,
  }

  if (associatedCompanies)
    include.push(associatedCompany);

  if (partners)
    associatedCompany.include.push(
      {model: Company, as: "partnerOrgs", attributes: {exclude: ["image", "description"]}},
      {model: Company, as: "partnerCompanies", attributes: {exclude: ["image", "description"]}},
    )

  return User.findAll({where, attributes: {exclude: ["signature", "key", "address"]}, include});

}