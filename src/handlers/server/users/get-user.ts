import {BadRequestErrors} from "@/classes/errors/http/types";
import {getUsers} from "@/handlers/server/users/get-users";

export async function getUser({userId = "", email = ""}, associatedCompanies = true, partners = true,) {
  if (!userId && !email)
    throw new Error(BadRequestErrors.MissingParameters);

  const [user] = await getUsers({userId, email}, associatedCompanies, partners,);

  return user;
}