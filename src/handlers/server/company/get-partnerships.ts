import {HttpBadRequestError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getCompanies} from "@/handlers/server/company/get-companies";


export async function getPartnerships({companyId = ""}) {
  if (!companyId || typeof companyId !== "string")
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const [company] = await getCompanies({companyId});
  if (!company)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  return (company as any).partnerOrgs

}