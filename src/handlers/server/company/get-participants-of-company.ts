import {getMissions} from "@/handlers/server/missions/get-missions";
import {Company, Mission, User} from "@/database/models/";
import {Op} from "sequelize";

export async function getParticipantsOfCompany({companyId = ""}) {
  const missions = await getMissions({companyId});
  const participants = Array.from(new Set<string>(missions.map(({participants}) => participants).flat()))
  return User.findAll({
    where: {id: {[Op.in]: participants}},
    include: [{model: Company, as: "associatedCompanies"}, {model: Mission, as: "missions"}],
    attributes: {exclude: ["key", "signature", "address", "profileImage"]}
  })
}