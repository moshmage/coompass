import {BadRequestErrors, ConflictErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {Company, User} from "@/database/models/";
import {Op} from "sequelize";
import {isBase64Image} from "@/validators/is-base64-image";
import {UserTypes} from "@/types/user/user";
import {UserCompany} from "@/database/models/user-company";
import {CompanyDistricts} from "@/database/models/company-districts";

export async function registerCompany({
                                        name = "",
                                        description = "",
                                        image = "",
                                        sdg = [] as string[],
                                        id: companyId = "",
                                        website = "",
                                        xcom = "",
                                        linkedin = "",
                                        facebook = "", youtube = "", instagram = "", districts = []
                                      }, {id = ""}) {
  if (!name || !description || !image)
    throw new Error(BadRequestErrors.MissingParameters);

  if (!isBase64Image(image))
    throw new Error(BadRequestErrors.WrongParamsNotAnImage);

  const user = await User.findOne({where: {id: {[Op.eq]: id}}})
  if (!user)
    throw new Error(NotFoundErrors.UserNotFound);

  if (user.type === UserTypes.user)
    throw new Error(BadRequestErrors.UserCannotMakeCompany);

  let _company: any;

  if (!companyId) {
    const company = await Company.findOne({where: {name: {[Op.iLike]: name}}})
    if (company)
      throw new Error(ConflictErrors.ConflictCompanyNameExists);

    const newCompany = await Company.create({
      name,
      description,
      image: "",
      sdg,
      website,
      xcom,
      linkedin,
      facebook,
      instagram,
      youtube,
      type: user.type,
      creator: id
    });
    await UserCompany.create({userId: user.id, companyId: newCompany.id});
    await user.update({companies: [...(user.companies || []), newCompany.id]})
    await user.save();

    if (districts.length) {
      await CompanyDistricts.bulkCreate(districts.map(id => ({companyId, districtId: id})));
    }

    _company = newCompany;
  } else {
    const company = await Company.findOne({where: {id: {[Op.eq]: companyId}}});
    if (!company)
      throw new Error(NotFoundErrors.CompanyNotFound);
    await company.update({name, description, sdg, website, xcom, linkedin, facebook, instagram, youtube});

    _company = company;
  }

  if (districts.length) {
    await CompanyDistricts.destroy({where: {companyId: {[Op.eq]: _company.id}}});
    await CompanyDistricts.bulkCreate(districts.map(id => ({companyId: _company.id, districtId: id})));
  }

  return {id: _company.id}
}