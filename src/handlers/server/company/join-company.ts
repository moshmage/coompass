import {BadRequestErrors, ConflictErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {Company, User} from "@/database/models/";
import {Op} from "sequelize";
import {createLog} from "@/handlers/server/user-log/create-log";
import {UserCompany} from "@/database/models/user-company";

export async function joinCompany({companyEmail = ""}, {id = ""}) {
  if (!companyEmail)
    throw new Error(BadRequestErrors.MissingParameters);
  if (!id)
    throw new Error(BadRequestErrors.MissingParametersUUID);

  const companyUser = await User.findOne({where: {email: {[Op.iLike]: companyEmail}}})
  if (!companyUser)
    throw new Error(NotFoundErrors.CompanyNotFound);

  const company = await Company.findOne({where: {creator: {[Op.eq]: companyUser.id}}});
  if (!company)
    throw new Error(NotFoundErrors.CompanyNotFound);

  const user = await User.findOne({where: {id: {[Op.eq]: id}}});
  if (!user)
    throw new Error(NotFoundErrors.UserNotFound);

  if (user.companies?.includes(company.id))
    throw new Error(ConflictErrors.Conflict);

  // if (user.email.split("@")[1] !== companyUser.email.split("@")[1])
  //   throw new Error(BadRequestErrors.WrongDomain);

  if (user.email === companyUser.email)
    throw new Error(BadRequestErrors.CompaniesCannotJoinCompanies)

  await user.update({companies: [...(user.companies || []), company.id] });
  await user.save();

  await company.update({employees: [...(company.employees || []), user.id]});
  await company.save();

  await createLog({userId: user.id, action: "joined_company"});

  await UserCompany.create({userId: user.id, companyId: company.id});

  return {companyId: company.id, company: company.dataValues}; // todo: return meaningful data
}