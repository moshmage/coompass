import {getMissions} from "@/handlers/server/missions/get-missions";
import {UserLog} from "@/database/models/";
import {sequelize} from "@/database/sequelize";
import {Op} from "sequelize";
import {addDays, differenceInDays, format, subDays} from "date-fns";

const dataLabel = (data: number[], labels: string[]) => ({data, labels});

export async function getCompanyGraph({companyId = ""}) {
  const missions = await getMissions({companyId});

  if (!missions.length)
    return dataLabel([], []);

  const thirtyDaysAgo = subDays(new Date, 30);

  const results = await UserLog.findAll({
    attributes: [
      'updatedAt',
      [sequelize.fn('COUNT', sequelize.col('userId')), 'userCount']
    ],
    where: {
      action: 'joined_mission',
      missionId: {
        [Op.in]: missions.map(({id}) => id),
      },
      updatedAt: {
        [Op.gte]: thirtyDaysAgo // updatedAt >= thirtyDaysAgo
      }
    },
    group: ["updatedAt"],
    raw: true,
  }) as any as {updatedAt: string, userCount: number}[];

  const reduced =
    results.reduce((p, c: {updatedAt: string, userCount: number}) =>
      ({...p, [format(c.updatedAt, "dd/MM/yyyy")]: ((p as any)[format(c.updatedAt, "dd/MM/yyyy")] || 0)+(+c.userCount)}), {})

  const today = new Date();
  let currentDate = thirtyDaysAgo;

  today.setHours(0, 0, 0)

  while (differenceInDays(today, currentDate) >= 0) {
    const formattedDate = format(currentDate, "dd/MM/yyyy");
    if (!reduced.hasOwnProperty(formattedDate))
      (reduced as any)[formattedDate] = 0;

    currentDate = addDays(currentDate, 1);
  }

  const sortedResult = Object.keys(reduced)
    .sort((a, b) => {
      const dateA = new Date(a.split('/').reverse().join('-'));
      const dateB = new Date(b.split('/').reverse().join('-'));
      return +dateA - +dateB;
    })
    .reduce((acc, date) => {
      (acc as any)[date] = (reduced as any)[date];
      return acc;
    }, {});


  return dataLabel(Object.values(sortedResult), Object.keys(sortedResult));
}