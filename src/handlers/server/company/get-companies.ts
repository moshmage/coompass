import {Company, Districts, Mission} from "@/database/models/";
import {Op} from "sequelize";

export function getCompanies({search = "", companyId = "", type = "", email = ""},
                             missions = true, districts = true, partners = true) {

  const where = {
    ... search ? {name: {[Op.iLike]: search}} : {},
    ... companyId ? {id: {[Op.eq]: companyId}} : {},
    ... type ? {type: {[Op.eq]: type}} : {},
    ... email ? {email: {[Op.eq]: email}} : {},
  }

  return Company.findAll({
    where,
    include: [
        {model: Mission, as: "companyMissions", attributes: {exclude: ["image", "description"]}},
        {model: Districts, as: "districts",},
        {
          model: Company,
          as: "partnerOrgs",
          include: [
            {
              model: Mission,
              as: "companyMissions",
              attributes: {exclude: ["image", "description"]},
            },
            {model: Districts, as: "districts",},
          ]
        },
        {model: Company, as: "partnerCompanies",},
    ]
  })
}