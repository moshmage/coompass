import {HttpBadRequestError, HttpConflictError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ConflictErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getUser} from "@/handlers/server/users/get-user";
import {createLog} from "@/handlers/server/user-log/create-log";
import {UserCompany} from "@/database/models";
import {Op} from "sequelize";
import {emailService} from "@/handlers/server/email/email-service";
import {UserJoinCompany} from "@/handlers/server/templates/email/company/join/pending-request";
import {User} from "@/types/user/user";

export async function requestCompanyMembership({companyEmail = ""}, userId: string) {
  if (!companyEmail || !userId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const companyOwner = await getUser({email: companyEmail}, true, false);
  if (!companyOwner)
    throw new HttpNotFoundError(NotFoundErrors.CompanyOwnerNotFound);

  const [company] = (companyOwner as unknown as User).associatedCompanies;
  if (!company)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  const user = await getUser({userId});
  if (!user)
    throw new HttpNotFoundError(NotFoundErrors.UserNotFound);

  if (user.companies.includes(company.id))
    throw new HttpConflictError(ConflictErrors.AlreadyCompanyMember);

  if (user.companies.length)
    throw new HttpConflictError(ConflictErrors.CanOnlyJoinOneCompany);

  if (user.email === companyOwner.email)
    throw new HttpBadRequestError(BadRequestErrors.CompaniesCannotJoinCompanies);

  await createLog({userId, action: "pending_join_company"})

  const userCompanyRelation = await UserCompany.findAll({
    where: {
      companyId: {[Op.eq]: company.id},
      userId: {[Op.eq]: userId}
    }
  })

  if (!userCompanyRelation)
    throw new HttpConflictError(ConflictErrors.RelationAlreadyExists);

  await UserCompany.create({userId, companyId: company.id, pending: true, accepted: false});

  emailService.sendEmail(
    UserJoinCompany.userPendingSubject(company.name),
    user.email,
    UserJoinCompany.userPendingContent(company.name, user.username!)
  );

  emailService.sendEmail(
    UserJoinCompany.companyPendingSubject(company.name),
    companyOwner.email,
    UserJoinCompany.companyPendingContent(company.name, user.username!, user.email, company.id)
  )

  return {message: "ok"}
}