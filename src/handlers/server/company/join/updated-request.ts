import {HttpBadRequestError, HttpConflictError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ConflictErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getUser} from "@/handlers/server/users/get-user";
import {UserCompany} from "@/database/models";
import {Op} from "sequelize";
import {createLog} from "@/handlers/server/user-log/create-log";
import {emailService} from "@/handlers/server/email/email-service";
import {UserJoinCompany} from "@/handlers/server/templates/email/company/join/pending-request";
import {User} from "@/types/user/user";

export async function updateRequest({userId = "", companyId = "", accept = false}, ownerId: string) {
  if (!userId || !ownerId || accept === undefined)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const owner = await getUser({userId: ownerId});
  if (!owner)
    throw new HttpNotFoundError(NotFoundErrors.CompanyOwnerNotFound);

  const [company] = (owner as unknown as User).associatedCompanies || [];
  if (!company)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  if (company.id !== companyId || company.creator !== ownerId)
    throw new HttpConflictError(ConflictErrors.Conflict);

  const user = await getUser({userId});
  if (!user)
    throw new HttpNotFoundError(NotFoundErrors.UserNotFound);

  const userCompany = await UserCompany.findOne({
    where: {
      companyId: {[Op.eq]: company.id},
      userId: {[Op.eq]: userId}}
  })

  if (!userCompany)
    throw new HttpNotFoundError(NotFoundErrors.RelationNotFound);

  createLog({userId, action: accept ? "accepted_join_company" : "denied_join_company"});

  if (accept) {

    await userCompany.update({pending: false, accepted: true});

    emailService.sendEmail(
      UserJoinCompany.userAcceptedSubject(company.name),
      user.email,
      UserJoinCompany.userAcceptedContent(company.name, user.username!)
    )

  } else {
    await userCompany.destroy();

    emailService.sendEmail(
      UserJoinCompany.userRejectedSubject(company.name),
      user.email,
      UserJoinCompany.userRejectContent(company.name, user.username!)
    )
  }



  return {message: "ok"};
}