import {
  HttpBadRequestError,
  HttpConflictError,
  HttpForbiddenError,
  HttpNotFoundError
} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ConflictErrors, ForbiddenErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getCompanies} from "@/handlers/server/company/get-companies";
import {Partnership} from "@/database/models";

export async function addPartnership({companyId = "", partnerOrgId = "", requesterId = ""}) {

  console.log(`\n\n\nPARTNER`, partnerOrgId, '\n\n');

  if (!companyId || !partnerOrgId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters)

  const [company] = await getCompanies({companyId})
  if (!company)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  if (requesterId !== company.creator)
    throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized)

  const [partner] = await getCompanies({companyId: partnerOrgId});
  if (!partner)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  if (company.partnerOrgs!.findIndex((partner) => partner.id === partnerOrgId) > -1)
    throw new HttpConflictError(ConflictErrors.Conflict);

  await Partnership.create({companyId, partnerOrgId});

  return {message: "ok"}
}