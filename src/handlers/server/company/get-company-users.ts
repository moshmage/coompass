import {Op} from "sequelize";
import {Company, Mission, User} from "@/database/models/";

export async function getCompanyUsers({companyId = ""}) {
  return User.findAll({
    // where: {companies: {[Op.contains]: [companyId]}},
    attributes: {
      exclude: ["signature", "address", "profileImage", "key"]
    },
    include: [
      {
        model: Mission, as: "missions",
        attributes: {exclude: ["image", "description", "requirements"]}
      },
      {
        model: Company,
        as: "associatedCompanies",
        where: {id: {[Op.eq]: companyId} },
      }
    ]});
}