import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {getParticipantsState} from "@/handlers/server/missions/get-participants-state";
import {isBefore, subDays, subMonths} from "date-fns";

export async function getCompanyEngagement({companyId = ""}) {
  if (!companyId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const participants = await getParticipantsState({companyId, state: "accepted"});

  const today = new Date();

  const data = {
    thirtyDays: 0,
    sevenDays: 0,
    all: participants.length
  }

  for (const participant of participants) {
    if (isBefore(subDays(today, 7), participant.updatedAt))
      data.sevenDays += 1;
    if (isBefore(subMonths(today, 1), participant.updatedAt))
      data.thirtyDays += 1;
  }

  return data;
}