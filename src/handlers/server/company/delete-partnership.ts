import {HttpBadRequestError, HttpForbiddenError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ForbiddenErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getCompanies} from "@/handlers/server/company/get-companies";
import {Partnership} from "@/database/models";

export async function deletePartnership({companyId = "", partnerOrgId = "", requesterId = ""}) {
  if (!companyId || !partnerOrgId || !requesterId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters)

  const [company] = await getCompanies({companyId})
  if (!company)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  if (requesterId !== company.creator)
    throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized)

  if (company.partnerOrgs!.findIndex(({id}) => id === partnerOrgId) === -1)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  await Partnership.destroy({where: {companyId: company.id, partnerOrgId}});

  return {message: "ok"};
}