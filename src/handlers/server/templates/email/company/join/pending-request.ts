import {subjectLine} from "@/handlers/server/templates/email/globals/subject-line";
import {introContentFooter} from "@/handlers/server/templates/email/globals/intro-content-footer";
import {userEmailLink} from "@/handlers/server/templates/email/globals/user-email-link";
import {appLink} from "@/handlers/server/templates/email/globals/app-domain";

const userPendingSubject =
  (companyName: string) => subjectLine(`Pending request to join ${companyName}`);

const userAcceptedSubject =
  (companyName: string) => subjectLine(`You joined ${companyName}!`);

const userRejectedSubject =
  (companyName: string) => subjectLine(`Denied access to ${companyName}`);

const companyPendingSubject =
  (companyName: string) => subjectLine(`There are pending requests to join ${companyName}`);

const userPendingContent = (companyName: string, userName: string) =>
  introContentFooter(userName,
    `We forwarded your request to join ${companyName}!`,
    "Soon, the company moderator will act on this request; You may be contacted via email.")

const userAcceptedContent = (companyName: string, userName: string) =>
  introContentFooter(userName,
    `Your membership in ${companyName} was accepted!`,
    `You are now free to access the missions posted by ${companyName} partnered organisations`);

const userRejectContent = (companyName: string, userName: string) =>
  introContentFooter(userName,
    `Your membership in ${companyName} was denied`,
    `The company moderator decided to deny your access to ${companyName}.`);

const companyPendingContent = (companyName: string,
                               userName: string,
                               userEmail: string,
                               companyId: string) =>
  introContentFooter(companyName,
    `A request to join ${companyName} is pending`,
    `
    <p>User ${userName} (${userEmailLink(userEmail)}) has requested to join ${companyName}</p>
    <p>You can decide the outcome on the <a href="${appLink(`/company/${companyId}/employees?search=${userEmail}`)}">company employees page</a> on your dashboard</p>
    `);

export const UserJoinCompany = {
  userPendingSubject,
  userPendingContent,
  userAcceptedSubject,
  userAcceptedContent,
  userRejectedSubject,
  userRejectContent,
  companyPendingContent,
  companyPendingSubject,

}