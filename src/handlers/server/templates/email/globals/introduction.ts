export const EmailIntroduction = (userName: string, tagline: string) => `
<p>
Hi ${userName},<br/>
<br/>
${tagline}
</p>
`