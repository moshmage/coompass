export const appDomain =
  process.env.APP_DOMAIN || 'http://localhost:3000/';

export const appLink = (link: string) => `${appDomain}${link}`;