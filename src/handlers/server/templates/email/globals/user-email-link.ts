export const userEmailLink =
  (email: string) => `<a href="mailto:${email}">${email}</a>`