const SUBJECT_TAGLINE =
  process.env.EMAIL_SUBJECT_LINE || "Coompass.io |";

export const subjectLine =
  (title: string) => `${SUBJECT_TAGLINE} ${title}`;