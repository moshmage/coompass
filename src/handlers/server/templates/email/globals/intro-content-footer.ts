import {EmailIntroduction} from "@/handlers/server/templates/email/globals/introduction";
import {EmailFooter} from "@/handlers/server/templates/email/globals/footer";

export const introContentFooter = (userName: string, subjectTagline: string, content: string) =>
`
${EmailIntroduction(userName, subjectTagline)}
<p>
${content}
</p>
${EmailFooter}
`