export const OrgParticipationPendingSubject =
  (missionName: string) => `Coompass.io | Participation Pending (${missionName})`;

export const OrgParticipationPendingTemplate =
  (missionName: string, companyId: string, name: string, email: string) => `
<p>A new participant <strong>(${name})</strong> for the mission <strong>${missionName}</strong> needs your approval, do so through the manage button on your <a href="https://app.coompass.io/organization/${companyId}/">dashboard</a></p>

<p>Contact participant through email <strong><a href="mailto:${email}">${email}</a></strong></p>

<p>You can approve or deny this participation on your organization dashboard.</p>

<p><a href="https://app.coompass.io/"><strong>Coompass.io</strong></a></p>

`