export const UserParticipationPendingSubject =
  (missionName: string) => `Coompass.io | Participation pending (${missionName})`;

export const UserParticipationPendingTemplate =
  (missionName: string, missionId: string, pointOfContact: string, orgName: string) => `
<p>Your participation for the mission <a href="https://app.coompass.io/missions/${missionId}"><strong>${missionName}</strong></a> of <strong>${orgName}</strong> is pending.</p>

<p>Soon the organization will contact you, or you can contact the organization through <a href="mailto:${pointOfContact}">${pointOfContact}</a> for more information.</p>

<p><a href="https://coompass.io/"><strong>Coompass.io</strong></a></p>

`