export const OrgParticipationRejectedSubject =
  (missionName: string) => `Coompass.io | Participation rejected (${missionName})`;

export const ParticipationRejectedTemplate =
  (missionName: string, missionId: string, pointOfContact: string) => `
<p>Your participation for the mission <a href="https://app.coompass.io/missions/${missionId}"><strong>${missionName}</strong></a> was rejected.</p>

<p>You can contact <a href="mailto:${pointOfContact}">${pointOfContact}</a> for more information.</p>

<p><a href="https://coompass.io/"><strong>Coompass.io</strong></a></p>

`