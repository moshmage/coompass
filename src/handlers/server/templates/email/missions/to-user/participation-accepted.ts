export const ParticipationAcceptedSubject =
  (missionName: string) => `Coompass.io | Participation accepted (${missionName})`;

export const ParticipationAcceptedTemplate =
  (missionName: string, missionId: string, pointOfContact: string) => `
<p>Your participation for the mission <a href="https://app.coompass.io/missions/${missionId}"><strong>${missionName}</strong></a> was accepted.</p>

<p>Soon the organization will contact you, or you can contact the organization through <a href="mailto:${pointOfContact}">${pointOfContact}</a> for more information.</p>

<p></p><a href="https://coompass.io/"><strong>Coompass.io</strong></a></p>

`