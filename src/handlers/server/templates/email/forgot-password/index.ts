import {EmailFooter} from "@/handlers/server/templates/email/globals/footer";
import {appLink} from "@/handlers/server/templates/email/globals/app-domain";

export const ForgotPasswordSubject = "Coompass | Reset password request"

export const confirmationKeyLink = (confirmationKey: string) =>
  appLink(`/session/reset-account/?key=${confirmationKey}`);

export const ForgotPasswordTemplate = (userName: string, confirmationKey: string) => `
<p>
Hi ${userName},<br/>
<br/>
We received a request to reset your password for your account. To reset your password, click the link below:</br>
<br/>
<a href="${confirmationKeyLink(confirmationKey)}">${confirmationKeyLink(confirmationKey)}</a>
</p>

<p>If you wish to copy-paste the confirmation key due to security concerns,<br/>
<ul>
<li>
Copy the confirmation-key: <strong>${confirmationKey}</strong>
</li>
<li>
Navigate to <a href="${appLink(`/session/reset-account/`)}">${appLink(`/session/reset-account/`)}</a>
</li>
<li>
Input the confirmation key manually on the input
</li>
</ul>
</p>

<p>This link will expire in 1 hour. If you did not request a password reset, please ignore this email or contact our support team for assistance.</p>

${EmailFooter}
`