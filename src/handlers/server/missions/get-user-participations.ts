import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {Mission, MissionParticipants} from "@/database/models";
import {Op} from "sequelize";

export async function getUserParticipations({userId = ""}) {
  if (!userId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  return await MissionParticipants.findAll({
    where: {userId: {[Op.eq]: userId}},
    include: [{model: Mission, as: "mission", attributes: {exclude: ["image", "description", "requirements"]}}]
  });;

}