import {getUser} from "@/handlers/server/users/get-user";
import {HttpBadRequestError, HttpForbiddenError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ForbiddenErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {Calendar, Mission, MissionParticipants} from "@/database/models/";
import {Op} from "sequelize";
import {createLog} from "@/handlers/server/user-log/create-log";
import {
  ParticipationAcceptedSubject,
  ParticipationAcceptedTemplate
} from "@/handlers/server/templates/email/missions/to-user/participation-accepted";
import {
  OrgParticipationRejectedSubject,
  ParticipationRejectedTemplate
} from "@/handlers/server/templates/email/missions/to-user/participation-rejected";
import {emailService} from "@/handlers/server/email/email-service"

export async function patchParticipantEntry({missionId = "", userId = "", state = "", requester = ""}) {
  if (!state || !['accepted', 'rejected', 'pending'].includes(state))
    throw new HttpBadRequestError(BadRequestErrors.WrongParameters);

  const user = await getUser({userId}); // make sure userId exists
  const mission = await Mission.findOne({where: {id: {[Op.eq]: missionId}}});
  if (!mission)
    throw new HttpNotFoundError(NotFoundErrors.CompanyNotFound);

  if (mission.creator !== requester)
    throw new HttpForbiddenError(ForbiddenErrors.NotTheOwner);

  const where = {
    missionId: {[Op.eq]: missionId},
    userId: {[Op.eq]: userId}
  }

  const participation = await MissionParticipants.findOne({where});
  if (!participation)
    throw new HttpNotFoundError(NotFoundErrors.ParticipationNotFound);

  await participation.update({state, updatedAt: new Date()});

  if (state === "accepted" && !mission.participants.includes(userId)) {
    await mission.update({participants: [...(mission.participants || []), userId]});

    createLog({userId, action: "joined_mission", amount: mission.reward, missionId})
    Calendar.bulkCreate([
      {userId: userId, entryType: "mission-start", date: mission.when_start, missionId: mission.id},
      {userId: userId, entryType: "mission-end", date: mission.when_finish, missionId: mission.id},
    ]);
  } else if (state === "rejected") {
    createLog({userId, action: "was_rejected", amount: mission.reward, missionId})
  }

  let subject, template;

  if (state === "accepted") {
    subject = ParticipationAcceptedSubject(mission.title);
    template = ParticipationAcceptedTemplate(mission.title, missionId, mission.pointOfContact);
  } else {
    subject = OrgParticipationRejectedSubject(mission.title);
    template = ParticipationRejectedTemplate(mission.title, missionId, mission.pointOfContact);
  }

  emailService.sendEmail(subject, user.email, template);

  return participation.dataValues;
}