import {Op} from "sequelize";
import {Company, Mission, MissionParticipants} from "@/database/models/";
import {NotFoundErrors} from "@/classes/errors/http/types";

export async function getMissions({
                                    companyId = "",
                                    missionId = "",
                                    title = "",
                                    participantsOf = "",
                                    ofUsers = [] as string[],
                                    fromStart = null as Date|null,
                                    toEnd = null as Date|null, state = ""
}) {

  const where = {
    ... companyId ? {companyId: {[Op.eq]: companyId}} : {},
    ... missionId ? {id: {[Op.eq]: missionId}} : {},
    ... title ? {title: {[Op.iLike]: title}} : {},
    ... ofUsers.length ? {participants: {[Op.overlap]: ofUsers}} : {},
    ... fromStart ? {startedAt: {[Op.gte]: fromStart}} : {},
    ... toEnd ? {finishedAt: {[Op.lte]: toEnd}} : {},
    ... state ? {state: {[Op.eq]: state}} : {}
  }

  const include: any[] = [
    {model: Company },
    {model: MissionParticipants, as: "participantsState"},
  ]

  if (participantsOf) {
    const company = await Company.findOne({where: {id: {[Op.eq]: participantsOf}}});
    if (!company)
      throw new Error(NotFoundErrors.CompanyNotFound);

    if (!company.employees.length)
      return [];

    (where as any).participants = {[Op.overlap]: company.employees};
  }

  const missions = await Mission.findAll({where, include, attributes: {exclude: ["image"]}});



  // @ts-ignore
  // return missions.map(mission => ({...mission?.dataValues, company: mission?.company?.dataValues})) || [];

  return missions;
}