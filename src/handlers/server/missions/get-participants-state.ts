import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";
import {Mission, MissionParticipants, User} from "@/database/models/";
import {Op} from "sequelize";
import {getMissions} from "@/handlers/server/missions/get-missions";

export async function getParticipantsState({missionId = "", state = "", companyId = "", userId = ""}) {
  if (!missionId && !companyId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const where = {
    ... !companyId ? {missionId: {[Op.eq]: missionId}} : {},
    ... state ? {state: {[Op.eq]: state}} : {},
    ... userId ? {userId: {[Op.eq]: userId}} : {},
  }

  if (companyId) {
    const missions = await getMissions({companyId});
    if (!missions.length)
      return [];
    where.missionId = {[Op.in]: missions.map((mission) => mission.id)} as any;
  }

  const include = [
    {model: User, as: "user", attributes: {exclude: ["key", "signature", "address"]}},
    {model: Mission, as: "mission", attributes: {exclude: ["image"]}},
  ]

  return MissionParticipants.findAll({where, include})
}