import {BadRequestErrors, ConflictErrors, ForbiddenErrors} from "@/classes/errors/http/types";
import {getMissions} from "@/handlers/server/missions/get-missions";
import {Company, Mission} from "@/database/models/";
import {Op} from "sequelize";
import {isEmail} from "validator";
import {HttpBadRequestError, HttpConflictError, HttpForbiddenError} from "@/classes/errors/http/http-errors";
import {Calendar} from "@/database/models";
import {createLog} from "@/handlers/server/user-log/create-log";

export async function createMission({
                                      title = "",
                                      description = "",
                                      companyId = "",
                                      image = "",
                                      creator = "",
                                      causes = [] as string[],
                                      skills = [] as string[],
                                      reward = 0,
                                      email = "",
                                      requirements = "",
                                      when_start = null,
                                      when_finish = null,
                                      location = "Virtual"
}) {
  if (!title || !description || !companyId || !creator || !email)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  // if (!isBase64Image(image))
  //   throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotAnImage);

  if (!isEmail(email))
    throw new HttpBadRequestError(BadRequestErrors.WrongParamsNotAnEmail)

  const company = await Company.findOne({where: {creator: {[Op.eq]: creator}}})
  if (!company || company.creator !== creator)
    throw new HttpForbiddenError(ForbiddenErrors.NotTheOwner);

  const _mission = await getMissions({title, companyId})
  if (_mission.length)
    throw new HttpConflictError(ConflictErrors.MissionWithSameTitle);

  const mission = await Mission.create({
    title,
    description,
    companyId,
    creator,
    image: "",
    causes,
    skills,
    reward,
    requirements,
    pointOfContact: email,
    state: "paused",
    when_start,
    when_finish,
    location,
  })

  Calendar.bulkCreate([
    {userId: creator, entryType: "mission-created", date: new Date(), missionId: mission.id},
    {userId: creator, entryType: "mission-start", date: when_start, missionId: mission.id},
    {userId: creator, entryType: "mission-end", date: when_finish, missionId: mission.id},
  ]);

  await createLog({userId: creator, action: "created_mission", missionId: mission.id})

  return {id: mission.id}

}