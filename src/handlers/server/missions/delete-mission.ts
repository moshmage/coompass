import {
  HttpBadRequestError,
  HttpConflictError,
  HttpForbiddenError,
  HttpNotFoundError
} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ConflictErrors, ForbiddenErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {getMissions} from "@/handlers/server/missions/get-missions";
import {getUser} from "@/handlers/server/users/get-user";
import {UserTypes} from "@/types/user/user";
import {Calendar, MissionParticipants, UserLog} from "@/database/models";
import {Op} from "sequelize";

export async function deleteMission({id = ""}, {requester = ""}) {
  if (!id)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);
  if (!requester)
    throw new HttpForbiddenError(ForbiddenErrors.NotAuthorized);

  const mission = await getMissions({missionId: id});
  if (!mission.length)
    throw new HttpNotFoundError(NotFoundErrors.MissionNotFound);

  const user = await getUser({userId: requester});
  if (!user)
    throw new HttpNotFoundError(NotFoundErrors.UserNotFound)

  console.log(`CREATOR`, mission[0].creator !== requester, mission[0].creator, requester, user.id);

  if (mission[0].creator !== requester && user.type !== UserTypes.admin)
    throw new HttpForbiddenError(ForbiddenErrors.NotTheOwner)

  if (mission[0].state == "started")
    throw new HttpConflictError(ConflictErrors.CannotDeleteMissionStarted)

  await mission[0].destroy();
  await MissionParticipants.destroy({where: {missionId: {[Op.eq]: id}}});
  await Calendar.destroy({where: {missionId: {[Op.eq]: id}}});
  await UserLog.destroy({where: {missionId: {[Op.eq]: id}}});

  return {};
}