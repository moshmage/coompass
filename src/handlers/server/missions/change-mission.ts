import {BadRequestErrors, ForbiddenErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {Mission} from "@/database/models/";
import {Op} from "sequelize";
import {HttpBadRequestError, HttpForbiddenError, HttpNotFoundError} from "@/classes/errors/http/http-errors";
import {Calendar} from "@/database/models";

export async function changeMission({
                                      id = "",
                                      title = "",
                                      description = "",
                                      reward = 0,
                                      state = "",
                                      image = ""
}, {requester = ""}) {
  if (!id)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  const mission = await Mission.findOne({where: {id: {[Op.eq]: id}}});

  if (!mission)
    throw new HttpNotFoundError(NotFoundErrors.MissionNotFound);

  if (mission.creator !== requester)
    throw new HttpForbiddenError(ForbiddenErrors.NotTheOwner);

  const update: any = {};

  if (description && mission.description !== description)
    update.description = description;

  if (title && mission.title !== title)
    update.title = title;

  if (reward && mission.reward !== reward)
    update.reward = reward;

  if (state && mission.state !== state)
    update.state = state;

  if (image && mission.image !== image)
    update.image = image;

  let updated;

  if (Object.keys(update).length) {
    if (state === "finished")
      update!.finishedAt = new Date();
    else if (state === "started")
      update!.startedAt = new Date();

    updated = await mission.update(update);
  } else updated = mission;

  if (update?.finishedAt)
    Calendar.bulkCreate(
      mission
        .participants
        .map(participant =>
          ({userId: participant, entryType: "mission-ended", date: new Date(), missionId: mission.id})))

  if (update?.startedAt)
    Calendar.bulkCreate(
      mission
        .participants
        .map(participant =>
          ({userId: participant, entryType: "mission-started", date: new Date(), missionId: mission.id})))

  return updated.dataValues;
}