import {getMissions} from "@/handlers/server/missions/get-missions";
import {HttpBadRequestError, HttpConflictError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ConflictErrors} from "@/classes/errors/http/types";
import {MissionParticipants} from "@/database/models/";
import {getUser} from "@/handlers/server/users/get-user";
import {Op} from "sequelize";
import {emailService} from "@/handlers/server/email/email-service"
import {
  UserParticipationPendingSubject,
  UserParticipationPendingTemplate
} from "@/handlers/server/templates/email/missions/to-user/user-participation-pending";
import {
  OrgParticipationPendingSubject,
  OrgParticipationPendingTemplate
} from "@/handlers/server/templates/email/missions/to-org/org-participation-pending";
import {createLog} from "@/handlers/server/user-log/create-log";

export async function addParticipantEntry({missionId = "", userId = "",}) {
  if (!userId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  /* Make sure userId and missionId exist */
  const user = await getUser({userId});
  const [mission] = await getMissions({missionId});

  const where = {
    missionId: {[Op.eq]: missionId},
    userId: {[Op.eq]: userId}
  }

  const prevParticipation = await MissionParticipants.findOne({where});
  if (prevParticipation)
    throw new HttpConflictError(ConflictErrors.ParticipationAlreadyExists)

  const entry = await MissionParticipants.create({missionId, userId, state: "pending"});

  createLog({userId, action: "pending_join_mission", amount: mission.reward, missionId});

  ([
    [
      UserParticipationPendingSubject(mission.title),
      user.email,
      UserParticipationPendingTemplate(mission.title, missionId, mission.pointOfContact, mission.company.name)
    ],
    [
      OrgParticipationPendingSubject(mission.title),
      mission.pointOfContact,
      OrgParticipationPendingTemplate(mission.title, (mission.company as any).id, user.username||"no name given", user.email)
    ]
  ] as any[]).forEach(([subject, to, content]) => {
    emailService.sendEmail(subject, to, content)
  })

  return entry.dataValues;
}