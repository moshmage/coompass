import {BadRequestErrors, ConflictErrors, NotFoundErrors} from "@/classes/errors/http/types";
import {Op} from "sequelize";
import {Mission, User} from "@/database/models/";
import {createLog} from "@/handlers/server/user-log/create-log";
import {Calendar} from "@/database/models";

export async function joinMission({missionId = "", userId = ""}) {
  if (!missionId || !userId)
    throw new Error(BadRequestErrors.MissingParameters);

  const mission = await Mission.findOne({where: {id: {[Op.eq]: missionId}}});
  if (!mission)
    throw new Error(NotFoundErrors.MissionNotFound);

  const user = await User.findOne({where: {id: {[Op.eq]: userId}}});
  if (!user)
    throw new Error(NotFoundErrors.UserNotFound);

  if (mission.participants.includes(userId))
    throw new Error(ConflictErrors.Conflict);

  await mission.update({participants: [... (mission.participants || []), userId]});
  await mission.save();

  Calendar.bulkCreate([
    {userId: user.id, entryType: "mission-start", date: mission.when_start, missionId: mission.id},
    {userId: user.id, entryType: "mission-end", date: mission.when_finish, missionId: mission.id},
  ]);

  createLog({userId: user.id, action: "joined_mission", missionId: mission.id});

  return mission.dataValues;
}