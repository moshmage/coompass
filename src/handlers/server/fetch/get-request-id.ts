import {NextRequest} from "next/server";

export const getId = (request: NextRequest, index = 3) => request.nextUrl.pathname.split("/")[index];