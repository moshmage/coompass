export async function fetchServerData<T = any>(url = "") {
  const _url = new URL(`${process.env.API}${url.replace("/api", "")}`);
  _url.searchParams.set("ts", (+new Date()).toString());

  return fetch(_url.toString(), {
    headers: {
      API_KEY: process.env.API_KEY || "",
      "Cache-Control": "no-store"
    },
    cache: "no-store",
    next: {revalidate: 0}
  }).then(async d => d.json()) as Promise<T>
}