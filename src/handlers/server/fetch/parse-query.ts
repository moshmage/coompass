export function parseQuery(url: string) {
  const _params = new URL(url).searchParams;
  const params: {[p: string]: string} = {};
  _params.forEach((value, name) => {
    params[name] = value;
  })
  return params;
}