import {DeleteObjectCommand, GetObjectCommand, ObjectCannedACL, PutObjectCommand, S3Client} from "@aws-sdk/client-s3";
import {Readable} from "node:stream";

export class S3Service {
  readonly Client: S3Client;

  constructor(accessKeyId: string,
              secretAccessKey: string,
              region: string,
              readonly Bucket: string,
              readonly S3Domain: string,) {
    const missing = (value: string) => `${value ? "not " : ""}missing`
    if (!accessKeyId || !secretAccessKey || !region || !Bucket || !S3Domain)
      throw new Error(`Missing AWS-S3 params! access: ${missing(accessKeyId)}, secret: ${missing(secretAccessKey)}, region: ${missing(region)}, bucket: ${missing(Bucket)}, domain: ${missing(S3Domain)}}`);

    this.Client = new S3Client({region, credentials: {accessKeyId, secretAccessKey}});
  }

  async upload(filename: string, Body: Buffer, ContentType: string, ACL: ObjectCannedACL = "public-read") {
    const [file, ext] = filename.split(".");
    const Key = `${Date.now()}_${file.substring(0, 15)}.${ext || ContentType.split("/")?.[1]}`;
    return {
      ...await this.Client.send(new PutObjectCommand({Bucket: this.Bucket, Key, ContentType, Body, ACL})),
      filename: Key,
      s3Path: this.S3Domain.concat(`${Key}`)
    }
  }

  async fetch(filename: string) {
    const response = await this.Client.send(new GetObjectCommand({Key: filename, Bucket: this.Bucket}));
    if (!response.Body)
      throw new Error(`Could not fetch ${filename}`);

    const chunks: Uint8Array[] = [];
    for await (const chunk of response.Body as Readable)
      chunks.push(chunk);

    return Buffer.concat(chunks);
  }

  async delete(filename: string) {
    return this.Client.send(new DeleteObjectCommand({Key: filename, Bucket: this.Bucket}));
  }
}

export const s3Service = new S3Service(
  process.env.AMAZON_SES_ACCESS_KEY_ID!,
  process.env.AMAZON_SES_SECRET!,
  process.env.AMAZON_SES_REGION!,
  process.env.AMAZON_S3_UPLOAD_BUCKET_NAME!,
  process.env.AMAZON_S3_DOMAIN!
  )
