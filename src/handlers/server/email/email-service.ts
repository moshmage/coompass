import {awsSimpleEmailService} from "@/handlers/server/email/aws-ses";
import {sendgridEmailService} from "@/handlers/server/email/sendgrid";

export const emailService =
  process.env.USE_AWS_SES
    ? awsSimpleEmailService
    : sendgridEmailService;