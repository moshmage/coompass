import {SESClient, SendEmailCommand} from "@aws-sdk/client-ses";
import {SendEmailCommandInput} from "@aws-sdk/client-ses/dist-types/commands/SendEmailCommand";
import {HttpBadRequestError} from "@/classes/errors/http/http-errors";

export class AWSSES {
  readonly Client!: SESClient;

  constructor(accessKeyId: string,
              secretAccessKey: string,
              region: string,
              private readonly from: string) {
    const missing = (value: string) => `${value ? "not " : ""}missing`
    if (!accessKeyId || !secretAccessKey || !region || !from)
      throw new HttpBadRequestError(`Missing AWS-SES params! access: ${missing(accessKeyId)}, secret: ${missing(secretAccessKey)}, region: ${missing(region)}, from: ${missing(from)}`);

    this.Client = new SESClient({region, credentials: {accessKeyId, secretAccessKey}});
  }

  async sendEmail(subject: string, to: string, content: string) {
    const params: SendEmailCommandInput = {
      Source: this.from,
      Destination: {
        ToAddresses: [to],
      },
      Message: {
        Body: {
          Html: {
            Data: content,
          },
        },
        Subject: {
          Data: subject,
        }
      }
    }

    return this.Client.send(new SendEmailCommand(params))
  }
}

export const awsSimpleEmailService =
  new AWSSES(
    process.env.AMAZON_SES_ACCESS_KEY_ID!,
    process.env.AMAZON_SES_SECRET!,
    process.env.AMAZON_SES_REGION!,
    process.env.DEFAULT_EMAIL_FROM!
  );