import {SendgridLog} from "@/database/models";
import {createVerify} from "node:crypto";

type LogSendgridActivityProps = {
    headers: Headers,
    body: object
}

export async function logSendgridActivity({body, headers}: LogSendgridActivityProps) {

    console.log(`HEADERS`, headers)
    console.log(`BODY`, body)

    async function validateSNSMessage(snsMessage: any) {
        // Extract relevant fields from the SNS message
        const {
            Type,
            Message,
            Signature,
            SigningCertURL,
            Timestamp,
            TopicArn,
            MessageId,
        } = snsMessage;

        // 1. Fetch the signing certificate
        const response = await fetch(SigningCertURL);
        const cert = await response.text();

        // 2. Create a verifier
        const verifier = createVerify('SHA-1');
        verifier.update(`Message:${Message}\n`);
        verifier.update(`MessageId:${MessageId}\n`);
        verifier.update(`Signature:${Signature}\n`);
        verifier.update(`Timestamp:${Timestamp}\n`);
        verifier.update(`TopicArn:${TopicArn}\n`);
        verifier.update(`Type:${Type}\n`);

        // 3. Verify the signature
        const isValid = verifier.verify(cert, Signature, 'base64');

        return isValid;
    }
    console.log("Valid",await validateSNSMessage(body));

    await SendgridLog.create({event: JSON.stringify(body)});

    return {message: "ok"}
}