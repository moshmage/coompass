import sgMail, {ClientResponse} from "@sendgrid/mail";

export class Sendgrid {
  constructor(private readonly apiKey: string, private readonly from: string) {
    sgMail.setApiKey(apiKey);
  }

  async sendEmail(subject: string, to: string, content: string): Promise<ClientResponse> {
    return new Promise((resolve, reject) => {
      sgMail.send({
        from: this.from,
        to,
        subject,
        text: subject,
        html: content
      }, false, (error, result) => {
        if (error) {
          console.error(`FAILED TO SEND EMAIL`, error)
          reject(error);
        } else {
          console.log(`SENT EMAIL`, result[0])
          resolve(result[0]);
        }
      });
    });
  }
}

export const sendgridEmailService = new Sendgrid(process.env.SENDGRID_API_KEY!, process.env.SENDGRID_API_FROM!);