import {AppRouteHandlerFn} from "next/dist/server/future/route-modules/app-route/module";
import {UserTypes} from "@/types/user/user";
import {auth} from "@/handlers/server/credentials/auth";
import {sessionHasRole} from "@/validators/roles/session-has-role";
import {UnauthorizedErrors} from "@/classes/errors/http/types";

export const withRole = (handler: AppRouteHandlerFn, role?: UserTypes | UserTypes[]) =>
  auth(async (req) => {
    if (!req.auth && !req.headers.get("api_key"))
      throw new Error(UnauthorizedErrors.Unauthorized);

    const withCorrectApiKey =
      req.headers.get("api_key")
        ? process.env.API_KEY === req.headers.get("api_key")
        : false; // if it does not have API Key we should check for role

    if (role && !withCorrectApiKey) {
      if (Array.isArray(role)) {
        if (!(req?.auth?.user as any)?.role.some((_role: UserTypes) => role.includes(_role)))
          throw new Error(UnauthorizedErrors.UnauthorizedNoRole)
      } else if (!Array.isArray(role) && !sessionHasRole(req.auth as any, role))
        throw new Error(UnauthorizedErrors.UnauthorizedNoRole)
    }

    return handler(req, {}) as Promise<Response>;
  })
