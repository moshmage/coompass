import {AppRouteHandlerFn} from "next/dist/server/future/route-modules/app-route/module";
import {NextRequest} from "next/server";

export const withErrorHandler = (handler: AppRouteHandlerFn) =>
  async (req: NextRequest, res: any) => {
    try {
      return await handler(req, res) as Promise<Response>;
    } catch (e: any) {
      console.log(`ERROR`, e?.stack);
      return Response.json({error: e?.message || "No message for error"}, {status: e?.status || 400});
    }
  };