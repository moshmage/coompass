import {Certificate, Company} from "@/database/models";
import {Op} from "sequelize";
import {subMonths} from "date-fns";
import {HttpBadRequestError} from "@/classes/errors/http/http-errors";
import {BadRequestErrors} from "@/classes/errors/http/types";

export function getCertificates({companyId = "", createdDate = new Date(), startDate = null as Date | null}) {
  if (!companyId)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  return Certificate.findAll({
    where: {
      companyId: {[Op.eq]: companyId},
      createdDate: {
        [Op.between]: [!startDate ? subMonths(createdDate, 6) : startDate, createdDate]
      }
    }, include: [Company]
  })
}