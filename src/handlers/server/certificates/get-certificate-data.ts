import {getMissions} from "@/handlers/server/missions/get-missions";
import {subMonths} from "date-fns";
import {getCompanies} from "@/handlers/server/company/get-companies";
import {CertificateData} from "@/types/certificate/certificate-data";
import {Company} from "@/types/company/company";

export async function getCertificateData({companyId = "", toEnd = new Date()}) {
  const missions = await getMissions({participantsOf: companyId, toEnd, fromStart: subMonths(toEnd, 6), state: "finished"});
  const [company] = await getCompanies({companyId});

  const data: Partial<CertificateData> = {
    collaborators: 0,
    hoursProvided: 0,
    initiatives: 0,
    causes: [] as string[],
    missions: [] as string[],
    createdDate: new Date(),
    sdgs: [],
  }

  if (!missions.length || !company)
    return {data, company};

  const filterParticipant = (participant: string) => company.employees.includes(participant)

  data.missions = missions.map(({id}) => id);
  data.sdgs = [...new Set(missions.map(({company}) => (company as unknown as Company).sdg).flat())];

  data.collaborators = [
    ...new Set(
      missions
        .map(({participants}) => participants)
        .flat()
        .filter(filterParticipant))
  ].length;

  data.hoursProvided =
    missions
      .map(({reward}) => reward)
      .reduce((p, c) => +p + (+c), 0) * data.collaborators;

  data.initiatives = missions.length;

  data.causes =
    missions
      .map(({causes}) => causes)
      .flat();

  return {data, company};
}