import {
  HttpBadRequestError,
  HttpForbiddenError,
  HttpNotAcceptableError,
  HttpServerError
} from "@/classes/errors/http/http-errors";
import {BadRequestErrors, ForbiddenErrors, NotAcceptableErrors} from "@/classes/errors/http/types";
import {Certificate, Company, User} from "@/database/models/";
import {getCertificateData} from "@/handlers/server/certificates/get-certificate-data";
import {getCertificates} from "@/handlers/server/certificates/get-certificates";
import {subYears} from "date-fns";
import {Erc721Standard} from "@taikai/dappkit";
import {Op} from "sequelize";
import {uploadBase64Image} from "@/handlers/chain/ipfs/upload-base64-image";
import {uploadJsonFileToInfura} from "@/handlers/chain/ipfs/upload-json-file-to-infura";

export async function createCertificate({companyId = "", image = "", toEnd = new Date()}, {id = ""}) {
  if (!companyId || !image)
    throw new HttpBadRequestError(BadRequestErrors.MissingParameters);

  if ((await getCertificates({companyId, createdDate: toEnd, startDate: subYears(toEnd, 1)})).length >= 99)
    throw new HttpNotAcceptableError(NotAcceptableErrors.TooManyCertificates)

  const company = await Company.findOne({where: {creator: {[Op.eq]: id}}, attributes: {exclude: ["image"]}, include: [{model: User, as: "companyCreator", attributes: {exclude: ["key", "signature"]}}]});
  if (company?.creator !== id)
    throw new HttpForbiddenError(ForbiddenErrors.NotTheOwner);

  const data = await getCertificateData({companyId, toEnd});

  const pop = new Erc721Standard(
    {web3Host: process.env.DEFAULT_CHAIN_RPC, privateKey: process.env.DEFAULT_PVT_KEY},
    process.env.CHAIN_CERTIFICATE_ADDRESS
  )

  await pop.start();
  let nextTokenId = (+(await pop.totalSupply()))+1;

  // todo fix this when not beta
  if (nextTokenId === 11)
    nextTokenId++;

  const tx =
    await pop.mint((company.companyCreator as any)!.address, nextTokenId)
      .catch(e => {
        console.error(`Failed to mint nft`, e);
        return null;
      });

  if (!tx)
    throw new HttpServerError("Failed to mint NFT");

  const tokenId = tx.logs[0].args.tokenId;

  const imageCid = await uploadBase64Image(image);
  if (!imageCid)
    throw new HttpServerError("Failed to upload image")

  const metadata = ({
    name: `CoompassPoP #${tokenId}`,
    description: `Coompass.io certificate for ${company.name}`,
    image: `https://coompass-io.infura-ipfs.io/ipfs/${imageCid}`,
    attributes: [
      {
        trait_type: "collaborators",
        value: data.data.collaborators
      },
      {
        trait_type: "hours",
        value: data.data.hoursProvided
      },
      {
        trait_type: "missions",
        value: (data.data.missions || []).join(', ')
      },
      {
        trait_type: "causes",
        value: (data.data.causes || []).join(', ')
      },
      {
        trait_type: "SDGs",
        value: data.data.sdgs?.join(', ')
      }
    ]
  })

  const metadataCid = await uploadJsonFileToInfura(metadata);
  if (!metadataCid)
    throw new HttpServerError("Failed to create metadata")

  await pop.setTokenURI(+tokenId, `https://coompass-io.infura-ipfs.io/ipfs/${metadataCid}`);

  await Certificate.create({...data.data, txHash: tx.transactionHash, companyId, tokenId, createdAt: new Date(), imageCid, metadataCid});

  return data
}