import {Mission, User, UserLog} from "@/database/models";

export function getAllLogs() {
  return UserLog.findAll({
    order: [["createdAt", "desc"]],
    include: [
      {model: User, attributes: ["username", "email"], as: "user"},
      {model: Mission, attributes: ["title"], as: "mission"},
    ], raw: true});
}